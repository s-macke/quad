#include"global.h"
#include"functions.h"
#include"CAtom.h"
#include"script.h"

void CSphereAtom::Set(const int LMAX, const c::complex<REAL> &KAPPA, const REAL EField)
{
	const int LMAXARRAY = 7;
	const int LMAX1ARRAY = LMAXARRAY+1;

	const int LMAX1 = LMAX+1;
	int L1;
	c::complex<REAL> C1, C2, C3, C4, C5, C6, AN, AJ, BN, BJ; 
	c::complex<REAL> ARG, ARGM;

	c::complex<REAL> J[LMAX1ARRAY+1], Y[LMAX1ARRAY+1], H[LMAX1ARRAY+1];
	c::complex<REAL> JM[LMAX1ARRAY+1],YM[LMAX1ARRAY+1],HM[LMAX1ARRAY+1];
	const c::complex<REAL> CI(0., 1.);

//-----------------------------------------------------------------------  

	ARG = r * KAPPA;
	ARGM = sqrt(e) * r * KAPPA;
	BESSEL(J,  Y,  H,  ARG,  LMAX1+1, true, true,  false);
	BESSEL(JM, YM, HM, ARGM, LMAX1+1, true, false, false);

	C1 = e - REAL(1.);
	C2 = sqrt(e) * ARG;
	C3 = -e * ARG;
	C4 = 0.;
	C5 = sqrt(e) * ARG;
	C6 = -ARG;
	
	c::complex<REAL> CZERO(0., 0.);
	TE.assign(LMAX+1, CZERO);
	TH.assign(LMAX+1, CZERO);
	//TE[0] = REAL(0.);
	//TH[0] = REAL(0.);
	
	for(int L1=0; L1<=LMAX; L1++)
	{
		AN = C1*REAL(L1+1)*JM[L1]*Y[L1] + C2*JM[L1+1]*Y[L1] + C3*JM[L1]*Y[L1+1];
		AJ = C1*REAL(L1+1)*JM[L1]*J[L1] + C2*JM[L1+1]*J[L1] + C3*JM[L1]*J[L1+1];
		BN = C4*REAL(L1+1)*JM[L1]*Y[L1] + C5*JM[L1+1]*Y[L1] + C6*JM[L1]*Y[L1+1];
		BJ = C4*REAL(L1+1)*JM[L1]*J[L1] + C5*JM[L1+1]*J[L1] + C6*JM[L1]*J[L1+1];
		TE[L1] = -AJ / (AJ + CI * AN);
		TH[L1] = -BJ / (BJ + CI * BN);
		//cerr << "Bessel: " << TE[L1].real() << " " << TE[L1].imag() << "\n";
		//cerr << "Bessel: " << TH[L1].real() << " " << TH[L1].imag() << "\n";
    }
	//TH[0] = 0.;
	//TH[1] = 0.;
	//TE[0] = 0.;
	//TE[1] = c::complex<REAL>(TE[1].real()*0., TE[1].imag());

//	cerr << "Bessel: " << JM[1].real() << "\n";
//	cerr << "Bessel: " << YM[1].real() << "\n";
//	cerr << "Bessel: " << HM[1].real() << "\n";
};

//C     ------------------------------------------------------------------  
//C     THIS SUBROUTINE  CALCULATES  THE  T-MATRIX FOR THE SCATTERING  
//C     OF ELECTROMAGNETIC  FIELD  OF  WAVE-LENGHT LAMDA  BY A SINGLE  
//C     SPHERE OF RADIUS S.  (RAP=S/LAMDA).  
//C     EPSSPH : COMPLEX RELATIVE DIELECTRIC CONSTANT OF THE SPHERE.  
//C     EPSMED : COMPLEX RELATIVE DIELECTRIC CONSTANT OF THE MEDIUM.  
//C     LMAX   : MAXIMUM ANGULAR MOMENTUM  
//C     ------------------------------------------------------------------  
//=======================================================================  

void TMTRX(int LMAX,
	const c::complex<REAL> &R,
	const c::complex<REAL> &KAPPA,
	const c::complex<REAL> &EPSSPH,
	c::complex<REAL> *TE,
	c::complex<REAL> *TH)
{
	int L1;
	c::complex<REAL> C1, C2, C3, C4, C5, C6, AN, AJ, BN, BJ; 
	c::complex<REAL> ARG, ARGM;
	//c::complex<REAL> XISQ, XISQM;
	//c::complex<REAL> AR;

	const int LMAXARRAY = 7;
	const int LMAX1ARRAY = LMAXARRAY+1;

	const int LMAX1 = LMAX + 1;

//C ..  LOCAL ARRAYS  .. 

	c::complex<REAL> J[LMAX1ARRAY+1], Y[LMAX1ARRAY+1], H[LMAX1ARRAY+1];
	c::complex<REAL> JM[LMAX1ARRAY+1],YM[LMAX1ARRAY+1],HM[LMAX1ARRAY+1];
	const c::complex<REAL> CI(0., 1.);

//C-----------------------------------------------------------------------  

	//XISQ = sqrt(EPSMED * MUMED);
	//XISQM = sqrt(EPSSPH * MUSPH);
//	XISQ = 1.;
//	XISQM = sqrt(EPSSPH);
	//c::complex<double> RAP = R * KAPPA / REAL(2.) / M_PI;

	ARG = R * KAPPA;
	ARGM = sqrt(EPSSPH) * R * KAPPA;
//	cout << "ARG: " << ARG.real() << "\n";
//	cout << "ARGM: " << ARGM.real() << "\n";
	BESSEL(J,  Y,  H,  ARG,  LMAX1+1, true, true,  false);
	BESSEL(JM, YM, HM, ARGM, LMAX1+1, true, false, false);
	//printf("%e %e\n", J[1].real(), J[1].imag());
	//printf("%e %e\n", J[2].real(), J[2].imag());
	//printf("%e %e\n", Y[1].real(), Y[1].imag());
	//printf("%e %e\n", Y[2].real(), Y[2].imag());
	//printf("%e %e\n", JM[1].real(), JM[1].imag());
	//printf("%e %e\n", JM[2].real(), JM[2].imag());
/*
	C1 = EPSSPH - EPSMED;
	C2 = EPSMED * ARGM;
	C3 = -EPSSPH * ARG;
	C4 = MUSPH - MUMED;
	C5 = MUMED * ARGM;
	C6 = -MUSPH * ARG;
*/

	C1 = EPSSPH - REAL(1.);
	C2 = sqrt(EPSSPH) * ARG;
	C3 = -EPSSPH * ARG;
	C4 = 0.;
	C5 = sqrt(EPSSPH) * ARG;
	C6 = -ARG;

	for(int L1=0; L1<=LMAX; L1++)
	{ 
		AN = C1*REAL(L1+1)*JM[L1]*Y[L1] + C2*JM[L1+1]*Y[L1] + C3*JM[L1]*Y[L1+1];
		AJ = C1*REAL(L1+1)*JM[L1]*J[L1] + C2*JM[L1+1]*J[L1] + C3*JM[L1]*J[L1+1];
		BN = C4*REAL(L1+1)*JM[L1]*Y[L1] + C5*JM[L1+1]*Y[L1] + C6*JM[L1]*Y[L1+1];
		BJ = C4*REAL(L1+1)*JM[L1]*J[L1] + C5*JM[L1+1]*J[L1] + C6*JM[L1]*J[L1+1];
		TE[L1] = -AJ / (AJ + CI * AN);
		TH[L1] = -BJ / (BJ + CI * BN);
//		cout << " " << TE[L1].real() << " " << TE[L1].imag() << " " << TH[L1].real() << " " << TH[L1].imag() << "\n";
    }

// for L1=1	
/*			
			AN = C1*REAL(2.)*JM[1]*Y[1] + C2*JM[2]*Y[1] + C3*JM[1]*Y[2];
			AJ = C1*REAL(2.)*JM[1]*J[1] + C2*JM[2]*J[1] + C3*JM[1]*J[2];
			BN = C4*REAL(2.)*JM[1]*Y[1] + C5*JM[2]*Y[1] + C6*JM[1]*Y[2];
			BJ = C4*REAL(2.)*JM[1]*J[1] + C5*JM[2]*J[1] + C6*JM[1]*J[2];
			TE[1] = -AJ / (AJ + CI * AN);
			TH[1] = -BJ / (BJ + CI * BN);
*/
/*
			J[1] = 1./3. * ARG;
			J[2] = 1./15. * ARG*ARG;
			JM[1] = 1./3. * ARGM;
			JM[2] = 1./15. * ARGM*ARGM;
			Y[2] = -3./(ARG*ARG*ARG);
			Y[1] = -1./(ARG*ARG);
*/
/*			
			AN = C1*REAL(2.)*JM[1]*Y[1] + ARGM*JM[2]*Y[1] + C3*JM[1]*Y[2];
			AJ = C1*REAL(2.)*JM[1]*J[1] + ARGM*JM[2]*J[1] + C3*JM[1]*J[2];
			BN =                          ARGM*JM[2]*Y[1] - ARG*JM[1]*Y[2];
			BJ =                          ARGM*JM[2]*J[1] - ARG*JM[1]*J[2];
*/
/*
			AN = C1*REAL(2./3.)*ARGM*Y[1] + 1./15.*ARGM*ARGM*ARGM*Y[1]      + 1./3.*C3*ARGM*Y[2];
			AJ = C1*REAL(2./9.)*ARGM*ARG  + 1./45.*ARGM*ARGM*ARGM*ARG 		+ 1./45.*C3*ARGM*ARG*ARG;
			BN =                            1./15.*ARGM*ARGM*ARGM*Y[1]      - 1./3.*ARG*ARGM*Y[2];
			BJ =                            1./45.*ARGM*ARGM*ARGM*ARG       - 1./45.*ARG*ARGM*ARG*ARG;

			AN = 1./3.*-EPSSPH*AR*sqrt(EPSSPH)*AR*Y[2];
			AJ = (EPSSPH-1.)*REAL(2./9.)*sqrt(EPSSPH)*AR*AR;
		
			//AJ = (EPSSPH-1.)*REAL(2./3.);
			//AN = EPSSPH * 3. / (AR*AR*AR);
			//TE[1] = -AJ / (CI * AN);
*/
			//TE[1] = CI*2./9. * R*R*R*KAPPA*KAPPA*KAPPA * (EPSSPH-1.);		
			//TH[1] = 0.;

			//cout << " " << TE[1].real() << " " << TE[1].imag() << " " << TH[1].real() << " " << TH[1].imag() << "\n";

	
}

		void CScatteringFactorFunctionAtom::Set(const int LMAX, const c::complex<REAL> &KAPPA, const REAL Efield)
		{
			//TE = CI*REAL(2.)/REAL(9.) * R*R*R*KAPPA*KAPPA*KAPPA * (EPSSPH-REAL(1.));		
			c::complex<REAL> CI(REAL(0.), REAL(1.));
			c::complex<REAL> CZERO(REAL(0.), REAL(0.));
			const REAL r0 = 2.8179403267e-5; // in A
			c::complex<REAL> pre = -CI * KAPPA * ( REAL(2.)/REAL(3.) * r0 );
			
			TE.assign(LMAX+1, CZERO);
			TH.assign(LMAX+1, CZERO);
			c::complex<double> f;
			REAL lambda = 2.*M_PI / KAPPA.real();

			f = CallLuaFunction(ref, 12398.326 /lambda, Efield);
			//printf("%f %f\n", f.real(), f.imag());
			f = conj(f);
			TE[0] = 0.;
			TH[0] = 0.;
			TE[1] = pre * f;
			TH[1] = 0.;
			for(int i=2; i<=LMAX; i++)
			{
				TE[i] = 0.;
				TH[i] = 0.;
			}
		}

		void CMagneticFunctionAtom::Set(const int LMAX, const c::complex<REAL> &KAPPA, const REAL EField)
		{
			c::complex<REAL> CI(REAL(0.), REAL(1.));
			c::complex<REAL> CZERO(REAL(0.), REAL(0.));

			REAL lambda = 2. * M_PI / KAPPA.real();
			REAL energy = 12398.326 / lambda;
			//CTableEntry te = GetOpticalConstants(name, energy);
			c::complex<double> f;
			c::complex<double> fm;
			f = CallLuaFunction(ref, energy, EField);
			fm = CallLuaFunction(refm, energy, EField);


			F[0][0] = c::complex<REAL>(f.real(), -f.imag());
			F[0][1] = c::complex<REAL>(fm.real(), -fm.imag()) * cos(theta);
			F[0][2] = c::complex<REAL>(fm.real(), -fm.imag()) * (-sin(theta)*sin(phi));

			F[1][0] = c::complex<REAL>(fm.real(), -fm.imag()) *(-cos(theta));
			F[1][1] = c::complex<REAL>(f.real(), -f.imag());
			F[1][2] = c::complex<REAL>(fm.real(), -fm.imag()) * (sin(theta)*cos(phi));
			
			F[2][0] = c::complex<REAL>(fm.real(), -fm.imag()) * (sin(theta)*sin(phi));
			F[2][1] = c::complex<REAL>(fm.real(), -fm.imag()) * (-sin(theta)*cos(phi));
			F[2][2] = c::complex<REAL>(f.real(), -f.imag()) ;
			//c::complex<REAL> f = c::complex<REAL>(te.y[0], -te.y[1]);

			c::complex<REAL> Cr[3][3]; // transformation right
			c::complex<REAL> Cl[3][3]; // transformation left
			
			Cr[0][0] = -2.*sqrt(M_PI/3.);
			Cr[0][1] = 0.;
			Cr[0][2] =  2.*sqrt(M_PI/3.);

			Cr[1][0] = 2.*CI*sqrt(M_PI/3.);
			Cr[1][1] = 0.;
			Cr[1][2] = 2.*CI*sqrt(M_PI/3.);

			Cr[2][0] = 0.;
			Cr[2][1] = -2.*sqrt(2.*M_PI/3.);
			Cr[2][2] = 0.;
			
			double t = 8./3.*M_PI;
			Cl[0][0] = conj(Cr[0][0])/t;
			Cl[0][1] = conj(Cr[1][0])/t;
			Cl[0][2] = conj(Cr[2][0])/t;
			Cl[1][0] = conj(Cr[0][1])/t;
			Cl[1][1] = conj(Cr[1][1])/t;
			Cl[1][2] = conj(Cr[2][1])/t;
			Cl[2][0] = conj(Cr[0][2])/t;
			Cl[2][1] = conj(Cr[1][2])/t;
			Cl[2][2] = conj(Cr[2][2])/t;


			MatrixMul(T, F, Cr);
			MatrixMul(Cr, Cl, T);

			const REAL r0 = 2.8179403267e-5; // in A
			c::complex<REAL> pre = -CI * KAPPA * ( REAL(2.)/REAL(3.) * r0 );
			
			for(int i=0; i<3; i++)
			for(int j=0; j<3; j++)
			{
				//printf("%f %f\n", Cr[i][j].real(), F[i][j].real());
				T[i][j] = Cr[i][j] * pre;
			}
		}

		void CMagneticFileAtom::Set(const int LMAX, const c::complex<REAL> &KAPPA, const REAL EField)
		{
			c::complex<REAL> CI(REAL(0.), REAL(1.));
			c::complex<REAL> CZERO(REAL(0.), REAL(0.));

			REAL lambda = 2.*M_PI / KAPPA.real();
			REAL energy = 12398.326 / lambda;
			//CTableEntry te = GetOpticalConstants(name, energy);
			CTableEntry te = GetOpticalConstants(name, energy);
			c::complex<REAL> f = c::complex<REAL>(te.y[0], -te.y[1]);
			c::complex<REAL> fm = c::complex<REAL>(scale*te.y[2], scale*te.y[3]);
			//printf("%f %f %f %f %f\n", energy, f.real(), f.imag(), fm.real(), fm.imag());

			F[0][0] = c::complex<REAL>(f.real(),  f.imag());
			F[0][1] = c::complex<REAL>(fm.real(), fm.imag()) * cos(theta);
			F[0][2] = c::complex<REAL>(fm.real(), fm.imag()) * (-sin(theta)*sin(phi));

			F[1][0] = c::complex<REAL>(fm.real(), fm.imag()) *(-cos(theta));
			F[1][1] = c::complex<REAL>(f.real(),  f.imag());
			F[1][2] = c::complex<REAL>(fm.real(), fm.imag()) * (sin(theta)*cos(phi));
			
			F[2][0] = c::complex<REAL>(fm.real(), fm.imag()) * (sin(theta)*sin(phi));
			F[2][1] = c::complex<REAL>(fm.real(), fm.imag()) * (-sin(theta)*cos(phi));
			F[2][2] = c::complex<REAL>(f.real(),  f.imag());
			//c::complex<REAL> f = c::complex<REAL>(te.y[0], -te.y[1]);

			c::complex<REAL> Cr[3][3]; // transformation right
			c::complex<REAL> Cl[3][3]; // transformation left
			
			Cr[0][0] = -2.*sqrt(M_PI/3.);
			Cr[0][1] = 0.;
			Cr[0][2] =  2.*sqrt(M_PI/3.);

			Cr[1][0] = 2.*CI*sqrt(M_PI/3.);
			Cr[1][1] = 0.;
			Cr[1][2] = 2.*CI*sqrt(M_PI/3.);

			Cr[2][0] = 0.;
			Cr[2][1] = -2.*sqrt(2.*M_PI/3.);
			Cr[2][2] = 0.;

			double t = 8. / 3. * M_PI;
			Cl[0][0] = conj(Cr[0][0]) / t;
			Cl[0][1] = conj(Cr[1][0]) / t;
			Cl[0][2] = conj(Cr[2][0]) / t;
			Cl[1][0] = conj(Cr[0][1]) / t;
			Cl[1][1] = conj(Cr[1][1]) / t;
			Cl[1][2] = conj(Cr[2][1]) / t;
			Cl[2][0] = conj(Cr[0][2]) / t;
			Cl[2][1] = conj(Cr[1][2]) / t;
			Cl[2][2] = conj(Cr[2][2]) / t;

			MatrixMul(T, F, Cr);
			MatrixMul(Cr, Cl, T);

			const REAL r0 = 2.8179403267e-5; // in A
			c::complex<REAL> pre = -CI * KAPPA * ( REAL(2.)/REAL(3.) * r0 );
			
			for(int i=0; i<3; i++)
			for(int j=0; j<3; j++)
			{
				//printf("%f %f\n", T[i][j].real(), T[i][j].imag());
				T[i][j] = Cr[i][j] * pre;
			}
		}


		void CTetragonalFunctionAtom::Set(const int LMAX, const c::complex<REAL> &KAPPA, const REAL EField)
		{
			c::complex<REAL> CI(REAL(0.), REAL(1.));
			c::complex<REAL> CZERO(REAL(0.), REAL(0.));

			REAL lambda = 2. * M_PI / KAPPA.real();
			REAL energy = 12398.326 / lambda;
			//CTableEntry te = GetOpticalConstants(name, energy);
			c::complex<double> f;
			c::complex<double> fm;
			f = CallLuaFunction(ref, energy, EField);
			fm = CallLuaFunction(refm, energy, EField);


			F[0][0] = c::complex<REAL>(f.real(), -f.imag());
			F[0][1] = 0.;
			F[0][2] = 0.;

			F[1][0] = 0.;
			F[1][1] = c::complex<REAL>(f.real(), -f.imag());
			F[1][2] = 0.;
			
			F[2][0] = 0.;
			F[2][1] = 0.;
			F[2][2] = c::complex<REAL>(fm.real(), -fm.imag()) ;
			//c::complex<REAL> f = c::complex<REAL>(te.y[0], -te.y[1]);

			c::complex<REAL> Cr[3][3]; // transformation right
			c::complex<REAL> Cl[3][3]; // transformation left
			
			Cr[0][0] = -2.*sqrt(M_PI/3.);
			Cr[0][1] = 0.;
			Cr[0][2] =  2.*sqrt(M_PI/3.);

			Cr[1][0] = 2.*CI*sqrt(M_PI/3.);
			Cr[1][1] = 0.;
			Cr[1][2] = 2.*CI*sqrt(M_PI/3.);

			Cr[2][0] = 0.;
			Cr[2][1] = -2.*sqrt(2.*M_PI/3.);
			Cr[2][2] = 0.;
			
			double t = 8./3.*M_PI;
			Cl[0][0] = conj(Cr[0][0])/t;
			Cl[0][1] = conj(Cr[1][0])/t;
			Cl[0][2] = conj(Cr[2][0])/t;
			Cl[1][0] = conj(Cr[0][1])/t;
			Cl[1][1] = conj(Cr[1][1])/t;
			Cl[1][2] = conj(Cr[2][1])/t;
			Cl[2][0] = conj(Cr[0][2])/t;
			Cl[2][1] = conj(Cr[1][2])/t;
			Cl[2][2] = conj(Cr[2][2])/t;

			MatrixMul(T, F, Cr);
			MatrixMul(Cr, Cl, T);

			const REAL r0 = 2.8179403267e-5; // in A
			c::complex<REAL> pre = -CI * KAPPA * ( REAL(2.)/REAL(3.) * r0 );
			
			for(int i=0; i<3; i++)
			for(int j=0; j<3; j++)
			{
				//printf("%f %f\n", Cr[i][j].real(), F[i][j].real());
				T[i][j] = Cr[i][j] * pre;
			}
		}
