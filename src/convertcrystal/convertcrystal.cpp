#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<vector>
#include<string>
#include<iostream>
#include<fstream>
#include<sstream>
#define M_PI 3.14159265358979323846

using namespace std;

class Vector3D
{
	public:
	double x, y, z;
	double Length()
	{
		return sqrt(x*x + y*y + z*z);
	}

	void Norm()
	{
		double r = Length();
		x /= r;
		y /= r;
		z /= r;
	}
	
	void Multiply(double m[3][3])
	{
		double xn, yn, zn;
		xn = m[0][0] * x + m[0][1] * y + m[0][2] * z;
		yn = m[1][0] * x + m[1][1] * y + m[1][2] * z;
		zn = m[2][0] * x + m[2][1] * y + m[2][2] * z;
		x = xn;
		y = yn;
		z = zn;
	}
};

vector<Vector3D> v;
vector<string> e;
vector<int> eno;
Vector3D a, b, c;
double rsm[3][3], csm[3][3]; // real space matrix and crystal space matrix

int Nmin = 0;
double epsilon = 0.1;

bool CheckExistence(Vector3D &p)
{
	for(int i=0; i<e.size(); i++)
	{
		Vector3D d;
		d.x = (v[i].x - p.x);
		d.y = (v[i].y - p.y);
		d.z = (v[i].z - p.z);
		if (d.Length() < epsilon) return true;
	}
	return false;
}

void SaveCoordinates(const char *filename)
{
	FILE *file = fopen(filename, "w");
	if (file == NULL)
	{
		fprintf(stderr, "Error: Cannot open file\n");
		fclose(file);
		exit(1);
	}
	fprintf(file, "%i\n\n", e.size());
	for(int i=0; i<e.size(); i++)
	{
		fprintf(file, "%s %f %f %f\n", e[i].c_str(), v[i].x, v[i].y, v[i].z);
	}
	fclose(file);
}

void LoadCoordinates(const char *filename)
{
	fstream file;
	file.open(filename,ios::in);
	if (file.is_open() == false)
	{
		fprintf(stderr, "Error: Cannot open file\n");
		exit(1);
	}
	string line;
	int N = 0;
	
	getline(file, line);
	
	if (file.fail())
	{
		fprintf(stderr, "Error: Could not read number of atoms\n");
		exit(1);
	}
	sscanf(line.c_str(), "%i", &N);
	if (N == 0)
	{
		fprintf(stderr, "Error: Could not read number of atoms\n");
		exit(1);
	}
	printf("  Number of atoms: %i\n", N);
	getline(file, line);
	if (file.fail())
	{
		exit(1);
	}
	
	for(int i=0; i<N; i++)
	{
		if (!getline(file, line))
		{
			fprintf(stderr, "Error reading atom no. %i\n", i);
			exit(1);
		}
		char element[256];
		Vector3D p;
		sscanf(line.c_str(), "%s %lf %lf %lf", element, &p.x, &p.y, &p.z);
		printf("  %i %s %f %f %f\n", i, element, p.x, p.y, p.z);
		e.push_back(string(element));
		v.push_back(p);
	}
	Nmin = N;
}

void Invert(double A[3][3], double result[3][3])
{
double determinant =    +A[0][0]*(A[1][1]*A[2][2]-A[2][1]*A[1][2])
                        -A[0][1]*(A[1][0]*A[2][2]-A[1][2]*A[2][0])
                        +A[0][2]*(A[1][0]*A[2][1]-A[1][1]*A[2][0]);
double invdet = 1. / determinant;
/*
result[0][0] =  (A[1][1]*A[2][2]-A[2][1]*A[1][2])*invdet;
result[1][0] = -(A[0][1]*A[2][2]-A[0][2]*A[2][1])*invdet;
result[2][0] =  (A[0][1]*A[1][2]-A[0][2]*A[1][1])*invdet;
result[0][1] = -(A[1][0]*A[2][2]-A[1][2]*A[2][0])*invdet;
result[1][1] =  (A[0][0]*A[2][2]-A[0][2]*A[2][0])*invdet;
result[2][1] = -(A[0][0]*A[1][2]-A[1][0]*A[0][2])*invdet;
result[0][2] =  (A[1][0]*A[2][1]-A[2][0]*A[1][1])*invdet;
result[1][2] = -(A[0][0]*A[2][1]-A[2][0]*A[0][1])*invdet;
result[2][2] =  (A[0][0]*A[1][1]-A[1][0]*A[0][1])*invdet;
*/

result[0][0] =  (A[1][1]*A[2][2]-A[2][1]*A[1][2])*invdet;
result[0][1] = -(A[0][1]*A[2][2]-A[0][2]*A[2][1])*invdet;
result[0][2] =  (A[0][1]*A[1][2]-A[0][2]*A[1][1])*invdet;
result[1][0] = -(A[1][0]*A[2][2]-A[1][2]*A[2][0])*invdet;
result[1][1] =  (A[0][0]*A[2][2]-A[0][2]*A[2][0])*invdet;
result[1][2] = -(A[0][0]*A[1][2]-A[1][0]*A[0][2])*invdet;
result[2][0] =  (A[1][0]*A[2][1]-A[2][0]*A[1][1])*invdet;
result[2][1] = -(A[0][0]*A[2][1]-A[2][0]*A[0][1])*invdet;
result[2][2] =  (A[0][0]*A[1][1]-A[1][0]*A[0][1])*invdet;
}

void CreateCSM()
{
	rsm[0][0] = a.x;
	rsm[0][1] = a.y;
	rsm[0][2] = a.z;
	rsm[1][0] = b.x;
	rsm[1][1] = b.y;
	rsm[1][2] = b.z;
	rsm[2][0] = c.x;
	rsm[2][1] = c.y;
	rsm[2][2] = c.z;
	Invert(rsm, csm);

}


void TransformToCS(vector<Vector3D> &csv)
{
	CreateCSM();
	csv.assign(e.size(), Vector3D());
	for(int i=0; i<e.size(); i++)
	{
		csv[i].x = v[i].x * csm[0][0] + v[i].y * csm[1][0] + v[i].z * csm[2][0];
		csv[i].y = v[i].x * csm[0][1] + v[i].y * csm[1][1] + v[i].z * csm[2][1];
		csv[i].z = v[i].x * csm[0][2] + v[i].y * csm[1][2] + v[i].z * csm[2][2];
	}
}


void Check()
{
	vector<Vector3D> csv;
	TransformToCS(csv);
	for(int i=0; i<e.size(); i++)
	{
		Vector3D f = csv[i];
		if ((f.x < -0.00001) || (f.x > 1.00001) || (f.y < -0.00001) || (f.y > 1.00001) || (f.z < -0.00001) || (f.z > 1.00001))
		{
			printf("Warning: Atom no %i is outside of unit cell\n", i);
		}
	}
}

void Removeoutside()
{
	vector<Vector3D> csv;
	TransformToCS(csv);
	double distance = fabs(epsilon/c.z);
	if (fabs(epsilon/a.x) > distance) distance = fabs(epsilon/a.x);
	if (fabs(epsilon/b.y) > distance) distance = fabs(epsilon/b.y);

	begin_new:
	
	for(int i=0; i<e.size();)
	{
		Vector3D f = csv[i];
		if ((f.x < -distance) || (f.x > (1.+distance)) || (f.y < -distance) || (f.y > (1.+distance)) || (f.z < -distance) || (f.z > (1.+distance)))
		{
			e.erase(e.begin()+i);
			v.erase(v.begin()+i);
			csv.erase(csv.begin()+i);
			continue;
		}
		i++;
	}
	printf("%i atoms remaining\n", e.size());
}


void FindNeighbors()
{
	eno.assign(e.size(), 0);
	printf("element_id   element_type   occurence\n");
	printf("-------------------------------------\n");
	for(int i=0; i<e.size(); i++)
	{
		for(int xx=-1;xx<=1; xx++)
		for(int yy=-1;yy<=1; yy++)
		for(int zz=-1;zz<=1; zz++)
		{
			//if ((xx == 0) && (yy == 0) && (zz == 0)) continue;
			Vector3D p;
			p.x = v[i].x + xx*a.x + yy*b.x + zz*c.x;
			p.y = v[i].y + xx*a.y + yy*b.y + zz*c.y;
			p.z = v[i].z + xx*a.z + yy*b.z + zz*c.z;
			if (CheckExistence(p))
			{
				//printf("  Found\n");
				eno[i]++;
			}
		}
		printf("%i    %s    %i", i, e[i].c_str(), eno[i]);
		if (eno[i] == 1) ; else
		if (eno[i] == 2) printf("    surface"); else
		if (eno[i] == 4) printf("    edge"); else
		if (eno[i] == 8) printf("    corner"); else
		printf("    check this");
		printf("\n"); 
	}
	fflush(stdout);
}

void Rotatex(double ang)
{
// around x axis by 45 degrees
	double m[3][3];
	m[0][0] = 0.; m[0][1] = 0.; m[0][2] = 0.;
	m[1][0] = 0.; m[1][1] = 0.; m[1][2] = 0.;
	m[2][0] = 0.; m[2][1] = 0.; m[2][2] = 0.;
	
	double angle = (ang) / 180. * M_PI;
	m[0][0] = 1.;
	m[1][1] =  cos(angle);
	m[1][2] =  sin(angle);
	m[2][1] = -sin(angle);
	m[2][2] =  cos(angle);
	a.Multiply(m);
	b.Multiply(m);
	c.Multiply(m);
	for(int i=0; i<e.size(); i++) v[i].Multiply(m);
	CreateCSM();

}

void Rotatez(double ang)
{
	double m[3][3];

	// 45 degrees around z
	m[0][0] = 0.; m[0][1] = 0.; m[0][2] = 0.;
	m[1][0] = 0.; m[1][1] = 0.; m[1][2] = 0.;
	m[2][0] = 0.; m[2][1] = 0.; m[2][2] = 0.;
	
	double angle = (ang) / 180. * M_PI;
	m[2][2] = 1.;
	m[0][0] =  cos(angle);
	m[0][1] =  sin(angle);
	m[1][0] = -sin(angle);
	m[1][1] =  cos(angle);
	a.Multiply(m);
	b.Multiply(m);
	c.Multiply(m);
	for(int i=0; i<e.size(); i++) v[i].Multiply(m);
	CreateCSM();
	// atom 2 and atom 7
	//printf("  %f %f %f\n", v[7-1].x-v[2-1].x, v[7-1].y-v[2-1].y, v[7-1].z-v[2-1].z);
	//printf("  %f %f %f\n", v[7-1].x-v[4-1].x, v[7-1].y-v[4-1].y, v[7-1].z-v[4-1].z);
	//printf("  %f %f %f\n", v[9-1].x-v[5-1].x, v[9-1].y-v[5-1].y, v[9-1].z-v[5-1].z);
	//printf("  %f %f %f\n", v[9-1].x-v[2-1].x, v[9-1].y-v[2-1].y, v[9-1].z-v[2-1].z);
}

void Rotatey(double ang)
{
	double m[3][3];

	// 45 degrees around z
	m[0][0] = 0.; m[0][1] = 0.; m[0][2] = 0.;
	m[1][0] = 0.; m[1][1] = 0.; m[1][2] = 0.;
	m[2][0] = 0.; m[2][1] = 0.; m[2][2] = 0.;
	
	double angle = (ang) / 180. * M_PI;
	m[1][1] = 1.;
	m[0][0] =  cos(angle);
	m[0][2] =  sin(angle);
	m[2][0] = -sin(angle);
	m[2][2] =  cos(angle);
	a.Multiply(m);
	b.Multiply(m);
	c.Multiply(m);
	for(int i=0; i<e.size(); i++) v[i].Multiply(m);
	CreateCSM();
	// atom 2 and atom 7
	//printf("  %f %f %f\n", v[7-1].x-v[2-1].x, v[7-1].y-v[2-1].y, v[7-1].z-v[2-1].z);
	//printf("  %f %f %f\n", v[7-1].x-v[4-1].x, v[7-1].y-v[4-1].y, v[7-1].z-v[4-1].z);
	//printf("  %f %f %f\n", v[9-1].x-v[5-1].x, v[9-1].y-v[5-1].y, v[9-1].z-v[5-1].z);
	//printf("  %f %f %f\n", v[9-1].x-v[2-1].x, v[9-1].y-v[2-1].y, v[9-1].z-v[2-1].z);
}

void Expand(int mine, int maxe)
{
	Vector3D min;
	Vector3D max;
	min.x = 1e99;
	min.y = 1e99;
	min.z = 1e99;
	max.x = -1e99;
	max.y = -1e99;
	max.z = -1e99;
/*
	for(int i=0; i<v.size(); i++)
	{
		if (v[i].x < min.x) min.x = v[i].x-0.1;
		if (v[i].y < min.y) min.y = v[i].y-0.1;
		if (v[i].z < min.z) min.z = v[i].z-0.1;
		if (v[i].x > max.x) max.x = v[i].x+0.1;
		if (v[i].y > max.y) max.y = v[i].y+0.1;
		if (v[i].z > max.z) max.z = v[i].z+0.1;
	}
	max.x += 10;
	max.y += 10;
	max.z += 10;
*/
	int n = v.size();
	for(int i=0; i<n; i++)
	{
		for(int xx=mine;xx<=maxe; xx++)
		for(int yy=mine;yy<=maxe; yy++)
		for(int zz=mine;zz<=maxe; zz++)
		{
			Vector3D p;
			p.x = v[i].x + xx*a.x + yy*b.x + zz*c.x;
			p.y = v[i].y + xx*a.y + yy*b.y + zz*c.y;
			p.z = v[i].z + xx*a.z + yy*b.z + zz*c.z;
/*
			if (p.x < min.x) continue;
			if (p.y < min.y) continue;
			if (p.z < min.z) continue;
			if (p.x > max.x) continue;
			if (p.y > max.y) continue;
			if (p.z > max.z) continue;
*/
			if (!CheckExistence(p))
			{
				v.push_back(p);
				e.push_back(e[i]);
			}
		}
	}
	//printf("a: %f %f %f\n", a.x, a.y, a.z);
	//printf("b: %f %f %f\n", b.x, b.y, b.z);
	//printf("c: %f %f %f\n", c.x, c.y, c.z);
}

void BuildCubicCell()
{
/*
	Vector3D move = v[0]; // move to La
	for(int i=0; i<v.size(); i++)
	{
		v[i].x -= move.x;
		v[i].y -= move.y;
		v[i].z -= move.z;
	}
*/
	//find smallestrepetition;
	double dminx = 0;
	double dminy = 0;
	double dminz = 0;
	
	//printf("%i\n", Nmin);
	//for(int j=0; j<Nmin; j++)
	for(int j=0; j<v.size(); j++)
	{
		double dx = 100;
		double dy = 100;
		double dz = 100;
		for(int i=0; i<v.size(); i++)
		{
			if (j == i) continue;
			if ((fabs(v[i].y - v[j].y) < epsilon) && (fabs(v[i].z - v[j].z) < epsilon))
			{
				double d = fabs(v[i].x - v[j].x);
				if (d < dx) dx = d;
			}
			if ((fabs(v[i].x - v[j].x) < epsilon) && (fabs(v[i].z - v[j].z) < epsilon))
			{
				double d = fabs(v[i].y - v[j].y);
				if (d < dy) dy = d;
			}
			if ((fabs(v[i].x - v[j].x) < epsilon) && (fabs(v[i].y - v[j].y) < epsilon))
			{
				double d = fabs(v[i].z - v[j].z);
				if (d < dz) dz = d;
			}
		}
		if ((dx < 20) && (dx > dminx)) dminx = dx;
		if ((dy < 20) && (dy > dminy)) dminy = dy;
		if ((dz < 20) && (dz > dminz)) dminz = dz;
	}
	
	//printf("  dx: %f\n", dminx);
	//printf("  dy: %f\n", dminy);
	//printf("  dz: %f\n", dminz);

	
	a.x = dminx;
	a.y = 0.;
	a.z = 0.;

	b.x = 0.;
	b.y = dminy;
	b.z = 0.;
	
	c.x = 0.;
	c.y = 0.;
	c.z = dminz;
	CreateCSM();
/*
vector<Vector3D> vnew;
vector<string> enew;

dminx += 0.1;
dminy += 0.1;
dminz += 0.1;

	// delete all neighbors
	for(int i=0; i<v.size(); i++)
	{
		if ((v[i].x >= -0.1) && (v[i].y >= -0.1) && (v[i].z >= -0.1))
		if ((v[i].x <= dminx) && (v[i].y <= dminy) && (v[i].z < dminz))
		{
			vnew.push_back(v[i]);
			enew.push_back(e[i]);
		}
	}
	v = vnew;
	e = enew;
*/
}

void Reduce()
{
	begin:
	for(int i=0; i<e.size();i++)
	{
		for(int zz=-2;zz<=0; zz++)
		for(int yy=-2;yy<=0; yy++)
		for(int xx=-2;xx<=0; xx++)
		{
			if ((xx == 0) && (yy == 0) && (zz == 0)) continue;
			Vector3D p;
			p.x = v[i].x + xx*a.x + yy*b.x + zz*c.x;
			p.y = v[i].y + xx*a.y + yy*b.y + zz*c.y;
			p.z = v[i].z + xx*a.z + yy*b.z + zz*c.z;
			if (CheckExistence(p))
			{
				e.erase(e.begin()+i);
				v.erase(v.begin()+i);
				goto begin;
			}
		}
	}
}

void inline GetSlabCoordinates(int i, double layerheight, double &zmin, double &zmax)
{
	double centerz = i * layerheight;
	zmin = centerz - layerheight * 0.5;
	zmax = centerz + layerheight * 0.5;
}


void BuildLayers(int nlayers)
{
	vector<int> edone;
	edone.assign(e.size(), -1);
	
	double height = (a.z + b.z + c.z);
	double slabheight = height / nlayers;

	if ((fabs(c.x) > 1e-2) || (fabs(c.y) > 1e-2))
	{
		printf("Warning, layers will be build up along z-axis and not around c-axis\n");
		printf("The unit cell will be repeatable but you should check the terminations\n");
	}
	
	vector<Vector3D> vtemp = v;
	double zmin, zmax;

	// all atoms present in the top layer should be moved to the layer 0
	GetSlabCoordinates(nlayers, slabheight, zmin, zmax);
	for(int i=0; i<edone.size(); i++)
	{
		if ((vtemp[i].z >= zmin) && (vtemp[i].z <= zmax)) 
		{
			vtemp[i].x -= c.x;
			vtemp[i].y -= c.y;
			vtemp[i].z -= c.z;
		}
	}

	printf("\tslabs.AddSlabs(%i, %f);\n", nlayers, slabheight);
	for(int i=0; i<nlayers; i++)
	{
		double centerz = i * slabheight;
		GetSlabCoordinates(i, slabheight, zmin, zmax);
		printf("\t-- z = %f\n", centerz);
		for(int j=0; j<edone.size(); j++)
		{
			if ((vtemp[j].z >= zmin) && (vtemp[j].z <= zmax))
			{
				printf("\tslabs.AddAtom(%i, %sAtom, Vec3(%f, %f, %f));\n", i, e[j].c_str(), vtemp[j].x, vtemp[j].y, vtemp[j].z-centerz);
				edone[j] = 1;
			}
	}
		printf("\n");
	}

		for(int j=0; j<edone.size(); j++)
		{
			if (edone[j] == -1) printf("Warning: atom no %i could not be assigned to a specific slab\n", j);
		}

	/*
	while(1)
	{
	int layeridx = -1;
	for(int i=0; i<edone.size(); i++)
	{
		if (edone[i] == -1)
		{
			layeridx = i;
			break;
		}
	}
	if (layeridx == -1) return; // nothing found
	
	double currentz = 0;
	for(int i=0;i<nlayers; i++)
	{
		double z = slabheight*i;
		if (fabs(z - v[layeridx].z) < fabs(currentz - v[layeridx].z))
			currentz = z;
	}
	printf("\t-- z = %f\n", currentz);

	for(int i=0; i<edone.size(); i++)
	{
		if (fabs(v[i].z-v[layeridx].z)<0.5)
		{
			printf("\tslabs.AddAtom(%i, %sAtom, Vec3(%f, %f, %f));\n", layer, e[i].c_str(), v[i].x, v[i].y, v[i].z-currentz);
			edone[i] = 1;
		}
	}
	layer++;
	printf("\n");
	}
*/
}


void BuildLayersStep(int nlayers)
{
	vector<int> edone;
	edone.assign(e.size(), -1);
	
	double height = (a.z + b.z + c.z);
	double slabheight = height / nlayers;

	if ((fabs(c.x) > 1e-2) || (fabs(c.y) > 1e-2))
	{
		printf("Warning, layers will be build up along z-axis and not around c-axis\n");
		printf("The unit cell will be repeatable but you should check the terminations\n");
	}
	
	vector<Vector3D> vtemp = v;
	double zmin, zmax;

	// all atoms present in the top layer should be moved to the layer 0
	GetSlabCoordinates(nlayers, slabheight, zmin, zmax);
	for(int i=0; i<edone.size(); i++)
	{
		if ((vtemp[i].z >= zmin) && (vtemp[i].z <= zmax)) 
		{
			vtemp[i].x -= c.x;
			vtemp[i].y -= c.y;
			vtemp[i].z -= c.z;
		}
	}

	printf("\tslabs.AddSlabs(%i, %f);\n", nlayers, slabheight);
	for(int i=0; i<nlayers; i++)
	{
		double centerz = i * slabheight;
		GetSlabCoordinates(i, slabheight, zmin, zmax);
		printf("\t-- z = %f\n", centerz);
		for(int j=0; j<edone.size(); j++)
		{
			if ((vtemp[j].z >= zmin) && (vtemp[j].z <= zmax))
			{
				//printf("\tslabs.AddAtom(%i, %sAtom, Vec3(%f, %f, %f));\n", i, e[j].c_str(), vtemp[j].x, vtemp[j].y, vtemp[j].z-centerz);
				Vector3D frac;
				
				frac.x = (int)(vtemp[j].x/a.x*4.+0.5)*a.x/4.;
				frac.y = (int)(vtemp[j].y/a.x*4.+0.5)*a.x/4.;
				frac.z = (int)((vtemp[j].z-centerz)/a.x*4+0.5)*a.x/4.;
				//printf("\tslabs.AddAtom(%i, %sAtom, Vec3(%f, %f, %f));\n", i, e[j].c_str(), frac.x, frac.y, frac.z);
				printf("\tslabs.AddAtom(%i, %sAtom, Vec3(%7f + %7f, %7f + %7f, %7f + %7f));\n", i, e[j].c_str(), 
				frac.x, (vtemp[j].x-frac.x), 
				frac.y, (vtemp[j].y-frac.y),
				frac.z, (vtemp[j].z-centerz-frac.z)
				);
				
				edone[j] = 1;
			}
	}
		printf("\n");
	}

		for(int j=0; j<edone.size(); j++)
		{
			if (edone[j] == -1) printf("Warning: atom no %i could not be assigned to a specific slab\n", j);
		}

}

void Sort()
{
	for(int j=0; j<v.size(); j++)
	for(int i=0; i<v.size(); i++)
	{
		if (i == j) continue;
		if (v[i].z > v[j].z)
		{
			Vector3D temp;
			temp = v[i];
			v[i] = v[j];
			v[j] = temp;
			string s;
			s = e[i];
			e[i] = e[j];
			e[j] = s;
		}		
	}
}


int main(int argc, char *argv[])
{
	if (argc != 2) 
	{
		fprintf(stderr, "Usage: %s scriptfile\n", argv[0]);
		return 0;
	}
	ifstream file(argv[1], ios::in);
	if (file.fail()) return 0;
	
	string s;
	string command;
	while(file.eof() == false)
	{
		getline(file, s);
		if (s.size() <= 2) continue;
		
		stringstream ss (stringstream::in | stringstream::out);
		ss << s;
		if (!(ss.good())) continue;
		ss >> command;
		if (command.c_str()[0] == '#') continue;
		printf("Command:  %s\n", command.c_str());
		fflush(stdout);
		if (command == "openxyz")
		{
			string filename;
			ss >> filename;
			LoadCoordinates(filename.c_str());
		} else
		if (command == "unitcell")
		{
			double al=1., bl=1., cl=1.;
			double alpha = 90.;
			double beta = 90.;
			double gamma = 90.;
			ss >> al;
			ss >> bl;
			ss >> cl;
			ss >> alpha;
			ss >> beta;
			ss >> gamma;
			alpha *=  M_PI / 180.;
			beta  *=  M_PI / 180.;
			gamma *=  M_PI / 180.;
			// http://en.wikipedia.org/wiki/Fractional_coordinates
			a.x = al;
			a.y = 0.;
			a.z = 0.;
			b.x = bl*cos(gamma);
			b.y = bl*sin(gamma);
			b.z = 0.;
			double v = sqrt(
				1. - cos(alpha)*cos(alpha) - cos(beta)*cos(beta) - cos(gamma)*cos(gamma)
				   + 2.*cos(alpha)*cos(beta)*cos(gamma)
			);
			c.x = cl * cos(beta);
			c.y = cl * (cos(alpha) - cos(beta)*cos(gamma)) / sin(gamma);
			c.z = cl * v/sin(gamma);
			printf("  vector a: (%f %f %f)\n", a.x, a.y, a.z);
			printf("  vector b: (%f %f %f)\n", b.x, b.y, b.z);
			printf("  vector c: (%f %f %f)\n", c.x, c.y, c.z);
			CreateCSM();
		} else
		if (command == "findneighbors")
		{
			FindNeighbors();
		} else
		if (command == "check")
		{
			Check();
		} else
		if (command == "printunitcellvectors")
		{
			printf("  vector a: (%f %f %f)\n", a.x, a.y, a.z);
			printf("  vector b: (%f %f %f)\n", b.x, b.y, b.z);
			printf("  vector c: (%f %f %f)\n", c.x, c.y, c.z);
		} else
	if (command == "removeoutside")
		{
			Removeoutside();
		} else
		if (command == "reduce")
		{
			Reduce();
		} else
		if (command == "expand")
		{
			int min = -2;
			int max = 2;
			ss >> min;
			ss >> max;
			Expand(min, max);
		} else
		if (command == "pause")
		{
			printf("Press enter to continue\n");
			char dummy[256];
			scanf("%s", dummy);
		} else
		if (command == "sort")
		{
			Sort();
		} else
		if (command == "savexyz")
		{
			string filename;
			ss >> filename;
			SaveCoordinates(filename.c_str());
		} else
		if (command == "buildlayers")
		{
			int nlayers;
			ss >> nlayers;
			BuildLayers(nlayers);
		} else
		if (command == "buildlayersstep")
		{
			int nlayers;
			ss >> nlayers;
			BuildLayersStep(nlayers);
		} else
		if (command == "findcubiccell")
		{
			BuildCubicCell();
		} else
		if (command == "atomdistance")
		{
			ss >> epsilon;
		} else
		if (command == "rotatex")
		{
			double ang;
			ss >> ang;
			Rotatex(ang);
		} else
		if (command == "rotatey")
		{
			double ang;
			ss >> ang;
			Rotatey(ang);
		} else
		if (command == "rotatez")
		{
			double ang;
			ss >> ang;
			Rotatez(ang);
		} else
		if (command == "moveatom")
		{
			int atomn;
			double dx, dy, dz;
			ss >> atomn;
			ss >> dx;
			ss >> dy;
			ss >> dz;
			if (atomn == -1)
			{
				for(int i=0; i<v.size(); i++)
				{
					v[i].x += dx;
					v[i].y += dy;
					v[i].z += dz;
				}
			} else
			{
			v[atomn-1].x += dx;
			v[atomn-1].y += dy;
			v[atomn-1].z += dz;
			}
		} else
		{
			printf("Command not found\n");
			return 1;
		}		
	}
	file.close();

	return 0;
}