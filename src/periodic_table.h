#ifndef PERIODIC_TABLE_H
#define PERIODIC_TABLE_H

#include "CString.h" 
#include "CTable.h"


class CElementInfo
{
	public:
	CString element;
	int index;
	double Z, A;
	CString name;   // fullname
	double cromer[9];

	double density; // used for ParseChemicalFormula

	CElementInfo()
	{
		index = 0;
		Z = -1;
		A = -1;
		density = -1;
	}
};
/*
class CElements
{
	public:
	double density; // mol / cm^3
	CString name;
};
*/

bool FindElement(CString name, CElementInfo &info);
bool GetElement(int i, CElementInfo &info);
bool ParseChemicalFormula(CString str, double density, CVector<CElementInfo> &elements);
bool GetChantlerTable(const int index, CTable &fftable);


#endif
