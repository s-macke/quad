#ifndef MULTIPLE_SCATTERING_H
#define MULTIPLE_SCATTERING_H


void XMAT(
	c::complex<REAL> **XODD,
	c::complex<REAL> **XEVEN,
	const int LMAX, 
	const c::complex<REAL> &KAPPA,
	const CCoor3D &AK,
	const REAL* ELM, const CCoor2D &AR1, const CCoor2D &AR2);

void SETUP(
	const int LMAX,
	c::complex<REAL> **XEVEN,
	c::complex<REAL> **XODD,
	const c::complex<REAL> *TE,
	const c::complex<REAL> *TH,
	c::complex<REAL> **XXMAT1,
	c::complex<REAL> **XXMAT2);

void Sort1(int LMAX, int LMXOD, int K2, c::complex<REAL> *BMEL1, c::complex<REAL> *BMEL2, const CExpansionCoefficients &B);
void Sort2(int LMAX, int LMXOD, int K1, c::complex<REAL> *BMEL1, c::complex<REAL> *BMEL2, c::complex<REAL> *LAME, c::complex<REAL> *LAMH, CExpansionCoefficients &D);


#endif