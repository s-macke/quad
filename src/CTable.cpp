#include "CTable.h"
#include <iostream>
#include <sstream>
#include <fstream>

#include <math.h>

void* memcpy(void* dest, const void* src, size_t count) {
        char* dst8 = (char*)dest;
        char* src8 = (char*)src;

        while (count--) {
            *dst8++ = *src8++;
        }
        return dest;
    }

/*
#ifdef __GNUC__
static	pthread_mutex_t tablemutex = PTHREAD_MUTEX_INITIALIZER;
#endif
*/

CTable::CTable(int _ncolumns) : ncolumns(_ncolumns)
{
	index = 0;
}

CTable::CTable()
{
	ncolumns = 0;
	index = 0;
}

	void CTable::DeleteRegion(double min, double max)
	{
		CVector<CTableEntry> e;

		for(int i=0; i<entry.size(); i++)
		{
			if ((entry[i].x > min) && (entry[i].x < max)) continue;
			e.push_back(entry[i]);
		}

		entry = e;
	}

	void CTable::DeleteDistance(double d)
	{
		CVector<CTableEntry> e;
		if (entry.size() <= 1) return;

		e.push_back(entry[0]);
		double lastx = entry[0].x;
		
		for(int i=1; i<entry.size(); i++)
		{
			if (fabs(lastx - entry[i].x) < d) continue;
			e.push_back(entry[i]);
			lastx = entry[i].x;
		}
		entry = e;
		
	}

	bool CTable::WithinRange(double x) const
	{
		if (entry.empty()) return false;
		if (x < entry[0].x) return false;
		if (x > entry.last().x) return false;
		return true;
	}

bool CTable::LoadAsciiTable(const CString filename)
{
	entry.clear();
    //printf("Loading file %s\n", filename.c_str());
    //printf("Loading file\n");
    fflush(stdout);
    
	std::ifstream file(filename.c_str(), std::ifstream::in);

	if (file.fail())
	{
	//	file.close();
		return false;
	}

	CTableEntry t;
/*
	if (ncolumns >= 4)
	{
		printf("Error, too much columns in table. Supported are 4\n");
		exit(1);
	}
*/
//	t.y.assign(ncolumns, 0.);

	std::string s;

	while(file.eof() == false)
	{
		//::getline(&(std::string), &n, optKonst);
		getline(file, s);
		if (s.size() <= 1) continue;
		if (s[0] == '#') continue;

		std::stringstream ss (std::stringstream::in | std::stringstream::out);
		//ss.str(s);
		ss << s;
		if (!(ss.good())) return false;
		ss >> t.x;
//		printf("%lf\n", t.x);
//	cout << s << "\n";

		for(int i=0; i<ncolumns; i++)
		{
			if (!(ss.good())) return false;
			//(ss.iostate != ios::good) return false;
			ss >> t.y[i];
			
//			printf("%lf\n", t.y[i]);
		}

		entry.push_back(t);
	}
	file.close();
	if (entry.empty()) return false;
	return true;
}

bool CTable::SaveAsciiTable(const CString filename)
{
//	cout << "write file " << filename.c_str() << "\n"; 

	std::ofstream file(filename.c_str());

	if (file.fail())
	{
		file.close();
		return false;
	}

	for(int i=0; i<entry.size(); i++)
	{
		file << entry[i].x;
		for(int j=0; j<ncolumns; j++)
		{
		file << "\t" << entry[i].y[j];
		}
		
		file << "\n";
	}
	
	return true;

}

CTableEntry CTable::Get(double x)
{
	CTableEntry y;
    y.x = x;

	if (entry.empty())
	{
		for(int i=0; i<ncolumns; i++) y.y[i] = 0.;
//	CVector<double> y;
//	y.assign(ncolumns, 0.);
#ifdef USECPP11
		return std::move(y);
#else
		return y;
#endif	
	}

	if (x == entry[0].x)
	{
		return entry[0];
	}
	if(x == entry.last().x)
	{
		return entry.last();
	}


	if (x <= entry[0].x)
	{
//		CVector<double> y;
//		y.assign(ncolumns, 0.);
//		for(int i=0; i<ncolumns; i++) y[i] = entry[0].y[i];
//		return y;
		for(int i=0; i<ncolumns; i++) y.y[i] = 0.;
		return y;
//		return entry[0];
	}

	if(x >= entry.last().x)
	{
//		CVector<double> y;
//		y.assign(ncolumns, 0.);
//		for(int i=0; i<ncolumns; i++) y[i] = entry.last().y[i];
//		return y;
		for(int i=0; i<ncolumns; i++) y.y[i] = 0.;
		return y;

//		return entry.last();
	}

	mutex.Lock();

	if (x > entry[index].x)
	{
		while(x > entry[index].x) index++;
	} else
	{
		while(x < entry[index].x) index--;
		index++;
	}
	int tempindex = index;

	mutex.Unlock();

//	if (tempindex == 0) return entry[0].y;
//	CVector<double> y;
//	y.assign(ncolumns, 0.);
	for(int i=0; i<ncolumns; i++)
	{
		double dist = (entry[tempindex].x - entry[tempindex-1].x);
		if (dist == 0) dist = 1e-5;
		y.y[i] = entry[tempindex-1].y[i] + (entry[tempindex].y[i] - entry[tempindex-1].y[i]) * ((x - entry[tempindex-1].x) / dist);
	}

#ifdef USECPP11
	return std::move(y);
#else
	return y;
#endif
}

int choose_pivot(int i,int j )
{
   return((i+j) /2);
}


void CTable::Swap(int m, int n)
{
	CTableEntry temp;
	temp = entry[m];
	entry[m] = entry[n];
	entry[n] = temp;

}

void CTable::Quicksort(int left, int right)
{
	double key;
	int i, j, k;
	//m = left, n=right
	if (left >= right) return;
	k = choose_pivot(left, right);
//	k = left;
	Swap(left, k);
	key = entry[left].x;
	i = left + 1;
	j = right;
	while (i <= j)
	{
		while((i <= right) && (entry[i].x <= key)) i++;
		while((j >= left) && (entry[j].x > key)) j--;
		if( i < j)	Swap(i, j);
	}
	  // swap two elements
	  Swap(left, j);
	  // recursively sort the lesser list
	  Quicksort(left, j-1);
	  Quicksort(j+1, right);
}

void CTable::Sort()
{
	if (entry.size() <= 1) return;
	Quicksort(0, entry.size()-1);

/*
	// simple bubble sort. Should be changed to something faster
	bool changed = false;
	int iter = 0;
	do
	{
		changed = false;
		for(int i=0; i<entry.size()-1; i++)
		{
			if (entry[i].x > entry[i+1].x)
			{
				CTableEntry temp;
				temp = entry[i];
				entry[i] = entry[i+1];
				entry[i+1] = temp;
				changed = true;
			}
		}
		iter++;
		if (iter > entry.size()) return;
	} while (changed);
*/
}

void CTable::BetaToF2()
{
	for(int i=0; i<entry.size(); i++)
	for(int j=1; j<ncolumns; j+=2)
	{
		entry[i].y[j] = entry[i].y[j] * entry[i].x * entry[i].x;
	}
}

void CTable::FFToDB()
{
	for(int i=0; i<entry.size(); i++)
	for(int j=0; j<ncolumns; j+=1)
	{
		entry[i].y[j] = entry[i].y[j] / entry[i].x / entry[i].x;
	}


/*
	for(int i=0; i<entry.size(); i++)
	{
		entry[i].y[1] = entry[i].y[1] / entry[i].x / entry[i].x;
		entry[i].y[0] = (Z + entry[i].y[0]) / entry[i].x / entry[i].x;
	}
*/
}

void CTable::KK(double Z, int src, int dest)
{
if (ncolumns <= src) return;
if (ncolumns <= dest) return;

for(int i=0; i<entry.size(); i++)
	entry[i].y[dest] = Z;

//pragma omp parallel for
for(int i=1; i<entry.size()-1; i++)
{
//	fprintf(stderr, "number %i\n", i);
//	fflush(stderr);
	LocalKK(i, entry[i].y[src], dest);
}

/*
	for(int i = entry.size()-1; i>=0; i--)
	{
		entry[i+1].y[0] = (entry[i].y[0] + entry[i+1].y[0]) * 0.5;
	}
*/
}


void CTable::LocalKK(int index, double beta, int dest)
{
if (index <= 0) return;
if (index >= entry.size()) return;

//	entry[index].y[1] = beta;
	long double xm = entry[index - 1].x;
	long double x0 = entry[index    ].x;
	long double xp = entry[index + 1].x;

	long double m1 = (0. - beta) / (xp - x0);
	long double b1 = beta - m1 * x0;

	long double m2 = (beta - 0.) / (x0 - xm);
	long double b2 = beta - m2 * x0;

	long double dEnergy = 0.5 * (xp - xm);
	
	long double dist = 10.; // 20eV is minimum where we calculate more accurate. 
	if (((xp-xm)*3.) > dist) dist = (xp-xm)*3.; // but at larger steps measure more accurate
	
	for(int j=0; j < entry.size(); j++)
	{
		long double delta = 0.;
		long double newE = entry[j].x + 0.00234563;

		if (fabs(x0 - newE) < dist)
		{

		delta	+= 0.5 * b1 *
							log(
							fabs(newE*newE - xp * xp)
							/
							fabs(newE*newE - x0 * x0))
							+              m1 * (xp - x0)
							+ 0.5 * newE * m1 * log((newE + x0) / (newE + xp))
							+ 0.5 * newE * m1 * log(fabs(newE - xp) / fabs(newE - x0));

		delta	+= 0.5 * b2 *
							log(
							fabs(newE*newE - x0 * x0)
							/
							fabs(newE*newE - xm * xm))
							+              m2 * (x0 - xm)
							+ 0.5 * newE * m2 * log((newE + xm) / (newE + x0))
							+ 0.5 * newE * m2 * log(fabs(newE - x0) / fabs(newE - xm));
		//printf("%i %f %f %f\n", j, delta, m1, newE);
		}
		else
		{
				long double f1 = (beta * x0) / (newE*newE - x0*x0);
				delta -= dEnergy * f1;
		}
		entry[j].y[dest] -= 2. / M_PI * delta;
	//fprintf(stderr, "%i %lf\n", j, entry[j].y[0]);
	//fflush(stderr);
	}
}

double CTable::FindLargestError(CTable &t, double stepsize)
{
	double maxerror = 0;
	double maxx = -1;
	for(int i=0; i<t.entry.size()-1; i++)
	{
		double min = t.entry[i+0].x;
		double max = t.entry[i+1].x;
		double error = 0;

		for(double x=min; x<=max; x+=stepsize)
		{
//			CVector<double> y1, y2;
			CTableEntry y1, y2;

			y1 = Get(x); 
			y2 = t.Get(x);
			for(int k=0; k<ncolumns; k++) error += (y1.y[k] - y2.y[k]) * (y1.y[k] - y2.y[k]);
		}
		error /= (max - min);
		if (error > maxerror)
		{
			maxerror = error;
			maxx = (max + min) * 0.5;
		}
	}
	return maxx;
}

void CTable::Reduce(double minx, double maxx, double dx, int npoints)
{
	CTable t(ncolumns);
	int n = (maxx - minx) / dx;
	dx = (maxx - minx) / double(n);

	for(int i=0; i<=n; i++)
	{
		CTableEntry te;
		te.x = minx + double(i)*dx;
//		CVector<double> y;
		CTableEntry y;

		y = Get(te.x);
		for(int j=0; j<ncolumns; j++) te.y[j] = y.y[j];
		t.entry.push_back(te);
	}

while(t.entry.size() < npoints)
{
	double newx = FindLargestError(t, 0.1);
	if (newx == -1) return;
	CTableEntry te;
	te.x = newx;
//	CVector<double> y;
	CTableEntry y;
  	y = Get(newx);
	for(int j=0; j<ncolumns; j++) te.y[j] = y.y[j];
	t.entry.push_back(te);
	t.Sort();
	t.index = 0;
	
}	
	Insert(t);
}

void CTable::Insert(const CTable &t)
{
	CVector<CTableEntry> e;
	
	if (ncolumns != t.ncolumns) return;
	
	double minx = t.entry[0].x;
	double maxx = t.entry.last().x;
	
	int i=0;
	for(; entry[i].x<minx; i++)
	{
		e.push_back(entry[i]);
	}
	
	for(int j=0; j<t.entry.size(); j++)
	{
		e.push_back(t.entry[j]);
	}
	
	for(; entry[i].x<=maxx; i++){}
	
	for(; i<entry.size(); i++)
	{
		e.push_back(entry[i]);
	} 
	Sort(); //just to be sure
	index = 0;
 	entry = e;
}



void CTable::Merge2(const CTable &t)
{
	if (ncolumns != t.ncolumns) return;
	if (t.entry.empty()) return;

	entry.Reserve(entry.size() + t.entry.size());
	for(int i=0; i<t.entry.size(); i++)
	{
		entry.push_back(t.entry[i]);
	}
	Sort();
	index = 0;
}

