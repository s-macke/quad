#ifndef CSLAB
#define CSLAB

#include "global.h"
#include <assert.h>

// 2d matrices
// first index = row = outgoing ray
// last index = column = incoming ray

class CSlab
{
	public:
	c::complex<REAL> **QI;	  // transmitted from top
	c::complex<REAL> **QII;  // reflected from bottom
	c::complex<REAL> **QIII; // reflected from top
	c::complex<REAL> **QIV;  // transmitted from bottom
	int IGKMAX;
	MultipleScatteringAccuracy msaccuracy;
	MatrixAccuracy matrixaccuracy;
	NumericAccuracy numaccuracy;
	
	// temp variables for pairing
	c::complex<REAL> **QINV1, **QINV2;
	c::complex<REAL> **W1, **W2, **W3, **W4;
	
	REAL ztop, zbottom;

	CSlab(int _IGKMAX);
	~CSlab();
	void Create(int _IGKMAX);
	void Destroy();

	void Zero();
	void Identity();
	void PAIREasy(const CSlab &slabB);
	void PAIR(const CSlab &slabB, const CVector<CKCoord3D> &GKK);
	void MultiplyPair(int multiplicator, const CVector<CKCoord3D> &GKK);
	void AddPropagator(CCoor3D dt, CCoor3D db, const CVector<CKCoord3D> &GKK);

	void SetAccuracy(MultipleScatteringAccuracy acc1, MatrixAccuracy acc2)
	{
		msaccuracy = acc1;
		matrixaccuracy = acc2;
	}
	
	CSlab()
	{
		IGKMAX = 0;
		QI = NULL;
		QII = NULL;
		QIII = NULL;
		QIV = NULL;

		QINV1 = NULL;
		QINV2 = NULL;
		W1 = NULL;
		W2 = NULL;
		W3 = NULL;
		W4 = NULL;
	}

	CSlab(const CSlab& r)
	{
		Create(r.IGKMAX);
		*this = r;
	}

	CSlab& operator=(const CSlab& r)
	{
		//assert(r.IGKMAX == IGKMAX);
		if (r.IGKMAX != IGKMAX)
		{
			Destroy();
			Create(r.IGKMAX);
		}
		
		for(int IGK1=0; IGK1<IGKMAX; IGK1++)
		for(int IGK2=0; IGK2<IGKMAX; IGK2++)
		{
			QI  [IGK1][IGK2] = r.QI  [IGK1][IGK2];
			QII [IGK1][IGK2] = r.QII [IGK1][IGK2];
			QIII[IGK1][IGK2] = r.QIII[IGK1][IGK2];
			QIV [IGK1][IGK2] = r.QIV [IGK1][IGK2];
		}
		matrixaccuracy = r.matrixaccuracy;
		msaccuracy = r.msaccuracy;
		numaccuracy = r.numaccuracy;
		ztop = r.ztop;
		zbottom = r.zbottom;
		
/*		
		if (matrixaccuracy == MDIAGONAL)
		{
			for(int IGK1=0; IGK1<IGKMAX; IGK1++)
			for(int IGK2=0; IGK2<IGKMAX; IGK2++)
			{
				if (IGK1 == IGK2) continue;
				QI  [IGK1][IGK2] = 0.;
				QII [IGK1][IGK2] = 0.;
				QIII[IGK1][IGK2] = 0.;
				QIV [IGK1][IGK2] = 0.;				
			}
}
*/	
		return *this;
	}
};

#endif
