#include <math.h>
#include <stdlib.h>
#include <stdio.h>

#include"CFit.h"

double CFit::CallError(CVector<double> &avars)
{
        VerifyVars(avars);
		for(int i=0; i < vars.size(); i++)
                vars[i].var = avars[i];

		lasterror = Error(vars);
        return lasterror;
}

void CFit::VerifyVars(CVector<double> &avars)
{
    for(int i=0; i < vars.size(); i++)
            vars[i].var = avars[i];

	for(int i=0; i < vars.size(); i++)
	{
		if (vars[i].var < vars[i].lb) vars[i].var = vars[i].lb;
		if (vars[i].var > vars[i].ub) vars[i].var = vars[i].ub;
	}

    for(int i=0; i < vars.size(); i++)
        avars[i] = vars[i].var;
}

	double inline LinInterpolation(double xa, double x, double xb, double ya, double yb)
	{
		return ya-(xa-x)/(xa-xb)*(ya-yb);
	}

	double drand()
	{
		return double(rand())/RAND_MAX;
	}
	
void CFit::LineSearch()
{
	int N = 20;

	CVector<TFitVariable> begin, end;
	begin = vars;
	end = vars;

	for(int i=0; i<vars.size(); i++)
	{
		begin[i].var = LinInterpolation(0., drand(), 1., begin[i].lb, begin[i].ub);
		end[i].var   = LinInterpolation(0., drand(), 1., end[i].lb, end[i].ub);
		end[i].delta = (end[i].var - begin[i].var) / double(N);
	}

//	double minerror = Error(begin);
	CVector<TFitVariable> best = begin;
/*
	for(int j=0; j<N; j++)
	{
		for(int i=0; i<vars.size(); i++)
		{
			if (!vars[i].active) continue;
			begin[i].var += end[i].delta;
		}
		double error = Error(begin);
		if (error < minerror)
		{
			minerror = error;
			best = begin;
		}
	}
*/

	vars = best;
	BeginFit();
	for(int i=0; i<100; i++)
	{
		Simplex();
	}

	if (lasterror < besterror)
	{
		besterror = lasterror;
		bestfit = vars;
	}
/*
	VerifyVars(bestfit);
	Error(bestfit);
*/
}

void CFit::Iterate()
{
		double eold;

        double e = CallError(activevars);

        for(unsigned int k=0; k < vars.size(); k++)
		{
				{
						eold = e;
						activevars[k] += vars[k].delta;
                        e = CallError(activevars);
                }

                if (e > eold)
                {
                        activevars[k] -= vars[k].delta;
                        vars[k].delta = -vars[k].delta*0.3;
                }

        }
}


//Extrapolates by a factor fac through the face of the simplex across from the high point, tries
//it, and replaces the high point if the new point is better.
double CFit::amotry(int ihi, double fac)
{
		unsigned int j;
        double fac1,fac2, etry;
        CVector<double> ptry;

        ptry.assign(activevars.size(), 0);

        fac1=(1.0-fac)/activevars.size();
        fac2=fac1 - fac;
        for (j=0; j < activevars.size(); j++) ptry[j] = psum[j]*fac1-p[ihi][j]*fac2;

        etry = CallError(ptry); //Evaluate the function at the trial point.
        if (etry < e[ihi])
        {
                //If it�s better than the highest, then replace the highest.
                e[ihi]=etry;
                for (j=0; j< activevars.size(); j++)
                {
                        psum[j] += ptry[j]-p[ihi][j];
                        p[ihi][j]=ptry[j];
                }
        }

        return etry;
}

void CFit::Simplex()
{
        double etry, esave;
        const double TINY=1.e-10;
        if (firstsimplex)
        {
                pb.assign(activevars.size(), 0);
                pb=activevars;

                p.assign(activevars.size()+1, activevars);
                e.assign(activevars.size()+1,0);
                for(unsigned int i=1; i < activevars.size()+1; i++)
				{
						p[i][i-1] += vars[i-1].delta;
                }

				for(unsigned int i=0; i < activevars.size()+1; i++)
                {
                        e[i] = CallError(p[i]);
                }

                psum.assign(activevars.size(), 0);
                for(unsigned int j=0; j < activevars.size(); j++)
                {
                        for(int i = 0; i < activevars.size()+1; i++)
                        {
                                psum[j] += p[i][j];
                        }
                }
                firstsimplex=false;
        }

        if (activevars.size()==0) return;

                int ilo, ihi, inhi;
                ilo=0;
                //First we must determine which point is the highest (worst), next-highest, and lowest
                //(best), by looping over the points in the simplex.
                ihi = e[0]>e[1] ? (inhi=1,0) : (inhi=0,1);
                for (int i=0;i<activevars.size()+1;i++)
                {
                        if (e[i] <= e[ilo]) ilo=i;
                        if (e[i] > e[ihi])
                        {
                                inhi=ihi;
                                ihi=i;
                        }
                        else if (e[i] > e[inhi] && i != ihi) inhi=i;

                }

                //double rtol=2.0*fabs(e[ihi]-e[ilo])/(fabs(e[ihi])+fabs(e[ilo])+TINY);
                //Compute the fractional range from highest to lowest and return if satisfactory.

                /*
                if (rtol < ftol)
                {
                        //If returning, put best point and value in slot 1.
                        double swap=e[0];e[0]=e[ilo];e[ilo]=swap;
                        for (int i=0;i<activevars.size();i++){ TFitVariable swap=p[0][i];p[0][i]=p[ilo][i];p[ilo][i]=swap;}
                        activevars = p[0];
                        firstsimplex=true;
                        break;
                }
                */
                activevars = p[ilo];

               //Begin a new iteration. First extrapolate by a factor -1 through the face of the simplex
                //across from the high point, i.e., reflect the simplex from the high point.

                etry = amotry(ihi,-1.0);


                if (etry <= e[ilo])
                {
                        //Gives a result better than the best point, so try an additional extrapolation by a
                        //factor 2.
                        etry=amotry(ihi,2.0);

                }
                else if (etry >= e[inhi])
                {
                        //The reflected point is worse than the second-highest, so look for an intermediate
                        //lower point, i.e., do a one-dimensional contraction.
                        esave=e[ihi];
                        etry=amotry(ihi,0.5);

                        if (etry >= esave)
                        {
                                //Can�t seem to get rid of that high point. Better
                                //contract around the lowest (best) point.
								for (int i=0; i<activevars.size()+1; i++)
                                {
                                        if (i != ilo)
										{
                                                for (unsigned int j=0; j<activevars.size(); j++)
                                                {
                                                        p[i][j]=psum[j]=0.5*(p[i][j]+p[ilo][j]);
                                                }
                                                e[i]=CallError(psum);
                                        }
                                }

                                //Recompute psum.
                                psum.assign(activevars.size(), 0);
                                for(unsigned int j=0; j < activevars.size(); j++)
                                {
                                        for(unsigned int i = 0; i < activevars.size()+1; i++) psum[j] += p[i][j];
                                }

                        }
                }

                activevars = p[ilo];
                CallError(activevars);

}


//Extrapolates by a factor fac through the face of the simplex across from the high point, tries
//it, and replaces the high point if the new point is better.
double CFit::amotsa( int ihi, double *ehi, double fac)
{
		int j;
        double fac1,fac2, eflu, etry;
        CVector<double> ptry;

        ptry.assign(activevars.size(), 0);

        fac1 = (1.0-fac)/activevars.size();
        fac2 = fac1-fac;
        for (j=0; j < activevars.size(); j++) ptry[j] = psum[j]*fac1-p[ihi][j]*fac2;
        etry = CallError(ptry); //Evaluate the function at the trial point.
        if (etry <= eb)
        {
                //If it�s better than the highest, then replace the highest.
                pb[j] = ptry[j];
                eb = etry;
        }

// We added a thermal fluctuation to all the current
// vertices, but we subtract it here, so as to give
// the simplex a thermal Brownian motion: It
// likes to accept any suggested change.

		eflu=etry+tempptr * log(drand());
		if (eflu < *ehi)
        {
                e[ihi] = etry;
                *ehi = eflu;
                for (j=0; j<activevars.size(); j++)
                {
                        psum[j] += ptry[j]-p[ihi][j];
                        p[ihi][j] = ptry[j];
                }
        }

        return eflu;
}


void CFit::Simplex2()
{
        double etry, esave;
        double enhi, elo, ehi, et;

        double ftol = 0.1;

        if (firstsimplex)
        {
                pb.assign(activevars.size(), 0);
                pb =activevars;
                p.assign(activevars.size()+1, activevars);
                e.assign(activevars.size()+1,0);
                /*
                for(int i=1; i < activevars.size()+1; i++)
                        p[i][activevarsindex[i-1]] += vars[activevarsindex[i-1]].delta;
                */

                for(unsigned int i=1; i < activevars.size()+1; i++)
                {
                        p[i][i-1] += vars[i-1].delta;
                }


                for(int i=0; i < activevars.size()+1; i++)
                {
                        e[i] = CallError(p[i]);
                }
                psum.assign(activevars.size(), 0);
                for(int j=0; j < activevars.size(); j++)
                        for( int i = 0; i < activevars.size()+1; i++) psum[j] += p[i][j];

                firstsimplex = false;
				return;
        }

        if (activevars.size()==0) return;

        tempptr += dtempptr;
        if (tempptr < 0) tempptr = 0;

                int ilo, ihi, inhi;
                //Determine which point is the highest (worst),
                //next-highest, and lowest (best).
                //Whenever we �look at� a vertex, it gets
                //a random thermal fluctuation.
                ilo=0;
                ihi=1;
				enhi=elo=e[0]=e[0]-tempptr*log(drand());
				ehi=e[1]-tempptr*log(drand());

                if (elo > ehi)
                {
                        ihi=0;
                        ilo=1;
                        enhi=ehi;
                        ehi=elo;
                        elo=enhi;
                }

                for (int i=2;i<activevars.size()+1;i++)
                {
                        //Loop over points in the simplex. More thermal fluctuations.
						et = e[i]-tempptr*log(drand());

                        if (et <= elo)
                        {
                                ilo = i;
                                elo = et;
                        }
                        if (et > ehi)
                        {
                                enhi = ehi;
                                ihi = i;
                                ehi = et;
                        } else if (et > enhi) enhi=et;
                }

                double rtol=2.0*fabs(ehi-elo)/(fabs(ehi)+fabs(elo));
                //Compute the fractional range from highest to lowest and return if satisfactory.

                /*
                if (rtol < ftol)
                {
                        //If returning, put best point and value in slot 1.
                        double swap=e[0];e[0]=e[ilo];e[ilo]=swap;
                        for (int i=0;i<activevars.size();i++){ TFitVariable swap=p[0][i];p[0][i]=p[ilo][i];p[ilo][i]=swap;}
                        activevars = p[0];
                        firstsimplex=true;
                        break;
                }
                */

                //Begin a new iteration. First extrapolate by a factor -1 through the face of the simplex
                //across from the high point, i.e., reflect the simplex from the high point.
                etry = amotsa(ihi, &ehi, -1.0);

                if (etry <= elo)
                {
                        //Gives a result better than the best point, so try an additional extrapolation by a
                        //factor 2.
                        etry=amotsa(ihi, &ehi, 2.0);
                }
                else if (etry >= enhi)
                {
                        //The reflected point is worse than the second-highest, so look for an intermediate
                        //lower point, i.e., do a one-dimensional contraction.
                        esave = ehi;
                        etry=amotsa(ihi, &ehi, 0.5);
                        if (etry >= esave)
                        {
                                //Can�t seem to get rid of that high point. Better
                                //contract around the lowest (best) point.
                                for (int i=0; i<activevars.size()+1; i++)
                                {
                                        if (i != ilo)
                                        {
                                                for (int j=0; j<activevars.size(); j++)
                                                {
                                                        psum[j]=0.5*(p[i][j] + p[ilo][j]);
                                                        p[i][j] = psum[j];
                                                }
                                                e[i]=CallError(psum);
                                        }
                                }


                                //Recompute psum.
                                psum.assign(activevars.size(), 0);
                                for(int j=0; j < activevars.size(); j++)
                                        for(int i = 0; i < activevars.size()+1; i++) psum[j] += p[i][j];

                        }
                }

                //activevars = pb;
                //CallError(activevars);
}

/*
        vector<double> v;
        vector<double> goodv;
        double e;

        double eold = CallError(activevars);

        goodv = activevars;
        for(int i=0; i < n; i++)
        {
                v = activevars;
                for(unsigned int k=0; k < activevars.size(); k++)
                {
                                v[k] += (double(rand())/double(RAND_MAX)*2.-1.)*vars[activevarsindex[k]].delta;
                }
                e = CallError(v);
                if (e < eold)
                {
                        goodv = v;
                        eold = e;
                }
        }
        CallError(goodv);

*/



