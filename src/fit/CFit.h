#ifndef CFIT_H
#define CFIT_H
#include <vector>
#include <limits.h>
#include <float.h>
#include "../CVector.h"

using namespace std;

//maybe not really chisquare, it depends how the error is calculated

class TFitVariable
{
     public:
        double var, delta;
		double ub, lb; //lower boundary and upper boundary
		int id;
		int additional_id;
     TFitVariable()
     {
        var = 0;
        delta = 0;
        ub = 1.e10;
        lb = -1.e10;
     }
};

class CFit
{
		public:
				double lasterror;
				CVector<TFitVariable> vars;

				CVector<double> activevars;

				CVector<double> x; //error of each point

				void LineSearch();

                void Iterate();
                void Simplex();
                void Simplex2();
                void Random(int n);
                double amotry(int ihi, double fac);

                double tempptr;
                double dtempptr;
                double amotsa( int ihi, double *yhi, double fac);

                void BeginFit()
				{
						firstsimplex = true;
						activevars.clear();
						for(int i=0; i<vars.size(); i++)
						{
							activevars.push_back(vars[i].var);
						}
				};

				double CallError(CVector<double> &avars);
				void VerifyVars(CVector<double> &avars);

				CFit(const CVector <TFitVariable> mvars, const long unsigned int seed) : vars(mvars)
				{
					firstsimplex=true; 
					tempptr=0; 
					dtempptr=0; 
					besterror=1e90;
				}
				CFit(const unsigned long seed)
				{
					firstsimplex = true;
					tempptr = 0.;
					dtempptr = 0.;
					besterror = 1e90;
				}
				~CFit()
				{
				}

        protected:
				virtual double Error(CVector<TFitVariable> &vars)=0;

		private:
				//for simplex
				bool firstsimplex;
				CVector< CVector<double> > p;
				CVector<double> pb; //best if simulated annealing
				CVector<double> e;
				CVector<double> psum;

				double eb;

				//for LineSearch
				CVector<TFitVariable> bestfit;
				double besterror;
};




#endif

