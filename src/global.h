#ifndef GLOBAL_H
#define GLOBAL_H

#include<iostream>
#include<assert.h>
#include"CVector.h"

/*
#include<complex>
using std::complex;
*/

using namespace std;

#define M_PI	3.14159265358979323846
#define MAX(a, b)  (((a) > (b)) ? (a) : (b))

// ------------------------------------------------------

class CSlab;
class CAtom;
class CReciprocalLattice;
class CLayer;
class CLight;

// ------------------------------------------------------
//enum Model{KINEMATIC = 0, DYNAMIC = 1};
enum MultipleScatteringAccuracy{KINEMATIC=1, NONE = 2, LAYER = 3, INTERATOMIC = 4};
enum NumericAccuracy{NUMFLOAT = 0, NUMDOUBLE = 1, NUMDOUBLEBLAS = 2, NUMLONGDOUBLE=3, NUMQUAD=4};
enum MatrixAccuracy{MDIAGONAL = 0, MFULL = 1, MBLAS = 2};

// ------------------------------------------------------

typedef double REAL;
const REAL EMACH = REAL(1e-10);

/*
typedef long double REAL;
const REAL EMACH = REAL(1e-12); 
*/

#ifdef QUADMATH
/*
typedef __float128 REAL;
const REAL EMACH = REAL(1e-20);
*/
#endif

// ------------------------------------------------------

// scaling behavior
// linear with the number of slabs
// quadratic with the number L
// cubic with the number of reciprocal lattice vectors
// log2 with the number of multilayers

#ifdef QUADMATH
extern "C"
{
	#include <quadmath.h>
}

inline __float128 cos(const __float128 x) {return cosq(x);}
inline __float128 acos(const __float128 x) {return acosq(x);}
inline __float128 sin(const __float128 x) {return sinq(x);}
inline __float128 asin(const __float128 x) {return asinq(x);}
inline __float128 exp(const __float128 x) {return expq(x);}
inline __float128 log(const __float128 x) {return logq(x);}
inline __float128 fabs(const __float128 x) {return fabsq(x);}
inline __float128 abs(const __float128 &x) {return fabsq(x);}
inline __float128 sqrt(const __float128 x) {return sqrtq(x);}
inline __float128 pow(const __float128 x, __float128 y) {return powq(x, y);}
inline __float128 atan2(const __float128 x, __float128 y) {return atan2q(x, y);}

inline std::ostream& operator<<(std::ostream& stream, const __float128& r)
{
	char buf[64];
//    int n = quadmath_snprintf(buf, sizeof buf, "%+-#*.20Qe", 26, r);
    int n = quadmath_snprintf(buf, sizeof buf, "%.20Qe", r);
	stream << buf;
	return stream;
}
#endif

// ------------------------------------------------------
#include"complex.h"
// ------------------------------------------------------

class CKCoord3D
{
	public:
		c::complex<REAL> x, y, z;
};

class CCoor3D
{
	public:
	REAL x, y, z;
	CCoor3D(REAL _x=0, REAL _y=0, REAL _z=0) : x(_x), y(_y), z(_z)
	{}
};

class CCoor2D
{
	public:
	REAL x, y;
	CCoor2D(REAL _x=0, REAL _y=0) : x(_x), y(_y)
	{}
};

/*
typedef union
{
    struct {
        unsigned char byte1;
        unsigned char byte2;
        unsigned char byte3;
        unsigned char byte4;
    } bytes;
    unsigned int dword;
} HW_Register;
*/

class CPolar
{
	public:
	c::complex<REAL> pol[2];
	//c::complex<<REAL> pi, sigma;
	CPolar(REAL _pi=0, REAL _sigma=0)
	{
		pol[0] = _pi;
		pol[1] = _sigma;
	}
};

// this class should contain all coefficients for the spherical expansion. But it is also used for DLMKG, which is some kind of inverse
class CExpansionCoefficients
{
	public:
	CExpansionCoefficients(int LMAX)
	{
		const int LMAX1D = LMAX+1;
		const int LM1SQD = LMAX1D*LMAX1D;
		Elm.assign(LM1SQD, CPolar());
		Hlm.assign(LM1SQD, CPolar());
	}
	CVector<CPolar> Elm, Hlm;
};

inline CKCoord3D ReciprocalWaveVector(const CCoor3D &AK, const CCoor2D &G, const c::complex<REAL> &KAPPA)
{
	CKCoord3D ret;
	ret.x = c::complex<REAL>(AK.x + G.x, 0.);
	ret.y = c::complex<REAL>(AK.y + G.y, 0.);
	ret.z = sqrt(KAPPA*KAPPA - ret.x*ret.x - ret.y*ret.y);
	if (AK.z < 0) ret.z = -ret.z;
	return ret;
}

template <class T>
void NewTable(T** &Q, int size1, int size2)
{
	if (size1 == 0) return;
	if (size2 == 0) return;
	Q = new T*[size1];
	//assert(Q != NULL);
	T* temp = new T[size1 * size2];
	//assert(T != NULL);
	for(int i=0; i<size1; i++)
		Q[i] = &temp[i*size2];
}

template <class T>
void DeleteTable(T** &Q, int size1, int size2)
{
	if (Q == NULL) return;
	delete[] Q[0];
	delete[] Q;
}

// ------------------------------------------------------

void PLW(const c::complex<REAL> &KAPPA, const CKCoord3D &GK, const int &LMAX, CExpansionCoefficients &A);

void DLMKG(
	const int LMAX,
	const REAL A0,
	const CKCoord3D &GK,
	const REAL &SIGNUS,
	const c::complex<REAL> &KAPPA,
	CExpansionCoefficients &D, bool volumecorrection);

void PCSLAB(
	const int LMAX,
	const CLayer &layer,
	const REAL field,
	const c::complex<REAL> &KAPPA,
	const CCoor3D &AK,
	const CReciprocalLattice &rl,
	const REAL *ELM,
	CSlab &slab, bool volumecorrection);


void PCSLABEASY(
	int LMAX,
	const CLayer &layer,
	const REAL field,
	const c::complex<REAL> &KAPPA,
	const CCoor3D &AK,
	const CReciprocalLattice &rl,
	const REAL *ELM,
	CSlab &slab, bool volumecorrection);

void HOSLAB(const int IGMAX, 
	const c::complex<REAL> KAPPA1,
	const c::complex<REAL> KAPPA2, 
	const c::complex<REAL> KAPPA3, 
	const CCoor3D &AK, 
	const CVector<CCoor2D> &G,
	const CCoor3D DL,
	const CCoor3D DR,
	const REAL D,
	const REAL sigmal, const REAL sigmar,
	CSlab &slab);


#endif


