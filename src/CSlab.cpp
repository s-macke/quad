#include"CSlab.h"
#include "matrix.h"

#ifdef USEBLAS
#include<cblas.h>
//#include "mkl.h"
#endif


void CSlab::Create(int _IGKMAX)
{
		IGKMAX = _IGKMAX;
		matrixaccuracy = MFULL;
		msaccuracy = LAYER;
		NewTable(QI,   IGKMAX, IGKMAX);
		NewTable(QII,  IGKMAX, IGKMAX);
		NewTable(QIII, IGKMAX, IGKMAX);
		NewTable(QIV,  IGKMAX, IGKMAX);
		
		QINV1 = NULL;
		QINV2 = NULL;
		W1 = NULL;
		W2 = NULL;
		W3 = NULL;
		W4 = NULL;
		ztop = 0;
		zbottom = 0;
}

void CSlab::Destroy()
{
	DeleteTable(QI,   IGKMAX, IGKMAX);
	DeleteTable(QII,  IGKMAX, IGKMAX);
	DeleteTable(QIII, IGKMAX, IGKMAX);
	DeleteTable(QIV,  IGKMAX, IGKMAX);
	
	// delete the pairing variables. They are chekced for NULL in DeleteTable		
	DeleteTable(QINV1,   IGKMAX, IGKMAX);
	DeleteTable(QINV2,   IGKMAX, IGKMAX);
	DeleteTable(W1,   IGKMAX, IGKMAX);
	DeleteTable(W2,   IGKMAX, IGKMAX);
	DeleteTable(W3,   IGKMAX, IGKMAX);
	DeleteTable(W4,   IGKMAX, IGKMAX);
}

CSlab::CSlab(int _IGKMAX)
{
	Create(_IGKMAX);
}

CSlab::~CSlab()
{
	Destroy();
}

void CSlab::Zero()
{
	const c::complex<REAL> CZERO(0., 0.), CONE(1., 0.);	
	for(int IG1=0; IG1<IGKMAX; IG1++) 
	for(int IG2=0; IG2<IGKMAX; IG2++) 
	{
		QI[IG1][IG2] = CZERO;
		QII[IG1][IG2] = CZERO;
		QIII[IG1][IG2] = CZERO;
		QIV[IG1][IG2] = CZERO;
	}
/*	
	memset(QI[0], 0, IGKMAX*IGKMAX*sizeof());
	memset(QIV[0], 0, IGKMAX*IGKMAX*sizeof());
	memset(QIII[0], 0, IGKMAX*IGKMAX*sizeof());
	memset(QII[0], 0, IGKMAX*IGKMAX*sizeof());
*/
}

void CSlab::Identity()
{
	const c::complex<REAL> CZERO(0., 0.), CONE(1., 0.);	
	Zero();
	for(int j=0; j<IGKMAX; j++)
	{
		QI[j][j] = CONE;
		QIV[j][j] = CONE;
	}
}

void MatrixMultiplyBLAS(c::complex<REAL> **R, c::complex<REAL> **A, c::complex<REAL> **B, int n)
{
	double alpha = 1.;
	double beta = 0.;
#ifdef USEBLAS
	cblas_zgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, n, n, n, &alpha, (double*)A[0], n, (double*)B[0], n, &beta, (double*)R[0], n);
#endif
}

// calculates R = A*B
void MatrixMultiply(c::complex<REAL> **R, c::complex<REAL> **A, c::complex<REAL> **B, int n)
{
#ifdef USEBLAS
	double alpha = 1.;
	double beta = 0.;
	cblas_zgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, n, n, n, &alpha, (double*)A[0], n, (double*)B[0], n, &beta, (double*)R[0], n);
#else
	c::complex<REAL> CZERO(0., 0.);
	c::complex<REAL> x;
	for(int IGK1=0; IGK1<n; IGK1++)
	for(int IGK2=0; IGK2<n; IGK2++)
	{
		x = CZERO;
		for(int IGK3=0; IGK3<n; IGK3++)
			x += A[IGK1][IGK3] * B[IGK3][IGK2];
			//x += A[IGK3][IGK1] * B[IGK2][IGK3];
		R[IGK1][IGK2] = x;
	}
#endif
}


void MatrixMultiplyDiagonal(c::complex<REAL> **R, c::complex<REAL> **A, c::complex<REAL> **B, int n)
{
	c::complex<REAL> CZERO(0., 0.);
/*
if (abs(A[0][0]) > abs(B[0][0]))
{
	for(int IGK1=0; IGK1<n; IGK1++)
	{
		x = A[IGK1][IGK1];
		for(int IGK2=0; IGK2<n; IGK2++)
			R[IGK1][IGK2] = x * B[IGK1][IGK2];
	}
} else
{
	for(int IGK1=0; IGK1<n; IGK1++)
	for(int IGK2=0; IGK2<n; IGK2++)
		R[IGK1][IGK2] = A[IGK1][IGK2] * B[IGK2][IGK2];
}
*/

	for(int IGK1=0; IGK1<n; IGK1++)
	for(int IGK2=0; IGK2<n; IGK2++)
	{
		if (IGK1 == IGK2)
			R[IGK1][IGK2] = A[IGK1][IGK1] * B[IGK1][IGK2]; 
		else 
			R[IGK1][IGK2] = A[IGK1][IGK1] * B[IGK1][IGK2] + A[IGK1][IGK2] * B[IGK2][IGK2];
	}

/*
static int num = 0;

	for(int IGK1=0; IGK1<n; IGK1++)
	{
	for(int IGK2=0; IGK2<n; IGK2++)
	{
		printf("%i %i %e %e %e\n", IGK1, IGK2 + num*(n+2), abs(A[IGK1][IGK2]), abs(B[IGK1][IGK2]), abs(R[IGK1][IGK2]));
	}
	printf("\n");
	}
	printf ("\n");
	num++;
if (num >= 5) exit(1);
*/


//fprintf(stderr, "Diagonal\n");
/*
	for(int IGK1=0; IGK1<n; IGK1++)
	for(int IGK2=0; IGK2<n; IGK2++)
	{
		R[IGK1][IGK2] = A[IGK1][IGK2] * B[IGK1][IGK2];
	}
*/
/*
	for(int IGK1=0; IGK1<n; IGK1++)
		R[IGK1][IGK1] = A[IGK1][IGK1] * B[IGK1][IGK1];	
*/
}

	void CSlab::MultiplyPair(int multiplicator, const CVector<CKCoord3D> &GKK)
	{
		//printf("multiplicator: %i\n", multiplicator);
		CSlab slab(IGKMAX);
		slab = *this;
		bool first = true;
		while(multiplicator>0)
		{
			if (multiplicator&1)
			{
				if (first) { *this = slab; first = false; } else this->PAIR(slab, GKK);
				multiplicator -= 1;
			}
			slab.PAIR(slab, GKK);
			multiplicator >>= 1;
		}
	}

/*
C     ------------------------------------------------------------------  
C     THIS SUBROUTINE CALCULATES SCATTERING Q-MATRICES FOR A  REAL  
C     LAYER, FROM THE CORRESPONDING MATRICES OF THE INDIVIDUAL, LEFT  
C     (L) AND RIGHT (R), LAYERS. THE RESULTS ARE STORED IN Q*L.  
C     -----------------------------------------------------------------  
*/
	void CSlab::PAIREasy(const CSlab &slabB)
	{
		// QI: incoming top and transmitted
		// QIII: incoming top and reflected	  DT coordinate
		// QII: incoming bottom and reflected DB coordinate
		// QIV: incoming bottom and transmitted

		// calculate
		// QFI = QRI*QLI
		// QFIV = QLIV*QRIV
		// QFIII = QLIII + QLIV * QRIII * QLI
		// QFII  = QRII  + QRI * QLII * QRIV

	if (W1 == NULL)
	{
		NewTable(QINV1, IGKMAX, IGKMAX);
		NewTable(QINV2, IGKMAX, IGKMAX);
		NewTable(W1, IGKMAX, IGKMAX);
		NewTable(W2, IGKMAX, IGKMAX);
		NewTable(W3, IGKMAX, IGKMAX);
		NewTable(W4, IGKMAX, IGKMAX);
	}

/*
	MatrixMultiplyBLAS(QINV1, slabB.QI, QI, IGKMAX);
	MatrixMultiplyBLAS(QINV2, QIV, slabB.QIV, IGKMAX);

	MatrixMultiplyBLAS(W1, QIV, slabB.QIII, IGKMAX);
	MatrixMultiplyBLAS(W2, W1, QI, IGKMAX);

	MatrixMultiplyBLAS(W3, slabB.QI, QII, IGKMAX);
	MatrixMultiplyBLAS(W4, W3, slabB.QIV, IGKMAX);
*/

if (matrixaccuracy == MDIAGONAL)
{
	MatrixMultiplyDiagonal(QINV1, slabB.QI, QI, IGKMAX);
	MatrixMultiplyDiagonal(QINV2, QIV, slabB.QIV, IGKMAX);

	MatrixMultiplyDiagonal(W1, QIV, slabB.QIII, IGKMAX);
	MatrixMultiplyDiagonal(W2, W1, QI, IGKMAX);

	MatrixMultiplyDiagonal(W3, slabB.QI, QII, IGKMAX);
	MatrixMultiplyDiagonal(W4, W3, slabB.QIV, IGKMAX);
/*
	for(int IGK1=0; IGK1<IGKMAX; IGK1++)
	{
		QIII[IGK1][IGK1] = QIII[IGK1][IGK1] + W2[IGK1][IGK1];
		QII[IGK1][IGK1] = slabB.QII[IGK1][IGK1] + W4[IGK1][IGK1];
		QI[IGK1][IGK1] = QINV1[IGK1][IGK1];
		QIV[IGK1][IGK1] = QINV2[IGK1][IGK1];
	}
*/
} else
{
	MatrixMultiply(QINV1, slabB.QI, QI, IGKMAX);
	MatrixMultiply(QINV2, QIV, slabB.QIV, IGKMAX);

	MatrixMultiply(W1, QIV, slabB.QIII, IGKMAX);
	MatrixMultiply(W2, W1, QI, IGKMAX);

	MatrixMultiply(W3, slabB.QI, QII, IGKMAX);
	MatrixMultiply(W4, W3, slabB.QIV, IGKMAX);
}
	for(int IGK1=0; IGK1<IGKMAX; IGK1++)
	for(int IGK2=0; IGK2<IGKMAX; IGK2++)
	{
		QIII[IGK1][IGK2] = QIII[IGK1][IGK2] + W2[IGK1][IGK2];
		QII[IGK1][IGK2] = slabB.QII[IGK1][IGK2] + W4[IGK1][IGK2];
		QI[IGK1][IGK2] = QINV1[IGK1][IGK2];
		QIV[IGK1][IGK2] = QINV2[IGK1][IGK2];
	}
}

	void CSlab::PAIR(const CSlab &slabB, const CVector<CKCoord3D> &GKK)
	{
		if (msaccuracy == KINEMATIC)
		{
			CSlab R(IGKMAX);
			R = slabB;

			CCoor3D tl;
			CCoor3D bl;

			CCoor3D tr;
			CCoor3D br;

			tl.z = 0.;
			bl.z = R.ztop + R.zbottom;
			tr.z = ztop + zbottom;
			br.z = zbottom + R.ztop;

/*
			tl.z = -ztop + ztop;
			bl.z = -zbottom + zbottom + R.ztop + R.zbottom;
			tr.z = -R.ztop + ztop + zbottom + R.ztop;
			br.z = -R.zbottom + R.zbottom;
*/

/*
			tl.z = R.ztop + R.zbottom;
			bl.z = R.zbottom + ztop;
			tr.z = 0.;
			br.z = zbottom + ztop;
*/

			AddPropagator(tl, bl, GKK);
			R.AddPropagator(tr, br, GKK);
			for(int IGK1=0; IGK1<IGKMAX; IGK1++)
			for(int IGK2=0; IGK2<IGKMAX; IGK2++)
			{
				QI[IGK1][IGK2] += R.QI[IGK1][IGK2];
				QII[IGK1][IGK2] += R.QII[IGK1][IGK2];
				QIII[IGK1][IGK2] += R.QIII[IGK1][IGK2];
				QIV[IGK1][IGK2] += R.QIV[IGK1][IGK2];
			}
			return;
		} else
		if (msaccuracy < LAYER)
		{
			PAIREasy(slabB);
			return;
		}
	c::complex<REAL> CZERO(0., 0.);
	c::complex<REAL> CONE(1., 0.);
	int INT[IGKMAX], JNT[IGKMAX];

	if (IGKMAX != slabB.IGKMAX)
	{
		fprintf(stderr, "Error in PAIR: Dimensions do not match.");
		exit(1);
	}

	if (W1 == NULL)
	{
		NewTable(QINV1, IGKMAX, IGKMAX);
		NewTable(QINV2, IGKMAX, IGKMAX);
		NewTable(W1, IGKMAX, IGKMAX);
		NewTable(W2, IGKMAX, IGKMAX);
		NewTable(W3, IGKMAX, IGKMAX);
		NewTable(W4, IGKMAX, IGKMAX);
	}
	
	for(int IGK1=0; IGK1<IGKMAX; IGK1++)
	for(int IGK2=0; IGK2<IGKMAX; IGK2++)
	{
		QINV1[IGK1][IGK2] = QI [IGK1][IGK2];
		QINV2[IGK1][IGK2] = slabB.QIV[IGK1][IGK2];
		W2[IGK1][IGK2] = CZERO;
		W3[IGK1][IGK2] = CZERO;
	}
	
	for(int IGK1=0; IGK1<IGKMAX; IGK1++)
	{
		W2[IGK1][IGK1] = CONE;
		W3[IGK1][IGK1] = CONE;
		for(int IGK2=0; IGK2<IGKMAX; IGK2++)
		for(int IGK3=0; IGK3<IGKMAX; IGK3++)
		{
			W2[IGK1][IGK2] = W2[IGK1][IGK2] - QII [IGK1][IGK3] * slabB.QIII[IGK3][IGK2];
			W3[IGK1][IGK2] = W3[IGK1][IGK2] - slabB.QIII[IGK1][IGK3] * QII [IGK3][IGK2];
		}
	}
  // W2 = (1. - QIIL * QIIIR)
  // W3 = (1. - QIIIR * QIIL)

	ZGE(W2, INT, IGKMAX);
	ZGE(W3, JNT, IGKMAX);

	//c::complex<REAL> x1[IGKMAX];
	//c::complex<REAL> x2[IGKMAX];
	CVector< c::complex<REAL> > x1;
        CVector< c::complex<REAL> > x2;
	x1.Reserve(IGKMAX);
	x2.Reserve(IGKMAX);


	for(int IGK2=0; IGK2<IGKMAX; IGK2++)
	{
		for(int i=0; i<IGKMAX; i++) x1[i] = QINV1[i][IGK2];
		for(int i=0; i<IGKMAX; i++) x2[i] = QINV2[i][IGK2];
		ZSU(W2, INT, &x1[0], IGKMAX);
		ZSU(W3, JNT, &x2[0], IGKMAX);
		for(int i=0; i<IGKMAX; i++) QINV1[i][IGK2] = x1[i];
		for(int i=0; i<IGKMAX; i++) QINV2[i][IGK2] = x2[i];
	}
	// QINV1 = W2^-1 * QIL
	// QINV2 = W3^-1 * QIVR

/*
	for(int i=0; i<IGKMAX; i++)
	for(int j=0; j<IGKMAX; j++)
	{
		c::complex<REAL> sum = 0;
		for(int k=0; k<IGKMAX; k++)
		{
			sum += W2[i][k]*QINV1[k][j];
		}		
		cout << i <<" " << j << " " << sum.real() << " " << sum.imag() << "\n";
	}
*/
//	printf("ZSU end\n");


	for(int IGK1=0; IGK1<IGKMAX; IGK1++)  
	for(int IGK2=0; IGK2<IGKMAX; IGK2++)
	{
		W1[IGK1][IGK2] = CZERO;
		W2[IGK1][IGK2] = CZERO;
		W3[IGK1][IGK2] = CZERO;
		W4[IGK1][IGK2] = CZERO;
		
		for(int IGK3=0; IGK3<IGKMAX; IGK3++)
		{	
			W1[IGK1][IGK2] += slabB.QI  [IGK1][IGK3] * QINV1[IGK3][IGK2];
			W2[IGK1][IGK2] +=       QII [IGK1][IGK3] * QINV2[IGK3][IGK2];
			W3[IGK1][IGK2] += slabB.QIII[IGK1][IGK3] * QINV1[IGK3][IGK2];
			W4[IGK1][IGK2] +=       QIV [IGK1][IGK3] * QINV2[IGK3][IGK2];
		}
	}
	
	// W1 = QIR   * QINV1
	// W2 = QIIL  * QINV2
	// W3 = QIIIR * QINV1
	// W4 = QIVL  * QINV2

	for(int IGK1=0; IGK1<IGKMAX; IGK1++)  
	for(int IGK2=0; IGK2<IGKMAX; IGK2++)
	{
		QINV1[IGK1][IGK2] = slabB.QII [IGK1][IGK2];
		QINV2[IGK1][IGK2] = QIII[IGK1][IGK2];
	
		for(int IGK3=0; IGK3<IGKMAX; IGK3++)
		{
			QINV1[IGK1][IGK2] += slabB.QI [IGK1][IGK3] * W2[IGK3][IGK2];
			QINV2[IGK1][IGK2] += QIV[IGK1][IGK3] * W3[IGK3][IGK2];
		}
	}

// QINV1 = QIIR + QIR*W2
// QINV2 = QIIIL + QIVL*W3

	for(int IGK1=0; IGK1<IGKMAX; IGK1++)  
	for(int IGK2=0; IGK2<IGKMAX; IGK2++)
	{
		QI  [IGK1][IGK2] = W1   [IGK1][IGK2];
		QII [IGK1][IGK2] = QINV1[IGK1][IGK2];
		QIII[IGK1][IGK2] = QINV2[IGK1][IGK2];
		QIV [IGK1][IGK2] = W4   [IGK1][IGK2];
	}

//finally:
	// QINV1 = QIIR + QIR*W2
	// QINV2 = QIIIL + QIVL*W3
	
	// QIL   = QIR * QINV1 
	// QIIL  = QIIR  + QIR*QIIL*QINV2
	// QIIIL = QIIIL + QIVL*QIIIR * QINV1
	// QIVL  = QIVL * QINV2
/*	
	DeleteTable(QINV1, IGKMAX, IGKMAX);
	DeleteTable(QINV2, IGKMAX, IGKMAX);
	DeleteTable(W1, IGKMAX, IGKMAX);
	DeleteTable(W2, IGKMAX, IGKMAX);
	DeleteTable(W3, IGKMAX, IGKMAX);
	DeleteTable(W4, IGKMAX, IGKMAX);	
*/
}

void CSlab::AddPropagator(CCoor3D dt, CCoor3D db, const CVector<CKCoord3D> &GKK)
{
	const c::complex<REAL> CZERO(0., 0.), CONE(1., 0.), CI(0., 1.);

	zbottom += db.z;
	ztop += dt.z;

	c::complex<REAL> CQI, CQII, CQIII, CQIV;
	int IGMAX = IGKMAX/2;

        for(int IG2=0; IG2<IGMAX; IG2++)
        for(int IG1=0; IG1<IGMAX; IG1++)
        {
                // this is what we need
                // CQI  = ++
                // CQII = -+
                // CQIII = +-
                // CQIV = --

                // this is what we do at the moment if dl=dr
                // CQI = ++
                // CQII = ++
                // CQIII = ++
                // CQIV = ++

                // short version
				// GKK.z is always given as negative value
				// all the same, but this is Ok
				//IGK2 = incoming
				/*
                CQI  = exp(CI*( -GKK[IG1].z*dr.z - GKK[IG2].z*dl.z)); // transmitted
                CQII = exp(CI*( -GKK[IG1].z*dr.z - GKK[IG2].z*dr.z)); // reflected
                CQIII= exp(CI*( -GKK[IG1].z*dl.z - GKK[IG2].z*dl.z)); // reflected
                CQIV = exp(CI*( -GKK[IG1].z*dl.z - GKK[IG2].z*dr.z)); // transmitted
				*/
				
				//printf("%f\n", GKK[IG1].z.real());
                CQI  = exp(CI*( +GKK[IG1].z * (-db.z) +GKK[IG2].z * (-dt.z))); // transmitted from top
                CQII = exp(CI*( +GKK[IG1].z * (-db.z) -GKK[IG2].z * (+db.z))); // reflected
                CQIII= exp(CI*( -GKK[IG1].z * (+dt.z) +GKK[IG2].z * (-dt.z))); // reflected from top
                CQIV = exp(CI*( -GKK[IG1].z * (+dt.z) -GKK[IG2].z * (+db.z))); // transmitted				
				
                // maybe corrected version
                /*
                CQI  = exp( CI*( GKK[IG1].z*DR.z + GKK[IG2].z * DL.z)); // inco$
                CQII = exp( CI*(-GKK[IG1].z*DR.z + GKK[IG2].z * DR.z));
                CQIII= exp( CI*( GKK[IG1].z*DL.z - GKK[IG2].z * DL.z)); // inco$
                CQIV = exp( CI*(-GKK[IG1].z*DL.z - GKK[IG2].z * DR.z));
				*/

/*
                // transmitted top
                CQI  = exp(CI*(
                                 GKK[IG1].x*layer.DR.x + GKK[IG1].y*layer.DR.y $
                                 GKK[IG2].x*layer.DL.x + GKK[IG2].y*layer.DL.y $
                // reflected bottom
                CQII = exp(CI*(
                                (GKK[IG1].x - GKK[IG2].x)*layer.DR.x +
                                (GKK[IG1].y - GKK[IG2].y)*layer.DR.y +
                                (GKK[IG1].z + GKK[IG2].z)*layer.DR.z));
                // reflected top
                CQIII= exp(-CI*(
                                (GKK[IG1].x - GKK[IG2].x)*layer.DL.x +
                                (GKK[IG1].y - GKK[IG2].y)*layer.DL.y -
                                (GKK[IG1].z + GKK[IG2].z)*layer.DL.z));
                //transmitted bottom
                CQIV = exp(-CI*(
                                 GKK[IG1].x*layer.DL.x + GKK[IG1].y*layer.DL.y $
                                 GKK[IG2].x*layer.DR.x + GKK[IG2].y*layer.DR.y $
*/
                int IGK1 = IG1<<1;
                int IGK2 = IG2<<1;

                QI  [IGK1+0][IGK2+0] *= CQI;
                QI  [IGK1+0][IGK2+1] *= CQI;
                QI  [IGK1+1][IGK2+0] *= CQI;
                QI  [IGK1+1][IGK2+1] *= CQI;

                QII [IGK1+0][IGK2+0] *= CQII;
                QII [IGK1+0][IGK2+1] *= CQII;
                QII [IGK1+1][IGK2+0] *= CQII;
                QII [IGK1+1][IGK2+1] *= CQII;

                QIII[IGK1+0][IGK2+0] *= CQIII;
                QIII[IGK1+0][IGK2+1] *= CQIII;
                QIII[IGK1+1][IGK2+0] *= CQIII;
                QIII[IGK1+1][IGK2+1] *= CQIII;

                QIV [IGK1+0][IGK2+0] *= CQIV;
                QIV [IGK1+0][IGK2+1] *= CQIV;
                QIV [IGK1+1][IGK2+0] *= CQIV;
                QIV [IGK1+1][IGK2+1] *= CQIV;
	}

}
