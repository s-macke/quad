#include<stdlib.h>
#include<stdio.h>
#include<iostream>

#include "global.h"


int zwofz (REAL *z, REAL *w)
{
  /* Local variables */
  const REAL rsqpd2 = 1.128379167095512573896158903121545171688;
  const REAL mxgoni = 59473682.;
  const REAL inogxm = 1.6814159916986475e-8;
  const REAL lgovfl  = 708.;
  int kapn, flag, itmp;
  REAL xabs, yabs, qrho, xsum, ysum, dtmp;
  int fadeeva;
  REAL c, h;
  int i, j, n;
  REAL u, v, x, y, xquad, yquad, h2, u1, u2, v1, v2, w1, qlamda;
  int nu;
  REAL rx, ry, sx, sy, tx, ty, uv;

  /* >>1992-10-13 ZWOFZ WVS Improve efficiency and avoid underflow. */
  /* >>1992-03-13 ZWOFZ FTK Removed implicit statements. */
  /* >>1991-08-23 ZWOFZ WV Snyder Initial adaptation to Math77 */
  /* >> HMP modified for IEEE parameters and no error print */
  /* >>> note that flag = 0 for yi > 0 */

  /*     Algorithm 680, collected algorithms from ACM. */
  /*     This work published in Transactions on Mathematical Software, */
  /*     Vol. 16, No. 1, Pp. 47. */

  /*     Modified by W. V. Snyder for inclusion in Math77: */
  /*     Reorganize checking for overflow and loss of precision so */
  /*     there are no redundant or unnecessary checks.  In the process, */
  /*     the range of applicability is expanded to the entire upper */
  /*     half-plane. */
  /*     Reorganize some calculations to be immune from overflow. */
  /*     Split loop for two outer regions into two loops -- faster in */
  /*     region. */
  /*     Use D1MACH to fetch machine characteristics. (NOT) */
  /*     Use Math77 error message processor. */

  /*  Given a complex number z = (xi,yi), this subroutine computes */
  /*  the value of the Faddeeva function w(z) = exp(-z**2)*erfc(-i*z), */
  /*  where erfc is the complex complementary error function and i */
  /*  means sqrt(-1). */
  /*  The accuracy of the algorithm for z in the 1st and 2nd quadrant */
  /*  is 14 significant digits; in the 3rd and 4th it is 13 significant */
  /*  digits outside a circular region with radius 0.126 around a zero */
  /*  of the function. */


  /*  Argument list */
  /*     Z [in]    = real and imaginary parts of z in Z(1) and Z(2) */
  /*     W [out]   = real and imaginary parts of w(z) in W(1) and W(2) */
  /*     FLAG [out] = an error flag indicating the status of the */
  /*       computation.  Type INTEGER, with values having the following */
  /*       meaning: */
  /*         0 : No error condition, */
  /*        -1 : Overflow would occur, */
  /*        +1 : There would be no significant digits in the answer. */


  /*  The routine is not underflow-protected but any variable can be */
  /*  put to zero upon underflow. */

  /*  Reference - GMP Poppe, CMJ Wijers: More efficient computation of */
  /*  the complex error-function, ACM Trans. Math. Software. */



  /*     RSQPD2 = 2/sqrt(pi) */
  /*     SQLGOV = sqrt(ln(RMAX)), where RMAX = the overflow limit for */
  /*              floating point arithmetic. */
  /*     LGOVFL  = ln(RMAX) - ln(2) */
  /*     LGUND  = ln(RMIN), where RMIN = underflow limit. */
  /*     MXGONI = the largest possible argument of sin or cos, restricted */
  /*              here to sqrt ( pi / (2*round-off-limit) ). */
  /*     INOGXM = 1 / MXGONI. */
  /*  The reason these values are needed as defined */
  /*  will be explained by comments in the code. */

  xabs = fabs(z[0]);
  yabs = fabs(z[1]);
  x = xabs / 6.3;
  y = yabs / 4.4;
  
  if (x > y) {
    dtmp = y / x;
    qrho = x * sqrt(dtmp * dtmp + 1.);
  } else if (y == 0.) {
    qrho = 0.;
  } else {
    dtmp = x / y;
    qrho = y * sqrt(dtmp * dtmp + 1.);
  }

  fadeeva = (qrho < 0.292);
  if (fadeeva) {

    /*  qrho .lt. 0.292, equivalently qrho**2 .lt. 0.085264: the Fadeeva */
    /*  function is evaluated using a power-series (Abramowitz and */
    /*  Stegun, equation (7.1.5), p.297). */
    /*  N is the minimum number of terms needed to obtain the required */
    /*  accuracy. */
    /*  We know xquad and exp(-xquad) and yqyad and sin(yquad) won't */
    /*  cause any trouble here, because qrho .lt. 1. */
    xquad = (xabs - yabs) * (xabs + yabs);
    yquad = 2. * xabs * yabs;
    n = (int) ((1. - 0.85 * y) * 72. * qrho + 6.5);
    j = (n << 1) + 1;
    xsum = rsqpd2 / j;
    ysum = 0.;
    for (i = n; i > 0; --i) {
      j -= 2;
      w1 = (xsum * xquad - ysum * yquad) / i;
      ysum = (xsum * yquad + ysum * xquad) / i;
      xsum = w1 + rsqpd2 / j;
    }
    u1 = 1. - (xsum * yabs + ysum * xabs);
    v1 = xsum * xabs - ysum * yabs;
    w1 = exp(-xquad);
    u2 = w1 * cos(yquad);
    v2 = -w1 * sin(yquad);
    u = u1 * u2 - v1 * v2;
    v = u1 * v2 + v1 * u2;
  } else {
    rx = 0.;
    ry = 0.;
    sx = 0.;
    sy = 0.;

    /*  The loops in both branches of the IF block below are similar. */
    /*  They could be combined to reduce space, but extra tests and */
    /*  unnecessary computation would be needed. */

    if (qrho < 1.) {
      /*  0.292 < qrho < 1.0: w(z) is evaluated by a truncated */
      /*  Taylor expansion, where the Laplace continued fraction */
      /*  is used to calculate the derivatives of w(z). */
      /*  KAPN is the minimum number of terms in the Taylor expansion */
      /*       needed to obtain the required accuracy. */
      /*  NU is the minimum number of terms of the continued fraction */
      /*     needed to calculate the derivatives with the required */
      /*     accuracy. */
      /*  x*x + y*y is more accurate than qrho*qrho here: */
      c = (1. - y) * sqrt(1. - x * x - y * y);
      h = 1.88 * c;  h2 = 2. * h;
      nu = (int) (26. * c + 17.5);
      kapn = (int) (34. * c + 8.5);
      /*  Select kapn so qlamda doesn't underflow.  Small kapn is good */
      /*         (when possible) for performance also. */
      if (h2 < 0.25) {
        itmp = (int) (-lgovfl / log(h2)) + 1;
        if (itmp < kapn) kapn = itmp;
      }
      qlamda = pow(h2, (REAL) (kapn - 1));
      /*  0 < qlamda < 3.76**41 < 3.85d23. */
      for (n = nu; n > kapn; --n) {
        tx = yabs + h + n * rx;
        ty = xabs - n * ry;
        /* No overflow because tx*rx + ty*ry = 1 and 0.292 < qrho < 1: */
        c = 0.5 / (tx * tx + ty * ty);
        rx = tx * c;
        ry = ty * c;
      }
      for (n = kapn; n > 0; --n) {
        tx = yabs + h + n * rx;
        ty = xabs - n * ry;
        /*  No overflow because tx*rx + ty*ry = 1 and 0.292 < qrho < 1: */
        c = 0.5 / (tx * tx + ty * ty);
        rx = tx * c;
        ry = ty * c;
        tx = qlamda + sx;
        sx = rx * tx - ry * sy;
        sy = ry * tx + rx * sy;
        qlamda /= h2;
      }
      u = sx * rsqpd2;
      v = sy * rsqpd2;
    } else {
      /*   qrho >= 1.: w(z) is evaluated using the Laplace continued */
      /*   fraction. */
      /*   NU is the minimum number of terms needed to obtain the */
      /*       required accuracy. */
      nu = (int) (1442. / (26. * qrho + 77.) + 4.5);
      for (n = nu; n > 0; --n) {
        tx = yabs + n * rx;
        ty = xabs - n * ry;
        if (tx > fabs(ty)) break;
        /*  rx = 0.5*tx/(tx**2+ty**2) and ry = 0.5*ty/(tx**2+ty**2), */
        /*  computed without overflow.  Underflow is OK. */
        c = tx / ty;
        ry = 0.5 / (ty * (c * c + 1.));
        rx = ry * c;
      }
      while (n > 0){
        /*  Once tx>abs(ty), it stays that way. */
        /*  rx = 0.5*tx/(tx**2+ty**2) and ry = 0.5*ty/(tx**2+ty**2), */
        /*  computed without overflow.  Underflow is OK. */
        c = ty / tx;
        rx = 0.5 / (tx * (c * c + 1.));
        ry = rx * c;
        if (--n == 0) break;
        tx = yabs + n * rx;
        ty = xabs - n * ry;
      }
      u = rx * rsqpd2;
      v = ry * rsqpd2;
    }
    if (yabs == 0.) {
      dtmp = xabs * xabs;
      if (dtmp > lgovfl) {
        u = 0.;
      } else {
        u = exp(-dtmp);
      }
    }
  }

  /*     Evaluation of w(z) in the other quadrants. */
  flag = 0;
  if (z[1] < 0.) { /* y is negative */
    if (fadeeva) {
      u = 2. * u2 - u;
      v = 2. * v2 - v;
    } else {
      /*  Check whether sin(2*xabs*yabs) has any precision, without */
      /*  allowing 2*xabs*yabs to overflow. */
      if (yabs > xabs) {
        if (yabs > inogxm) {
          /*  The following protects 2*exp(-z**2) against overflow. */
          if (lgovfl / yabs < yabs - xabs * (xabs / yabs)) 
            flag = -1;
        }else{
          w1 = 2. * exp((yabs - xabs) * (yabs + xabs));
          uv = fabs(u); dtmp = fabs(v);
          if (dtmp < uv) uv = dtmp; /* uv = min (abs(u), abs(v)) */
          dtmp = xabs * yabs;
          if (w1 < uv) dtmp *= (w1 / uv);
          if (dtmp > mxgoni) flag = 1;
        }
      } else if (xabs > inogxm) {
        if (lgovfl / xabs < xabs - yabs * (yabs / xabs)) {
          /*   (yabs-xabs)*(yabs+xabs) might have overflowed, but in that */
          /*   case, exp((yabs-xabs)*(yabs+xabs)) would underflow. */
          u = -u; v = -v; flag = 2;
        }else{
          /* (yabs-xabs)*(yabs+xabs) can't overflow here. */
          w1 = 2. * exp((yabs - xabs) * (yabs + xabs));
          uv = fabs(u); dtmp = fabs(v);
        }
		if (abs(u) < abs(v)){ uv = abs(u);} else {uv = abs(v);}
        if (dtmp < uv) uv = dtmp; /* uv = min (abs(u), abs(v)) */
        dtmp = xabs * yabs;
        if (w1 < uv) dtmp *= (w1 / uv);
        if (dtmp > mxgoni) flag = 1;
      }
    }
    if (flag == 0){
      yquad = 2. * xabs * yabs;
      u =  w1 * cos(yquad) - u;
      v = -w1 * sin(yquad) - v;
    }
    if (flag == 2) flag = 0;
  }
  if (flag) {
    w[0] = exp(lgovfl);
    w[1] = w[0];
  }else{
    w[0] = u;
    if (z[0] < 0.)  v = -v; 
    w[1] = v;
  }
  return flag;
} /* zwofz */


/*
C     ------------------------------------------------------------------  
C     CERF,GIVEN COMPLEX ARGUMENT Z,PROVIDES THE COMPLEX ERROR FUNCTION: 
C     W(Z)=EXP(-Z**2)*(1.0-ERF(-I*Z))  
C     THE  EVALUATION  ALWAYS  TAKES   PLACE  IN  THE  FIRST   QUADRANT.  
C     ONE  OF  THREE METHODS  IS  EXPLOYED  DEPENDING ON THE SIZE OF THE 
C     ARGUMENT (A POWER SERIES,A RECURRENCE BASED ON CONTINUED FRACTIONS  
C     THEORY, OR AN ASYMPTOTIC SERIES). EMACH IS THE MACHINE ACCURACY  
C     ------------------------------------------------------------------  
*/
//Faddeeva function

#include"math/Faddeeva.h"
c::complex<REAL> CERF2(const c::complex<REAL> &Z, const REAL &EMACH)
{
	std::complex<REAL> z(Z.real(), Z.imag());
	std::complex<REAL> res = Faddeeva::w(z, EMACH);
	return c::complex<REAL>(res.real(), res.imag());
}


c::complex<REAL> CERF(const c::complex<REAL> &Z, const REAL &EMACH)
{
	int NN, N;
	REAL ABSZ, ABTERM, API, EPS, FACT, FACTD, FACTN;
	REAL Q, RTPI, TEST, X, Y, YY;
    c::complex<REAL> ZZ, SUM, ZZS, XZZS, CER, CERF;
	c::complex<REAL> H1, H2, H3, U1, U2, U3, TERM1, TERM2;

	const c::complex<REAL> CONE(1., 0.);
	const c::complex<REAL> CI(0., 1.);
	const c::complex<REAL> CZERO(0., 0.);

//     ------------------------------------------------------------------  
//
REAL z[2], w[2];
z[0] = Z.real();
z[1] = Z.imag();

int ret = zwofz (z, w);
if (ret != 0)
{
	fprintf(stderr, "Error in CERF: return code %i\n", ret);
	exit(1);
}
return c::complex<REAL>(w[0], w[1]);

	EPS = 5.0 * EMACH;
	API = 1.0 / M_PI;
	if (abs(Z) == 0)
	{
		CERF = CONE;
		return CERF;
	}
	
//     THE ARGUMENT IS TRANSLATED TO THE FIRST QUADRANT FROM  
//     THE NN_TH QUADRANT, BEFORE THE METHOD FOR THE FUNCTION  
//     EVALUATION IS CHOSEN  
 
	X = Z.real();
	Y = Z.imag();
	
	YY = Y;
	if (Y >= 0)
	{
		if (X >= 0)
		{
			ZZ = Z;
			NN = 1;
			goto pos9;
		} else
		{
			ZZ = c::complex<REAL>(-X, Y);
			NN = 2;
			goto pos9;
		}
	}
	YY = -Y;
	
	if (X < 0)
	{
		ZZ = -Z;
		NN = 3;
		goto pos9;
	}
	
	ZZ = c::complex<REAL>(X, -Y);
	NN = 4;
	  
pos9:

	ZZS = ZZ*ZZ;
	XZZS = exp(-ZZS);
	ABSZ = abs(ZZ);
	
	if ((ABSZ-10.) > 0) goto pos23;
	
	if (YY-1. < 0)
	{
		if (ABSZ-4. < 0) goto pos13; else goto pos18;
	} else
	{
		if (ABSZ-1. < 0) goto pos13; else goto pos18;
	}
	
//     POWER SERIES(SEE ABRAMOWITZ AND STEGUN HANDBOOK OF
//     MATHEMATICAL FUNCTIONS, P297)

pos13:  
	Q = 1.;
	FACTN = -1.;
	FACTD = 1.;
	TERM1 = ZZ;
	SUM = ZZ;
	
pos14:
	for(N=1; N<=5; N++)
	{
		FACTN = FACTN + 2.;
		FACTD = FACTD + 2.;
		FACT = FACTN / (Q*FACTD);
		TERM1 = FACT * ZZS * TERM1;
		SUM = SUM + TERM1;
		Q = Q + 1.;
	}
	
	ABTERM = abs(TERM1);
	if ((ABTERM-EPS) >= 0)
	if ((Q - 100.) < 0) goto pos14;
  
	FACT = 2. * sqrt(API);
	SUM = FACT * CI * SUM;
	CER = XZZS + XZZS * SUM;
	goto pos24;

//     CONTINUED FRACTION THEORY(W(Z) IS RELATED TO THE LIMITING
//     VALUE OF U(N,Z)/H(N,Z), WHERE U AND H OBEY THE SAME
//     RECURRENCE RELATION IN N. SEE FADDEEVA AND TERENTIEV
//     (TABLES OF VALUES OF W(Z) FOR COMPLEX ARGUMENTS,PERGAMON
//       N.Y. 1961)
 
pos18:
	TERM2 = c::complex<REAL>(1e6, 0.);
	Q = 1.;
	H1 = CONE;
	H2 = REAL(2.) * ZZ;
	U1 = CZERO;
	RTPI = 2. * sqrt(M_PI);
	U2 = c::complex<REAL>(RTPI, 0.);

pos19:
	TERM1 = TERM2;
	
	for(N=1; N<=5; N++)
	{
		H3 = H2*ZZ - Q*H1;
		U3 = U2*ZZ - Q*U1;
		H1 = H2;
		H2 = REAL(2.)*H3;
		U1 = U2;
		U2 = REAL(2.)*U3;
		Q = Q + 1.;
	}
	
	TERM2 = U3 / H3;
	TEST = abs((TERM2-TERM1)/TERM1);
	if (TEST - EPS >= 0)
	{
	if ((Q - 60.) <= 0) goto pos19; else goto pos13;
	}
	CER = API * CI * TERM2;
	goto pos24;

//     ASYMPTOTIC SERIES: SEE ABRAMOWITZ AND STEGUN, P328  

pos23:  
	CER = REAL(0.5124242) / (ZZS - REAL(0.2752551)) + REAL(0.05176536) / (ZZS - REAL(2.724745));
	CER = CI * ZZ * CER;
	  
//     SYMMETRY RELATIONS ARE NOW USED TO TRANSFORM THE FUNCTION  
//     BACK TO QUADRANT NN  

pos24:
	switch(NN)
	{
		case 1:
			CERF = CER;
			break;

		case 2:
			CERF = conj(CER);
			break;
	
		case 3:
			CERF = REAL(2.) * XZZS - CER;
			break;
	
		case 4:
			CER = REAL(2.) * XZZS - CER;
			CERF = conj(CER);
			break;
	}
	return CERF;

}

/*
C     ------------------------------------------------------------------  
C     THIS SUBROUTINE COMPUTES THE COEFFICIENTS IN THE CHEBYCHEV  
C     EXPANTION OF 0F1(;C;Z). P.80 IN ALGORITHMS FOR THE COMPUTATION  
C     OF MATHEMATICAL FUNCTIONS ,Y.L.LUKE, ACADEMIC PRESS, LONDON 1977  
C       
C     ON INPUT--->  
C     CP   PARAMETER C IN 0F1(;C;Z)  
C     W    THIS IS A PRESELECTED SCALE FACTOR SUCH THAT 0.LE.(Z/W).LE.1  
C     N    TWO LESS THAN THE NUMBER OF COEFFICIENTS TO BE GENERATED  
C 
C     ON OUTPUT--->  
C     C    A VECTOR CONTAINING THE N+2 CHEBYCHEV COEFFICIENTS  
C     SUM  THE SUM OF THE COEFFICIENTS  
C     ------------------------------------------------------------------  
*/

void CNWF01(const REAL CP, const REAL W, const int N, REAL *C, REAL &SUM)
{
	REAL A1, A2, A3, C1, DIVFAC, P, RHO, X1, Z1;
	int I, K, L, N1, N2, NCOUNT;  

	const REAL ZERO =0., ONE=1., TWO=2., FOUR=4., START=1e-20;

	N1 = N + 1;
	N2 = N + 2;

//       -------START COMPUTING COEFFICIENTS BY MEANS OF  -------  
//       -------BACKWARD RECURRENCE SCHEME                -------  
	A3 = ZERO;
	A2 = ZERO;
	A1 = START;
	Z1 = FOUR / W;
	NCOUNT = N2;
	C[NCOUNT-1] = START;
	X1 = N2;
	C1 = ONE - CP;
	for(K=1; K<=N1; K++)
	{
		DIVFAC = ONE / X1;
		X1 = X1 - ONE;  
		NCOUNT = NCOUNT - 1;
		C[NCOUNT-1] = X1*((DIVFAC+Z1* (X1-C1))*A1 + (ONE/X1+Z1* (X1+C1+ONE))*A2-DIVFAC*A3);
		A3 = A2;
		A2 = A1;
		A1 = C[NCOUNT-1];
	}
	C[0] = C[0] / TWO;
  
//     ------------ COMPUTE SCALE FACTOR     -----------------  
	RHO = C[0];
	SUM = RHO;
	P = ONE;
    for(I=1; I<N2; I++)
	{
        RHO = RHO - P*C[I];
        SUM = SUM + C[I];
        P = -P;
	}
//    ---------- SCALE COEFFICIENTS          ------------------  
	for (L=0; L < N2; L++)
	{
		C[L] = C[L] / RHO;
	}
	SUM = SUM / RHO;

}


/*
     ------------------------------------------------------------------  
     THIS  SUBROUTINE COMPUTES THE  SPHERICAL BESSEL FUNCTIONS OF  
     FIRST, SECOND  AND  THIRD  KIND  USING A CHEBYCHEV EXPANSION  
     GIVEN  BY  Y. L. LUKE , ALGORITHMS FOR  THE  COMPUTATION  OF  
     MATHEMATICAL FUNCTIONS, ACADEMIC PRESS, LONDON (1977). 
      
     ON INPUT--->  
     LMAX   MAX. ORDER OF THE BESSEL FUNCTIONS  
            (LIMITED UP TO 25 IN THE VERSION)  
     LJ     LOGICAL : IF LJ IS TRUE THE SPHERICAL BESSEL  
            FUNCTIONS OF THE FIRST KIND ARE CALCULATED UP TO LMAX  
     LY     LOGICAL : IF LY IS TRUE THE SPHERICAL BESSEL  
            FUNCTIONS OF THE SECOND KIND ARE CALCULATED UP TO LMAX  
     LH     LOGICAL : IF LH IS TRUE THE SPHERICAL BESSEL  
            FUNCTIONS OF THE THIRD KIND ARE CALCULATED UP TO LMAX  
     LCALL  LOGICAL : IF LCALL IS FALSE THE CHEBYCHEV  
            COEFFICIENTS ARE CALCULATED -THIS PART HAS TO  
            BE CALLED ONCE  
      
     ON OUTPUT--->
     BJ     AN ARRAY CONTAINING THE BESSEL FUNCTIONS OF  
            THE FIRST KIND UP TO LMAX IF LJ IS TRUE.  
            REMEMBER, THAT BJ(1) CONTAINS THE FUNCTION OF  
            L=0 AND SO ON.  
     Y      AN ARRAY CONTAINING THE BESSEL FUNCTIONS OF  
            THE SECOND KIND UP TO LMAX IF LY IS TRUE.  
            REMEMBER,THAT  Y(1) CONTAINS THE FUNCTION OF L=0 AND SO ON.  
    H      AN ARRAY CONTAINING THE BESSEL FUNCTIONS OF  
            THE THIRD KIND UP TO LMAX IF LH IS TRUE.  
            REMEMBER,THAT H (1) CONTAINS THE FUNCTION OF L=0 AND SO ON.  
      
     THE BESSEL FUNCTIONS OF 3RD KIND ARE DEFINED AS: H(L)=BJ(L)+I*Y(L)  
     ------------------------------------------------------------------  
*/

	const int NDIM = 24;
	const int NDIMP1 = NDIM+1;
	const int NMAX = 30; // changed from 20	
	REAL W;
	int  LMSAVE = 0;
	REAL CNWJ[NDIMP1][NMAX], CNWY[NDIMP1][NMAX];

void PrepareChebychevTables(int LMAX)
{
	const REAL W2 = 24.; // changed from 10

	REAL CPJ, CPY, FJ, FY, SUM;
	int NCNW;

	if (LMAX > 24)
	{		 
		fprintf(stderr, "LMAX IS TOO HIGH, ERROR STOP IN BESSEL\n");
		exit(1);
	}
	LMSAVE= LMAX + 1;
	NCNW = NMAX - 2;
	W = -W2 * W2 * REAL(0.25); // scale factor ???
	FJ = 1.;
	FY = -1.;
	for(int L=1; L<=LMSAVE; L++)
	{
		CPJ = 0.5 + L;
		CPY = 1.5 - L;
		CNWF01(CPJ, W, NCNW, &CNWJ[L-1][0], SUM);
		CNWF01(CPY, W, NCNW, &CNWY[L-1][0], SUM);
		for(int N=0; N<NMAX; N++)
		{
			CNWJ[L-1][N] = CNWJ[L-1][N] / FJ;
			CNWY[L-1][N] = CNWY[L-1][N] * FY;
		}
		FJ = FJ * (L+L+1);
		FY = FY * (L+L-1);
	}
}

/*
void BESSEL(c::complex<REAL> *BJ, c::complex<REAL> *Y, c::complex<REAL> *H, const c::complex<REAL> ARG, const int LMAX, const bool LJ, const bool LY, const bool LH)
{
	const int NDIMP2 = NDIM+2;  
	int L, N;
    c::complex<REAL> RES, T0, T1, T2, TARG, TT1;
//C
//C ..  LOCAL ARRAYS  ..
//C
	c::complex<REAL> TN[NMAX], ZN[NDIMP2];
	const c::complex<REAL> CZERO(0., 0.), CONE(1., 0.), CI(0., 1.), CTWO(2., 0.);	
//C     ------------------------------------------------------------------
//C  
//C-------CALCULATE ARG**N AND  TN*(-ARG**2/4)  
//C  
	ZN[0] = CONE;
	for(L=1; L<LMSAVE; L++)
	{
		ZN[L] = ZN[L-1] * ARG;
	}
	ZN[LMSAVE] = ZN[LMSAVE-1] * ARG;
    T0 = CONE;
    TARG = -ZN[2] * REAL(0.25) / W;
    T1 = CTWO * TARG - CONE;
    TN[0] = T0;
    TN[1] = T1;
    TT1 = T1 + T1;
    for(N = 2; N<NMAX; N++)
	{
		T2 = TT1*T1 - T0;
		TN[N] = T2;
		T0 = T1;
		T1 = T2;
	}

    if (LJ || LH)
	{
		for(L=0; L<LMSAVE; L++)
		{
			RES = CZERO;
			for(N=0; N<NMAX; N++)
			{			 
				RES = RES + TN[N] * CNWJ[L][N];
			}
			BJ[L] = RES * ZN[L];
		}
		if (!(LY || LH)) return;  
    }
	
	if (LY || LH)
	{		
		for(L=0; L<LMSAVE; L++)  
		{
			RES = CZERO;
			for(N=0; N<NMAX; N++)
			{
				RES = RES + TN[N] * CNWY[L][N];
//				fprintf(stderr, "%lf\n", CNWY[L][N]);
			}
//				fprintf(stderr, "%lf\n", ZN[L+1].real());
			Y[L] = RES / ZN[L+1];
		}
			
		if (LH) for(L=0; L<LMSAVE; L++) H[L] = BJ[L] + CI*Y[L];
	}
	else
	{
		fprintf(stderr, "****** ERROR WARNING FROM BESSEL: NO OUTPUT\n");
	}
}
*/


void BESSEL(c::complex<REAL> *BJ, c::complex<REAL> *Y, c::complex<REAL> *H, const c::complex<REAL> ARG, const int LMAX, const bool LJ, const bool LY, const bool LH)
{
	const int NDIM = 24;
	const int NDIMP1 = NDIM+1;
	const int NDIMP2 = NDIM+2;  
	
	int L, N;
    c::complex<REAL> RES, T0, T1, T2, TARG, TT1;
//C
//C ..  LOCAL ARRAYS  ..
//C
	const int NMAX = 20;

//	static REAL W;
//	static int LMSAVE = 0;
//	static REAL CNWJ[NDIMP1][NMAX], CNWY[NDIMP1][NMAX];
	REAL W;
	int LMSAVE = 0;
	REAL CNWJ[NDIMP1][NMAX], CNWY[NDIMP1][NMAX];

	c::complex<REAL> TN[NMAX], ZN[NDIMP2];

	const c::complex<REAL> CZERO(0., 0.), CONE(1., 0.), CI(0., 1.), CTWO(2., 0.);
	const REAL W2 = 10.;
	const REAL ONE = 1.;
	
//C     ------------------------------------------------------------------

 bool LCALL = false;

    if (!LCALL)
	{	
	REAL CPJ, CPY, FJ, FY, SUM;
	int NCNW;


		if (LMAX > 24)
		{		 
			fprintf(stderr, "LMAX IS TOO HIGH, ERROR STOP IN BESSER\n");
			exit(1);
		}
			LMSAVE= LMAX + 1;
			NCNW = NMAX - 2;
			W = -W2 * W2 * REAL(0.25); // scale factor ???
			FJ = ONE;
			FY = -ONE;
			for(L=1; L<=LMSAVE; L++)
			{
				CPJ = 0.5 + L;
				CPY = 1.5 - L;
				CNWF01(CPJ, W, NCNW, &CNWJ[L-1][0], SUM);
				CNWF01(CPY, W, NCNW, &CNWY[L-1][0], SUM);
				for(N=0; N<NMAX; N++)
				{
					CNWJ[L-1][N] = CNWJ[L-1][N] / FJ;
					CNWY[L-1][N] = CNWY[L-1][N] * FY;
				}
				FJ = FJ * (L+L+1);
				FY = FY * (L+L-1);
			}
			LCALL = true;
	}
	  
	if ((LMAX > (LMSAVE-1)) || (abs(ARG) > W2))
	{
		fprintf(stderr, "LMAX IS HIGHER THAN PREVIOUSLY GIVEN OR ARGUMENT TOO HIGH... \n");
/*
	WRITE (6,9010) LMAX,ABS(ARG)  
9010  FORMAT (' LMAX IS HIGHER THAN PREVIOUSLY GIVEN',  
     +       '    **********   OR ARGUMENT TOO HIGH, ERROR IN BESSEL'  
     +       ,/,13X,'LMAX : ',I5,'  ABS(ARG) : ',F12.6)  

          STOP  
*/
		exit(1);
	}
	
//C  
//C-------CALCULATE ARG**N AND  TN*(-ARG**2/4)  
//C  
	ZN[0] = CONE;
	for(L=1; L<LMSAVE; L++)
	{
		ZN[L] = ZN[L-1] * ARG;
	}
	ZN[LMSAVE] = ZN[LMSAVE-1] * ARG;
    T0 = CONE;
    TARG = -ZN[2] * REAL(0.25) / W;
    T1 = CTWO * TARG - CONE;
    TN[0] = T0;
    TN[1] = T1;
    TT1 = T1 + T1;
    for(N = 2; N<NMAX; N++)
	{
		T2 = TT1*T1 - T0;
		TN[N] = T2;
		T0 = T1;
		T1 = T2;
	}

    if (LJ || LH)
	{
		for(L=0; L<LMSAVE; L++)
		{
			RES = CZERO;
			for(N=0; N<NMAX; N++)
			{			 
				RES = RES + TN[N] * CNWJ[L][N];
			}
			BJ[L] = RES * ZN[L];
		}
		if (!(LY || LH)) return;  
    }

		
	if (LY || LH)
	{		
		for(L=0; L<LMSAVE; L++)  
		{
			RES = CZERO;
			for(N=0; N<NMAX; N++)
			{
				RES = RES + TN[N] * CNWY[L][N];
//				fprintf(stderr, "%lf\n", CNWY[L][N]);
			}
//				fprintf(stderr, "%lf\n", ZN[L+1].real());
			Y[L] = RES / ZN[L+1];
		}
			
		if (LH) for(L=0; L<LMSAVE; L++) H[L] = BJ[L] + CI*Y[L];
	}
	else
	{
		fprintf(stderr, "****** ERROR WARNING FROM BESSEL: NO OUTPUT\n");
/*		
               WRITE(6,9020)
9020  FORMAT ('  ********** ERROR WARNING FROM BESSEL : NO OUTPUT ',  
     +       ' REQUIRED  *********',/,/)  
*/
	}
		
	
}





/*
C-----------------------------------------------------------------------  
C     FUNCTION BLM  PROVIDES  THE  INTEGRAL  OF  THE  PRODUCT  OF THREE  
C     SPHERICAL HARMONICS,EACH OF WHICH CAN BE EXPRESSED AS A PREFACTOR  
C     TIMES  A  LEGENDRE  FUNCTION. THE  THREE  PREFACTORS  ARE  LUMPED  
C     TOGETHER AS  FACTOR 'C'; AND   THE INTEGRAL OF THE THREE LEGENDRE  
C     FUNCTIONS FOLLOWS GAUNT SUMMATION SCHEME SET OUT BY SLATER(ATOMIC  
C     STRUCTURE, VOL1, 309,310  
C-----------------------------------------------------------------------  
*/

#define min(a,b) ((a)<(b)?(a):(b))
#define max(a,b) ((a)>(b)?(a):(b))

REAL  BLM(const int L1, const int M1, const int L2, const int M2, const int L3, const int M3, const int LMAX)
{
	int I, IA1, IA2, IA3, IA4, IA5, IA6, IA7, IA8, IA9, IB1, IB2, IB3, IB4;
	int IB5, IC, IC1, IC2, IC3, IC4, IC5, IC6, IS, IT, IT1, IT2, NL1, NL2;
	int NL3, NM1, NM2, NM3, NTEMP, NN;
	REAL SIGN, A, AD, AN, B, BD, BN, C, CD, CN;

	REAL FAC[4*LMAX+2];
//-----------------------------------------------------------------------  
	FAC[0] = 1.; 
	NN = 4*LMAX + 1;
	for(I=0; I<NN; I++)
	{
		FAC[I+1] = REAL(I+1) * FAC[I];
	}
	
	if ((M1+M2+M3) != 0)
	{
		return 0.;  
	}
	
	if (L1-LMAX-LMAX > 0)
	{
		printf("Error");
		exit(1);
	}
  
	if (L2-LMAX > 0)
	{
		printf("Error");
		exit(1);
	}
   
	if (L3-LMAX > 0)
	{
		printf("Error");
		exit(1);
	}
   
	if (L1-abs(M1) < 0)
	{
		printf("Error");
		exit(1);
	}
   
	if (L2-abs(M2) < 0)
	{
		printf("Error");
		exit(1);
	}
	
	if (L3-abs(M3) < 0)
	{
		printf("Error");
		exit(1);
	}
   
   
   if (((L1+L2+L3) % 2) != 0)
   {
		return 0.;  
   }
	
	NL1 = L1;
	NL2 = L2;
	NL3 = L3;
	NM1 = abs(M1);
	NM2 = abs(M2);
	NM3 = abs(M3);
	IC = (NM1 + NM2 + NM3) / 2;

	if ((max(max(NM1, NM2), NM3)-NM1) > 0)
	{
		if ((max(NM2, NM3) - NM2) <= 0)
		{  
			NL1 = L2;
			NL2 = L1;
			NM1 = NM2;
			NM2 = abs(M1);
		} else
		{
			NL1 = L3;
			NL3 = L1;
			NM1 = NM3;
			NM3 = abs(M1);
		}  
	}

	if ((NL2 - NL3) < 0)
	{
		NTEMP = NL2;
		NL2 = NL3;
		NL3 = NTEMP;
		NTEMP = NM2;
		NM2 = NM3;
		NM3 = NTEMP;
	}
//<0  ==0  >0	

	if(NL3 - abs(NL2-NL1) < 0)
	{
		return 0.;
	}
  
//C  
//C     CALCULATION OF FACTOR  'A'.  
//C  

	IS = (NL1 + NL2 + NL3) / 2;
	IA1 = IS - NL2 - NM3;
	IA2 = NL2 + NM2;
	IA3 = NL2 - NM2;
	IA4 = NL3 + NM3;
	IA5 = NL1 + NL2-NL3;
	IA6 = IS - NL1;
	IA7 = IS - NL2;
	IA8 = IS - NL3;
	IA9 = NL1 + NL2 + NL3 + 1;
	AN = pow(-1.0, IA1) * FAC[IA2] * FAC[IA4] * FAC[IA5] * FAC[IS];
	AD = FAC[IA3] * FAC[IA6] * FAC[IA7] * FAC[IA8] * FAC[IA9];
	A = AN / AD;
//C
//C     CALCULATION OF SUM 'B'
//C  
	IB1 = NL1 + NM1;
	IB2 = NL2 + NL3 - NM1;
	IB3 = NL1 - NM1;
	IB4 = NL2 - NL3 + NM1;
	IB5 = NL3 - NM3;
	IT1 = max(0, -IB4) + 1;
	IT2 = min(IB2, min(IB3, IB5)) + 1;
	B = 0.0;
	SIGN = pow(-1., IT1);
	IB1 = IB1 + IT1 - 2;
	IB2 = IB2 - IT1 + 2;
	IB3 = IB3 - IT1 + 2;
	IB4 = IB4 + IT1 - 2;
	IB5 = IB5 - IT1 + 2;
	for(IT=IT1; IT<=IT2; IT++)
	{
		SIGN = -SIGN;
		IB1 = IB1 + 1;
		IB2 = IB2 - 1;
		IB3 = IB3 - 1;
		IB4 = IB4 + 1;
		IB5 = IB5 - 1;
		BN = SIGN * FAC[IB1] * FAC[IB2];
		BD = FAC[IT-1] * FAC[IB3] * FAC[IB4] * FAC[IB5];
		B = B + (BN / BD);
	}
//C  
//C       CALCULATION OF FACTOR 'C'  
//C  
		IC1 = NL1 - NM1;
		IC2 = NL1 + NM1;
		IC3 = NL2 - NM2;
		IC4 = NL2 + NM2;
		IC5 = NL3 - NM3;
		IC6 = NL3 + NM3;
		CN = REAL((2*NL1+1) * (2*NL2+1) * (2*NL3+1)) * FAC[IC1] * FAC[IC3] * FAC[IC5];
		CD = FAC[IC2] * FAC[IC4] * FAC[IC6];
		C = CN / (M_PI * CD);
		C = (sqrt(C)) / 2.;
		return pow(REAL(-1.), IC) * A * B * C;
		
//  19  WRITE(6,20)L1,L2,M2,L3,M3  
//  20  FORMAT(28H INVALID ARGUMENTS FOR BLM. ,5(I2,1H,))  
	}

	
/*
C     ------------------------------------------------------------------  
C     ROUTINE TO TABULATE THE CLEBSCH-GORDON TYPE COEFFICIENTS ELM,  FOR 
C     USE WITH THE SUBROUTINE XMAT. THE NON-ZERO ELM ARE TABULATED FIRST 
C     FOR  L2,M2; AND L3,M3; ODD. THEN FOR L2,M2; AND L3,M3; EVEN, USING   
C     THE SAME SCHEME AS THAT BY WHICH THEY ARE ACCESSED IN XMAT.  
C     ------------------------------------------------------------------  
*/

void ELMGEN(REAL *ELM, const int &NELMD, const int &LMAX)
{
	int K, II, LL, IL2, L2, M2, I2, IL3, L3, M3, I3, LA1, LB1, LA11, LB11, M1;
	int L11, L1, L;
	const REAL FOURPI = 4. * M_PI;

	K = 1;
	II = 0;
	
	while(1)
	{
		LL = LMAX + II;
		for(IL2=1; IL2<=LL; IL2++)
		{
			L2 = IL2 - II;
			M2 = -L2+1-II;
			for(I2=1; I2<=IL2; I2++)
			{		
				for(IL3=1; IL3<=LL; IL3++)
				{		
					L3 = IL3 - II;
					M3 = -L3 + 1 - II;
					for(I3=1; I3<=IL3; I3++)
					{
						LA1 = max(abs(L2-L3), abs(M2-M3));
						LB1 = L2 + L3;
						LA11 = LA1 + 1;
						LB11 = LB1 + 1;
						M1 = M2 - M3;
						for(L11=LA11; L11<=LB11; L11+=2)
						{
							L1 = L11 - 1;
							L = (L2 - L3 - L1) / 2 + M2;
							ELM[K-1] = pow(-1., L) * FOURPI * BLM(L1, M1, L3, M3, L2, -M2, LMAX);
							K = K + 1;
						}
						M3 = M3 + 2;
					}
				}
				M2 = M2 + 2;
			}
		}

		if (II <= 0)
		{
			II = 1;
		} else
		return;
	}
}

//     -----------------------------------------------------------------
//     GIVEN  CT=COS(THETA),  ST=SIN(THETA),  AND CF=EXP(I*FI), THIS  
//     SUBROUTINE  CALCULATES  ALL THE  YLM(THETA,FI) UP TO  L=LMAX. 
//     SUBSCRIPTS ARE ORDERED THUS:(L,M)=(0,0),(1,-1),(1,0),(1,1)...  
//     -----------------------------------------------------------------

void SPHRM4(c::complex<REAL> *YLM, const c::complex<REAL> &CT, const c::complex<REAL> &ST, const c::complex<REAL> &CF, const int LMAX)
{
//      COMPLEX*16 YLM(LM1SQD);
	const int LMAX1D = LMAX+1;
	const int LM1SQD = LMAX1D*LMAX1D;

	int L, LL, LM, LM2, LM3, LN, LO, LP, LQ, M;
	REAL A, ASG, B, CL, CM;
	c::complex<REAL> SF, SA;
	REAL FAC1[LMAX1D], FAC3[LMAX1D], FAC2[LM1SQD];

//C-----------------------------------------------------------------------

if (LMAX == 1)
{
	YLM[0] = 0.5 / sqrt(M_PI);
	YLM[1] = 0.5 / CF * sqrt(3./2./M_PI) * ST;
	YLM[2] = 0.5 * sqrt(3./M_PI) * CT;
	YLM[3] = -0.5 * CF * sqrt(3./2./M_PI) * ST;
	return;
}

	LM = 0;
	CL = 0.;
	A = 1.;
	B = 1.;
	ASG = 1.;
	LL = LMAX + 1;
	  
//****** MULTIPLICATIVE FACTORS REQUIRED ******  
	for(L=1; L<=LL; L++)
	{
		FAC1[L-1] = ASG * sqrt((2.*CL + 1.)*A/(4.*M_PI*B*B));
		FAC3[L-1] = sqrt(REAL(2.) * CL);
		CM = -CL;
		LN = L + L - 1;
		for(M=1; M<=LN; M++)
		{
			LO = LM + M;
			FAC2[LO-1] = sqrt((CL+1.+CM) * (CL+1.-CM) / ((2.*CL+3.) * (2.*CL+1.)));
			CM = CM + 1.;
		}
		CL = CL + 1.;
		A = A * 2. * CL * (2.*CL - 1.) / 4.;
		B = B * CL;
		ASG = -ASG;
		LM = LM + LN;
	}
	
//****** FIRST ALL THE YLM FOR M=+-L AND M=+-(L-1) ARE ******  
//****** CALCULATED BY EXPLICIT FORMULAE               ******  
	LM = 1;
	CL = 1.;
	ASG = -1.;
	SF = CF;
	SA = c::complex<REAL>(1., 0.);
	YLM[0] = c::complex<REAL>(FAC1[0], 0.);
	for(L=1; L<=LMAX; L++)
	{
		LN = LM + L + L + 1;
		YLM[LN-1] = FAC1[L]*SA*SF*ST;
		YLM[LM+1-1] = ASG*FAC1[L]*SA*ST/SF;
		YLM[LN-1-1] = -FAC3[L]*FAC1[L]*SA*SF*CT/CF;
		YLM[LM+2-1] = ASG*FAC3[L]*FAC1[L]*SA*CT*CF/SF;
		SA = ST*SA;
		SF = SF*CF;
		CL = CL + 1.;
		ASG = -ASG;
		LM = LN;
   }
//****** USING YLM AND YL(M-1) IN A RECURENCE RELATION ******  
//****** YL(M+1) IS CALCULATED                         ******  
	LM = 1;
	LL = LMAX - 1;
	for(L=1; L<=LL; L++)
	{
		LN = L + L - 1;
		LM2 = LM + LN + 4;
		LM3 = LM - LN;
		for(M=1; M<=LN; M++)
		{
			LO = LM2 + M;
			LP = LM3 + M;
			LQ = LM + M + 1;
			YLM[LO-1] = -(FAC2[LP-1]*YLM[LP-1] - CT*YLM[LQ-1]) / FAC2[LQ-1];
		}
		LM = LM + L + L + 1;
	}  
}


// GIVEN the Wave vector GK and the length KAPPA
// SPHR4 CALCULATES  ALL THE  YLM(THETA,FI) UP TO  L=LMAX
// and return ct, st, cf for further usage
void SPHRM4(c::complex<REAL> *YLM, const CKCoord3D &GK, const c::complex<REAL> &KAPPA, const int LMAX, c::complex<REAL> &CT, c::complex<REAL> &ST, c::complex<REAL> &CF)
{
	REAL AKPAR, AKG1, AKG2;
	//c::complex<REAL> CT, ST, CF;
	const c::complex<REAL> CONE(1., 0.);

	AKG1 = GK.x.real();
	AKG2 = GK.y.real();

	//double KAPPA = sqrt(AKG1*AKG1 + AKG2*AKG2 + GK.z.real()*GK.z.real());

	
	AKPAR = sqrt(AKG1*AKG1 + AKG2*AKG2);
	CT = GK.z / KAPPA; // cos theta
	ST = AKPAR / KAPPA;  //sin theta
	CF = CONE; // exp(I*Fi)
	if (AKPAR > EMACH) CF = c::complex<REAL>(AKG1 / AKPAR, AKG2 / AKPAR);
	SPHRM4(YLM, CT, ST, CF, LMAX);
}


/*
int main()
{
	REAL ELM[7680];
    ELMGEN(ELM, 7680, 4);
	fprintf(stderr, "%lf\n", ELM[0]);
	fprintf(stderr, "%lf\n", ELM[1]);
	fprintf(stderr, "%lf\n", ELM[2]);
	fprintf(stderr, "%lf\n", ELM[3]);
	fprintf(stderr, "%lf\n", ELM[4]);
	fprintf(stderr, "%lf\n", ELM[5]);
	fprintf(stderr, "%lf\n", ELM[6]);
	fprintf(stderr, "%lf\n", ELM[7]);
	
	// 3.54490
	// -3.1706
	// 2.7458
	// -2.935462
	

	bool LCALL = false;
	//	c::complex<REAL> erf = CERF(c::complex<REAL>(3., -2.), 1e-20);
	//	printf("%lf %lf\n", erf.real(), erf.imag());
	c::complex<REAL> BJ[26];
	c::complex<REAL> Y[26];
	c::complex<REAL> H[26];

	BESSEL(BJ, Y, H, c::complex<REAL>(1., 0.), 1, 24, true, true, true, LCALL);
	fprintf(stderr, "%lf %lf\n", BJ[0].real(), BJ[0].imag());
	fprintf(stderr, "%lf %lf\n", BJ[1].real(), BJ[1].imag());
	fprintf(stderr, "%lf %lf\n", BJ[2].real(), BJ[2].imag());
	fprintf(stderr, "%lf %lf\n", Y[0].real(), Y[0].imag());
	fprintf(stderr, "%lf %lf\n", Y[1].real(), Y[1].imag());
	fprintf(stderr, "%lf %lf\n", Y[2].real(), Y[2].imag());
	fprintf(stderr, "%lf %lf\n", H[0].real(), H[0].imag());
	fprintf(stderr, "%lf %lf\n", H[1].real(), H[1].imag());
	fprintf(stderr, "%lf %lf\n", H[2].real(), H[2].imag());
	return 0;
	
}
*/

