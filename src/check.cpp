#include<stdlib.h>
#include<stdio.h>
#include"global.h"
#include"world.h"
#include"CAtom.h"
#include"functions.h"
#include"multilayer.h"

/*
     ------------------------------------------------------------------  
     A B S T R A C T   
     THIS PROGRAM CALCULATES EITHER THE ABSORBANCE, REFLECTIVITY  AND  
     TRANSMITTANCE  OF   LIGHT  BY  A   FINITE  SLAB   CONSISTING  OF  
     HOMOGENEOUS   PLATES AND   MULTILAYERS  OF  SPHERICAL  PARTICLES  
     ARRANGED IN  A TWO-DIMENSIONAL  BRAVAIS LATTICE, OR THE  COMPLEX  
     PHOTONIC  BAND STRUCTURE OF SUCH AN INFINITE PERIODIC STRUCTURE.    
  
     D E S C R I P T I O N    O F    I N P U T    D A T A  
     KTYPE=     1: THE DIRECTION OF AN INCIDENT  EM WAVE IS SPECIFIED  
                   BY THE POLAR ANGLES OF INCIDENCE "THETA" AND "FI".  
                   THE PROGRAM CALCULATES THE TRANSMISSION,REFLECTION  
                   AND  ABSORPTION   COEFFICIENTS OF  A  FINITE  SLAB     
                2: THE DIRECTION OF  AN INCIDENT EM WAVE IS SPECIFIED  
                   BY THE COMPONENTS  OF THE WAVEVECTOR  PARALLEL  TO  
                   THE  INTERFACES OF THE STRUCTURE:   
                   AQ(1) AND AQ(2) (AND THE  FREQUENCY). THE  
                   PROGRAM  CALCULATES  THE TRANSMISSION, REFLECTION, 
                   ABSORPTION COEFFICIENTS OF A FINITE SLAB  
                3: THE PROGRAM CALCULATES  THE PHOTONIC  COMPLEX BAND  
                   STRUCTURE OF SUCH  AN INFINITE PERIODIC  STRUCTURE  
                   FOR A  WAVEVECTOR WITH COMPONENTS PARALLEL TO  THE   
                   INTERFACES OF THE STRUCTURE: AQ(1) AND AQ(2)  
     KSCAN=     1: SCANNING OVER FREQUENCIES  
                2: SCANNING OVER WAVELENGTHS  
     KEMB        : INDICATES THE PRESENCE (=1) OR ABSENCE (=0) OF A  
                   DIFFERENT EMBEDDING MEDIUM   
     LMAX        : CUTOFF IN SPHERICAL WAVES EXPANSIONS  
     NCOMP       : NUMBER OF DIFFERENT COMPONENTS IN THE UNIT SLICE.  
                   THEIR TYPE IS SPECIFIED  BY THE INTEGER ARRAY  
                   IT(ICOMP)  
     IT=        1: HOMOGENEOUS PLATE OF THICKNESS "D"  
                2: MULTILAYER  OF SPHERICAL  PARTICLES ARRANGED IN  A  
                   2D  BRAVAIS LATTICE.EACH LAYER CONSISTS OF "NPLAN"  
                   NON-PRIMITIVE  PLANES OF SPHERES WITH THE SAME 2-D  
                   PERIODICITY. THE NUMBER OF UNIT LAYERS IS EQUAL TO  
                   2**(NLAYER-1).  
     DL, DR      : POSITION VECTORS INDICATING THE ORIGIN ON THE LEFT 
                   AND ON THE RIGHT OF THE  UNIT,  RESPECTIVELY. BOTH 
                   ARE DIRECTED FROM LEFT TO RIGHT.  
     AL          : PRIMITIVE  TRANSLATION  VECTOR  OF THE  UNIT SLICE  
                   (EFFECTIVE ONLY FOR BAND STRUCTURE CALCULATION).IT  
                   IS GIVEN IN PROGRAM UNITS.  
     NUNIT       : SPECIFIES THE NUMBER OF UNIT SLICES (2**(NUNIT-1))  
                   OF THE SAMPLE  
     ALPHA,ALPHAP: LENGTH OF PRIMITIVE VECTORS OF THE TWO-DIMENSIONAL  
                   LATTICE. IN PROGRAM UNITS THE SIZE OF ALPHA SERVES  
                   AS  THE UNIT LENGTH.  THUS  ALPHA MUST BE EQUAL TO  
                   1.D0  
     FAB         : ANGLE (IN DEG) BETWEEN ALPHA AND ALPHAP 
     RMAX        : UPPER LIMIT FOR THE LENGTH OF  RECIPROCAL  LATTICE  
                   VECTORS (IN UNITS OF 1/ALPHA) WHICH  MUST BE TAKEN  
                   INTO ACCOUNT  
     ZINF,ZSUP   : MINIMUM  AND  MAXIMUM  VALUES  OF  FREQUENCY   (IN  
                   PROGRAM UNITS: OMEGA*ALPHA/C), OR  WAVELENGTH  (IN  
                   PROGRAM UNITS: LAMDA/ALPHA  ),  ACCORDING  TO  THE  
                   VALUE OF KSCAN. C AND LAMDA REFER TO VACUUM  
     NP          : NUMBER OF EQUALLY SPACED POINTS BETWEEN ZINF, ZSUP  
     POLAR       : POLARIZATION ('S ' OR  'P ') OF THE INCIDENT LIGHT  
     AQ(1,2)     : WAVEVECTOR COMPONENTS PARALLEL  TO THE  INTERFACES 
                   OF THE STRUCTURE (XY-PLANE) IN UNITS OF 2*PI/ALPHA  
     THETA,FI    : POLAR ANGLES OF INCIDENCE (IN DEG) OF THE INCIDENT  
                   LIGHT  
     FEIN        : ANGLE  (IN DEG) SPECIFYING  THE DIRECTION  OF  THE  
                   POLARIZATION  VECTOR  FOR  NORMAL  INCIDENCE.  NOT  
                   EFFECTIVE OTHERWISE  
     EPS*,MU*    : RELATIVE DIELECTRIC FUNCTIONS AND MAGNETIC PERMEA-  
                   BILITIES OF THE VARIOUS MEDIA  
     ------------------------------------------------------------------  
*/

// inp5.txt
// full multiple scattering must be enabled
// double accuracy
// EMACH = REAL(1e-8)

/*
                ********************************************
          	   ********INPUT FILE FOR TRANSMISSION*********
		   ********************************************
   KTYPE = 1   KSCAN = 1   KEMB  = 0    LMAX = 1   NCOMP = 1   NUNIT = 1
 ALPHA =    4.000000  BETA =    4.000000   FAB =   90.000000  RMAX =    4.000000
  NP =  20  ZINF =    0.100000  ZSUP =    2.000000
  THETA/AK(1) =  45.00000000     FI/AK(2) =   0.00000000   POLAR =S     FEIN =   0.00

Give information for the "NCOMP" components 

     IT  = 2
     MUMED =   1.00000000   0.00000000     EPSMED=   1.00000000   0.00000000
   NPLAN = 1  NLAYER =20
       S =   0.10000000     MUSPH =   1.00000000   0.00000000     EPSSPH=   0.50000000   0.000000000
xyzDL 0.0  0.0  2.0
xyzDR 0.0 0.0 2.0
     MUEMBL=   1.00000000   0.00000000    EPSEMBL=   1.00000000   0.00000000
     MUEMBR=   1.00000000   0.00000000    EPSEMBR=   1.00000000   0.00000000
CCC
*/


void TestINP5Atk(CWorld &world, const double k)
{
	const c::complex<REAL> CZERO(0., 0.);
    const c::complex<REAL> CONE(1., 0.);

	world.light.THETA = 45.0; // perpendicular incidence
	world.light.FI = 0.;
	world.light.POLAR = 'S';	
	world.light.SetWavevector(k);

	CSlab slab(world.rl.IGMAX * 2);
	CVector< c::complex<REAL> > EINCID;
	EINCID.assign(2 * world.rl.IGMAX, CZERO);

	CVector<CKCoord3D> GKK;
	GKK.assign(world.rl.IGMAX, CKCoord3D());
	
	GetIncomingLightasReciprocalLattice(world.rl.IGMAX, EINCID, world.rl, world.light);
	BuildScatterMatrix(world, slab); // here I am not sure, this is 2^20
	//cout << "QI: " << slab.QI[0][0].real() << " " << slab.QI[0][0].imag()  << "\n";
	//cerr << "lambda: " << REAL(2.*M_PI)/light.KAPPA0 << "\n";
	//cerr << "AK " << light.AK.x << " " << light.AK.y << "\n";

	for(int IG1=0; IG1<world.rl.IGMAX; IG1++) GKK[IG1] = ReciprocalWaveVector(world.light.AK, world.rl.G[IG1], world.light.KAPPA0);

	int IGK1, IG1, K1, IGK2;

	//c::complex<REAL> ETRANS[2*rl.IGMAX], EREFLE[2*rl.IGMAX];
	c::complex<REAL> ETRANS_SIGMA, ETRANS_PI;
	c::complex<REAL> EREFLE_SIGMA, EREFLE_PI;
	c::complex<REAL> EIN_SIGMA, EIN_PI;
	double intensitytsum = 0.; 
	double intensityrsum = 0.;
	double intensityisum = 0.;	
	
	for(IG1=0; IG1<world.rl.IGMAX; IG1++)
	{
		ETRANS_SIGMA = CZERO;
		ETRANS_PI = CZERO;
		EREFLE_SIGMA = CZERO;
		EREFLE_PI = CZERO;
		EIN_SIGMA = CZERO;
		EIN_PI = CZERO;
		int IGK1 = IG1*2;
		for (IGK2=0; IGK2 < 2*world.rl.IGMAX; IGK2++)
		{
			ETRANS_SIGMA += slab.QI  [IGK1+0][IGK2] * EINCID[IGK2];
			EREFLE_SIGMA += slab.QIII[IGK1+0][IGK2] * EINCID[IGK2];
			ETRANS_PI    += slab.QI  [IGK1+1][IGK2] * EINCID[IGK2];
			EREFLE_PI    += slab.QIII[IGK1+1][IGK2] * EINCID[IGK2];
		}
		EIN_SIGMA	 = EINCID[IGK1+0];
		EIN_PI		 = EINCID[IGK1+1];

		double intensityi = (EIN_SIGMA*conj(EIN_SIGMA) + EIN_PI*conj(EIN_PI)).real();
		double intensityt = (ETRANS_SIGMA*conj(ETRANS_SIGMA) + ETRANS_PI*conj(ETRANS_PI)).real();
		double intensityr = (EREFLE_SIGMA*conj(EREFLE_SIGMA) + EREFLE_PI*conj(EREFLE_PI)).real();
		//cout << "GKK " << GKK[IG1].z.real() <<"\n";
		intensityisum += intensityi * GKK[IG1].z.real();
		intensitytsum += intensityt * GKK[IG1].z.real();
		intensityrsum += intensityr * GKK[IG1].z.real();
	}
	intensitytsum /= intensityisum;
	intensityrsum /= intensityisum;
	cerr << "k: " << k << " i:" << intensityisum << " t:" << intensitytsum << " r:" << intensityrsum << " a:" << 1.-intensitytsum-intensityrsum << "\n";
	fflush(stderr);
}

void TestINP5()
{
	CWorld world;
	world.SetLMAX(1);

// ---------------------
	REAL S = 0.1;
	CLayer l;

	l.IT = 2; // Atoms
	l.EPSMED = 1.;
	//l.NLAYER = 20;
	l.NLAYER = 0;

	//l.AddAtom(new CScatteringFactorAtom(10));
	l.AddAtom(new CSphereAtom(0.5, S), CCoor3D(0., 0., 0.));
	l.DT = CCoor3D(0., 0., 2.);
	l.DB = CCoor3D(0., 0., 2.);
	world.layer.push_back(l);
	world.multilayer = "1048576*(0)";
	//l.atom = NULL; // otherwise it is deleted twice. TODO: better solution, maybe shared_ptr

// ****** DEFINE THE 2D DIRECT AND RECIPROCAL-LATTICE VECTORS ******  
	world.rl = CReciprocalLattice(CCoor2D(4., 0.), CCoor2D(0., 4.));
	int IGMAX =      world.rl.IGMAX;
	int IGKMAX = 2 * world.rl.IGMAX;
	fprintf(stderr, "Number of reciprocal lattice vectors = %i\n", IGMAX);

	for(double k=0.1; k<=20.0001; k += 0.1)
	for(double k=4.442; k<=5.5001; k += 0.0001)
	{
		TestINP5Atk(world, k);	
	}	
}












