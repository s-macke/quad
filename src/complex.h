#ifndef COMPLEX_H
#define COMPLEX_H

#include<cmath>
#include<iostream>

// complex class for use with quadmath. 

namespace c
{

template<class T>
class complex
{
	public:
	constexpr T real() const { return r; }
	constexpr T imag() const { return i; }
	complex(const T& _r = T(), const T& _i = T()) : r(_r), i(_i) { }
	complex(const complex<T>& _z) : r(_z.real()),i(_z.imag()) {}
	
    complex& operator+=(const complex<T>& _r)
	{
	  r += _r.real();
	  i += _r.imag();
	  return *this;
	}
	
    complex& operator*=(const complex<T>& _r)
	{
		const complex<T> l(r, i);
		r = l.real()*_r.real() - l.imag()*_r.imag();
		i = l.real()*_r.imag() + l.imag()*_r.real();
		return *this;
	}
	
	T r, i;

};

	template<class T> 
	inline T norm(const complex<T>& z)
	{
		const T x = z.real();
		const T y = z.imag();
		return x*x + y*y;
	}


	template<class T> 
	inline complex<T> operator*(const complex<T>& l, const complex<T>& r)
	{
		complex<T> z(l.real()*r.real() - l.imag()*r.imag(), l.real()*r.imag() + l.imag()*r.real()) ;
		return z;
	}

	template<class T> 
	inline complex<T> operator*(const complex<T>& l, const T& r)
	{
		complex<T> z(l.real()*r, l.imag()*r);
		return z;
	}

	template<class T> 
	inline complex<T> operator/(const complex<T>& l, const T& r)
	{
		return complex<T> (l.real()/r, l.imag()/r);
	}

	template<class T> 
	inline complex<T> operator/(const complex<T>& l, const complex<T>& r)
	{
		T den = norm(r);
		complex<T> z( (l.real()*r.real() + l.imag()*r.imag())/den, (l.imag()*r.real() - l.real()*r.imag())/den);
		return z;
	}

	template<class T> 
	inline complex<T> operator/(const T& l, const complex<T>& r)
	{
		T den = norm(r);
		return complex<T> (l*r.real()/den, -l*r.imag()/den);
	}

	template<class T> 
	inline complex<T> operator*(const T& l, const complex<T>& r)
	{
		return complex<T> (l*r.real(), l*r.imag());
	}

	template<class T> 
	inline complex<T> operator-(const complex<T>& l, const complex<T>& r)
	{
		return complex<T>(l.real() - r.real(), l.imag() - r.imag());
	}

	template<class T> 
	inline complex<T> operator-(const complex<T>& l, const T& r)
	{
		return complex<T>(l.real() - r, l.imag());
	}

	template<class T> 
	inline complex<T> operator+(const complex<T>& l, const T& r)
	{
		return complex<T>(l.real() + r, l.imag());
	}

	template<class T> 
	inline complex<T> operator+(const T& l, const complex<T>& r)
	{
		return complex<T>(l + r.real(), r.imag());
	}

	template<class T> 
	inline complex<T> operator+(const complex<T>& l, const complex<T>& r)
	{
		return complex<T>(l.real() + r.real(), l.imag() + r.imag());
	}

	template<class T> 
	inline complex<T> operator-(const T& l, const complex<T>& r)
	{
		return complex<T>(l - r.real(), r.imag());
	}
	
	
	template<class T> 
	inline complex<T> operator-(const complex<T>& r)
	{
		return complex<T>(-r.real(), -r.imag());
	}

	template<class T> 
	inline complex<T> operator+(const complex<T>& r)
	{
		return complex<T>(r.real(), r.imag());
	}

	
	template<class T> 
	inline T abs(const complex<T>& z)
	{
      T x = z.real();
      T y = z.imag();
      const T s = std::max(fabs(x), fabs(y));
      if (s == T())  // well ...
        return s;
      x /= s; 
      y /= s;
      return s * std::sqrt(x * x + y * y);
	}

	template<class T> 
	complex<T> sqrt(const complex<T>& z)
	{
		T x = z.real();
		T y = z.imag();

		if (x == T())
		{
			T t = std::sqrt(fabs(y) / 2);
			return complex<T>(t, y < T() ? -t : t);
        }
		else
        {
			T t = std::sqrt(2 * (abs(z) + fabs(x)));
			T u = t / 2;
			return x > T() ? complex<T>(u, y / t) : complex<T>(fabs(y) / t, y < T() ? -u : u);
		}
	}

	template<class T> 
	inline complex<T> conj(const complex<T>& z)
	{
		return complex<T>(z.real(), -z.imag());
	}

	template<class T> 
	complex<T> exp(const complex<T>& z)
	{
		T x = std::exp(z.real());
		T y = z.imag();
		return complex<T>(x*cos(y), x*sin(y));
	}


} // namespace
// ------------------------------------------------------

#endif
