#include"export.h"
#include"multilayer.h"

void Interpret()
{
/*
	while(1)
	{
		token = ReadNextToken(, int &pos, CString &var, int &number);			
	}
*/
}

void Export2ReMagX(const char *filename, CWorld &world)
{
	FILE *file = fopen(filename, "w");
	if (file == NULL)
	{
		fprintf(stderr, "Error: Could not write file %s\n", filename);
		return;
	}

	fprintf(file, "fileversion = 1\n");
	fprintf(file, "layerversion = 3\n");

	printf("Export structure to ReMagX\n");
	printf("Number of layers : %i\n", world.layer.size());
	//TOKEN token;
	//Interpret()
	printf("Number of layers: %i\n", world.layer.size());
	for(int i=0; i<world.layer.size(); i++)
	{
		const CLayer *l = &world.layer[i];

		fprintf(file, "layer = %i\n", i);
		fprintf(file, "layer_d = %f\n", l->D);
		fprintf(file, "layer_db = layer%i\n", i);
		
		c::complex<REAL> TE[2];
		c::complex<REAL> TH[2];
		c::complex<REAL> fsum = 0.;		
		for(int atomidx=0; atomidx<l->atoms.size(); atomidx++)
		{
			CAtom *atom = dynamic_cast<CAtom*>(l->atoms[atomidx]->Clone());
			atom->Set(world.LMAX, world.light.KAPPA0, 1.);
			atom->GetIsotropicScatterMatrix(TE, TH, 1);
			fsum += TE[1];
			//printf("TE: %e %e\n", TE[1].real(), TE[1].imag());
			delete atom;
		}
		fprintf(file, "layer_delta = %f\n", fsum.real());
		fprintf(file, "layer_beta = %f\n", fsum.imag());
	}
	fclose(file);
}