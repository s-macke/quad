#include"script.h"
#include"OpticalConstants.h"
#include"multilayer.h"
#include"fit/CFit.h"
#include"lm/levmar.h"

#include"export.h"

extern "C"
{
#include"lua/src/lua.h"
#include "lua/src/lauxlib.h"
#include "lua/src/lualib.h"
}

#include"lua_typed_enums.h"

static CWorld *world;
static CSlab *slab;
lua_State *L;
#include"scriptvec.h"
#include"scripttable.h"
// -------------------------------------------------
void PLW(const c::complex<REAL> &KAPPA, const CKCoord3D &GK, const int &LMAX, CExpansionCoefficients &A);
void DLMKG(
	const int LMAX,
	const REAL A0,
	const CKCoord3D &GK,
	const REAL &SIGNUS,
	const c::complex<REAL> &KAPPA,
	CExpansionCoefficients &D);

void Xlm(const int LMAX, CKCoord3D *Y, CKCoord3D *P, CKCoord3D *X, const CCoor3D &p);
void PrintEFieldPLW(const CExpansionCoefficients &A);

static int LuaAtomScatter(lua_State *L)
{
	const int LMAX = world->LMAX;
	const c::complex<REAL> CZERO(0., 0.);
	const c::complex<REAL> CIMAG(0., 1.);
	double KAPPA = world->light.KAPPA0;

	CExpansionCoefficients A(LMAX);
	CExpansionCoefficients B(LMAX);
	CExpansionCoefficients D(LMAX);

	CAtom *atom = (CAtom*)lua_touserdata(L, 1);
	//vec3 *GKIN  = (vec3*)luaL_checkudata(L, 2, "Lua.vec3");
	vec3 *GKOUT = (vec3*)luaL_checkudata(L, 2, "Lua.vec3");
	
	CKCoord3D GK;
	GK.x = world->light.AK.x;
	GK.y = world->light.AK.y;
	GK.z = world->light.AK.z;
	
	PLW(KAPPA, GK, LMAX, A);
	/*PrintEFieldPLW(A);*/

	atom->Set(LMAX, KAPPA, 1.);
	atom->Scatter(B, A);
	
	/*
	printf("1. %f %f\n", A.Elm[1].pol[0].real(), A.Elm[1].pol[0].imag());
	printf("2. %f %f\n", A.Elm[2].pol[0].real(), A.Elm[2].pol[0].imag());
	printf("3. %f %f\n", A.Elm[3].pol[0].real(), A.Elm[3].pol[0].imag());
	printf("1. %f %f\n", A.Elm[1].pol[1].real(), A.Elm[1].pol[1].imag());
	printf("2. %f %f\n", A.Elm[2].pol[1].real(), A.Elm[2].pol[1].imag());
	printf("3. %f %f\n", A.Elm[3].pol[1].real(), A.Elm[3].pol[1].imag());
	printf("1. %f %f\n", B.Elm[1].pol[0].real(), B.Elm[1].pol[0].imag());
	printf("2. %f %f\n", B.Elm[2].pol[0].real(), B.Elm[2].pol[0].imag());
	printf("3. %f %f\n", B.Elm[3].pol[0].real(), B.Elm[3].pol[0].imag());
	printf("1. %f %f\n", B.Elm[1].pol[1].real(), B.Elm[1].pol[1].imag());
	printf("2. %f %f\n", B.Elm[2].pol[1].real(), B.Elm[2].pol[1].imag());
	printf("3. %f %f\n", B.Elm[3].pol[1].real(), B.Elm[3].pol[1].imag());
	*/
	
	//const int NL = (LMAX+1)*(LMAX+1);
	const int NL = 5*5;
	CKCoord3D X[NL];
	CKCoord3D Y[NL];
	CKCoord3D P[NL];
	CCoor3D gkout;
	gkout.x = GKOUT->x;
	gkout.y = GKOUT->y;
	gkout.z = GKOUT->z;
	GK.x = gkout.x;
	GK.y = gkout.y;
	GK.z = gkout.z;
	double r = sqrt(gkout.x*gkout.x + gkout.y*gkout.y + gkout.z*gkout.z);
	gkout.x = gkout.x / r * KAPPA;
	gkout.y = gkout.y / r * KAPPA;
	gkout.z = gkout.z / r * KAPPA;
	GK.x = GK.x / r * KAPPA;
	GK.y = GK.y / r * KAPPA;
	GK.z = GK.z / r * KAPPA;

//	DLMKG(LMAX, 1., GK, 1, KAPPA, D);
	
	c::complex<REAL> SC[2][2];
	for(int j=0; j<2; j++)
	for(int i=0; i<2; i++)
	{
		c::complex<REAL> Eout(0., 0.);
		/*
		SC[j][i] = 
		B.Elm[1].pol[i] * D.Elm[1].pol[j] + 
		B.Elm[2].pol[i] * D.Elm[2].pol[j] + 
		B.Elm[3].pol[i] * D.Elm[3].pol[j];
		*/
		/*
		double I = (conj(Eout)*Eout).real();
		I = sqrt(I) * fabs(GK.z.real()) / (2. * M_PI) / 2.8179403267e-5;
		printf("In:%i Out:%i %e\n", i, j, I);
		*/
	}
	/*
	printf("Intens %f\n", sqrt(
		(
			conj(world->light.EIN.pol[0])*world->light.EIN.pol[0] + 
			conj(world->light.EIN.pol[1])*world->light.EIN.pol[1]
		).real()) );
	
	c::complex<REAL> Ep = SC[0][0] * world->light.EIN.pol[0] + SC[0][1] * world->light.EIN.pol[1];
	c::complex<REAL> Es = SC[1][0] * world->light.EIN.pol[0] + SC[1][1] * world->light.EIN.pol[1];
	//printf("%e %e\n", (conj(Ep)*Ep).real(),(conj(Ep)*Ep).imag());
	double I = (conj(Ep)*Ep + conj(Es)*Es).real();
	I = sqrt(I) * fabs(GK.z.real()) / (2. * M_PI) / 2.8179403267e-5;
	//printf("DLMG SC %f\n", I);
	*/
	//-------------
	/*
	CCoor3D Td;
	CCoor3D Pd;
	CKCoord3D E;
	double theta = acos(gkout.z);
	double phi = atan2(gkout.y, gkout.x);
	
	Pd.x = -sin(phi);
	Pd.y = cos(phi);
	Pd.z = 0.;
	Td.x = cos(theta)*cos(phi);
	Td.y = cos(theta)*sin(phi);
	Td.z = -sin(theta);
	E.x = Pd.y* Pd.x + Td.y*Td.x;
	E.y = Pd.y* Pd.y + Td.y*Td.y;
	E.z = Pd.y* Pd.z + Td.y*Td.z;
	E.x *= 20 * -2.8179403267e-5;
	E.y *= 20 * -2.8179403267e-5;
	E.z *= 20 * -2.8179403267e-5;
	printf("E (%f %f) (%f %f) (%f %f)\n", E.x.real(), E.x.imag(), E.y.real(), E.y.imag(), E.z.real(), E.z.imag());
	*/
	//-------------
	
	Xlm(LMAX, Y, P, X, gkout);
	CKCoord3D Eoutpi;
	CKCoord3D Eoutsigma;
	CKCoord3D Eout;
	c::complex<REAL> pre = -CIMAG;
	int i = 0;
	for(int L=0; L<=LMAX; L++)
	{
	for(int M=-L; M<=L; M++)
	{
		Eoutpi.x    += B.Elm[i].pol[0] * P[i].x;
		Eoutpi.y    += B.Elm[i].pol[0] * P[i].y;
		Eoutpi.z    += B.Elm[i].pol[0] * P[i].z;
		Eoutsigma.x += B.Elm[i].pol[1] * P[i].x;
		Eoutsigma.y += B.Elm[i].pol[1] * P[i].y;
		Eoutsigma.z += B.Elm[i].pol[1] * P[i].z;
		i++;
	}
		pre = pre * -CIMAG;
	}
	Eout.x = world->light.EIN.pol[0] * Eoutpi.x + world->light.EIN.pol[1] * Eoutsigma.x;
	Eout.y = world->light.EIN.pol[0] * Eoutpi.y + world->light.EIN.pol[1] * Eoutsigma.y;
	Eout.z = world->light.EIN.pol[0] * Eoutpi.z + world->light.EIN.pol[1] * Eoutsigma.z;
	
	//cout << "Belm: " << B.Elm[1].pol[1].real() << " " << B.Elm[1].pol[1].imag() << "\n";
	//cout << "Belm: " << B.Elm[2].pol[1].real() << " " << B.Elm[2].pol[1].imag() << "\n";
	//cout << "Belm: " << B.Elm[3].pol[1].real() << " " << B.Elm[3].pol[1].imag() << "\n";
	/*
	printf("light.out %e \n", 
	(conj(Eoutpi.x)*Eoutsigma.x + conj(Eoutpi.y)*Eoutsigma.y + conj(Eoutpi.z)*Eoutsigma.z).real());
	printf("light.out %e \n", 
	((Eoutpi.x)*Eoutsigma.x + (Eoutpi.y)*Eoutsigma.y + (Eoutpi.z)*Eoutsigma.z).real());
	*/
	
	Eout.x = Eout.x / KAPPA;
	Eout.y = Eout.y / KAPPA;
	Eout.z = Eout.z / KAPPA;
	//printf("Eout %f %f %f\n", Eout.x.real(), Eout.y.real(), Eout.z.real());
	
	double a = (conj(Eout.x) * Eout.x + conj(Eout.y) * Eout.y + conj(Eout.z) * Eout.z).real();
	
	lua_pushnumber(L, a);
	return 1;
}

static int lightnewindex(lua_State *L)
{
//	cerr << "newindex\n";
	CString var(lua_tostring(L, 2));
	
	if (var == "theta")
	{
		double x = lua_tonumber(L, 3);
		world->light.THETA = x;
		world->light.CalculateWavevector();
		return 0;
	}
	if (var == "polarization")
	{
		const char* str = lua_tostring(L, 3);
		world->light.POLAR = str[0];
		world->light.CalculateWavevector();
		return 0;
	}
	if (var == "filter")
	{
		const char* str = lua_tostring(L, 3);
		world->light.filterstr = str[0];
		world->light.CalculateWavevector();
		return 0;
	}
	if (var == "out")
	{
		int out = lua_tonumber(L, 3);
		CCoor3D SAin, PAin;
		CCoor3D SAout, PAout;
		// the basis the program is using for sigma and pi
		
		// Sigma
		SAin.x = -sin(world->light.FI * M_PI / 180.);
		SAin.y = cos(world->light.FI * M_PI / 180.);
		SAin.z = 0.;
		// Pi
		PAin.x = cos(world->light.THETA * M_PI / 180.) * cos(world->light.FI * M_PI / 180.);
		PAin.y = cos(world->light.THETA * M_PI / 180.) * sin(world->light.FI * M_PI / 180.);
		PAin.z = -sin(world->light.THETA * M_PI / 180.);

		//printf("Sigma in %f %f %f\n", SAin.x, SAin.y, SAin.z);
		//printf("Pi in %f %f %f\n", PAin.x, PAin.y, PAin.z);
		
		CKCoord3D GK;
		GK = ReciprocalWaveVector(world->light.AK, world->rl.G[out], world->light.KAPPA0);
		GK.z = -GK.z;

		REAL r = sqrt((GK.x*GK.x + GK.y*GK.y + GK.z*GK.z).real());
		REAL thetaout = acos(GK.z.real()/r);
		REAL phiout   = atan2(GK.y.real(), GK.x.real());
		
		//Sigma
		SAout.x = -sin(phiout);
		SAout.y = cos(phiout);
		SAout.z = 0.;
		// Pi
		PAout.x = cos(thetaout) * cos(phiout);
		PAout.y = cos(thetaout) * sin(phiout);
		PAout.z = -sin(thetaout);
		
		/*
		printf("light.out %f %f %f\n", 
		(SA.x*world->light.AK.x + SA.y*world->light.AK.y + SA.z*world->light.AK.z),
		(PA.x*world->light.AK.x + PA.y*world->light.AK.y + PA.z*world->light.AK.z),
		(SA.x*PA.x + SA.y*PA.y + SA.z*PA.z)
		);
		*/
		
		CCoor3D SB; // sigma
		CCoor3D PBin; // pi
		CCoor3D PBout; // pi
		
		//sigma basis b , in and out is the same
		SB.x = world->light.AK.y*GK.z.real() - world->light.AK.z*GK.y.real();
		SB.y = world->light.AK.z*GK.x.real() - world->light.AK.x*GK.z.real();
		SB.z = world->light.AK.x*GK.y.real() - world->light.AK.y*GK.x.real();
		//printf("%f %f %f\n", SB.x, SB.y, SB.z);
		double le = sqrt((SB.x*SB.x + SB.y*SB.y + SB.z*SB.z));
		SB.x = SB.x / le;
		SB.y = SB.y / le;
		SB.z = SB.z / le;
		
		// pi basis b
		PBin.x = world->light.AK.y*SB.z - world->light.AK.z*SB.y;
		PBin.y = world->light.AK.z*SB.x - world->light.AK.x*SB.z;
		PBin.z = world->light.AK.x*SB.y - world->light.AK.y*SB.x;
		le = sqrt(PBin.x*PBin.x + PBin.y*PBin.y + PBin.z*PBin.z);
		PBin.x = PBin.x / le;
		PBin.y = PBin.y / le;
		PBin.z = PBin.z / le;

		PBout.x = GK.y.real()*SB.z - GK.z.real()*SB.y;
		PBout.y = GK.z.real()*SB.x - GK.x.real()*SB.z;
		PBout.z = GK.x.real()*SB.y - GK.y.real()*SB.x;
		le = sqrt(PBout.x*PBout.x + PBout.y*PBout.y + PBout.z*PBout.z);
		PBout.x = PBout.x / le;
		PBout.y = PBout.y / le;
		PBout.z = PBout.z / le;
		
		/*
		printf("light.out %f %f %f\n", 
		(SB.x*world->light.AK.x + SB.y*world->light.AK.y + SB.z*world->light.AK.z),
		(PB.x*world->light.AK.x + PB.y*world->light.AK.y + PB.z*world->light.AK.z),
		(SB.x*PB.x + SB.y*PB.y + SB.z*PB.z)
		);
		*/
		
		double PBPAin = (PBin.x*PAin.x + PBin.y*PAin.y + PBin.z*PAin.z);
		double PBSAin = (PBin.x*SAin.x + PBin.y*SAin.y + PBin.z*SAin.z);
		double PBPAout = (PBout.x*PAout.x + PBout.y*PAout.y + PBout.z*PAout.z);
		double PBSAout = (PBout.x*SAout.x + PBout.y*SAout.y + PBout.z*SAout.z);
		
		double SBPAin  = (SB.x*PAin.x + SB.y*PAin.y + SB.z*PAin.z);
		double SBSAin  = (SB.x*SAin.x + SB.y*SAin.y + SB.z*SAin.z);
		double SBPAout = (SB.x*PAout.x + SB.y*PAout.y + SB.z*PAout.z);
		double SBSAout = (SB.x*SAout.x + SB.y*SAout.y + SB.z*SAout.z);
	
		switch(world->light.POLAR)
		{
			case 'P':
			case 'p':
				world->light.EIN.pol[0] = PBPAin;
				world->light.EIN.pol[1] = PBSAin;
				//printf("pol E pi %f %f %f\n", PBPAin*PAin.x+PBSAin*SAin.x, PBPAin*PAin.y+PBSAin*SAin.y, PBPAin*PAin.z+PBSAin*SAin.z);
				break;
			case 'S':
			case 's':
				world->light.EIN.pol[0] = SBPAin;
				world->light.EIN.pol[1] = SBSAin;
				//printf("pol facts %f %f\n", SBPAin, SBSAin);
				//printf("pol E sigma %f %f %f\n", SBPAin*PAin.x+SBSAin*SAin.x, SBPAin*PAin.y+SBSAin*SAin.y, SBPAin*PAin.z+SBSAin*SAin.z);
				break;
			case 'L':
			case 'l':
				world->light.EIN.pol[0] = (SBPAin + c::complex<REAL>(0., 1.)*PBPAin ) / sqrt(2.);
				world->light.EIN.pol[1] = (SBSAin + c::complex<REAL>(0., 1.)*PBSAin ) / sqrt(2.);
				break;
			case 'R':
			case 'r':
				world->light.EIN.pol[0] = (SBPAin - c::complex<REAL>(0., 1.)*PBPAin ) / sqrt(2.);
				world->light.EIN.pol[1] = (SBSAin - c::complex<REAL>(0., 1.)*PBSAin ) / sqrt(2.);
				break;

			default:
				fprintf(stderr, "Error: Polarization unknown");
				exit(1);
		}
		
		world->light.filter.pol[0] = 1.;
		world->light.filter.pol[1] = 1.;
		switch(world->light.filterstr)
		{
			case 'P':
			case 'p':
				world->light.filter.pol[0] = PBPAout;
				world->light.filter.pol[1] = PBSAout;
				break;
			case 'S':
			case 's':
				world->light.filter.pol[0] = SBPAout;
				world->light.filter.pol[1] = SBSAout;
				break;
			case 'L':
			case 'l':
				world->light.filter.pol[0] = (SBPAout + c::complex<REAL>(0., 1.)*PBPAout ) / sqrt(2.);
				world->light.filter.pol[1] = (SBSAout + c::complex<REAL>(0., 1.)*PBSAout ) / sqrt(2.);
				break;
			case 'R':
			case 'r':
				world->light.filter.pol[0] = (SBPAout - c::complex<REAL>(0., 1.)*PBPAout ) / sqrt(2.);
				world->light.filter.pol[1] = (SBSAout - c::complex<REAL>(0., 1.)*PBSAout ) / sqrt(2.);
				break;
			case 'N':
			case 'n':
				world->light.filter.pol[0] = 1.;
				world->light.filter.pol[1] = 1.;
				break;
			default:
				fprintf(stderr, "Error: Polarization filter unknown");
				exit(1);
		}
		//printf("%f %f %f %f %f\n", (abs(world->light.EIN.pol[0])*abs(world->light.EIN.pol[0]) + abs(world->light.EIN.pol[1])*abs(world->light.EIN.pol[1])), po, pi, so, si);

/*
typedef struct vec3{
	double x;
	double y;
	double z;
} vec3;
*/
		return 0;
	}	
	if (var == "phi")
	{
		double x = lua_tonumber(L, 3);
		world->light.FI = x;
		world->light.CalculateWavevector();
		return 0;
	}
	if (var == "amplitude")
	{
		double x = lua_tonumber(L, 3);
		world->light.amplitude = x;
		world->light.CalculateWavevector();
		return 0;
	}
	if (var == "energy")
	{
		double x = lua_tonumber(L, 3);
		world->light.SetEnergy(x);
		return 0;
	}
	if (var == "k0")
	{
		double x = lua_tonumber(L, 3);
		world->light.SetWavevector(x);
		return 0;
	}
	return luaL_error(L, "Error: Variable not found");
}

static int lightindex(lua_State *L)
{
	CString var(lua_tostring(L, 2));
	
	if (var == "theta")
	{
		lua_pushnumber(L, world->light.THETA);
		return 1;
	}
	if (var == "phi")
	{
		lua_pushnumber(L, world->light.FI);
		return 1;
	}
	if (var == "amplitude")
	{
		lua_pushnumber(L, world->light.amplitude);
		return 1;
	}
	if (var == "k0")
	{
		lua_pushnumber(L, world->light.KAPPA0);
		return 1;
	}	
	if (var == "energy")
	{
		lua_pushnumber(L, world->light.GetEnergy());
		//world->light.SetEnergy(x);
		return 1;
	}

	if (var == "k")
	{
		lua_pop(L,1);
		lua_pop(L,1);
		lua_pushnumber(L, world->light.AK.x);
		lua_pushnumber(L, world->light.AK.y);
		lua_pushnumber(L, world->light.AK.z);
		return vec3new(L);
	}	

	
	return luaL_error(L, "Error: Variable not found");
}

static const struct luaL_Reg light_func2[] = 
{
	{"__index", lightindex},
	{"__newindex", lightnewindex},
	{NULL, NULL}
};

// -------------------------------------------------

static int LightSetThetaPhi(lua_State *L)
{
	const double theta = luaL_checknumber(L, 1);
	const double phi = luaL_checknumber(L, 2);
	world->light.THETA = theta;
	world->light.FI = phi;
	world->light.CalculateWavevector();

	return 0;
}

static int LightSetEnergy(lua_State *L)
{
	const double energy = luaL_checknumber(L, 1);
	world->light.SetEnergy(energy);
	return 0;
}

static int LightGetK(lua_State *L)
{
	lua_pushnumber(L, world->light.AK.x);
	lua_pushnumber(L, world->light.AK.y);
	lua_pushnumber(L, world->light.AK.z);
	return vec3new(L);
}

static int LightSetPolarization(lua_State *L)
{
	const char* str = luaL_checkstring(L, 1);
	world->light.POLAR = str[0];
	world->light.CalculateWavevector();
	
	if (lua_gettop(L) != 2)	return 0;
	const int out = luaL_checknumber(L, 2);

	CCoor3D SAin, PAin;
	CCoor3D SAout, PAout;
	// the basis the program is using for sigma and pi
	
	// Sigma
	SAin.x = -sin(world->light.FI * M_PI / 180.);
	SAin.y = cos(world->light.FI * M_PI / 180.);
	SAin.z = 0.;
	// Pi
	PAin.x = cos(world->light.THETA * M_PI / 180.) * cos(world->light.FI * M_PI / 180.);
	PAin.y = cos(world->light.THETA * M_PI / 180.) * sin(world->light.FI * M_PI / 180.);
	PAin.z = -sin(world->light.THETA * M_PI / 180.);

	//printf("Sigma in %f %f %f\n", SAin.x, SAin.y, SAin.z);
	//printf("Pi in %f %f %f\n", PAin.x, PAin.y, PAin.z);
	
	CKCoord3D GK;
	GK = ReciprocalWaveVector(world->light.AK, world->rl.G[out], world->light.KAPPA0);
	GK.z = -GK.z;

	REAL r = sqrt((GK.x*GK.x + GK.y*GK.y + GK.z*GK.z).real());
	REAL thetaout = acos(GK.z.real()/r);
	REAL phiout   = atan2(GK.y.real(), GK.x.real());
	
	//Sigma
	SAout.x = -sin(phiout);
	SAout.y = cos(phiout);
	SAout.z = 0.;
	// Pi
	PAout.x = cos(thetaout) * cos(phiout);
	PAout.y = cos(thetaout) * sin(phiout);
	PAout.z = -sin(thetaout);
	
	/*
	printf("light.out %f %f %f\n", 
	(SA.x*world->light.AK.x + SA.y*world->light.AK.y + SA.z*world->light.AK.z),
	(PA.x*world->light.AK.x + PA.y*world->light.AK.y + PA.z*world->light.AK.z),
	(SA.x*PA.x + SA.y*PA.y + SA.z*PA.z)
	);
	*/
	
	CCoor3D SB; // sigma
	CCoor3D PBin; // pi
	CCoor3D PBout; // pi
	
	//sigma basis b , in and out is the same
	SB.x = world->light.AK.y*GK.z.real() - world->light.AK.z*GK.y.real();
	SB.y = world->light.AK.z*GK.x.real() - world->light.AK.x*GK.z.real();
	SB.z = world->light.AK.x*GK.y.real() - world->light.AK.y*GK.x.real();
	//printf("%f %f %f\n", SB.x, SB.y, SB.z);
	double le = sqrt((SB.x*SB.x + SB.y*SB.y + SB.z*SB.z));
	SB.x = SB.x / le;
	SB.y = SB.y / le;
	SB.z = SB.z / le;
	
	// pi basis b
	PBin.x = world->light.AK.y*SB.z - world->light.AK.z*SB.y;
	PBin.y = world->light.AK.z*SB.x - world->light.AK.x*SB.z;
	PBin.z = world->light.AK.x*SB.y - world->light.AK.y*SB.x;
	le = sqrt(PBin.x*PBin.x + PBin.y*PBin.y + PBin.z*PBin.z);
	PBin.x = PBin.x / le;
	PBin.y = PBin.y / le;
	PBin.z = PBin.z / le;

	PBout.x = GK.y.real()*SB.z - GK.z.real()*SB.y;
	PBout.y = GK.z.real()*SB.x - GK.x.real()*SB.z;
	PBout.z = GK.x.real()*SB.y - GK.y.real()*SB.x;
	le = sqrt(PBout.x*PBout.x + PBout.y*PBout.y + PBout.z*PBout.z);
	PBout.x = PBout.x / le;
	PBout.y = PBout.y / le;
	PBout.z = PBout.z / le;
	
	/*
	printf("light.out %f %f %f\n", 
	(SB.x*world->light.AK.x + SB.y*world->light.AK.y + SB.z*world->light.AK.z),
	(PB.x*world->light.AK.x + PB.y*world->light.AK.y + PB.z*world->light.AK.z),
	(SB.x*PB.x + SB.y*PB.y + SB.z*PB.z)
	);
	*/
	
	double PBPAin = (PBin.x*PAin.x + PBin.y*PAin.y + PBin.z*PAin.z);
	double PBSAin = (PBin.x*SAin.x + PBin.y*SAin.y + PBin.z*SAin.z);
	double PBPAout = (PBout.x*PAout.x + PBout.y*PAout.y + PBout.z*PAout.z);
	double PBSAout = (PBout.x*SAout.x + PBout.y*SAout.y + PBout.z*SAout.z);
	
	double SBPAin  = (SB.x*PAin.x + SB.y*PAin.y + SB.z*PAin.z);
	double SBSAin  = (SB.x*SAin.x + SB.y*SAin.y + SB.z*SAin.z);
	double SBPAout = (SB.x*PAout.x + SB.y*PAout.y + SB.z*PAout.z);
	double SBSAout = (SB.x*SAout.x + SB.y*SAout.y + SB.z*SAout.z);

	switch(world->light.POLAR)
	{
		case 'P':
		case 'p':
			world->light.EIN.pol[0] = PBPAin;
			world->light.EIN.pol[1] = PBSAin;
			//printf("pol E pi %f %f %f\n", PBPAin*PAin.x+PBSAin*SAin.x, PBPAin*PAin.y+PBSAin*SAin.y, PBPAin*PAin.z+PBSAin*SAin.z);
			break;
		case 'S':
		case 's':
			world->light.EIN.pol[0] = SBPAin;
			world->light.EIN.pol[1] = SBSAin;
			//printf("pol facts %f %f\n", SBPAin, SBSAin);
			//printf("pol E sigma %f %f %f\n", SBPAin*PAin.x+SBSAin*SAin.x, SBPAin*PAin.y+SBSAin*SAin.y, SBPAin*PAin.z+SBSAin*SAin.z);
			break;
		case 'L':
		case 'l':
			world->light.EIN.pol[0] = (SBPAin + c::complex<REAL>(0., 1.)*PBPAin ) / sqrt(2.);
			world->light.EIN.pol[1] = (SBSAin + c::complex<REAL>(0., 1.)*PBSAin ) / sqrt(2.);
			break;
		case 'R':
		case 'r':
			world->light.EIN.pol[0] = (SBPAin - c::complex<REAL>(0., 1.)*PBPAin ) / sqrt(2.);
			world->light.EIN.pol[1] = (SBSAin - c::complex<REAL>(0., 1.)*PBSAin ) / sqrt(2.);
			break;

		default:
			fprintf(stderr, "Error: Polarization unknown");
			exit(1);
	}
	
	world->light.filter.pol[0] = 1.;
	world->light.filter.pol[1] = 1.;
	switch(world->light.filterstr)
	{
		case 'P':
		case 'p':
			world->light.filter.pol[0] = PBPAout;
			world->light.filter.pol[1] = PBSAout;
			break;
		case 'S':
		case 's':
			world->light.filter.pol[0] = SBPAout;
			world->light.filter.pol[1] = SBSAout;
			break;
		case 'L':
		case 'l':
			world->light.filter.pol[0] = (SBPAout + c::complex<REAL>(0., 1.)*PBPAout ) / sqrt(2.);
			world->light.filter.pol[1] = (SBSAout + c::complex<REAL>(0., 1.)*PBSAout ) / sqrt(2.);
			break;
		case 'R':
		case 'r':
			world->light.filter.pol[0] = (SBPAout - c::complex<REAL>(0., 1.)*PBPAout ) / sqrt(2.);
			world->light.filter.pol[1] = (SBSAout - c::complex<REAL>(0., 1.)*PBSAout ) / sqrt(2.);
			break;
		case 'N':
		case 'n':
			world->light.filter.pol[0] = 1.;
			world->light.filter.pol[1] = 1.;
			break;
		default:
			fprintf(stderr, "Error: Polarization filter unknown");
			exit(1);
	}

	return 0;
}

static int LightSetFilter(lua_State *L)
{
	const char* str = luaL_checkstring(L, 1);
	world->light.filterstr = str[0];
	world->light.CalculateWavevector();
	return 0;
}

static int LuaSetThetaPhiHKL(lua_State *L);

static const struct luaL_Reg light_func[] = 
{
	{"SetThetaPhi", LightSetThetaPhi},
	{"SetThetaPhiHKL", LuaSetThetaPhiHKL},
	{"SetPolarization", LightSetPolarization},
	{"SetFilter", LightSetFilter},
	{"SetEnergy", LightSetEnergy},
	{"GetKVector", LightGetK},
	{NULL, NULL}
};

// -------------------------------------------------

static int SlabAdd(lua_State *L)
{
	const int slabs = luaL_checknumber(L, 1); 
	const double thickness = luaL_checknumber(L, 2); 
	for(int i=0; i<slabs; i++)
	{
		world->layer.push_back(CLayer());
		world->layer.last().DT = CCoor3D(0., 0., thickness/2.);
		world->layer.last().DB = CCoor3D(0., 0., thickness/2.);
		world->layer.last().D = thickness;
	}
	return 0;
}

static int SlabClear(lua_State *L)
{
	world->layer.clear();
	return 0;
}

static int SlabSetStructure(lua_State *L)
{
	const char* structure = luaL_checkstring(L, 1); 
	world->multilayer = structure;
	return 0;
}

extern int FindVariable(const CWorld &world, CString variable);

static int SlabCreateCrystal(lua_State *L)
{
	const char* varname = luaL_checkstring(L, 1);
	const char* vararg = luaL_checkstring(L, 2);
	int n = FindVariable(*world, CString(varname));
	CVarargType vt;
	vt.crystal = vararg;
	vt.type = CRYSTAL;
	if (n == -1)
	{
		world->varnames.push_back(varname);
		world->varargs.push_back(vt);
	} else
	{
		world->varnames[n] = varname;
		world->varargs[n] = vt;
	}
	return 0;
}

static int SlabCreateHom(lua_State *L)
{
	const char* varname = luaL_checkstring(L, 1);
	const char* vararg = luaL_checkstring(L, 2);
	int n = FindVariable(*world, CString(varname));
	CVarargType vt;
	vt.crystal = vararg;
	vt.type = HOM;
	if (n == -1)
	{
		world->varnames.push_back(varname);
		world->varargs.push_back(vt);
	} else
	{
		world->varnames[n] = varname;
		world->varargs[n] = vt;
	}
	return 0;
}

static int SlabCreateInterface(lua_State *L)
{
	const char* varname = luaL_checkstring(L, 1);
	const char* vararg1 = luaL_checkstring(L, 2);
	const char* vararg2 = luaL_checkstring(L, 3);
	const double d = luaL_checknumber(L, 4);
	const int nlayers = luaL_checknumber(L, 5);
	const double sigma = luaL_checknumber(L, 6);
	int n = FindVariable(*world, CString(varname));
	CVarargType vt;
	vt.crystal = vararg1;
	vt.crystal2 = vararg2;
	vt.d = d;
	vt.n = nlayers;
	vt.sigma = sigma;
	vt.type = INTERFACE;
	if (n == -1)
	{
		world->varnames.push_back(varname);
		world->varargs.push_back(vt);
	} else
	{
		world->varnames[n] = varname;
		world->varargs[n] = vt;
	}
	return 0;
}

static int SlabAddAtom(lua_State *L)
{
	const int index = luaL_checknumber(L, 1); 
	
	if (!lua_islightuserdata (L, 2))
	{
		//fprintf(stderr, "Error in AddAtom: Second argument is not an atomtype");
		//exit(1);
		return luaL_error(L, "Second argument is not an atomtype");
	}
	CAtom *atom = (CAtom*)lua_touserdata(L, 2);
	vec3 *a = (vec3*)luaL_checkudata(L, 3, "Lua.vec3");
	//printf("%f %f %f\n", a->x, a->y, a->z);
	
	world->layer[index].AddAtom(atom, CCoor3D(a->x, a->y, a->z));
	//world->layer[index].AddAtom(atom, CCoor3D(a->x*cos(M_PI/2.) + a->y*sin(M_PI/2.), a->x * -sin(M_PI/2.) + a->y*cos(M_PI/2.), a->z));
	return 0;
}

static const struct luaL_Reg slabs_func[] = 
{
	{"AddSlabs", SlabAdd},
	{"SetStructure", SlabSetStructure},
	{"CreateCrystal", SlabCreateCrystal},
	{"CreateInterface", SlabCreateInterface},
	{"CreateHom", SlabCreateHom},
	{"AddAtom", SlabAddAtom},
	{"Clear", SlabClear},
	{NULL, NULL}
};

// -------------------------------------------------

static int RLSet(lua_State *L)
{
	int n = lua_gettop(L);
	if (n > 3)
	{
		fprintf(stdout, "Error: You tried to call the function rl.Set with a fourth parameter. This parameter is obsolete.\n");
		fprintf(stdout, "Use rl.AddReciprocalLatticeVector(H, K) to add one index or \n");
		fprintf(stdout, "Use rl.SetRadius(r) to add (h, k) indices in a certain radius\n");
		exit(1);
	}
	
	vec2 *a = (vec2*)luaL_checkudata(L, 1, "Lua.vec2");
	vec2 *b = (vec2*)luaL_checkudata(L, 2, "Lua.vec2");
	double c = luaL_checknumber(L, 3);
	
	//double radius = luaL_checknumber(L, 4);
	world->rl = CReciprocalLattice(CCoor2D(a->x, a->y), CCoor2D(b->x, b->y));
	world->rl.az = c;
	return 0;
}

static int RLAddReciprocalLatticeVector(lua_State *L)
{
    int h = luaL_checknumber(L, 1);
    int k = luaL_checknumber(L, 2);
    world->rl.AddReciprocalLatticeVector(h, k);
    return 0;
}

static int RLSetReciprocalLatticeVector(lua_State *L)
{
    int h = luaL_checknumber(L, 1);
    int k = luaL_checknumber(L, 2);
    world->rl.SetReciprocalLatticeVector(h, k);
    return 0;
}

static int RLSetRadius(lua_State *L)
{
    double r = luaL_checknumber(L, 1);
    world->rl.SetRadius(r);
    return 0;
}

static int RLGetN(lua_State *L)
{
	lua_pushnumber(L, world->rl.G.size());
	return 1;
}

static int RLg(lua_State *L)
{
	int i = luaL_checknumber(L, 1);
	luaL_argcheck(L, 0 <= i && i < world->rl.G.size(), 1, "index out of range");
	lua_pop(L, 1);
	lua_pushnumber(L, world->rl.G[i].x);
	lua_pushnumber(L, world->rl.G[i].y);
	return vec2new(L);
	return 1;
}

static int RLb1(lua_State *L)
{
	lua_pushnumber(L, world->rl.B1.x);
	lua_pushnumber(L, world->rl.B1.y);
	lua_pushnumber(L, 0.);
	return vec3new(L);	
}

static int RLb2(lua_State *L)
{
	lua_pushnumber(L, world->rl.B2.x);
	lua_pushnumber(L, world->rl.B2.y);
	lua_pushnumber(L, 0.);
	return vec3new(L);
}

static int RLb3(lua_State *L)
{
	lua_pushnumber(L, 0.);
	lua_pushnumber(L, 0.);
	lua_pushnumber(L, 2.*M_PI/world->rl.az);
	return vec3new(L);
}

static const struct luaL_Reg rl_func[] = 
{
	{"Set", RLSet},
	{"AddReciprocalLatticeVector", RLAddReciprocalLatticeVector},
	{"SetReciprocalLatticeVector", RLSetReciprocalLatticeVector},
	{"SetRadius", RLSetRadius},
	{"GetN", RLGetN},
	{"g", RLg},
	{"b1", RLb1},
	{"b2", RLb2},
	{"b3", RLb3},
	{NULL, NULL}
};

// -------------------------------------------------
/*
class CAtomUserData
{
	public:
	CAtom *atom;
};
*/
static int NewScatteringFactorAtom(lua_State *L)
{
	double f1 = luaL_checknumber(L, 1);
	double f2 = luaL_checknumber(L, 2);
	//CAtomUserData *atom = (CAtomUserData*)lua_newuserdata(L, sizeof(CAtomUserData));	
	//atom->atom = new CScatteringFactorAtom(c::complex<REAL>(f1, f2));
	lua_pushlightuserdata(L, new CScatteringFactorAtom(c::complex<REAL>(f1, f2)));
	return 1;
}

static int NewFormFactorAtom(lua_State *L)
{
	double f1 = luaL_checknumber(L, 1);
	double f2 = luaL_checknumber(L, 2);
	//CAtomUserData *atom = (CAtomUserData*)lua_newuserdata(L, sizeof(CAtomUserData));	
	//atom->atom = new CScatteringFactorAtom(c::complex<REAL>(f1, f2));
	lua_pushlightuserdata(L, new CFormFactorAtom(c::complex<REAL>(f1, f2)));
	return 1;
}


static int NewScatteringFactorFunctionAtom(lua_State *L)
{
	//double f1 = luaL_checkfunction(L, 1);	
	int ref;
	ref = luaL_ref(L, LUA_REGISTRYINDEX);
	lua_pushlightuserdata(L, new CScatteringFactorFunctionAtom(ref));
	return 1;
}

static int NewMagneticScatteringFactorFunctionAtom(lua_State *L)
{
	int ref, refm;
	refm = luaL_ref(L, LUA_REGISTRYINDEX);
	ref = luaL_ref(L, LUA_REGISTRYINDEX);
	double theta = luaL_checknumber(L, 1);
	double phi = luaL_checknumber(L, 2);
	lua_pushlightuserdata(L, new CMagneticFunctionAtom(ref, refm, theta, phi));
	return 1;
}

static int NewTetragonalFunctionAtom(lua_State *L)
{
	int ref, refm;
	refm = luaL_ref(L, LUA_REGISTRYINDEX);
	ref = luaL_ref(L, LUA_REGISTRYINDEX);
	lua_pushlightuserdata(L, new CTetragonalFunctionAtom(ref, refm));
	return 1;
}



static int NewMagneticAtom(lua_State *L)
{
	double f1 = luaL_checknumber(L, 1);
	double f2 = luaL_checknumber(L, 2);
	double f1m = luaL_checknumber(L, 3);
	double f2m = luaL_checknumber(L, 4);
	double theta = luaL_checknumber(L, 5);
	double phi = luaL_checknumber(L, 6);
	//CAtomUserData *atom = (CAtomUserData*)lua_newuserdata(L, sizeof(CAtomUserData));	
	//atom->atom = new CScatteringFactorAtom(c::complex<REAL>(f1, f2));
	lua_pushlightuserdata(L, new CMagneticAtom(c::complex<REAL>(f1, f2), c::complex<REAL>(f1m, f2m), theta, phi));
	return 1;
}

static int SetMagneticAngle(lua_State *L)
{
        if (!lua_islightuserdata (L, 1))
        {
                return luaL_error(L, "First argument is not an atomtype");
        }
        //CAtom *atom = (CAtom*)lua_touserdata(L, 1)
        CMagneticAtom *atom = (CMagneticAtom*)lua_touserdata(L, 1);

        double theta = luaL_checknumber(L, 2);
        double phi = luaL_checknumber(L, 3);
		atom->SetAngle(theta, phi);
		
        return 0;
}

static int SetMagneticScatteringFactor(lua_State *L)
{
        if (!lua_islightuserdata (L, 1))
        {
                return luaL_error(L, "First argument is not an atomtype");
        }
        //CAtom *atom = (CAtom*)lua_touserdata(L, 1)
        CMagneticAtom *atom = (CMagneticAtom*)lua_touserdata(L, 1);

        double f1m = luaL_checknumber(L, 2);
        double f2m = luaL_checknumber(L, 3);
        atom->Setfm(f1m, f2m);

        return 0;
}

static int NewScatteringFactorFileAtom(lua_State *L)
{
	static int materialindex = 0;
	CString name = "matfactora" + CString(materialindex);
	materialindex++;
	const char* filename = luaL_checkstring(L, 1); // 1. paramter from stack
	//const char* name = luaL_checkstring(L, 1);
	LoadOpticalConstants(name.c_str(), filename);
	lua_pushlightuserdata(L, new CScatteringFactorFileAtom(name.c_str()));
	return 1;
}

static int NewMagneticFileAtom(lua_State *L)
{
	static int materialindex = 0;
	CString name = "matfactormag" + CString(materialindex);
	materialindex++;
	double theta = luaL_checknumber(L, 1);
	double phi = luaL_checknumber(L, 2);
	double scale = luaL_checknumber(L, 5);
	const char* ffilename = luaL_checkstring(L, 3);
	const char* mfilename = luaL_checkstring(L, 4);
	LoadMagneticConstants(name.c_str(), ffilename, mfilename);
	lua_pushlightuserdata(L, new CMagneticFileAtom(name.c_str(), theta, phi, scale));
	return 1;
}

static int NewAtom(lua_State *L)
{
	static int materialindex = 0;
	CString name = "matfactorb" + CString(materialindex);
	materialindex++;

	int n = lua_gettop (L);
	const char* filename = luaL_checkstring(L, 1);
	double q = 0;
	if (n == 2) q = luaL_checknumber(L, 2);
	
	LoadOpticalConstantsFromDatabase(name.c_str(), filename, q);
	
	//const char* name = luaL_checkstring(L, 1);	
	lua_pushlightuserdata(L, new CScatteringFactorFileAtom(name.c_str()));
	return 1;
}

static int NewScatteringTensorFileAtom(lua_State *L)
{
	static int materialindex = 0;
	CString name = "mattensorc" + CString(materialindex);
	materialindex++;
	const char* filename = luaL_checkstring(L, 1); // 1. paramter from stack	
	//const char* name = luaL_checkstring(L, 1);	
	LoadScatteringTensor(name.c_str(), filename);
	lua_pushlightuserdata(L, new CScatteringTensorFileAtom(name.c_str()));
	return 1;
}

static const struct luaL_Reg atom_func[] = 
{
	{"NewScatteringFactorAtom", NewScatteringFactorAtom},
	{"NewFormFactorAtom", NewFormFactorAtom},
	{"NewScatteringFactorFunctionAtom", NewScatteringFactorFunctionAtom},
	{"NewMagneticAtom", NewMagneticAtom},
	{"NewMagneticFileAtom", NewMagneticFileAtom},
	{"NewScatteringFactorFileAtom", NewScatteringFactorFileAtom},
	{"NewMagneticFileAtom", NewMagneticFileAtom},
	{"NewScatteringTensorFileAtom", NewScatteringTensorFileAtom},
	{"NewMagneticFunctionAtom", NewMagneticScatteringFactorFunctionAtom},
	{"NewTetragonalFunctionAtom", NewTetragonalFunctionAtom},
	{"NewAtom", NewAtom},
	{"SetMagneticAngle", SetMagneticAngle},
	{"SetMagneticScatteringFactor", SetMagneticScatteringFactor},
	{NULL, NULL}
};

// -------------------------------------------------

static int LuaPrintStats(lua_State *L)
{
	world->PrintStats();
	return 0;
}

static int LuaSetMultipleScattering(lua_State *L)
{
	if (!check_enum_type(L, "MS", -1))
	{
		return 0;
	}
	const int value = get_enum_value(L, -1);	
	world->msaccuracy = (MultipleScatteringAccuracy)value;
	return 0;
}

static int LuaSetNumericAccuracy(lua_State *L)
{
	if (!check_enum_type(L, "NUM", -1))
	{
		return 0;
	}
	const int value = get_enum_value(L, -1);
	world->numaccuracy = (NumericAccuracy)value;
	return 0;
}

static int LuaSetMatrixAccuracy(lua_State *L)
{
	if (!check_enum_type(L, "M", -1))
	{
		return 0;
	}
	const int value = get_enum_value(L, -1);
	world->matrixaccuracy = (MatrixAccuracy)value;
	return 0;
}

// -----------------------

static int LuaExpandStructure(lua_State *L)
{
	std::vector<int> layeridx;
	BuildLayerList(*world, world->multilayer.c_str(), 0, layeridx);
	world->multilayer = "";
	for(int i=0; i<layeridx.size(); i++)
	{
		world->multilayer += CString(layeridx[0]) + ",";
	}
	cout << world->multilayer << "\n";
        return 0;
}

static int LuaGetLayeredField(lua_State *L)
{
	GetLayeredField(*world);
	return 0;
}

// -----------------------

static int LuaBuildScatterMatrix(lua_State *L)
{
	if (slab != NULL) delete slab;
	slab = new CSlab(world->rl.IGMAX * 2);
	BuildScatterMatrix(*world, *slab);
	world->GetTransRefl(slab);
	return 0;
}

static int LuaSetVolumeCorrection(lua_State *L)
{
	int index = luaL_checknumber(L, 1);
	world->volumecorrection = index;
	return 0;
}


static int LuaExportToReMagX(lua_State *L)
{
	const char* filename = luaL_checkstring(L, 1);
	Export2ReMagX(filename, *world);
	//if (slab != NULL) delete slab;
	//slab = new CSlab(world->rl.IGMAX * 2);
	//BuildScatterMatrix(*world, *slab);
	return 0;
}

double GetRefl(CWorld *world, int index)
{
	const c::complex<REAL> CZERO(0., 0.);
	CVector< c::complex<REAL> > EINCID;
	EINCID.assign(2 * world->rl.IGMAX, CZERO);
	GetIncomingLightasReciprocalLattice(world->rl.IGMAX, EINCID, world->rl, world->light);
		c::complex<REAL> E_SIGMA = CZERO;
		c::complex<REAL> E_PI = CZERO;
		int IGK1 = index*2;
		for (int IGK2=0; IGK2 < 2*world->rl.IGMAX; IGK2++)
		{
			E_SIGMA += slab->QIII[IGK1+0][IGK2] * EINCID[IGK2];
			E_PI    += slab->QIII[IGK1+1][IGK2] * EINCID[IGK2];
		}
		REAL intensity = (E_SIGMA*conj(E_SIGMA) + E_PI*conj(E_PI)).real();				
	return intensity;
}

double GetTrans(CWorld *world, int index)
{
	const c::complex<REAL> CZERO(0., 0.);
	CVector< c::complex<REAL> > EINCID;
	EINCID.assign(2 * world->rl.IGMAX, CZERO);
	GetIncomingLightasReciprocalLattice(world->rl.IGMAX, EINCID, world->rl, world->light);
		c::complex<REAL> E_SIGMA = CZERO;
		c::complex<REAL> E_PI = CZERO;
		int IGK1 = index*2;
		for (int IGK2=0; IGK2 < 2*world->rl.IGMAX; IGK2++)
		{
			E_SIGMA += slab->QI[IGK1+0][IGK2] * EINCID[IGK2];
			E_PI    += slab->QI[IGK1+1][IGK2] * EINCID[IGK2];
		}
		REAL intensity = (E_SIGMA*conj(E_SIGMA) + E_PI*conj(E_PI)).real();				
	return intensity;
}

double GetIncomingEnergy()
{
	const c::complex<REAL> CZERO(0., 0.);
	int IGMAX = world->rl.G.size();
	CVector<CKCoord3D> GKK;
	GKK.assign(IGMAX, CKCoord3D());
	for(int IG1=0; IG1<IGMAX; IG1++)
		GKK[IG1] = ReciprocalWaveVector(world->light.AK, world->rl.G[IG1], world->light.KAPPA0);
	
	CVector< c::complex<REAL> > EINCID;
	EINCID.assign(2 * world->rl.IGMAX, CZERO);
	GetIncomingLightasReciprocalLattice(world->rl.IGMAX, EINCID, world->rl, world->light);

	double DOWN = 0.;
	for (int IGK1=0; IGK1 < 2*world->rl.IGMAX; IGK1++)
	{
		DOWN  += (EINCID[IGK1] * conj(EINCID[IGK1])).real() * -GKK[IGK1/2].z.real();	
	}
	return DOWN;
}

static int LuaGetAbsorbance(lua_State *L)
{
	double REFL = 0.;
	double TRANS = 0.;

	//printf("%i\n", world->rl.IGMAX);

	for(int IG1=0; IG1<world->rl.IGMAX; IG1++)
	{
		REFL += world->r[IG1];
		TRANS += world->t[IG1];
	}
	lua_pushnumber(L, 1. - REFL - TRANS);
	return 1;
	
	/*
	printf("Bis hier hin1\n");
	const c::complex<REAL> CZERO(0., 0.);
	double DOWN = 0.;
	double REFL = 0.;
	double TRANS = 0.;
	
	int IGMAX = world->rl.G.size();
	CVector<CKCoord3D> GKK;
	GKK.assign(IGMAX, CKCoord3D());
	for(int IG1=0; IG1<IGMAX; IG1++)
		GKK[IG1] = ReciprocalWaveVector(world->light.AK, world->rl.G[IG1], world->light.KAPPA0);
	
	printf("Bis hier hin2\n");
	CVector< c::complex<REAL> > EINCID;
	EINCID.assign(2 * world->rl.IGMAX, CZERO);
	GetIncomingLightasReciprocalLattice(world->rl.IGMAX, EINCID, world->rl, world->light);
	
	c::complex<REAL> E_SIGMA = CZERO;
	c::complex<REAL> E_PI = CZERO;
	
	for (int IGK1=0; IGK1 < 2*world->rl.IGMAX; IGK1++)
	{
		DOWN  += (EINCID[IGK1] * conj(EINCID[IGK1])).real() * -GKK[IGK1/2].z.real();	
	}
	printf("Bis hier hin4\n");
	
	for (int IGK1=0; IGK1 < 2*world->rl.IGMAX; IGK1+=2)
	{
		c::complex<REAL> E_SIGMA = CZERO;
		c::complex<REAL> E_PI = CZERO;
		for (int IGK2=0; IGK2 < 2*world->rl.IGMAX; IGK2++)
		{
			E_SIGMA += slab->QIII[IGK1+0][IGK2] * EINCID[IGK2];
			E_PI    += slab->QIII[IGK1+1][IGK2] * EINCID[IGK2];
		}
		REFL += (E_SIGMA*conj(E_SIGMA) + E_PI*conj(E_PI)).real() * -GKK[IGK1/2].z.real();
			printf("Bis hier hin5\n");

		E_SIGMA = CZERO;
		E_PI = CZERO;
		for (int IGK2=0; IGK2 < 2*world->rl.IGMAX; IGK2++)
		{
			E_SIGMA += slab->QI[IGK1+0][IGK2] * EINCID[IGK2];
			E_PI    += slab->QI[IGK1+1][IGK2] * EINCID[IGK2];
		}
		TRANS += (E_SIGMA*conj(E_SIGMA) + E_PI*conj(E_PI)).real() * -GKK[IGK1].z.real();
	}
	lua_pushnumber(L, 1.-REFL/DOWN-TRANS/DOWN);
	return 1;
*/
}


static int LuaGetRefl(lua_State *L)
{
	int index = luaL_checknumber(L, 1);
	luaL_argcheck(L, 0 <= index && index < world->rl.IGMAX, 1, "index out of range");
	//REAL intensity = GetRefl(world, index);
	lua_pushnumber(L, world->r[index]);
	return 1;
}

static int LuaGetTrans(lua_State *L)
{
	int index = luaL_checknumber(L, 1);
	luaL_argcheck(L, 0 <= index && index < world->rl.IGMAX, 1, "index out of range");
	//REAL intensity = GetTrans(world, index);
	lua_pushnumber(L, world->t[index]);
	return 1;
}

static int LuaGetTransK(lua_State *L)
{
	int index = luaL_checknumber(L, 1);
	luaL_argcheck(L, 0 <= index && index < world->rl.IGMAX, 1, "index out of range");
	CKCoord3D GK;
	GK = ReciprocalWaveVector(world->light.AK, world->rl.G[index], world->light.KAPPA0);	
	lua_pop(L, 1);
	lua_pushnumber(L, GK.x.real());
	lua_pushnumber(L, GK.y.real());
	lua_pushnumber(L, GK.z.real());
	return vec3new(L);	
}

static int LuaGetReflK(lua_State *L)
{
	int index = luaL_checknumber(L, 1);
	luaL_argcheck(L, 0 <= index && index < world->rl.IGMAX, 1, "index out of range");
	CKCoord3D GK;
	GK = ReciprocalWaveVector(world->light.AK, world->rl.G[index], world->light.KAPPA0);	
	lua_pop(L, 1);
	lua_pushnumber(L, GK.x.real());
	lua_pushnumber(L, GK.y.real());
	lua_pushnumber(L, -GK.z.real());
	return vec3new(L);	
}

int GetIncomingBeamFromHKL(int h, int k, double l, CLight &light, const CReciprocalLattice &rl);
void ChangeQforScatteringplane(CLight &light, const CCoor3D &out, double q);

static int LauSetThetaHKL(lua_State *L)
{
	int h = luaL_checknumber(L, 1);
	int k = luaL_checknumber(L, 2);
	double l = luaL_checknumber(L, 3);
	int index = GetIncomingBeamFromHKL(h, k, l, world->light, world->rl);
	lua_pushnumber(L, index);
	return 1;
}

void NormVector(REAL &x, REAL &y, REAL &z)
{
	REAL r = sqrt(x*x + y*y + z*z);
	x /= r;
	y /= r;
	z /= r;
}

REAL CrossNorm(REAL &gx, REAL &gy, REAL &gz, REAL &v1x, REAL &v1y, REAL &v1z)
{	
	REAL nx = gy*v1z - gz*v1y;
	REAL ny = gz*v1x - gx*v1z;
	REAL nz = gx*v1y - gy*v1x;
	return nx*nx+ny*ny+nz*nz;
}

int SetThetaPhiHKL(int h, int k, double l, double psi, CLight &light, const CReciprocalLattice &rl)
{
	CCoor3D B3;
	B3.x = 0.0;
	B3.y = 0.0;
	B3.z = REAL(2.*M_PI) / rl.az;

	int index = -1;
	for(int i=0; i<rl.IGMAX; i++)
	{
		if ((world->rl.NT1[i] != h) || (world->rl.NT2[i] != k)) continue;
		index = i;
		break;
	}

	if (index == -1)
	{
		//fprintf(stderr, "Error in HKL: proper index not found\n");
		return index;
	}

	//CCoor3D g;
	REAL gx, gy, gz;
	gx = rl.B1.x * h + rl.B2.x * k + B3.x * l;
	gy = rl.B1.y * h + rl.B2.y * k + B3.y * l;
	gz =      0. * h +      0. * k + B3.z * l;
	REAL k0 = light.KAPPA0;

	REAL v1x = 0.;
	REAL v1y = 1.;
	REAL v1z = 0.;
	if (CrossNorm(gx, gy, gz, v1x, v1y, v1z) < 1e-5) 
	{
		v1x = 1.;
		v1y = 0.;
		v1z = 0.;
	}
	if (CrossNorm(gx, gy, gz, v1x, v1y, v1z) < 1e-5) 
	{
		v1x = 1./sqrt(2.);
		v1y = 1./sqrt(2.);
		v1z = 0.;
	}
	if (CrossNorm(gx, gy, gz, v1x, v1y, v1z) < 1e-5) 
	{
		fprintf(stderr, "Error: Cannot find scattering plane for plane normal %f %f %f\n", gx, gy, gz);
		exit(1);
	}
	
	
	
	/*
	REAL v1x = 1.;
	REAL v1y = 0.;
	REAL v1z = 0.;
*/
	// gram schmidt
	REAL s = (gx*v1x + gy*v1y + gz*v1z) / (gx*gx + gy*gy + gz*gz);
	v1x -= s*gx;
	v1y -= s*gy;
	v1z -= s*gz;
	
	
	REAL v2x = gy*v1z - gz*v1y;
	REAL v2y = gz*v1x - gx*v1z;
	REAL v2z = gx*v1y - gy*v1x;
	REAL v3x = gx;
	REAL v3y = gy;
	REAL v3z = gz;
	NormVector(v1x, v1y, v1z);
	NormVector(v2x, v2y, v2z);
	NormVector(v3x, v3y, v3z);

	REAL a = cos(psi/180.*M_PI);
	REAL b = sin(psi/180.*M_PI);
	
	// vperp and vpar have norm = 1
	REAL vperpx = v3x;
	REAL vperpy = v3y;
	REAL vperpz = v3z;
	REAL vparx = a*v1x + b*v2x;
	REAL vpary = a*v1y + b*v2y;
	REAL vparz = a*v1z + b*v2z;

	// definitions
	// kin = cos(psi)*vpar + sin(psi)*vperp
	// kout = cos(psi)*vpar - sin(psi)*vperp
	REAL sinpsi = gz/k0/2./v3z; // psi in special coordinate system
	if ((sinpsi < -1) || (sinpsi > 1)) return -1;
	
	a = sqrt(1. - sinpsi*sinpsi); // cos
	b = sinpsi;                   // and sin
	
	if ((a*vparz + b*vperpz) < 0) return -1; // transmission
	if ((-a*vparz + b*vperpz) < 0) return -1; // transmission
	REAL px = (a*vparx + b*vperpx);
	REAL py = (a*vpary + b*vperpy);
	REAL pz = (a*vparz + b*vperpz);
	world->light.THETA = acos(-pz)*180./M_PI;
	world->light.FI   = atan2(-py, -px)*180./M_PI;
	world->light.CalculateWavevector();
	return index;
}

static int LuaSetThetaPhiHKL(lua_State *L)
{
	int h = luaL_checknumber(L, 1);
	int k = luaL_checknumber(L, 2);
	double l = luaL_checknumber(L, 3);
	double psi = luaL_checknumber(L, 4);
	int index = SetThetaPhiHKL(h, k, l, psi, world->light, world->rl);
	lua_pushnumber(L, index);
	return 1;
}


static int LuaSetQScatteringPlane(lua_State *L)
{
	vec3 *v = (vec3*)luaL_checkudata(L, 1, "Lua.vec3");
	double q = luaL_checknumber(L, 2);	
	ChangeQforScatteringplane(world->light, CCoor3D(v->x, v->y, v->z), q);
	return 0;
}

// ----------------------------------------------------------

static const struct luaL_Reg quad_func[] = 
{
	{"Scatter", LuaBuildScatterMatrix},
	{"ScatterAtom", LuaAtomScatter},
	{"SetVolumeCorrection", LuaSetVolumeCorrection},

	{"GetReflectedIntensity", LuaGetRefl},
	{"GetTransmittedIntensity", LuaGetTrans},
	{"GetReflectedKVector", LuaGetReflK},
	{"GetTransmittedKVector", LuaGetTransK},
	{"GetAbsorbance", LuaGetAbsorbance},

	{"SetMultipleScattering", LuaSetMultipleScattering},
	{"SetNumericAccuracy", LuaSetNumericAccuracy},
	{"SetMatrixAccuracy", LuaSetMatrixAccuracy},
	{"PrintStatus", LuaPrintStats},

	{NULL, NULL}
};



// ----------------------------------------------------------

class CQueue
{
	public:
	CQueue() {}
	CQueue(int IGKMAX)
	{
		//slab = new CSlab(IGKMAX);
	}
	~CQueue()
	{
		//if (slab != NULL) delete slab;
	}
	CLight light;
	CVector<REAL> r, t;
	CVector<REAL> fields;
};

CVector<CQueue> queue;

static int LuaAddQueue(lua_State *L)
{
	CQueue q(world->rl.IGMAX*2);
	q.light = world->light;
	queue.push_back(q);	
	return 0;
}

static int LuaClearQueue(lua_State *L)
{
	queue.clear();
	return 0;
}

static int LuaRunQueue(lua_State *L)
{
	int n = queue.size();
	CWorld threadworld;
	CSlab *locslab;
	#pragma omp parallel private (threadworld, locslab)
	{
		locslab = new CSlab(world->rl.IGMAX*2);
		threadworld = *world;
		#pragma omp for schedule(dynamic)
		for(int i=0; i<n; i++)
		{
			threadworld.light = queue[i].light;
			threadworld.queueindex = i;
			/*
			if (queue[i].slab == NULL)
			{
				fprintf(stderr, "Error in LuaRunQueue: slab is NULL\n");
				exit(1);
			}
			*/
			BuildScatterMatrix(threadworld, *(locslab));
			threadworld.GetTransRefl(locslab);
			queue[i].r = threadworld.r;
			queue[i].t = threadworld.t;
			printf("finished %i\n", i);
		}
	}
	return 0;
}

static int LuaActiveQueue(lua_State *L)
{
	int index = luaL_checknumber(L, 1);	
	luaL_argcheck(L, 0 <= index && index < queue.size(), 1, "index out of range");
	//if (slab == NULL) slab = new CSlab(world->rl.IGMAX * 2);
	//*slab = *queue[index].slab;
	world->r = queue[index].r;
	world->t = queue[index].t;
	world->fields = queue[index].fields;
	world->light = queue[index].light;
	world->queueindex = index;
	return 0;
}

static int LuaGetQueueN(lua_State *L)
{
	lua_pushnumber(L, queue.size());
	return 1;
}

static int LuaCalcLayeredFieldQueue(lua_State *L)
{
	for(int i=0; i<queue.size(); i++)
	{
		world->fields = queue[i].fields;
		world->light = queue[i].light;
		world->queueindex = i;
		GetLayeredField(*world);
		queue[i].fields = world->fields;
	}
	return 0;
}

static int LuaGetFieldQueue(lua_State *L)
{
	int index = luaL_checknumber(L, 1);
	luaL_argcheck(L, 0 <= index && index < queue.size(), 1, "index out of range");
	int layer = luaL_checknumber(L, 2);
	luaL_argcheck(L, 0 <= layer, 1, "index out of range");
	double Efield = 1;
	if (layer < queue[index].fields.size()) Efield = queue[index].fields[layer];
	lua_pop(L, 2);
	lua_pushnumber(L, Efield);
	return 1;
}


static const struct luaL_Reg queue_func[] = 
{
	{"Add", LuaAddQueue},
	{"Clear", LuaClearQueue},
	{"Run", LuaRunQueue},
	{"Activate", LuaActiveQueue},
	{"GetN", LuaGetQueueN},
	{"CalcFields", LuaCalcLayeredFieldQueue},
	{"GetField", LuaGetFieldQueue},
	{NULL, NULL}
};

// ----------------------------------------------------------

class CLuaFit : public CFit
{
	public:
		CLuaFit():CFit(0) {};
		int ref = -1;
		double Error(CVector<TFitVariable> &vars)
		{
			lua_pop(L, lua_gettop(L));
			
			lua_rawgeti(L, LUA_REGISTRYINDEX, ref);
			
			lua_newtable(L);
			for(int i=0; i<vars.size(); i++)
			{
				lua_pushnumber(L, i);
				lua_pushnumber(L, vars[i].var);
				lua_settable(L, -3);
			}

			if (lua_pcall(L, 1, 1, 0) != 0)
			{
				fprintf(stderr, "Error running lua function: %s", lua_tostring(L, -1));
				/*
				error(L, "error running function `f': %s",
                 lua_tostring(L, -1))
				*/
				exit(1);
			}
			return luaL_checknumber(L, 1);
		}
};

CLuaFit fit;

static int LuaAddFitVariable(lua_State *L)
{
	double init = luaL_checknumber(L, 1);
	double delta = luaL_checknumber(L, 2);
	double lower = luaL_checknumber(L, 3);
	double higher = luaL_checknumber(L, 4);
	
	TFitVariable fv;
	fv.var = init;
	fv.delta = delta;
	fv.lb = lower;
	fv.ub = higher;
	fit.vars.push_back(fv);
	return 0;
}

static int LuaFitClear(lua_State *L)
{
	fit.vars.clear();
	return 0;
}


static int LuaIterate(lua_State *L)
{
	double niter = luaL_checknumber(L, 1);
	fit.BeginFit();
	for(int i=0; i<niter; i++)
		fit.Simplex();

	//fit = CLuaFit();
	return 0;
}

void LMErrorFunc(double *p, double *x, int m, int n, void *adata)
{
	lua_pop(L, lua_gettop(L));

	lua_rawgeti(L, LUA_REGISTRYINDEX, fit.ref);

	lua_newtable(L);
	for(int i=0; i<fit.vars.size(); i++)
	{
		lua_pushnumber(L, i);
		lua_pushnumber(L, p[i]);
		lua_settable(L, -3);
	}

	if (lua_pcall(L, 1, 1, 0) != 0)
	{
		fprintf(stderr, "Error running lua function: %s", lua_tostring(L, -1));
		/*
		error(L, "error running function `f': %s",
        lua_tostring(L, -1))
		*/
		exit(1);
	}
	//printf("stack after call: %i\n", lua_gettop(L));
	
	luaL_checktype(L, 1, LUA_TTABLE);
	int ncheck = lua_rawlen(L, 1);
	if (ncheck != n)
	{
		luaL_error (L, "returned array of error function must have size %d but got %d", n, ncheck);
	}
	
	for(int i=0; i<n; ++i) 
	{
		lua_rawgeti (L, 1, i+1);
		x[i] = luaL_checknumber(L, 2);
		lua_pop(L, 1);
		//printf("%f %i\n", x[i], lua_gettop(L));
	}
	//printf("stack %i\n", lua_gettop(L));

	// pop return table, function and initial table from stack
	lua_pop(L, lua_gettop(L));
	
}

static int LuaLM(lua_State *L)
{
	double info[LM_INFO_SZ];
	double opts[5];

	void *adata = NULL;
	double *work = NULL;
	double *x = NULL;
	int itmax = luaL_checknumber(L, 1);
	int nmeas = luaL_checknumber(L, 2);
	//int nmeas = fit.vars.size();

	opts[0] = 1e-03; // tau or mu
	opts[1] = 1E-17; // eps1
	opts[2] = 1E-17; // eps2
	opts[3] = 1E-17; // eps3
	opts[4] = 1e-6; // delta
	
	double *covar = new double[fit.vars.size() * fit.vars.size()];

	double *p = new double[fit.vars.size()];
    for(int i=0; i<fit.vars.size(); i++) 
	{
        p[i] = fit.vars[i].var;
	}
	int nit = dlevmar_dif(LMErrorFunc,
      p, x, fit.vars.size(), nmeas,
	  itmax, opts, info, work, covar, adata);
	printf("===============================================\n");
	printf(" Levenberg Marquardt report\n");
	printf(" initial ||e||_2: %f\n", info[0]);
	printf(" final   ||e||_2: %f\n", info[1]);
	printf(" final reason %i\n", (int)info[6]);
	printf(" func eval: %i\n", (int)info[7]);
	printf(" jac eval:  %i\n", (int)info[8]);
	
	for(int i=0; i<fit.vars.size(); i++) {
		printf("  var %i: %f +-%f\n", i, p[i], dlevmar_stddev( covar, fit.vars.size(), i) );
	}
	printf("===============================================\n");
	
	delete[] covar;
	delete[] p;
	return 0;
}

static int LuaSetErrorFunction(lua_State *L)
{
	fit.ref = luaL_ref(L, LUA_REGISTRYINDEX);
	return 0;
}

// ----------------------------------------------------------

static const struct luaL_Reg fit_func[] = 
{
	{"AddVariable", LuaAddFitVariable},
	{"SetErrorFunction", LuaSetErrorFunction},
	{"Iterate", LuaIterate},
	{"Clear", LuaFitClear},
	{"LM", LuaLM},
	{NULL, NULL}
};

// ----------------------------------------------------------

#ifdef EMSCRIPTEN
#include <emscripten.h>
/*
size_t fwrite(const void *ptr, size_t size, size_t nmemb,FILE *stream)
{
  EM_ASM_({
    //console.log(Pointer_stringify($0, $1));
    Print(Pointer_stringify($0, $1));
  }, ptr, size*nmemb);
  return 0;
}
*/
// ----------------------------------------
static int LuaDraw (lua_State *L)
{
  EM_ASM(
    PlotDraw(false);
  );
  return 0;
}

static int LuaDrawLog (lua_State *L)
{
  EM_ASM(
    PlotDraw(true);
  );
  return 0;
}

static int LuaSetLabels(lua_State *L)
{
  const char *xl = lua_tostring(L, 1);
  const char *yl = lua_tostring(L, 2);

  EM_ASM_({
    PlotSetLabels(Pointer_stringify($0), Pointer_stringify($1));
  }, xl, yl);
  return 0;
}


static int LuaAddPoint(lua_State *L)
{
 double x = lua_tonumber(L, 1);
 double y = lua_tonumber(L, 2);
  EM_ASM_({
    PlotAddPoint($0, $1);
  }, x, y);
  return 0;
}

static const struct luaL_Reg plot_func[] =
{
      {"Draw", LuaDraw},
      {"DrawLog", LuaDrawLog},
      {"SetLabels", LuaSetLabels},
      {"AddPoint", LuaAddPoint},
      {NULL, NULL}
};

void CreateLua();

extern "C"
{
void ExecLUA(const char *buff)
{
	CWorld _world;

	slab = NULL;
	world = NULL;
	SetWorld(_world);
	world->SetLMAX(1);
	CreateLua();
	fit = CLuaFit();
	
        int error = 0;
        error = 
            luaL_loadbuffer(L, buff, strlen(buff), "line") ||
            lua_pcall(L, 0, 0, 0);
	//fprintf(stderr, "Error %i\n", error);
        if (error)
        {
                fprintf(stderr, "%s\n", lua_tostring(L, -1));
                lua_pop(L, 1);
        }
        lua_close(L);
	fflush(stdout);
	fflush(stderr);
}
}

#endif

// ----------------------------------------------------------

static void stackDump (lua_State *L) {
      int i;
      int top = lua_gettop(L);
      for (i = 1; i <= top; i++) {  /* repeat for each level */
        int t = lua_type(L, i);
        switch (t) {
    
          case LUA_TSTRING:  /* strings */
            printf("`%s'", lua_tostring(L, i));
            break;
    
          case LUA_TBOOLEAN:  /* booleans */
            printf(lua_toboolean(L, i) ? "true" : "false");
            break;
    
          case LUA_TNUMBER:  /* numbers */
            printf("%g", lua_tonumber(L, i));
            break;
    
          default:  /* other values */
            printf("%s", lua_typename(L, t));
            break;
    
        }
        printf("  ");  /* put a separator */
      }
      printf("\n");  /* end the listing */
    }

c::complex<REAL> CallLuaFunction(int ref, double energy, double Efield)
{
	lua_pop(L, lua_gettop(L));

	lua_rawgeti(L, LUA_REGISTRYINDEX, ref);
	lua_pushnumber(L, energy);
	lua_pushnumber(L, Efield);
	//stackDump (L);
	if (lua_pcall(L, 2, 1, 0) != 0)
	{
		fprintf(stderr, "Error running lua function: %s", lua_tostring(L, -1));
        /*
		error(L, "error running function `f': %s",
                 lua_tostring(L, -1))
		*/
		exit(1);
	}
	vec2 *a = (vec2*)luaL_checkudata(L, 1, "Lua.vec2");
	return c::complex<REAL>(a->x, a->y);
}

static int LuaGetLayerIndex(lua_State *L)
{
	lua_pushnumber(L, world->currentlayerindex);
	return 1;
}

// ----------------------------------------------------------

CWorld *tempworld;
void SetWorld(CWorld &_world)
{
	tempworld = world;
	world = &_world;
}

void RestoreWorld(CWorld &_world)
{
	world = tempworld;
}

static int LuaSetLMAX(lua_State *L)
{
	int LMAX = luaL_checknumber(L, 1);
	world->SetLMAX(LMAX);
	return 0;
}

void CreateLua()
{
	L = luaL_newstate();

	luaL_openlibs(L);
	luaopen_vec3(L);
	luaopen_vec2(L);
    //luaopen_table(L);

	add_enum_to_lua(L, "MS",
		"KINEMATIC", MultipleScatteringAccuracy::KINEMATIC,
		"NONE", MultipleScatteringAccuracy::NONE,
		"LAYER", MultipleScatteringAccuracy::LAYER,
		"INTERATOMIC", MultipleScatteringAccuracy::INTERATOMIC, 
		""
	);

	add_enum_to_lua(L, "NUM",
		"FLOAT", NumericAccuracy::NUMFLOAT,
		"DOUBLE", NumericAccuracy::NUMDOUBLE,
		"LONGDOUBLE", NumericAccuracy::NUMLONGDOUBLE,
		"QUAD", NumericAccuracy::NUMQUAD,
		""
	);

	add_enum_to_lua(L, "M",
		"DIAGONAL", MatrixAccuracy::MDIAGONAL,
		"FULL", MatrixAccuracy::MFULL,
		"BLAS", MatrixAccuracy::MBLAS,
		""
	);

/*
	luaL_newmetatable(L, "lightname");
	luaL_setfuncs(L, light_func2, 0);
	lua_newuserdata(L, 0);
	luaL_setmetatable(L, "lightname");
	lua_setglobal(L, "light2");
	//lua_pop(L,1);
	//lua_pop(L,1);
	

	luaL_newlib (L, fit_func);
	lua_setglobal(L, "fit");

	luaL_newlib (L, slabs_func);
	lua_setglobal(L, "slabs");

	luaL_newlib (L, atom_func);
	lua_setglobal(L, "atom");

	luaL_newlib (L, rl_func);
	lua_setglobal(L, "rl");
	
	luaL_newlib (L, queue_func);
	lua_setglobal(L, "queue");
*/
	
// ----
// generate global object "quad"
	luaL_newlib (L, quad_func);

	lua_pushstring(L, "light");
	luaL_newlib (L, light_func);
	lua_settable(L, -3);

	lua_pushstring(L, "atom");
	luaL_newlib (L, atom_func);
	lua_settable(L, -3);
	
	lua_pushstring(L, "slab");
	luaL_newlib (L, slabs_func);
	lua_settable(L, -3);

	lua_pushstring(L, "slab");
	luaL_newlib (L, slabs_func);
	lua_settable(L, -3);

	lua_pushstring(L, "rl");
	luaL_newlib (L, rl_func);
	lua_settable(L, -3);

	lua_pushstring(L, "queue");
	luaL_newlib (L, queue_func);
	lua_settable(L, -3);

	lua_pushstring(L, "fit");
	luaL_newlib (L, fit_func);
	lua_settable(L, -3);

	/*
	lua_pushstring(L, "light2");
	lua_getglobal(L, "light");
	lua_settable(L, -3);
*/	
	lua_setglobal(L, "quad");

// ----

	lua_pushcfunction(L, LuaGetAbsorbance);
	lua_setglobal(L, "GetAbsorbance");
	
	lua_pushcfunction(L, LuaSetLMAX);
	lua_setglobal(L, "SetLMAX");
	

#ifdef EMSCRIPTEN
	luaL_newlib (L, plot_func);
	lua_setglobal(L, "plot");
#endif	
/*
	lua_pushcfunction(L, LuaPrintStats);
	lua_setglobal(L, "PrintStats");

	lua_pushcfunction(L, LuaSetMultipleScattering);
	lua_setglobal(L, "SetMultipleScattering");

	lua_pushcfunction(L, LuaSetNumericAccuracy);
	lua_setglobal(L, "SetNumericAccuracy");	

	lua_pushcfunction(L, LuaSetMatrixAccuracy);
	lua_setglobal(L, "SetMatrixAccuracy");
	
	lua_pushcfunction(L, LuaBuildScatterMatrix);
	lua_setglobal(L, "BuildScatterMatrix");

	lua_pushcfunction(L, LuaSetVolumeCorrection);
	lua_setglobal(L, "SetVolumeCorrection");
*/

	lua_pushcfunction(L, LuaGetLayeredField);
	lua_setglobal(L, "GetLayeredField");

	lua_pushcfunction(L, LuaExpandStructure);
	lua_setglobal(L, "ExpandStructure");

	lua_pushcfunction(L, LuaGetLayerIndex);
	lua_setglobal(L, "GetLayerIndex");
	
/*
	lua_pushcfunction(L, LuaGetRefl);
	lua_setglobal(L, "GetRefl");
	
	lua_pushcfunction(L, LuaGetTrans);
	lua_setglobal(L, "GetTrans");

	lua_pushcfunction(L, LuaGetTransK);
	lua_setglobal(L, "GetTransK");

	lua_pushcfunction(L, LuaGetReflK);
	lua_setglobal(L, "GetReflK");
*/
/*
	lua_pushcfunction(L, LauSetThetaHKL);
	lua_setglobal(L, "SetThetaHKL");

	lua_pushcfunction(L, LauSetThetaPhiHKL);
	lua_setglobal(L, "SetThetaPhiHKL");
*/
/*
	lua_pushcfunction(L, LuaSetQScatteringPlane);
	lua_setglobal(L, "SetQScatteringPlane");	

	lua_pushcfunction(L, LuaExportToReMagX);
	lua_setglobal(L, "ExportToReMagX");	
*/
/*
	lua_pushcfunction(L, LuaAtomScatter);
	lua_setglobal(L, "Scatter");	
*/

}


int ExecuteFile(char *filename, CWorld &_world)
{
	slab = NULL;
	world = NULL;
	SetWorld(_world);
	int result;
	CreateLua();
	fit = CLuaFit();
	
	result = luaL_loadfile(L, filename);
	if (result)
	{
		fprintf(stderr, "Error while loading the script: %s\n", lua_tostring(L, -1));
		lua_close(L);
		return 1;
	}
	
	result = lua_pcall(L, 0, 0, 0);
	if (result)
	{
		fprintf(stderr, "Error while executing the script: %s\n", lua_tostring(L, -1));
		lua_close(L);
		return 1;
	}
	lua_close(L);
	return 0;
}




