extern "C"
{
#include"lua/src/lua.h"
#include "lua/src/lauxlib.h"
#include "lua/src/lualib.h"
}

#include"scripttable.h"
// -------------------------------------------------

int tablenew(lua_State *L)
{
	luatable *res;
	
	if (lua_isstring(L, 1) && lua_isnumber(L, 2))
	{
		const char *s = lua_tostring(L, 1);
		const int n = lua_tonumber(L, 2);
		res = (luatable*)lua_newuserdata(L, sizeof(luatable));
        res->table = new CTable(n);
        bool ret = res->table->LoadAsciiTable(CString(s));
        if (!ret) {
            return luaL_error(L, "Cannot load file: %s\n", s);
        }
		luaL_setmetatable(L, "Lua.table");
		return 1;
	}
	else
	{
		return luaL_error(L, "Error Table must contain a string and a number");
	}
}

static int LuaGetTableEntries(lua_State *L)
{
    luatable *a = (luatable*)luaL_checkudata(L, 1, "Lua.table");
    const double energy = luaL_checknumber(L, 2);
    CTableEntry e = a->table->Get(energy);
    for(int i=0; i<a->table->ncolumns; i++) {
        lua_pushnumber(L, e.y[i]);
    }
    //printf("Get %i %f\n", lua_gettop (L), energy);
    return a->table->ncolumns;
}

static int tableindex(lua_State *L)
{
	luatable *a = (luatable*)luaL_checkudata(L, 1, "Lua.table");
	const char *str = lua_tostring(L, 2);
    if (strcmp(str, "Get") == 0) {
        lua_pushcfunction(L, LuaGetTableEntries);
        return 1;
    }

/*
	switch(str[0])
	{
		case 'x':
			lua_pushnumber(L, a->x);
			return 1;
		case 'y':
			lua_pushnumber(L, a->y);	
			return 1;
	}
*/
	return luaL_error(L, "Error table variable not found");;
}

static int tablenewindex(lua_State *L)
{
/*
	vec2 *a = (vec2*)luaL_checkudata(L, 1, "Lua.table");
	const char *str = lua_tostring(L, 2);
	double v = lua_tonumber(L, 3);

	switch(str[0])
	{
		case 'x':
			a->x = v;
			return 0;
		case 'y':
			a->y = v;	
			return 0;
	}
	//fprintf(stderr, "Index %s\n", str);
	return luaL_error(L, "Error vec3 variable not found");;
*/
	return 0;
}

static const struct luaL_Reg table_meta [] = {
	{"__index", tableindex},
	//{"__newindex", tablenewindex},
	{NULL, NULL}
};

int luaopen_table (lua_State *L) 
{
	luaL_newmetatable(L, "Lua.table");
	luaL_setfuncs(L, table_meta, 0);
	lua_pushcfunction(L, tablenew);
	lua_setglobal(L, "OCTable");
	return 1;
}


