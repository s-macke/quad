#ifndef SCRIPTVEC_H
#define SCRIPTVEC_H

typedef struct vec2{
	double x;
	double y;
} vec2;

typedef struct vec3{
	double x;
	double y;
	double z;
} vec3;


int luaopen_vec3 (lua_State *L);
int luaopen_vec2 (lua_State *L);
int vec3new(lua_State *L);
int vec2new(lua_State *L);


#endif
