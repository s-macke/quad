#ifndef SCRIPT_H
#define SCRIPT_H

#include"global.h"
#include"world.h"

	int ExecuteFile(char *filename, CWorld &world);
	c::complex<REAL> CallLuaFunction(int ref, double energy, double Efield);
	void SetWorld(CWorld &_world);
	void RestoreWorld(CWorld &_world);

#endif