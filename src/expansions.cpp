#include "global.h"
#include "functions.h"

#define LMINDEX(L, M) ((L)*(L) + (L) + (M))

//=======================================================================  
//C     ------------------------------------------------------------------  
//C     THIS ROUTINE CALCULATES THE EXPANSION COEFFICIENTS 'AE,AH' OF AN  
//C     INCIDENT PLANE ELECTROMAGNETIC WAVE OF WAVE VECTOR  'KAPPA' WITH  
//C     COMPONENTS PARALLEL TO THE SURFACE EQUAL TO   '(GK(1),GK(2))'.  
//C     ------------------------------------------------------------------  
void PLW(const c::complex<REAL> &KAPPA, const CKCoord3D &GK, const int &LMAX, CExpansionCoefficients &A)
{
	int M, II, L, I, K;
	REAL B, SIGNUS;
	c::complex<REAL> CT, ST, CF; // spherical coordinates of GK
	c::complex<REAL> CC, CC1, Z1, Z2, Z3;

	const int LMAXARRAY = 7;
	c::complex<REAL> YLM[(LMAXARRAY+1)*(LMAXARRAY+1)]; // save all spherical harmonics till L=LMAX
   	const c::complex<REAL> CZERO(0., 0.), CONE(1., 0.), CI(0., 1.);
//C-----------------------------------------------------------------------

	A.Elm[0].pol[0] = CZERO;
	A.Elm[0].pol[1] = CZERO;
	A.Hlm[0].pol[0] = CZERO;
	A.Hlm[0].pol[1] = CZERO;
	
	// calculate angles for vector GK.x, GK.y and GK.z, the length of this vector is KAPPA
	SPHRM4(YLM, GK, KAPPA, LMAX, CT, ST, CF);
	
	II = 1;
	CC = c::complex<REAL>(REAL(4.)*M_PI, 0.);
	SIGNUS = -1.;
	for(L=1; L<=LMAX; L++)
	{
		CC = CC * CI;
		B = REAL(L * (L + 1));
		CC1 = CC / sqrt(B);
		for(M=-L; M<=L; M++)
		{
			SIGNUS = -SIGNUS;
			if (abs(M+1) <= L)
			{
				//I = L*L + L - M; //l, -m-1
				I = LMINDEX(L, -M-1);
				Z1 = CC1*sqrt(REAL((L-M)*(L+M+1)))*YLM[I]/REAL(2.);
			} else Z1 = CZERO;

			if (abs(M-1) <= L)
			{
				//I = L*L + L - M + 2; //l, -m+1
				I = LMINDEX(L, -M+1);		
				Z2 = CC1 * sqrt(REAL((L+M)*(L-M+1)))*YLM[I]/REAL(2.);
			} else Z2 = CZERO;
	
			//I = L*L + L - M + 1; // l, -m ??
			I = LMINDEX(L, -M);
			
			Z3 = CC1 * REAL(M) * YLM[I];
			A.Elm[II].pol[0] =  SIGNUS * CI*(CF*Z1 - conj(CF)*Z2);
			A.Hlm[II].pol[1] =  SIGNUS * CI*(CF*Z1 - conj(CF)*Z2);
			A.Elm[II].pol[1] = -SIGNUS * (CT*CF*Z1+ST*Z3 + CT*conj(CF)*Z2);
			A.Hlm[II].pol[0] =  SIGNUS * (CT*CF*Z1+ST*Z3 + CT*conj(CF)*Z2);
			//printf("L=%i, M=%i, AE[%i] = %e %e\n", L, M, II, AE[II].sigma.real(), AE[II].sigma.imag());
			II++;
		}
    }
}

// calcuate periodic plane of atoms plane wave expansion for all reciprocal lattice constants
//C=======================================================================  
//C     ------------------------------------------------------------------  
//C     THIS SUBROUTINE CALCULATES THE COEFFICIENTS DLM(KG)  
//C     ------------------------------------------------------------------  
//C  
void DLMKG(
	const int LMAX,
	const REAL A0,
	const CKCoord3D &GK,
	const REAL &SIGNUS,
	const c::complex<REAL> &KAPPA,
	CExpansionCoefficients &D, bool volumecorrection)
{
//C ..  PARAMETER STATEMENTS  ..  
	const int LMAXARRAY = 7;
	const int LMAX1DARRAY = LMAXARRAY+1;
	const int LM1SQDARRAY = LMAX1DARRAY*LMAX1DARRAY;

	const int LMAX1D = LMAX+1;
	const int LM1SQD = LMAX1D*LMAX1D;

//C ..  ARRAY ARGUMENTS  ..  
	//c::complex<*16 DLME(2,LM1SQD),DLMH(2,LM1SQD),GK(3)  

//C  .. LOCAL SCALARS ..  

	int K, II, L, M;
	REAL ALPHA, BETA;
	c::complex<REAL> C0, CC, COEF, Z1, Z2, Z3;
	c::complex<REAL> CT, ST, CF;
	
	c::complex<REAL> YLM[LM1SQDARRAY];

// C  .. DATA STATEMENTS .
	const c::complex<REAL> CZERO(0., 0.), CONE(1., 0.), CI(0., 1.);

//C     ------------------------------------------------------------------  
	
	D.Elm[0].pol[0] = CZERO;
	D.Hlm[0].pol[0] = CZERO;
	D.Elm[0].pol[1] = CZERO;
	D.Hlm[0].pol[1] = CZERO;
	
	if (abs(GK.z) < EMACH)
	{
		fprintf(stderr, "Error in DLMKG: wavevector component is too close to machine accuracy\n");
		exit(1);
/*
	101 FORMAT(13X,'FATAL ERROR FROM DLMKG:'/3X,'GK(3) IS TOO SMALL.' 
     & /3X,'GIVE A SMALL BUT NONZERO VALUE FOR "EPSILON"'/3X,  
     & 'IN THE DATA STATEMENT OF THE MAIN PROGRAM.' 
     & /3X,'THIS DEFINES A SMALL IMAGINARY PART' 
     & /3X,'IN THE FREQUENCY OR WAVELENGTH VALUE.') 
	
		WRITE(7,101)
		STOP
*/
	}

	if (volumecorrection)
	{
		C0 = REAL(2.) * M_PI / (KAPPA * A0 * GK.z * SIGNUS);
	} else
	{
		C0 = REAL(2.) * M_PI / (KAPPA * A0 * SIGNUS);
	}
	SPHRM4(YLM, GK, KAPPA, LMAX, CT, ST, CF);

	II = 0;
	CC = CONE;
	for(L=1; L<=LMAX; L++)
	{
		CC = CC / CI;
		COEF = C0 * CC / sqrt(REAL(L*(L+1)));
		for(M=-L; M<=L; M++)
		{
			II++;
			
			if (abs(M+1) <= L)
			{
				//I = L*L+L+M+2;
				ALPHA = sqrt(REAL((L-M) * (L+M+1))) / REAL(2.);
				Z1 = ALPHA*YLM[LMINDEX(L, M+1)] * conj(CF);
			} else Z1 = CZERO;
			if (abs(M-1) <= L)
			{
				BETA  = sqrt(REAL((L+M) * (L-M+1))) / REAL(2.);
				//I = L*L + L + M;
				Z2 = BETA*YLM[LMINDEX(L, M-1)] * CF;
			} else Z2 = CZERO;
			//I = L*L+L+M+1;
			Z3 = REAL(M) * YLM[LMINDEX(L, M)];
			D.Hlm[II].pol[0] = COEF*(CT*Z2 - ST*Z3 + CT*Z1);
			D.Hlm[II].pol[1] = COEF*CI*(Z2 - Z1);
			D.Elm[II].pol[0] = D.Hlm[II].pol[1];
			D.Elm[II].pol[1] = -D.Hlm[II].pol[0];
		}
	}
}

