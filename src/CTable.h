#ifndef CTABLE_H
#define CTABLE_H

#include"CVector.h"
#include"CString.h"

#ifdef __GNUC__
#include <pthread.h>
#endif

// why do I define it here ???
#ifdef USECPP11
#define M_PI 3.14159265358979323846
#endif

class CMutex
{
	public:

CMutex()
{
#ifdef __GNUC__
//	pthread_spin_init(&tablemutex, PTHREAD_PROCESS_SHARED);
//	tablemutex2 = PTHREAD_MUTEX_INITIALIZER;
	pthread_mutex_init(&tablemutex2, NULL);
	initialized = true;
#else
	initialized = false;
#endif
}

const CMutex& operator=(const CMutex& right)
{
#ifdef __GNUC__
	if (initialized)
	{
	    pthread_mutex_destroy(&tablemutex2);
	}
	pthread_mutex_init(&tablemutex2, NULL);
	initialized = true;
#else
	initialized = false;
#endif
	return *this;
}

CMutex(const CMutex &m)
{
#ifdef __GNUC__
//	pthread_spin_init(&tablemutex, PTHREAD_PROCESS_SHARED);
//	tablemutex2 = PTHREAD_MUTEX_INITIALIZER;
	pthread_mutex_init(&tablemutex2, NULL);
	initialized = true;
#else
	initialized = false;
#endif
}


~CMutex()
{
#ifdef __GNUC__
	pthread_mutex_destroy(&tablemutex2);
#endif
}

void inline Lock()
{
#ifdef __GNUC__
//	pthread_spin_lock(&tablemutex);
if (!initialized)
{
	pthread_mutex_init(&tablemutex2, NULL);
	initialized = true;
}
	pthread_mutex_lock(&tablemutex2);

#endif
}

void inline Unlock()
{
#ifdef __GNUC__
//	pthread_spin_lock(&tablemutex);
	pthread_mutex_unlock(&tablemutex2);
#endif
}


#ifdef __GNUC__
	pthread_mutex_t tablemutex2;
//	pthread_spinlock_t tablemutex;
#else
	int tablemutex_dummy1;
#endif
	int initialized;

private:

// non copyable
CMutex(const CMutex &&m);
const CMutex& operator=(const CMutex&& right);
};

class CTableEntry
{
	public:
	double x;
	//CVector<double> y;
//	double y[6];
	double y[20];
};

// CTable implements a  x y y y y .... table
// x and y are mainly used for energy and optical constants respectively

class CTable
{
public:
	CTable(int _ncolumns);
	CTable();

	bool LoadAsciiTable(const CString x);
	bool SaveAsciiTable(const CString x);

//	CVector<double> Get(double x);
	CTableEntry Get(double x);

	bool WithinRange(double x) const;
	void Sort();
	void DeleteRegion(double min, double max);
	void DeleteDistance(double d);
	void KK(double Z, int src=1, int dest=0);
	void LocalKK(int index, double beta, int dest=0);
	void BetaToF2();
	void FFToDB();

	void Reduce(double minx, double maxx, double dx, int npoints);
	double FindLargestError(CTable &tm, double stepsize);
	void Insert(const CTable &t);
	void Merge2(const CTable &t); // The name of this function is Merge2 because there was another function called Merge with similar functionality. This forces to check the code

	void Swap(int m, int n);
	void Quicksort(int m, int n);

	CVector<CTableEntry> entry;

	int ncolumns;
	int index;
	CMutex mutex;
};






#endif
