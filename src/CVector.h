#ifndef CVECTOR_H
#define CVECTOR_H

#include "stdlib.h"
#include "stdio.h"
//include <move>
#include <iostream>   // for std::move

//extern int memorycounterequal;
//extern int memorycounterassign;
//extern int memorycounterreserve;
/*
#ifdef __GNUC__
#include <pthread.h>
extern pthread_mutex_t vectorcountermutex;
#endif
*/

template<class T>
class CVector
{
public:

	CVector()
	{
		create();
	}

	~CVector()
	{
		clear();
	}

	void SetConst()
	{
		if (isconst) return;
		isconst = true;
		reference = new int;
		*reference = 1;
	}

	void clear()
	{
		n = 0;
		nbuf = 0;
		if (isconst)
		{
			(*reference)--;
			if (*reference > 0) return;
			isconst = false;
			delete reference;
		}
		if (buf == NULL) return;
		delete[] buf;
		buf = NULL;
	}

	void clear_but_dont_destroy()
	{
		if (isconst)
		{
			fprintf(stderr, "Error: Invalid function in CVector (clear_but_dont_destroy)\n");
			exit(1);
		}
		n = 0;
	}

	bool inline empty() const
	{
		return (n == 0);
	}

	void assign(const int nn, const T &x)
	{
		if (isconst)
		{
			fprintf(stderr, "Error: Invalid function in CVector (assign)\n");
			exit(1);
		}

		if (nbuf < nn)
		{
			if (buf != NULL) delete[] buf;
			nbuf = nn;
			buf = new T[nbuf];
/*
#ifdef __GNUC__
	pthread_mutex_lock(&vectorcountermutex);
#endif
*/
//			memorycounterassign++;
/*
#ifdef __GNUC__
	pthread_mutex_unlock(&vectorcountermutex);
#endif
			  */
			if (buf == NULL) exit(1);
		}


		n = nn;
		for(int i=0; i<n; i++) buf[i] = x;
	}

	unsigned int size() const
	{
		return n;
	}

	// assignment operator
	 const CVector& operator=(CVector const& right)
	 {
		if ((&right) == this) return *this;
		clear();
		if (right.empty()) return *this;

		if (right.isconst)
		{
			isconst = true;
			nbuf = right.size();
			buf = right.buf;
			n = right.size();
			reference = right.reference;
			(*reference)++;
			return *this;
		}

		if (nbuf < right.size())
		{
			if (buf != NULL) delete[] buf;
			nbuf = right.size();
			buf = new T[nbuf];
/*
#ifdef __GNUC__
	pthread_mutex_lock(&vectorcountermutex);
#endif
 */
//			memorycounterequal++;
 /*
 #ifdef __GNUC__
	pthread_mutex_unlock(&vectorcountermutex);
#endif
*/
			if (buf == NULL) exit(1);
		}

		n = right.size();
		for(int i=0; i<n; i++)
			buf[i] = right[i];

		return *this;
	 }

	// move assignment operator
	 const CVector& operator=(CVector&& right)
	 {
		 if ((&right) == this) return *this;

		clear();
		if (right.empty()) return *this;

		isconst = right.isconst;
		reference = right.reference;
		nbuf = right.nbuf;
		buf = right.buf;
		n = right.n;

		 right.buf = NULL;
		 right.isconst = false;
		 right.reference = NULL;
		 return *this;
	 }

	// move constructor
	 CVector( CVector&& right)
	 {
		if ((&right) == this) return;
		create();
		if (right.empty()) return;

		isconst = right.isconst;
		reference = right.reference;
		nbuf = right.nbuf;
		buf = right.buf;
		n = right.n;

		right.buf = NULL;
		right.isconst = false;
		right.reference = NULL;
		right.buf = NULL;
	}

	// copy constructor
	CVector( CVector const& right)
	{
		if ((&right) == this) return;
		create();
		if (right.empty()) return;

		if (right.isconst)
		{
			isconst = true;
			nbuf = right.size();
			buf = right.buf;
			n = right.size();
			reference = right.reference;
			(*reference)++;
			return;
		}

		nbuf = right.size();
		buf = new T[nbuf];
/*
#ifdef __GNUC__
	pthread_mutex_lock(&vectorcountermutex);
#endif
 */
//			memorycounterreserve++;
 /*
 #ifdef __GNUC__
	pthread_mutex_unlock(&vectorcountermutex);
#endif
*/
		if (buf == NULL) exit(1);

		n = right.size();
		for(int i=0; i<n; i++)
			buf[i] = right.buf[i];
	}

    inline T& last() const
    {
        return buf[n-1];
	}


	inline T& operator[](int i)
	{
/*
		if ((i < 0) || (i > n-1))
		{
			fprintf(stderr, "controlled segmentation fault\n");
			*(int*)0 = 0; // controlled segmentation fault
		}
*/
		return buf[i];
	}

	inline const T& operator[](int i) const
	{
/*
		if ((i < 0) || (i > n-1))
		{
			fprintf(stderr, "controlled segmentation fault\n");
			*(int*)0 = 0; // controlled segmentation fault
		}
*/
		return buf[i];
	}

	void Reserve(int newnbuf)
	{
		if (isconst)
		{
			fprintf(stderr, "Error: Invalid function in CVector (push_back)\n");
			exit(1);
		}
		if (newnbuf < n) return;
		if (nbuf >= newnbuf) return;

		T *newbuf = new T[newnbuf];
	/*
	#ifdef __GNUC__
	pthread_mutex_lock(&vectorcountermutex);
#endif
}*/

//			memorycounterreserve++;
/*
#ifdef __GNUC__
	pthread_mutex_unlock(&vectorcountermutex);
#endif
*/

#ifdef USECPP11
		for(int i=0; i<n; i++)
			newbuf[i] = std::move(buf[i]);
#else
		for(int i=0; i<n; i++)
			newbuf[i] = buf[i];
#endif

		if (buf != NULL) delete[] buf;

		buf = newbuf;
		nbuf = newnbuf;
	}

	void Implement( int count, CVector const& right)
	{
		// do a little bit more
		Reserve((n + right.size()*count)+16);

		for(int i=0; i<count; i++)
		for(int j=0; j<right.size(); j++)
		{
			buf[n] = right.buf[j];
			n++;
		}

	}


	void push_back(const T &x)
	{
		if (isconst)
		{
			fprintf(stderr, "Error: Invalid function in CVector (push_back)\n");
			exit(1);
		}
		//printf("push bis hier1\n");
		if (nbuf > n)
		{
			buf[n] = x;
			n++;
			return;
		}

		int newnbuf = nbuf*3/2 + 16;
		Reserve(newnbuf);
		buf[n] = x;
		n++;

	}

	void erase(int j)
	{
		if (isconst)
		{
			fprintf(stderr, "Error: Invalid function in CVector (erase)\n");
			exit(1);
		}

		if ((j >= n) && (j<0)) return;

		for(int i=j; i<n-1; i++)
            buf[i] = buf[i+1];

        n--;
	}

	void Setn(int nn)
	{
		Reserve(nn);
		n = nn;
	}

/*
    void insert(int j, const T &x)
    {
        if ((j >= n) && (j<0)) return;


        for(int i=j; i<n-1; i++)
            buf[i] = buf[i+1];

        n--;
    }
*/

//   template<class T>
   class Iterator
   {
   public:
      Iterator(T* p) : node(p) {}
	  Iterator() : node(NULL) {}
     ~Iterator() {}

      // The assignment and relational operators are straightforward
      Iterator& operator=(Iterator const& other)
      {
         node = other.node;
         return(*this);
      }

      bool operator==(const Iterator& other)
      {
         return(node == other.node);
      }

      bool operator!=(const Iterator& other)
      {
         return(node != other.node);
      }

      // Update my state such that I refer to the next element in the
      // SQueue.
      Iterator& operator++()
      {
		 node++;
         return (*this);
      }

      Iterator& operator--()
      {
         node--;
         return (*this);
      }

/*
      Iterator& operator++(int)
      {
		 Iterator tmp(*this);
         ++(*this);
         return tmp;
      }
*/

	  // Return a reference to the value in the node.  I do this instead
      // of returning by value so a caller can update the value in the
      // node directly.
      T& operator*()
      {
         return *node;
      }

      T* operator->()
      {
         return node;
      }

   private:
      T* node;
   };


   Iterator begin() const
   {
      return Iterator(&buf[0]);
   }

   Iterator end() const
   {
      return Iterator(&buf[n]);
   }

//friend class CVector;
private:

	void create()
	{
		n = 0;
		nbuf = 0;
		isconst = false;
		reference = NULL;
		buf = NULL;
	}

	int n;
	int nbuf;
	int isconst;
    int *reference;
    T *buf;
};


#endif
