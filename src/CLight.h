#ifndef CLIGHT_H
#define CLIGHT_H

#include "global.h"

class CLight
{
	public:
//incoming light
	char POLAR;
	char filterstr;
//incoming light definition 2
	REAL THETA, FI;
	CPolar EIN;
	CPolar filter;
	REAL amplitude;
	
	REAL KAPPA0; // length of wave vector
	CCoor3D AK; // wave vector
	int out;
	
	CLight()
	{
		POLAR = 'S';
		filterstr = 'N';
		THETA = 0;
		FI = 0;
		KAPPA0 = 1;
		amplitude = 1;
		CalculateWavevector();
		out = 0;
		filter.pol[0] = 1.;
		filter.pol[1] = 1.;
	}

	void SetEnergy(REAL E) // in eV
	{
		REAL lambda = 12398.326 / E; // in A
		KAPPA0 = 2.*M_PI / lambda;
		CalculateWavevector();
	}

	double GetEnergy()
	{
		REAL lambda = 2.*M_PI / KAPPA0;
		return 12398.326 / lambda; 
	}
	
	void SetWavevector(REAL k) // in 1/A
	{
		KAPPA0 = k;
		CalculateWavevector();
	}
	
	void CalculateWavevector()
	{
		switch(POLAR)
		{
			case 'P':
			case 'p':
				EIN.pol[0] = 1.;
				EIN.pol[1] = 0.;
				break;
			case 'S':
			case 's':
				EIN.pol[0] = 0.;
				EIN.pol[1] = 1.;
				break;
			case 'L':
			case 'l':
				EIN.pol[0] = 1./sqrt(2.);
				EIN.pol[1] = c::complex<REAL>(0., 1.)/sqrt(REAL(2.));
				break;
			case 'R':
			case 'r':
				EIN.pol[0] = 1./sqrt(2.);
				EIN.pol[1] = -c::complex<REAL>(0., 1.)/sqrt(REAL(2.));
				break;

			default:
				fprintf(stderr, "Error: Polarization unknown");
				exit(1);
		}
		EIN.pol[0] *= amplitude;
		EIN.pol[1] *= amplitude;
		
		switch(filterstr)
		{
			case 'P':
			case 'p':
				filter.pol[0] = 1.;
				filter.pol[1] = 0.;
				break;
			case 'S':
			case 's':
				filter.pol[0] = 0.;
				filter.pol[1] = 1.;
				break;
			case 'L':
			case 'l':
				filter.pol[0] = 1./sqrt(2.);
				filter.pol[1] = c::complex<REAL>(0., 1.)/sqrt(REAL(2.));
				break;
			case 'R':
			case 'r':
				filter.pol[0] = 1./sqrt(2.);
				filter.pol[1] = -c::complex<REAL>(0., 1.)/sqrt(REAL(2.));
				break;
			case 'n':
			case 'N':
				filter.pol[0] = 1.;
				filter.pol[1] = 1.;
				break;
			default:
				fprintf(stderr, "Error: Polarization filter unknown");
				exit(1);
		}
		
		//	if (KSCAN == 1) KAPPA0 = c::complex<REAL>(KAPPA0, 0.);  
		//	if (KSCAN == 2) KAPPA0 = c::complex<REAL>(2. * PI / KAPPA0, 0.);

		AK.x = KAPPA0 * sin(THETA * M_PI / 180.) * cos(FI * M_PI / 180.);
		AK.y = KAPPA0 * sin(THETA * M_PI / 180.) * sin(FI * M_PI / 180.);
		AK.z = KAPPA0 * cos(THETA * M_PI / 180.);
		/*
		REAL AKXY = AK.x * AK.x + AK.y * AK.y;
		AK.z = KAPPA0 * KAPPA0 - AKXY;
		if (AK.z < 0.)
		{
			fprintf(stderr, "IMPROPER INCIDENT WAVE\n");
			cerr << "theta: " << THETA << " phi: " << FI << "\n";
			exit(1);
		}
		AK.z = sqrt(AK.z);
		*/
		
	}
};

#endif