#ifndef MULTILAYER_H
#define MULTILAYER_H

#include"global.h"
#include"world.h"
#include<vector>

enum TOKEN{VARIABLE, NUMBER, END, NUMBERMUL, UNKNOWN};
TOKEN ReadNextToken(const char *str, int &pos, CString &var, int &number);

void BuildScatterMatrix(CWorld &world, CSlab &slab);
void GetLayeredField(CWorld &world);
int BuildLayerList(const CWorld &world, const char *str, int pos, std::vector<int> &layeridx);

//void BuildScatterMatrix(const int LMAX, const CVector<CLayer> &layer, const CReciprocalLattice &rl, const CLight &light, CSlab &slab, const REAL *ELM, const char* str);

#endif