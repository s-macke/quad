#include"multilayer.h"
#include<vector>

TOKEN ReadNextToken(const char *str, int &pos, CString &var, int &number)
{
	var.clear();
	number = 0;
	TOKEN tok = END;
	while(1)
	{
		//printf("sign: %c %i\n", str[pos], pos);
		if (str[pos] == 0) 
		{
			if (tok == NUMBER) number = atoi(var.c_str());
			return tok; // don't move the pointer. We have to exit every recursive function
		}
		if (str[pos] == ',')
		{
			if (tok == NUMBER) number = atoi(var.c_str());
			if (tok == END) // could happen for "1), 2". Maybe a new token should be defined here
			{
				//fprintf(stderr, "Error: Unexcpected ',' in %s at position %i\n", str, pos);
				//throw 1;
				tok = UNKNOWN;
			}
			pos++;
			return tok;
		}
		
		if (str[pos] == ')')
		{
			if (tok == NUMBER) number = atoi(var.c_str());
			if (tok == END) pos++;
			return tok;
		}
		
		if ((str[pos] == '*')) 
		{
			if (tok != NUMBER)
			{
				fprintf(stderr, "Error: Cannot parse number int %s at position %i\n", str, pos-1);
				throw 1;
			}
			tok = NUMBERMUL;
		}
		if ((str[pos] == '('))
		{
			if (tok == END)
			{
				number = 1;
			} else
			if (tok != NUMBERMUL)
			{
				fprintf(stderr, "Error: Expect '*' in %s at position %i\n", str, pos-1);
				throw 1;
			} else
				number = atoi(var.c_str());
			pos++;
			return NUMBERMUL;
		}
		if (
			((str[pos] >= '0') && (str[pos] <= '9')) ||
			((str[pos] >= 'A') && (str[pos] <= 'Z')) ||
			((str[pos] >= 'a') && (str[pos] <= 'z')) ||
			(str[pos] == '_')
		)
		{
			if (var.empty())
			{
				if ((str[pos] >= '0') && (str[pos] <= '9')) tok = NUMBER; else tok = VARIABLE;
			}
			var += str[pos];
		}
		pos++;
	}
}

int FindVariable(const CWorld &world, CString variable)
{
	for(int i=0; i<world.varnames.size(); i++)
		if (variable == world.varnames[i]) return i;
	return -1;
}

int Interpret(/*const*/ CWorld &world, const char *str, int pos, CSlab &slabL, CVector<CKCoord3D> &GKK)
{
	CString estr;
	int number, n;
	int multiplicator = 1;
	//CSlab slabL(world.rl.IGMAX*2);
	CSlab slabR(world.rl.IGMAX*2);
	int idx = 0;
	slabR.SetAccuracy(world.msaccuracy, world.matrixaccuracy);
	bool first = true;
	REAL field = 1;

	while(1)
	{
		TOKEN tok = ReadNextToken(str, pos, estr, number);
		//printf("tok %i %s %i\n", tok, estr.c_str(), number);
		switch(tok)
		{
			case END:
				return pos;

			case VARIABLE:
				n = FindVariable(world, estr);
				if (n == -1)
				{
					fprintf(stderr, "Error: Variable %s not found\n", estr.c_str());
					throw 1;
				}
				if (first) {slabL = world.varslabs[n]; first = false;} else slabL.PAIR(world.varslabs[n], GKK);
				break;

			case NUMBER:
				if ((number < 0) || (number >= world.layer.size()))
				{
					fprintf(stderr, "Error while parsing structure: Index %s out of range\n", estr.c_str());
					throw 1;
					//return pos;
				}
				world.currentlayerindex = idx;
				if (world.fields.size() != 0) field = world.fields[idx];
				if (slabR.msaccuracy == INTERATOMIC)
					PCSLAB(world.LMAX, world.layer[number], field, world.light.KAPPA0, world.light.AK, world.rl, world.ELM, slabR, world.volumecorrection);
				else
					PCSLABEASY(world.LMAX, world.layer[number], field, world.light.KAPPA0, world.light.AK, world.rl, world.ELM, slabR, world.volumecorrection);
				
				slabR.ztop = 0;
				slabR.zbottom = 0;
				slabR.AddPropagator(world.layer[number].DT, world.layer[number].DB, GKK);
				if (first) {slabL = slabR; first = false;} else slabL.PAIR(slabR, GKK);
				idx++;
				break;

			case NUMBERMUL:
				multiplicator = number;
				pos = Interpret(world, str, pos, slabR, GKK);
				slabR.MultiplyPair(multiplicator, GKK);
				if (first) {slabL = slabR; first=false;} else slabL.PAIR(slabR, GKK);
				break;
			default:
				break;
		}
	}

	return pos;
}

int BuildHomLayer(const CWorld &world, const char *str, int pos, c::complex<REAL> &fsum, REAL &thickness, CVector<CKCoord3D> &GKK)
{
	CString estr;
	int number, n;
	while(1)
	{
		TOKEN tok = ReadNextToken(str, pos, estr, number);
		//printf("homslab tok %i %s %i\n", tok, estr.c_str(), number);
		switch(tok)
		{
			case END:
				return pos;

			case VARIABLE:
				n = FindVariable(world, estr);
				if (n == -1)
				{
					fprintf(stderr, "Error: Variable %s not found\n", estr.c_str());
					throw 1;
				}
				if (world.varargs[n].type != CRYSTAL)
				{
					fprintf(stderr, "Error: Building homogeneous slabs by using homogeneous slabs is not supported yet.\n");
					throw 1;
				}
				BuildHomLayer(world, world.varargs[n].crystal.c_str(), 0, fsum, thickness, GKK);
				break;

			case NUMBER:
				if ((number < 0) || (number >= world.layer.size()))
				{
					fprintf(stderr, "Error while parsing structure: Index %s out of range\n", estr.c_str());
					throw 1;
				}
				//PCSLABEASY(world.LMAX, world.layer[number], world.light.KAPPA0, world.light.AK, world.rl, world.ELM, slabR, world.volumecorrection);
				{
					const CLayer *l = &world.layer[number];
					c::complex<REAL> TE[2];
					c::complex<REAL> TH[2];
					for(int atomidx=0; atomidx<l->atoms.size(); atomidx++)
					{
						CAtom *atom = dynamic_cast<CAtom*>(l->atoms[atomidx]->Clone());
						atom->Set(world.LMAX, world.light.KAPPA0, 1.);
						atom->GetIsotropicScatterMatrix(TE, TH, 1);
						fsum += TE[1];
						//printf("TE: %e %e\n", TE[1].real(), TE[1].imag());
						delete atom;
					}
					thickness += l->D;
					//printf("thickness %e totalthickness %e\n", l->D, thickness);
				}
				
				break;

			case NUMBERMUL:
			{
				c::complex<REAL> ftemp = 0.;
				REAL thicknesstemp = 0.;
				pos = BuildHomLayer(world, str, pos, ftemp, thicknesstemp, GKK);
				fsum += ftemp*REAL(number);
				thickness += thicknesstemp*REAL(number);
			}
				break;
			default:
				break;
		}
	}
return pos;
}


int BuildLayerList(const CWorld &world, const char *str, int pos, std::vector<int> &layeridx)
{
	CString estr;
	int number, n;

	while(1)
	{
		TOKEN tok = ReadNextToken(str, pos, estr, number);
		switch(tok)
		{
			case END:
				return pos;

			case VARIABLE:
				fprintf(stderr, "Error: Variables are currently not supported in this mode");
				throw 1;
				break;

			case NUMBER:
				if ((number < 0) || (number >= world.layer.size()))
				{
					fprintf(stderr, "Error while parsing structure: Index %s out of range\n", estr.c_str());
					throw 1;
					//return pos;
				}
				layeridx.push_back(number);
				break;

			case NUMBERMUL:
			{
				int multiplicator = number;
				std::vector<int> templayeridx;
				pos = BuildLayerList(world, str, pos, templayeridx);
				for(int i=0; i<multiplicator; i++)
				{
					layeridx.insert( layeridx.end(), templayeridx.begin(), templayeridx.end() );
				}
				break;
			}
			default:
				break;
		}
	}

}

REAL GetTotalField(CWorld &world, const CVector< c::complex<REAL> > &EINCID)
{
	const c::complex<REAL> CZERO(0., 0.);
	c::complex<REAL> E_SIGMA = CZERO;
	c::complex<REAL> E_PI = CZERO;
	for (int IGK2=0; IGK2 < 2*world.rl.IGMAX; IGK2+=2)
	{
		E_SIGMA += EINCID[IGK2];
		E_PI    += EINCID[IGK2+1];
	}
	REAL intensity = (E_SIGMA*conj(E_SIGMA)+ E_PI*conj(E_PI)).real();
	return sqrt(intensity);
}

void GetLayeredField(CWorld &world)
{
	CVector< c::complex<REAL> > EINCID;
	CVector< c::complex<REAL> > OUTCID;
	const c::complex<REAL> CZERO(0., 0.);

	CSlab slab(world.rl.IGMAX*2);
	slab.SetAccuracy(world.msaccuracy, world.matrixaccuracy);

	std::vector<int> layeridx;
	BuildLayerList(world, world.multilayer.c_str(), 0, layeridx);
	//printf("number of total layers: %i\n", layeridx.size());
	
	EINCID.assign(2 * world.rl.IGMAX, CZERO);
	OUTCID.assign(2 * world.rl.IGMAX, CZERO);
	GetIncomingLightasReciprocalLattice(world.rl.IGMAX, EINCID, world.rl, world.light);

	CVector<CKCoord3D> GKK;
	GKK.assign(world.rl.IGMAX, CKCoord3D());
	for(int IG1=0; IG1<world.rl.IGMAX; IG1++)
		GKK[IG1] = ReciprocalWaveVector(world.light.AK, world.rl.G[IG1], world.light.KAPPA0);

	world.fields.assign(layeridx.size(), 1.);
	for(int i=layeridx.size()-1; i>=0; i--)
	{
		REAL Eabs = GetTotalField(world, EINCID);
		world.fields[i] = Eabs;
		world.currentlayerindex = i;
		//printf("layer %i %i %e\n", layeridx[i], i, Eabs);
		if (slab.msaccuracy == INTERATOMIC) 
			PCSLAB(world.LMAX, world.layer[layeridx[i]], world.fields[i], world.light.KAPPA0, world.light.AK, world.rl, world.ELM, slab, world.volumecorrection);
		else
			PCSLABEASY(world.LMAX, world.layer[layeridx[i]], world.fields[i], world.light.KAPPA0, world.light.AK, world.rl, world.ELM, slab, world.volumecorrection);
		slab.AddPropagator(world.layer[layeridx[i]].DT, world.layer[layeridx[i]].DB, GKK);

		for (int IGK1=0; IGK1 < 2*world.rl.IGMAX; IGK1++)
		{
			OUTCID[IGK1] = CZERO;
			for (int IGK2=0; IGK2 < 2*world.rl.IGMAX; IGK2++)
			{
				OUTCID[IGK1] += slab.QI[IGK1][IGK2] * EINCID[IGK2];
			}
		}
		EINCID = OUTCID;
	}
}

void BuildScatterMatrix(CWorld &world, CSlab &slab)
{
	
	c::complex<REAL> CI(REAL(0.), REAL(1.));
		c::complex<REAL> eps;
		c::complex<REAL> eps2;
		c::complex<REAL> leps;
		REAL totalthickness;
		const REAL r0 = 2.8179403267e-5; // in A
		const c::complex<REAL> pre1 = -CI * world.light.KAPPA0 * ( REAL(2.)/REAL(3.) * r0 );
		const c::complex<REAL> pre2 = REAL(4.) * M_PI * r0 / (world.light.KAPPA0*world.light.KAPPA0);
		const REAL f = 1./sqrt(2.);
	CVector<CKCoord3D> GKK;
	GKK.assign(world.rl.IGMAX, CKCoord3D());
	for(int IG1=0; IG1<world.rl.IGMAX; IG1++)
		GKK[IG1] = ReciprocalWaveVector(world.light.AK, world.rl.G[IG1], world.light.KAPPA0);

	world.varslabs.assign(world.varnames.size(), CSlab(world.rl.IGMAX*2));
	for(int i=0; i<world.varnames.size(); i++)
	{
		world.varslabs[i].SetAccuracy(world.msaccuracy, world.matrixaccuracy);
		switch(world.varargs[i].type)
		{
			case CRYSTAL:
				world.varslabs[i].Identity();
				Interpret(world, world.varargs[i].crystal.c_str(), 0, world.varslabs[i], GKK);
			break;
			case HOM:
				eps = 0.;
				totalthickness = 0.;
				BuildHomLayer(world, world.varargs[i].crystal.c_str(), 0, eps, totalthickness, GKK);
				if (totalthickness != 0.) eps = eps / (totalthickness * world.rl.A0);
				eps *= (pre2/pre1); eps = conj(eps); eps = REAL(1.) - eps;
				HOSLAB(world.rl.IGMAX, 
				world.light.KAPPA0,
				sqrt(eps) * world.light.KAPPA0,
				world.light.KAPPA0,
				world.light.AK, 
				world.rl.G,
				CCoor3D(),
				CCoor3D(),
				totalthickness,
				0.,0.,
				world.varslabs[i]);
				//printf("%e %e\n", eps.real(), eps.imag());
			break;
			
			case INTERFACE:
				eps = 0.;
				totalthickness = 0.;
				BuildHomLayer(world, world.varargs[i].crystal.c_str(), 0, eps, totalthickness, GKK);
				if (totalthickness != 0.) eps = eps / (totalthickness * world.rl.A0);
				eps *= (pre2/pre1); eps = conj(eps); eps = REAL(1.) - eps;
				
				eps2 = 0.;
				totalthickness = 0.;
				BuildHomLayer(world, world.varargs[i].crystal2.c_str(), 0, eps2, totalthickness, GKK);
				if (totalthickness != 0.) eps2 = eps2 / (totalthickness * world.rl.A0);
				eps2 *= (pre2/pre1); eps2 = conj(eps2); eps2 = REAL(1.) - eps2;
				
				REAL dslab = world.varargs[i].d / world.varargs[i].n;
				REAL x = -world.varargs[i].d * 0.5  + dslab*0.5;
				world.varslabs[i].Identity();
				for(int j=0; j<world.varargs[i].n; j++)
				{
					leps = eps + (eps2 - eps) * ((erf(x / world.varargs[i].sigma * f) + 1.) * 0.5);
					c::complex<double> kappamiddle = sqrt(leps) * world.light.KAPPA0;
					leps = eps + (eps2 - eps) * ((erf((x-dslab) / world.varargs[i].sigma * f) + 1.) * 0.5);
					c::complex<double> kappatop = sqrt(leps) * world.light.KAPPA0;
					leps = eps + (eps2 - eps) * ((erf((x+dslab) / world.varargs[i].sigma * f) + 1.) * 0.5);
					c::complex<double> kappabottom = sqrt(leps) * world.light.KAPPA0;
					if (j == 0) kappatop = world.light.KAPPA0;
					if (j == world.varargs[i].n-1) kappabottom = world.light.KAPPA0;
					
					HOSLAB(world.rl.IGMAX,
						kappatop,  // vacuum
						kappamiddle, // film
						kappabottom, // vacuum
						world.light.AK,
						world.rl.G,
						CCoor3D(),
						CCoor3D(),
						dslab,
						0.,0.,
						slab);
						world.varslabs[i].PAIR(slab, GKK);
					//printf("%f %i\n", x, world.varargs[i].n);
					x += dslab;
				}
				
				//printf("%e %e %e %e %e %e\n", eps.real(), eps.imag(), eps2.real(), eps2.imag(), world.varargs[i].d, world.varargs[i].sigma);
				
				/*
				HOSLAB(world.rl.IGMAX, 
				world.light.KAPPA0,  // vacuum
				sqrt(eps) * world.light.KAPPA0, // film1
				sqrt(eps2) * world.light.KAPPA0, // film2
				world.light.AK,
				world.rl.G,
				CCoor3D(),
				CCoor3D(),
				world.varargs[i].d*0.5, 
				0.,world.varargs[i].sigma,
				world.varslabs[i]);
				
				HOSLAB(world.rl.IGMAX, 
				sqrt(eps) * world.light.KAPPA0,
				sqrt(eps2) * world.light.KAPPA0,
				world.light.KAPPA0,
				world.light.AK, 
				world.rl.G,
				CCoor3D(),
				CCoor3D(),
				world.varargs[i].d*0.5, 
				world.varargs[i].sigma,0.,
				slab); // At this moment we can use the variable slab
				world.varslabs[i].PAIR(slab, GKK);
				*/
			break;
		}
	}

/*
	CWorld world;
	world.LMAX = LMAX;
	world.layer = layer;
	world.rl = rl;
	world.light = light;
	world.ELM = ELM;
*/
	slab.Identity();
	
	slab.SetAccuracy(world.msaccuracy, world.matrixaccuracy);
	try
	{
		Interpret(world, world.multilayer.c_str(), 0, slab, GKK);
	} catch(...)
	{
		fprintf(stderr, "Error in BuildScatterMatrix: Could not interpret script\n");
		//exit(1);
	}
	
	//printf("%f %f\n", slab.ztop, slab.zbottom);
	//printf("multilayer finished\n");
	/*
	CSlab slabR(IGKMAX);
	CSlab slabW(IGKMAX);
	PCSLABEASY(LMAX, layer[0], light.KAPPA0, light.AK, rl, ELM, slabL, world.volumecorrection);
*/
}


