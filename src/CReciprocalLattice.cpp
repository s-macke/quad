#include"global.h"

#include"CReciprocalLattice.h"

//     --------------------------------------------------------------  
//     GIVEN A TWO DIMENSIONAL BRAVAIS LATTICE WITH PRIMITIVE VECTORS  
//     (A(1),A(2)) , (B(1),B(2)) , DEFINED SO THAT 'B' IS LONGER THAN  
//     'A' AND THEIR SCALAR PRODUCT IS POSITIVE,THIS ROUTINE CALCULA-  
//     TES THE 'IMAX' LATTICE VECTORS: NTA(I) * A + NTB(I) * B,HAVING  
//     LENGTH 'VECMOD(I)' LESS THAN 'RMAX'.  
//     --------------------------------------------------------------  
	void LAT2D(CCoor2D A, CCoor2D B, const REAL RMAX, int &IMAX, CVector<int> &NTA, CVector<int> &NTB, CVector<REAL> &VECMOD)
	{
		int I, NA, NB, NA0, J, NMA, NMB, IORD;  
		REAL RMAX2, SP, AMOD2, BMOD2, VMOD2, VM;
	
		RMAX2 = RMAX * RMAX;
	  
// ***  CHECK IF PRIMITIVE VECTORS HAVE POSITIVE SCALAR PRODUCT  
	SP = A.x * B.x + A.y * B.y;  
	if (SP < -1e-06) 
	{
		B.x = -B.x;
		B.y = -B.y;
		SP = -SP;  
		//WRITE(6,100) A(1),A(2),B(1),B(2)  
	}

// ***  CHECK IF 'B' IS LONGER THAN 'A'  

	AMOD2 = A.x * A.x + A.y * A.y;
	BMOD2 = B.x * B.x + B.y * B.y;
	if (BMOD2 < AMOD2)
	{
		//WRITE(6,101);
		CCoor2D	dum;	
		dum = A;
		A = B;
		B = dum;
		
		REAL DUM;
		DUM = AMOD2;
		AMOD2 = BMOD2;  
		BMOD2 = DUM;  
	}
                   
	I = 0;
	NB = 0;
pos9:

	if ((NB*NB*BMOD2) > RMAX2)  goto pos8;  

	NA = 0;
    //7 CONTINUE
	while(1)
	{
		VMOD2 = NA * NA * AMOD2 + NB * NB * BMOD2 + 2 * NA * NB * SP;
		if (VMOD2 > RMAX2) break;
		NTA.push_back(NA);
		NTB.push_back(NB);
		VECMOD.push_back(sqrt(VMOD2));
		I++;
		
		if ((NA == 0) && (NB == 0)) //GO TO 11
		{
			NA = NA + 1;
			continue;
		}		
		NTA.push_back(-NA);
		NTB.push_back(-NB);
		VECMOD.push_back(sqrt(VMOD2));
		I++;  
   // 11 
		NA++;
    } //  GO TO 7
	

//pos6: 
      NB++;
      goto pos9;
pos8:
	NA0 = SP / AMOD2 + 1;
	NB = 1;
pos5:
	if ((NB * NB * (BMOD2 - SP * SP / AMOD2)) > RMAX2) goto pos4;  
	NA = NA0;
  
pos3:
	VMOD2 = NA * NA * AMOD2 + NB * NB * BMOD2 - 2 * NA * NB * SP;

	if (VMOD2 <= RMAX2) // GO TO 2???
	{
		NTA.push_back(NA);
		NTB.push_back(-NB);
		VECMOD.push_back(sqrt(VMOD2));
		I++;
		NTA.push_back(-NA);
		NTB.push_back(NB);
		VECMOD.push_back(sqrt(VMOD2));
		I++;
		NA++;
		goto pos3;
    }
	
//pos2:	
	NA = NA0 - 1;
pos1:
	VMOD2 = NA * NA * AMOD2 + NB * NB * BMOD2 - 2 * NA * NB * SP;
	if ((VMOD2 > RMAX2) ||(NA <= 0))  goto pos12;
		NTA.push_back(NA);
		NTB.push_back(-NB);
		VECMOD.push_back(sqrt(VMOD2));
		I++;
		NTA.push_back(-NA);
		NTB.push_back(NB);	
		VECMOD.push_back(sqrt(VMOD2));
		I++;
		NA--;
		goto pos1;
pos12:
	NB++;
	goto pos5;
pos4:
	IMAX = I;

	for (IORD=1; IORD<=IMAX; IORD++)
	{
		VM = VECMOD[IORD-1];
		for(I=IMAX; I>=IORD; I--)
		{	
			if (VECMOD[I-1] > VM) continue; 
			VM = VECMOD[I-1];
			VECMOD[I-1] = VECMOD[IORD-1];
			VECMOD[IORD-1] = VM;
			NMA = NTA[I-1];
			NTA[I-1] = NTA[IORD-1];
			NTA[IORD-1] = NMA;
			NMB = NTB[I-1];
			NTB[I-1] = NTB[IORD-1];
			NTB[IORD-1] = NMB;

		}
	}
	return;
/*
   13 IMAX=I-1  
      WRITE(6,102) IMAX  
      DO 14 I=1,IMAX  
      WRITE(6,103) I,NTA(I),A(1),A(2),NTB(I),B(1),B(2),VECMOD(I)  
   14 CONTINUE  
      STOP
*/

/*
  100 FORMAT(/13X,'NEW PRIMITIVE VECTORS DEFINED TO HAVE POSITIVE SCALAR  
     & PRODUCT'/13X,'A=(',2E14.6,')'/13X,'B=(',2E14.6,')')  
  101 FORMAT(/13X,'W A R N I N G ! !'/'INTERCHANGE PRIMITIVE VECTORS IN  
     &CALL LAT2D'/)  
  102 FORMAT(//33X,'FROM LAT2D: MAXIMUM NUMBER OF NEIGHBOURS=',I4,  
     &'  EXCEEDED'//6X,'LATTICE POINTS FOUND (NON ORDERED)')  
  103 FORMAT(I3,3X,I5,'*(',2E14.6,') +',I5,'*(',2E14.6,')',8X,E14.6)  

*/  
}

//C----------------------------------------------------------------------  
//C     GIVEN THE PRIMITIVE VECTORS AR1,AR2 OF A 2D LATTICE (IN UNITS OF  
//C     ALPHA), THIS  SUBROUTINE  REDUCES A WAVECTOR "AK" (IN  UNITS  OF  
//C     2*PI/ALPHA) WITHIN THE SBZ BY ADDING AN APPROPRIATE  RECIPROCAL- 
//C     LATTICE VECTOR G(IG0)  
//C----------------------------------------------------------------------  
void REDUCE(const CReciprocalLattice &rl, CCoor3D &AK, const int IGMAX, int &IG0)
{
	int I, J, N, I1, I2;
	REAL D, B, P, AFI, AX, AY, FI0, AM, BM, ALPHA, RA;
	REAL AKX, AKY;

	REAL VX[6], VY[6], FI[6], X[6], Y[6]; // a brioullin zone in 2D at maximum has six points

//C----------------------------------------------------------------------  
	ALPHA = rl.AR1.x;
	RA = 2. * M_PI / ALPHA;
	D = rl.AR2.x;
	B = rl.AR2.y;
	if ( (fabs(D) - REAL(0.5)) > EMACH) 
	{
		fprintf(stderr, "IMPROPER LATTICE VECTORS\n");
		exit(1);
	}
	
	//if (D < EMACH)
	if ((fabs(fabs(D) - REAL(0.5)) < EMACH) && (fabs(fabs(B) - sqrt(REAL(3.))*REAL(0.5)) > EMACH))
	{
		//cerr << "centred rectangular lattice\n";
		B = 2. * B;           //! CENTRED RECTANGULAR LATTICE 
		if ((fabs(B) - 1.) < 0.)
		{
			VX[0] = 1.;
			VY[0] = 0.5 * (1. / B - B);
			VX[1] = 0.;
			VY[1] = 0.5 * (1. / B + B);
			VX[2] =-1.;
			VY[2] = VY[0];
			VX[3] =-1.;
			VY[3] = -VY[0];
			VX[4] = 0.;
			VY[4] = -VY[1];
			VX[5] = 1.;
			VY[5] = -VY[0];
		} else
		{
			VX[0] = 0.5 + 0.5 / B / B;
			VY[0] = 0.;
			VX[1] = 0.5 - 0.5 / B / B;
			VY[1] = 1. / B;
			VX[2] = -VX[1];
			VY[2] =  VY[1];
			VX[3] = -VX[0];
			VY[3] = 0.;
			VX[4] = -VX[1];
			VY[4] = -VY[1];
			VX[5] =  VX[1];
			VY[5] = -VY[1];
		} 
	
	} else         //    !OBLIQUE OR HEXAGONAL LATTICE 
	{
		if (D > 0.)
		{
		//cerr << "oblique lattice I think\n";
			P     = 0.5 * D * (D - 1) / B / B;
			VX[0] = 0.5 - P;
			VY[0] = 0.5 * (1. - 2. * D) / B;
			VX[1] = 0.5 + P;
			VY[1] = 0.5 / B;
		} else
		{
			//cerr << "hexagonal lattice I think\n";
			P = 0.5 * D * (D + 1.) / B / B;
			VX[0] =  0.5 - P;
			VY[0] = -0.5 * (1. + 2. * D) / B;
			VX[1] =  0.5 + P;
			VY[1] = -0.5 / B;
		}	
		VX[2] = -VX[1];
		VY[2] =  VY[1];
		VX[3] = -VX[0];
		VY[3] = -VY[0];
		VX[4] = -VX[1];
		VY[4] = -VY[1];
		VX[5] =  VX[1];
		VY[5] = -VY[1];
	}

	N = 6;
	for(I=0; I< N; I++)
	{
		X[I] = VX[I] * RA;
		Y[I] = VY[I] * RA;
	}

	if (fabs(D) < EMACH)
	{
		N = 4;         //     !RECTANGULAR OR SQUARE LATTICE  
		if (B > 0.)
		{
			X[0] = VX[5] * RA;
			Y[0] = VY[5] * RA;
			X[1] = VX[3] * RA;
			Y[1] = VY[3] * RA;
			X[3] = VX[0] * RA;
			Y[3] = VY[0] * RA;
		} else
		{
			X[1] = VX[2] * RA;
			Y[1] = VY[2] * RA;
			X[2] = VX[3] * RA;
			Y[2] = VY[3] * RA;
			X[3] = VX[5] * RA;
			Y[3] = VY[5] * RA;
		}
	}
	
	// to correct quadratic systems. Change by Sebastian Macke
	Y[0] = X[0];
	Y[1] = X[0];
	Y[2] = -X[0];
	Y[3] = -X[0];

//*****VERTICES ARE ARRANGED IN ASCENDING ORDER OF THE POLAR ANGLE FI  
	for(I=0; I<N; I++)
	{
		cerr << X[I] << " " << Y[I] << "\n";
		FI[I] = atan2(Y[I], X[I]);
		if (FI[I] < 0.) FI[I] = FI[I] + 2. * M_PI;
	}
   
	for(J=2; J<=N; J++)
	{
		AFI = FI[J-1];
		AX = X[J-1];
		AY = Y[J-1];
	
		for(I=J-1; I>=1; I--)
		{
			if (FI[I-1] <= AFI) goto pos5;  
			FI[I+1-1] = FI[I-1];
			X[I+1-1] = X[I-1];
			Y[I+1-1] = Y[I-1];
		}
		I = 0;  
pos5:  
		FI[I+1-1] = AFI;
		X[I+1-1] = AX;
		Y[I+1-1] = AY;
//   3  CONTINUE
	}
	
	
	/*
	for(I=0; I<N; I++)
	{
		cerr << X[I] << " " << Y[I] << "\n";
	}
	*/

// *****"AK" IS REDUCED WITHIN THE SBZ  
	
	for(IG0=1; IG0<=IGMAX; IG0++)	
	{		
		//cerr << rl.G[IG0-1].x << " " << rl.G[IG0-1].y << " " << IG0 << " " << rl.G.size() << "\n";
		//cerr << AK.x << " " << AK.y << " ";
		AKX = AK.x - rl.G[IG0-1].x;
		AKY = AK.y - rl.G[IG0-1].y;
		//printf("%f %f ", AKX, AKY);
		//cerr << AKX << " " << AKY << " " << sqrt(AKX*AKX+AKY*AKY) << " " <<IG0 << "\n";
		//continue;
		if ((fabs(AKX) < EMACH) && (fabs(AKY) < EMACH)) // I am definitely intern
		{
			AK.x = AKX;
			AK.y = AKY;		
			return;
		}
		FI0 = atan2(AKY, AKX);   //! FIND POLAR ANGLES OF THE WAVEVECTOR  
		if (FI0 < 0.) FI0 = FI0 + 2. * M_PI;
		
		// check the right angle
		I1 = N;
		I2 = 1;
		I = 1;
		while(1)
		{
			I2 = I;
			if (FI0 < FI[I-1]) break;
			I++;
			I1 = I2;	
			if (I <= N) continue;	
			I1 = N;
			I2 = 1;  
		}
		
		AM = fabs(Y[I2-1] * X[I1-1] - X[I2-1] * Y[I1-1]);
		BM = fabs((X[I1-1] - X[I2-1]) * AKY + (Y[I2-1] - Y[I1-1]) * AKX);
	
		if (AM >= BM)
		{
			AK.x = AKX;
			AK.y = AKY; 
			return;
		}
	}
	
	fprintf(stderr, "ERROR FROM REDUCE:  INSUFFICIENT NR. OF RECIPROCAL LATTICE VECTORS\n");  
	exit(1);
   
}

void GetIncomingLightasReciprocalLattice(int IGMAX, CVector<c::complex<REAL> > &EINCID, const CReciprocalLattice &rl, CLight &light)
{
    const c::complex<REAL> CZERO(0., 0.);
	assert(EINCID.size() == 2*IGMAX);
	for(int I=0; I<2*IGMAX; I++) EINCID[I] = CZERO;
	int IG0 = 1;
	//REDUCE(rl, light.AK, IGMAX, IG0);   //"AK" IN SBZ*******
	//IG0 = 1;
	EINCID[2 * IG0 - 2 + 0] = light.EIN.pol[0];  
	EINCID[2 * IG0 - 2 + 1] = light.EIN.pol[1];  
	//std::cerr << IG0 << " <- IGO ";
}

