#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include"global.h"

c::complex<REAL> CERF(const c::complex<REAL> &Z, const REAL &EMACH);
void PrepareChebychevTables(int LMAX);
void BESSEL(c::complex<REAL> *BJ, c::complex<REAL> *Y, c::complex<REAL> *H, const c::complex<REAL> ARG, const int LMAX, const bool LJ, const bool LY, const bool LH);
REAL BLM(const int L1, const int M1, const int L2, const int M2, const int L3, const int M3, const int LMAX);
void SPHRM4(c::complex<REAL> *YLM, const c::complex<REAL> &CT, const c::complex<REAL> &ST, const c::complex<REAL> &CF, const int LMAX);
void SPHRM4(c::complex<REAL> *YLM, const CKCoord3D &GK, const c::complex<REAL> &KAPPA, const int LMAX, c::complex<REAL> &CT, c::complex<REAL> &ST, c::complex<REAL> &CF);
void ELMGEN(REAL *ELM, const int &NELMD, const int &LMAX);

#endif