#include "CLayer.h"
#include "CSlab.h"
#include "CReciprocalLattice.h"

void HOSLAB(const int IGMAX,
	const c::complex<REAL> KAPPA1,
	const c::complex<REAL> KAPPA2,
	const c::complex<REAL> KAPPA3,
	const CCoor3D &AK,
	const CVector<CCoor2D> &G,
	const CCoor3D DL,
	const CCoor3D DR,
	const REAL D,
	CSlab &s);

