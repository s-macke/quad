#include"sphrm.h"

#include"functions.h"

#define LMINDEX(L, M) ((L)*(L)+(L)+(M))

// the same as in the wikipedia article, but with the normalization from the paper
// and in Maple:
// f := proc (l, m, theta, phi) options operator, arrow; `<,>`(sin(theta)*cos(phi)*SphericalY(l, m, theta, phi), sin(theta)*sin(phi)*SphericalY(l, m, theta, phi), cos(theta)*SphericalY(l, m, theta, phi)) end proc

void Xlm(const int LMAX, CKCoord3D *Y, CKCoord3D *P, CKCoord3D *X, const CCoor3D &p)
{
	const int MNUM = 5*5;
	//const int MNUM = (LMAX+1)*(LMAX+1); // number of M values from L=0..LMAX
	c::complex<REAL> imag(0., 1.);
	const c::complex<REAL> CZERO(0., 0.);
	
	REAL r = sqrt(p.x*p.x + p.y*p.y + p.z*p.z);
	REAL theta = acos(p.z / r);
	REAL phi = atan2(p.y, p.x);

	c::complex<REAL> YLM[MNUM]; 
	c::complex<REAL> CT, ST, CF;
	//SPHRM4(YLM, cos(theta), sin(theta), exp(imag*phi), LMAX);
	
	CKCoord3D p2;
	p2.x = p.x / r;
	p2.y = p.y / r;
	p2.z = p.z / r;
	
	r = 1.;
	SPHRM4(YLM, p2, r, LMAX, CT, ST, CF);
	
	/*
	void SPHRM4(c::complex<REAL> *YLM, 
	const CKCoord3D &GK, 
	const c::complex<REAL> &KAPPA, 
	const int LMAX, 
	c::complex<REAL> &CT, c::complex<REAL> &ST, c::complex<REAL> &CF);
*/

	//SPHRM4(YLM, cos(theta), sin(theta), exp(imag*phi), LMAX);
	
	Y[0].x = YLM[0] * p2.x;
	Y[0].y = YLM[0] * p2.y;
	Y[0].z = YLM[0] * p2.z;
	P[0].x = CZERO;
	P[0].y = CZERO;
	P[0].z = CZERO;
	X[0].x = CZERO;
	X[0].y = CZERO;
	X[0].z = CZERO;
	
	//c::complex<REAL> pre = 1.;

	int idx = 1;
	for(int L= 1; L<=LMAX; L++)
	{
	for(int M=-L; M<=L;    M++)
	{
		c::complex<REAL> pre = 1. / sqrt(L * (L + 1.));
		
		int index = LMINDEX(L, M);
		
		Y[idx].x = YLM[idx]*p2.x;
		Y[idx].y = YLM[idx]*p2.y;
		Y[idx].z = YLM[idx]*p2.z;
		
		X[idx].x = 0.;
		X[idx].y = 0.;
		X[idx].z = REAL(M) * YLM[idx];

		REAL alm = 0.5*sqrt((L-M) * (L+M+1.));
		REAL blm = 0.5*sqrt((L+M) * (L-M+1.));
		
		if (abs(M+1) <= L)
		{
			index = LMINDEX(L, M+1);
			X[idx].x +=       alm*YLM[index];
			X[idx].y += -imag*alm*YLM[index];
		}
		if (abs(M-1) <= L)
		{
			index = LMINDEX(L, M-1);
			X[idx].x +=       blm*YLM[index];
			X[idx].y +=  imag*blm*YLM[index];
		}

		X[idx].x *= pre;
		X[idx].y *= pre;
		X[idx].z *= pre;

		P[idx].x = -(X[idx].y)*p2.z + (X[idx].z)*p2.y;
		P[idx].y = -(X[idx].z)*p2.x + (X[idx].x)*p2.z;
		P[idx].z = -(X[idx].x)*p2.y + (X[idx].y)*p2.x;
		idx++;
	}
		//pre *= -imag;
	}
	
}

void TestSphericalHarmonics()
{
	const int LMAX = 2;
	const int MNUM = (LMAX+1)*(LMAX+1);	
	
	CKCoord3D P[MNUM], X[MNUM], Y[MNUM]; 
	
	CCoor3D p;

	p.x = 1;
	p.y = 0;
	p.z = 0;
	
	Xlm(LMAX, Y, P, X, p);

/*
	c::complex<double> a = sqrt(
		conj(P[1].x)*P[1].x + 
		conj(P[1].y)*P[1].y + 
		conj(P[1].z)*P[1].z);
	c::complex<double> b = sqrt(
		conj(X[1].x)*X[1].x + 
		conj(X[1].y)*X[1].y + 
		conj(X[1].z)*X[1].z);
	c::complex<double> c = sqrt(
		conj(Y[1].x)*Y[1].x + 
		conj(Y[1].y)*Y[1].y + 
		conj(Y[1].z)*Y[1].z);
	cout << a.real() << " " << a.imag() << std::endl;
	cout << b.real() << " " << b.imag() << std::endl;
	cout << c.real() << " " << c.imag() << std::endl;
*/
	for(int i=0; i<MNUM; i++) 
	{
		cout << std::endl;
		cout << i << std::endl;
		cout << Y[i].x.real() << " " << Y[i].x.imag() << std::endl;
		cout << Y[i].y.real() << " " << Y[i].y.imag() << std::endl;
		cout << Y[i].z.real() << " " << Y[i].z.imag() << std::endl;
	}
}

void IntegrateSpherical()
{
	const int LMAX = 2;
	const int NL = (LMAX+1)*(LMAX+1);
	CKCoord3D X[NL];
	CKCoord3D Y[NL];
	CKCoord3D P[NL];

	CCoor3D p;
	CCoor3D Td;
	CCoor3D Pd;
	CKCoord3D E;
	E.x = 0.;
	E.y = 0.;
	E.z = 1.;

	c::complex<REAL> I[NL];
	for(int i=0; i<NL; i++) I[i] = 0.;
	
	c::complex<REAL> a;
	REAL dtheta =  M_PI / 200.;
	REAL dphi = 2.*M_PI / 200.;
	
	CCoor3D kin;
	kin.x = 0.;
	kin.y = 0.;
	kin.z = 1.;
	
	for(REAL theta=0; theta<=M_PI; theta+=dtheta)
	for(REAL phi=0; phi<=2.*M_PI; phi+=dphi)
	{
		double dA = sin(theta) * REAL(dtheta) * REAL(dphi);
		
		// this is also kout
		p.x = sin(theta) * cos(phi);
		p.y = sin(theta) * sin(phi);
		p.z = cos(theta);
		
		Xlm(LMAX, Y, P, X, p);
		/*
		Pd.x = -sin(phi);
		Pd.y =  cos(phi);
		Pd.z = 0.;
		Td.x =  cos(theta) * cos(phi);
		Td.y =  cos(theta) * sin(phi);
		Td.z = -sin(theta);
		E.x = Pd.x* Pd.x + Td.x*Td.x;
		E.y = Pd.x* Pd.y + Td.x*Td.y;
		E.z = Pd.x* Pd.z + Td.x*Td.z;
		*/
		//cout << E.x.real() << " " << E.y.real() << "\n";
		//E.x = Pd.x + Td.x;
		//E.y = 0;
		//E.z = 0;
		
		for(int i=0; i<NL; i++)
		{
			//E.x = P[i].x;
			//E.y = 0;
			//E.z = 0;
			//a = conj(P[i].x) * E.x + conj(P[i].y) * E.y + conj(P[i].z) * E.z;
			a = conj(P[i].x) * P[i].x + conj(P[i].y) * P[i].y + conj(P[i].z) * P[i].z;
			//a = (conj(X[i].x) * X[i].x + conj(X[i].y) * X[i].y + conj(X[i].z) * X[i].z);
			//a = conj(Y[i].x) * Y[i].x + conj(Y[i].y) * Y[i].y + conj(Y[i].z) * Y[i].z;
			I[i] += a * dA;
		}
	}

	for(int i=0; i<NL; i++)
	{
		cout << I[i].real() << " " << I[i].imag() << "\n";
	}

	// rot X1 = - 2*Y1 - P1

// 1.4472  =    sqrt(Pi*2/3)
// 2.8944  = 2.*sqrt(Pi*2/3)
// 1.0233  =    sqrt(Pi/3)
// 2.04665 = 2.*sqrt(Pi/3)

// (1 0 0) = I * 1.02333 * Y1-1 - I * 1.02333 * Y11  + I * 2.04665 * P1-1 - I * 2.04665 * P11
// (0 1 0) =    -1.02333 * Y1-1 -     1.02333 * Y11  -     2.04665 * P1-1 -     2.04665 * P11
// (0 0 1) = I * 1.4472  * Y10  + I * 2.89441 * P10

// format that means
// 100 =  I*T1-1 - I*T11
// 010 =   -T1-1 -   T11
// 001 =  I*T10
}



void TestPLW()
{
	/*
	const int LMAX = 1;
	c::complex<REAL> CONE(1., 0.);
	c::complex<REAL> CI(0., 1.);
	REAL KAPPA = 1.;
	CKCoord3D GK; // seems, that must have the same length as kappa
	GK.x = KAPPA*1.;
	GK.y = KAPPA*0.;
	GK.z = KAPPA*0.;

	CPolar AE[(LMAX+1)*(LMAX+1)];
	CPolar AH[(LMAX+1)*(LMAX+1)];
	CPolar BE[(LMAX+1)*(LMAX+1)];
	CPolar BH[(LMAX+1)*(LMAX+1)];
	for(int i=0; i<(LMAX+1)*(LMAX+1); i++)
	{
		AE[i].sigma = 0.;
		AH[i].sigma = 0.;
		BE[i].sigma = 0.;
		BH[i].sigma = 0.;
		AE[i].pi = 0.;
		AH[i].pi = 0.;
		BE[i].pi = 0.;
		BH[i].pi = 0.;
	}

	PLW(KAPPA, GK, LMAX, EMACH, AE, AH);
	
	// electric field at r = 0
	const double A1 = 1./sqrt(12.*M_PI);
	const double A2 = sqrt(2.) * A1; // 1/sqrt(6.*M_PI)
	CKCoord3D E0, H0;
	E0.x =    -A1 * AE[1].sigma +    A1 * AE[3].sigma;
	E0.y =  CI*A1 * AE[1].sigma + CI*A1 * AE[3].sigma;
	E0.z =    -A2 * AE[2].sigma;
	H0.x =    -A1 * AH[1].sigma +    A1 * AH[3].sigma;
	H0.y =  CI*A1 * AH[1].sigma + CI*A1 * AH[3].sigma;
	H0.z =    -A2 * AH[2].sigma;
*/
	
}

// -------------------------------------------


void IntegrateSpherical3()
{
	const int LMAX = 7;
	const int NL = (LMAX+1)*(LMAX+1);
	CKCoord3D X[NL];
	CKCoord3D Y[NL];
	CKCoord3D P[NL];

	CCoor3D kin;
	kin.x = 0.;
	kin.y = 0.;
	kin.z = -1.;
	double k0 = 0.1;
	
	CCoor3D p;
	CCoor3D Td;
	CCoor3D Pd;
	CKCoord3D E;
	E.x = 0.;
	E.y = 0.;
	E.z = 1.;

	c::complex<REAL> I[NL];
	for(int i=0; i<NL; i++) I[i] = 0.;
	
	c::complex<REAL> a;
	REAL dtheta =  M_PI / 10000.;
	REAL dphi = 2.*M_PI / 10000.;
	
	for(REAL theta=0; theta<=M_PI; theta+=dtheta)
	for(REAL phi=0; phi<=2.*M_PI; phi+=dphi)
	{
		double dA = sin(theta) * REAL(dtheta) * REAL(dphi);
		p.x = sin(theta)*cos(phi);
		p.y = sin(theta)*sin(phi);
		p.z = cos(theta);
		
		CCoor3D Q;
		Q.x = p.x - kin.x;
		Q.y = p.y - kin.y;
		Q.z = p.z - kin.z;
		double q = sqrt(Q.x*Q.x + Q.y*Q.y + Q.z*Q.z) * k0;
		double f = 1. * exp(-10.*q*q);

		Xlm(LMAX, Y, P, X, p);
		
		Pd.x = -sin(phi);
		Pd.y = cos(phi);
		Pd.z = 0.;
		Td.x = cos(theta)*cos(phi);
		Td.y = cos(theta)*sin(phi);
		Td.z = -sin(theta);
		E.x = Pd.x* Pd.x + Td.x*Td.x;
		E.y = Pd.x* Pd.y + Td.x*Td.y;
		E.z = Pd.x* Pd.z + Td.x*Td.z;
		//cout << E.x.real() << " " << E.y.real() << " " << E.z.real() << "\n";
		int i = 0;
		for(int L=0; L<=LMAX; L++)
		for(int M=-L; M<=L; M++)
		{
			a = conj(P[i].x) * E.x + conj(P[i].y) * E.y + conj(P[i].z) * E.z;
			I[i] += a*dA*f;
			i++;
		}
	}

	int i = 0;		
	for(int L=0; L<=LMAX; L++)
	for(int M=-L; M<=L; M++)
	{			
		cout << "L=" << L <<  " M=" << M << " " << I[i].real() << " " << I[i].imag() << "\n";
		i++;
	}
	
	// rot X1 = - 2*Y1 - P1

// 1.4472  =    sqrt(Pi*2/3)
// 2.8944  = 2.*sqrt(Pi*2/3)
// 1.0233  =    sqrt(Pi/3)
// 2.04665 = 2.*sqrt(Pi/3)

// (1 0 0) = I * 1.02333 * Y1-1 - I * 1.02333 * Y11  + I * 2.04665 * P1-1 - I * 2.04665 * P11
// (0 1 0) =    -1.02333 * Y1-1 -     1.02333 * Y11  -     2.04665 * P1-1 -     2.04665 * P11
// (0 0 1) = I * 1.4472  * Y10  + I * 2.89441 * P10

// format that means
// 100 =  I*T1-1 - I*T11
// 010 =   -T1-1 -   T11
// 001 =  I*T10
}


// kin should be (0, 0, 1)
// ein should be (1,0,0)
// eout should be (1,0,0)
// q is then  

void IntegrateSpherical2()
{
	const int LMAX = 3;
	const int NL = (LMAX+1)*(LMAX+1);
	CKCoord3D X[NL];
	CKCoord3D Y[NL];
	CKCoord3D P[NL];

	CCoor3D p, ptheta, pphi;
	CCoor3D Td;
	CCoor3D Pd;
	CKCoord3D E;
	E.x = 1.;
	E.y = 0.;
	E.z = 0.;

	CKCoord3D Eout;
	
	c::complex<REAL> I[NL];
	for(int i=0; i<NL; i++) I[i] = 0.;
	
	c::complex<REAL> a;
	REAL dtheta = M_PI / 400.;
	REAL dphi = 2.*M_PI / 400.;
	
	CCoor3D kin;
	kin.x = 0.;
	kin.y = 0.;
	kin.z = -1.;
	
	for(REAL theta=0; theta<=M_PI; theta+=dtheta)
	for(REAL phi=0; phi<=2.*M_PI; phi+=dphi)
	{
		double dA = sin(theta) * REAL(dtheta) * REAL(dphi);

		// this is also kout
		p.x = sin(theta) * cos(phi);
		p.y = sin(theta) * sin(phi);
		p.z = cos(theta);

		ptheta.x = cos(theta) * cos(phi);
		ptheta.y = cos(theta) * sin(phi);
		ptheta.z = -sin(theta);

		pphi.x = -sin(phi);
		pphi.y =  cos(phi);
		pphi.z = 0.;

		double alpha = ptheta.x;
		double beta = pphi.x;
		Eout.x = alpha*ptheta.x + beta*pphi.x;
		Eout.y = alpha*ptheta.y + beta*pphi.y;
		Eout.z = alpha*ptheta.z + beta*pphi.z;
		
		/*
		printf("scalar %f %f\n", 
			ptheta.x*pphi.x + ptheta.y*pphi.y + ptheta.z*pphi.z, 
			p.x*pphi.x + p.y*pphi.y + p.z*pphi.z
			);
		*/
	//printf("Eout %f %f %f\n", Eout.x.real(), Eout.y.real(), Eout.z.real());

		CCoor3D Q;
		Q.x = p.x - kin.x;
		Q.y = p.y - kin.y;
		Q.z = p.z - kin.z;
		double q = sqrt(Q.x*Q.x + Q.y*Q.y + Q.z*Q.z);
		
		Xlm(LMAX, Y, P, X, p);

		/*
		Pd.x = -sin(phi);
		Pd.y =  cos(phi);
		Pd.z = 0.;
		Td.x =  cos(theta) * cos(phi);
		Td.y =  cos(theta) * sin(phi);
		Td.z = -sin(theta);
		E.x = Pd.x* Pd.x + Td.x*Td.x;
		E.y = Pd.x* Pd.y + Td.x*Td.y;
		E.z = Pd.x* Pd.z + Td.x*Td.z;
		*/
		//cout << E.x.real() << " " << E.y.real() << "\n";
		//E.x = Pd.x + Td.x;
		//E.y = 0;
		//E.z = 0;
		
		for(int i=0; i<NL; i++)
		{
			a = conj(Eout.x) * P[i].x + conj(Eout.y) * P[i].y + conj(Eout.z) * P[i].z;
			//a = (conj(X[i].x) * X[i].x + conj(X[i].y) * X[i].y + conj(X[i].z) * X[i].z);
			//a = conj(Y[i].x) * Y[i].x + conj(Y[i].y) * Y[i].y + conj(Y[i].z) * Y[i].z;
			I[i] += a * dA;
		}
	}

	for(int i=0; i<NL; i++)
	{
		cout << I[i].real() << " " << I[i].imag() << "\n";
	}

	// rot X1 = - 2*Y1 - P1

// 1.4472  =    sqrt(Pi*2/3)
// 2.8944  = 2.*sqrt(Pi*2/3)
// 1.0233  =    sqrt(Pi/3)
// 2.04665 = 2.*sqrt(Pi/3)

// (1 0 0) = I * 1.02333 * Y1-1 - I * 1.02333 * Y11  + I * 2.04665 * P1-1 - I * 2.04665 * P11
// (0 1 0) =    -1.02333 * Y1-1 -     1.02333 * Y11  -     2.04665 * P1-1 -     2.04665 * P11
// (0 0 1) = I * 1.4472  * Y10  + I * 2.89441 * P10

// format that means
// 100 =  I*T1-1 - I*T11
// 010 =   -T1-1 -   T11
// 001 =  I*T10
}




