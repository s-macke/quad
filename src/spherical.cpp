#include<stdlib.h>
#include<stdio.h>
#include<iostream>

#include"CVector.h"
#include"global.h"
#include"CReciprocalLattice.h"
#include"CLayer.h"

#include"multiple_scattering.h"
#include"CSlab.h"
#include"matrix.h"
#include"CAtom.h"


void PrintEFieldPLW(const CExpansionCoefficients &A)
{
    // Is only determined by L=1
	const c::complex<REAL> CZERO(0., 0.), CONE(1., 0.), CI(0., 1.);
	c::complex<REAL> Cr[3][3]; // transformation right

	c::complex<REAL> Ex, Ey, Ez;
	c::complex<REAL> Bx, By, Bz;
	c::complex<REAL> kx, ky, kz;

	c::complex<REAL> x, y, z;
			Cr[0][0] = -2.*sqrt(M_PI/3.);
			Cr[0][1] = 0.;
			Cr[0][2] =  2.*sqrt(M_PI/3.);

			Cr[1][0] = 2.*CI*sqrt(M_PI/3.);
			Cr[1][1] = 0.;
			Cr[1][2] = 2.*CI*sqrt(M_PI/3.);

			Cr[2][0] = 0.;
			Cr[2][1] = -2.*sqrt(2.*M_PI/3.);
			Cr[2][2] = 0.;
   // looks like, there is something wrong here now with the new matrix. result no longer complex. 
	cout << "for PI\n";
	x = A.Elm[1].pol[0];
	y = A.Elm[2].pol[0];
	z = A.Elm[3].pol[0];
	Ex = (Cr[0][0] * x + Cr[0][1] * y + Cr[0][2] * z) / (4.*M_PI);
	Ey = (Cr[1][0] * x + Cr[1][1] * y + Cr[1][2] * z) / (4.*M_PI);
	Ez = (Cr[2][0] * x + Cr[2][1] * y + Cr[2][2] * z) / (4.*M_PI);
	cout << "  Efield polarization : " << 
		"(" << Ex.real() << ", " << Ex.imag() << ") " <<
		"(" << Ey.real() << ", " << Ey.imag() << ") " <<
		"(" << Ez.real() << ", " << Ez.imag() << ")" <<
	"\n";
	cout << "  Efield ampl : " << 
	(conj(Ex)*Ex + conj(Ey)*Ey + conj(Ez)*Ez).real()  
	<< "\n";
	
	x = A.Hlm[1].pol[0];
	y = A.Hlm[2].pol[0];
	z = A.Hlm[3].pol[0];
	Bx = (Cr[0][0] * x + Cr[0][1] * y + Cr[0][2] * z)*CI/(-4.*M_PI);
	By = (Cr[1][0] * x + Cr[1][1] * y + Cr[1][2] * z)*CI/(-4.*M_PI);
	Bz = (Cr[2][0] * x + Cr[2][1] * y + Cr[2][2] * z)*CI/(-4.*M_PI);

	cout << "  Bfield polarization : " << 
		"(" << Bx.real() << ", " << Bx.imag() << ") " <<
		"(" << By.real() << ", " << By.imag() << ") " <<
		"(" << Bz.real() << ", " << Bz.imag() << ")" <<
	"\n";

	kx = Ey*Bz - Ez*By;
	ky = Ez*Bx - Ex*Bz;
	kz = Ex*By - Ey*Bx;

	cout << "  k polarization      : " << 
		"(" << kx.real() << ", " << kx.imag() << ") " <<
		"(" << ky.real() << ", " << ky.imag() << ") " <<
		"(" << kz.real() << ", " << kz.imag() << ")" <<
	"\n";

cout << "for SIGMA\n";
	x = A.Elm[1].pol[1];
	y = A.Elm[2].pol[1];
	z = A.Elm[3].pol[1];
	Ex = (Cr[0][0] * x + Cr[0][1] * y + Cr[0][2] * z)*CI/(-4.*M_PI);
	Ey = (Cr[1][0] * x + Cr[1][1] * y + Cr[1][2] * z)*CI/(-4.*M_PI);
	Ez = (Cr[2][0] * x + Cr[2][1] * y + Cr[2][2] * z)*CI/(-4.*M_PI);
	cout << "  Efield polarization : " << 
		"(" << Ex.real() << ", " << Ex.imag() << ") " <<
		"(" << Ey.real() << ", " << Ey.imag() << ") " <<
		"(" << Ez.real() << ", " << Ez.imag() << ")" <<
	"\n";
	cout << "  Efield ampl : " << 
	(conj(Ex)*Ex + conj(Ey)*Ey + conj(Ez)*Ez).real()  
	<< "\n";
	
	x = A.Hlm[1].pol[1];
	y = A.Hlm[2].pol[1];
	z = A.Hlm[3].pol[1];
	Bx = (Cr[0][0] * x + Cr[0][1] * y + Cr[0][2] * z)*CI/(-4.*M_PI);
	By = (Cr[1][0] * x + Cr[1][1] * y + Cr[1][2] * z)*CI/(-4.*M_PI);
	Bz = (Cr[2][0] * x + Cr[2][1] * y + Cr[2][2] * z)*CI/(-4.*M_PI);

	cout << "  Bfield polarization : " << 
		"(" << Bx.real() << ", " << Bx.imag() << ") " <<
		"(" << By.real() << ", " << By.imag() << ") " <<
		"(" << Bz.real() << ", " << Bz.imag() << ")" <<
	"\n";

	kx = Ey*Bz - Ez*By;
	ky = Ez*Bx - Ex*Bz;
	kz = Ex*By - Ey*Bx;

	cout << "  k polarization      : " << 
		"(" << kx.real() << ", " << kx.imag() << ") " <<
		"(" << ky.real() << ", " << ky.imag() << ") " <<
		"(" << kz.real() << ", " << kz.imag() << ")" <<
	"\n";


}


//C     ------------------------------------------------------------------  
//C     THIS SUBROUTINE COMPUTES THE TRANSMISSION/REFLECTION MATRICES FOR 
//C     A PLANE OF SPHERES EMBEDDED IN A HOMOGENEOUS HOST MEDIUM. 
//C     ------------------------------------------------------------------  
//C  
//=======================================================================

void PCSLAB(
	const int LMAX,
	const CLayer &layer,
	const REAL Efield,
	const c::complex<REAL> &KAPPA,
	const CCoor3D &AK,
	const CReciprocalLattice &rl,
	const REAL *ELM,
	CSlab &slab, bool volumecorrection)
{
	const int LMAXARRAY = 7;
	const int LMAX1DARRAY = LMAXARRAY+1;
	const int LMODDARRAY = (LMAXARRAY*LMAX1DARRAY)/2;
	const int LM1SQDARRAY = LMAX1DARRAY*LMAX1DARRAY;
	const int LMEVENARRAY = (LMAX1DARRAY*(LMAX1DARRAY+1))/2;
	const int LMTDARRAY = LM1SQDARRAY-1;


	const int LMAX1D = LMAX+1;
	const int LMODD = (LMAX*LMAX1D)/2;
	const int LM1SQD = LMAX1D*LMAX1D;
	const int LMEVEN = (LMAX1D*(LMAX1D+1))/2;
	const int LMTD = LM1SQD-1;
	const int IGMAX = rl.G.size();
	const int IGKMAX = 2 * IGMAX;
	const bool MULTIPLESCATTERING = true;
	c::complex<REAL> phase = 0.;
	
	const c::complex<REAL> CZERO(0., 0.), CONE(1., 0.), CI(0., 1.);

	int INT1[LMTD], INT2[LMTD];
	c::complex<REAL> **XXMAT1, **XXMAT2;
	c::complex<REAL> **XEVEN, **XODD;
	
	if (MULTIPLESCATTERING)
	{
		NewTable(XEVEN, LMEVEN, LMEVEN);
		NewTable(XODD, LMODD, LMODD);
		NewTable(XXMAT1, LMTD, LMTD);
		NewTable(XXMAT2, LMTD, LMTD);
	}
	
	//GKK[i].z should be always negative or imaginary
	CVector<CKCoord3D> GKK;
	GKK.assign(IGMAX, CKCoord3D());
	for(int IG1=0; IG1<IGMAX; IG1++)
		GKK[IG1] = ReciprocalWaveVector(AK, rl.G[IG1], KAPPA);
	
	slab.Zero();
	
	for(int atomidx=0; atomidx<layer.atoms.size(); atomidx++)
	{
	
	//int atomidx = 0;

	//printf("Calculate PCSLAB with %i\n", MULTIPLESCATTERING);
	CAtom *atom = layer.atoms[atomidx]->Clone();
	
	c::complex<REAL> CQI, CQII, CQIII, CQIV;
	c::complex<REAL> LAME[2], LAMH[2];
	c::complex<REAL> TE[LMAX1DARRAY], TH[LMAX1DARRAY];
	//c::complex<REAL> TE[9], TH[9];
	
	c::complex<REAL> BMEL1[LMTDARRAY], BMEL2[LMTDARRAY];
	
	CExpansionCoefficients A(LMAX); // for plane wave
	CExpansionCoefficients B(LMAX); // for scattered wave
	CExpansionCoefficients D(LMAX); // transformation to reciprocal lattice vector plane waves

//  ------------------------------------------------------------------  

	atom->Set(LMAX, KAPPA, 1.);
	atom->GetIsotropicScatterMatrix(TE, TH, LMAX);
	
//	cerr << TE[1].real() << " " << TE[1].imag() << "\n";


	if (MULTIPLESCATTERING)
	{
		XMAT(XODD, XEVEN, LMAX, KAPPA, AK, ELM, rl.AR1, rl.AR2);
		SETUP(LMAX, XEVEN, XODD, TE, TH, XXMAT1, XXMAT2);
		//Gaussian Elimination of XXMAT to perform inversion in ZSU
		ZGE(XXMAT1, INT1, LMTD);
		ZGE(XXMAT2, INT2, LMTD);
	}

	int ISIGN2 = 1;
	REAL SIGN2 = REAL(3.) - REAL(2.) * ISIGN2;
	int IGK2 = 0;
	for(int IG2=0; IG2<IGMAX; IG2++) // loop over all incoming rays
	{
		CKCoord3D GKIN;
		GKIN.x =       GKK[IG2].x;
		GKIN.y =       GKK[IG2].y;
		GKIN.z = SIGN2*GKK[IG2].z;
		PLW(KAPPA, GKIN, LMAX, A);
		//PrintEFieldPLW(A);
		atom->Scatter(B, A);

		for(int K2=0; K2<2; K2++) // polarization
		{
			IGK2++;
			Sort1(LMAX, LMODD, K2, BMEL1, BMEL2, B);
			
			// calculate XXMAT1*X = BMEL    =>   X = XXMAT1^(-1) * BMEL  stored BMEL=X
			if (MULTIPLESCATTERING)
			{
				ZSU(XXMAT1, INT1, BMEL1, LMTD);
				ZSU(XXMAT2, INT2, BMEL2, LMTD);
			}

			for(int ISIGN1=1; ISIGN1<=2; ISIGN1++) // ISIGN1=2: reflected, ISIGN1=1: transmitted
			{
				REAL SIGN1 = 3. - 2. * ISIGN1;
				int IGK1 = 0;
				for(int IG1=0; IG1<IGMAX; IG1++) // loop over all outgoing rays
				{
					CKCoord3D GKOUT;
					GKOUT.x =       GKK[IG1].x;
					GKOUT.y =       GKK[IG1].y;
					GKOUT.z = SIGN1*GKK[IG1].z;

					phase = exp( CI*( (GKIN.x-GKOUT.x)*layer.position[atomidx].x + (GKIN.y-GKOUT.y)*layer.position[atomidx].y + (GKIN.z-GKOUT.z)*layer.position[atomidx].z));

					DLMKG(LMAX, rl.A0, GKOUT, -SIGN1, KAPPA, D, volumecorrection);

					for(int K1=0; K1<2; K1++)
					{
						Sort2(LMAX, LMODD, K1, BMEL1, BMEL2, LAME, LAMH, D);
					}

					for(int K1=0; K1<2; K1++)
					{
						IGK1++;
						if (ISIGN1 == 1) slab.QI  [IGK1-1][IGK2-1] += (LAMH[K1] + LAME[K1])*phase;
						if (ISIGN1 == 2) slab.QIII[IGK1-1][IGK2-1] += (LAMH[K1] + LAME[K1])*phase;
					}
				}
			}
		} // for K2
	}

	// other symmetry
	ISIGN2 = 2;
	SIGN2 = REAL(3.) - REAL(2.) * ISIGN2;
	IGK2 = 0;
	for(int IG2=0; IG2<IGMAX; IG2++) // loop over all incoming rays
	{
		CKCoord3D GKIN;
		GKIN.x =       GKK[IG2].x;
		GKIN.y =       GKK[IG2].y;
		GKIN.z = SIGN2*GKK[IG2].z;
		PLW(KAPPA, GKIN, LMAX, A);
		//PrintEFieldPLW(A);
		atom->Scatter(B, A);

		for(int K2=0; K2<2; K2++) // polarization
		{
			IGK2++;
			Sort1(LMAX, LMODD, K2, BMEL1, BMEL2, B);
			
			// calculate XXMAT1*X = BMEL    =>   X = XXMAT1^(-1) * BMEL  stored BMEL=X
			if (MULTIPLESCATTERING)
			{
				ZSU(XXMAT1, INT1, BMEL1, LMTD);
				ZSU(XXMAT2, INT2, BMEL2, LMTD);
			}

			for(int ISIGN1=1; ISIGN1<=2; ISIGN1++) // ISIGN1=2: reflected, ISIGN1=1: transmitted
			{
				REAL SIGN1 = 3. - 2. * ISIGN1;
				int IGK1 = 0;
				for(int IG1=0; IG1<IGMAX; IG1++) // loop over all outgoing rays
				{
					CKCoord3D GKOUT;
					GKOUT.x =       GKK[IG1].x;
					GKOUT.y =       GKK[IG1].y;
					GKOUT.z = SIGN1*GKK[IG1].z;
					phase = exp( CI*( (GKIN.x-GKOUT.x)*layer.position[atomidx].x + (GKIN.y-GKOUT.y)*layer.position[atomidx].y + (GKIN.z-GKOUT.z)*layer.position[atomidx].z));
					DLMKG(LMAX, rl.A0, GKOUT, -SIGN1, KAPPA, D, volumecorrection);

					for(int K1=0; K1<2; K1++)
					{
						Sort2(LMAX, LMODD, K1, BMEL1, BMEL2, LAME, LAMH, D);
					}

					for(int K1=0; K1<2; K1++)
					{
						IGK1++;
						if (ISIGN1 == 1) slab.QII[IGK1-1][IGK2-1] += (LAMH[K1] + LAME[K1])*phase;
						if (ISIGN1 == 2) slab.QIV[IGK1-1][IGK2-1] += (LAMH[K1] + LAME[K1])*phase;
					}
				}
			}
		} // for K2

	}
	delete atom;

} // for atomidx
/*
	// use symmetry to determine QII and QIV
	IGK2 = 0;
	for(int IG2=1; IG2<=IGMAX; IG2++)
	for(int K2=0; K2<2; K2++)
	{
		int IGK1 = 0;
		for(int IG1=1; IG1<=IGMAX; IG1++)
		for(int K1=0; K1<2; K1++)
		{
			REAL SIGNUS = 1.;
			if (K2 != K1) SIGNUS = -1.;
			slab.QII[IGK1][IGK2] = -SIGNUS * slab.QIII[IGK1][IGK2];
			slab.QIV[IGK1][IGK2] = -SIGNUS * slab.QI  [IGK1][IGK2];
			//cout << slab.QII[IGK1][IGK2].real() << "\n";
			IGK1++;
		}
		IGK2++;
}
*/

	// add 1 to transmitted matrices
	for(int IGK1=0; IGK1<IGKMAX; IGK1++)
	{
	  //if (slab.QI [IGK1][IGK1].real() > 0.)
	  //cerr << "Error: forward scattering with positive sign: " << scientific << slab.QI[IGK1][IGK1].real() << "\n";
      slab.QI [IGK1][IGK1] = CONE + slab.QI [IGK1][IGK1];
      slab.QIV[IGK1][IGK1] = CONE + slab.QIV[IGK1][IGK1];
	}
/*
	// propagation matrices
	for(int IG2=0; IG2<IGMAX; IG2++)
	for(int IG1=0; IG1<IGMAX; IG1++)
	{
		CQI  = exp(CI*(
				 GKK[IG1].x*layer.DR.x + GKK[IG1].y*layer.DR.y + GKK[IG1].z*layer.DR.z +
				 GKK[IG2].x*layer.DL.x + GKK[IG2].y*layer.DL.y + GKK[IG2].z*layer.DL.z));
		CQII = exp(CI*(
				(GKK[IG1].x - GKK[IG2].x)*layer.DR.x + 
				(GKK[IG1].y - GKK[IG2].y)*layer.DR.y + 
				(GKK[IG1].z + GKK[IG2].z)*layer.DR.z));
		CQIII= exp(-CI*(
				(GKK[IG1].x - GKK[IG2].x)*layer.DL.x + 
				(GKK[IG1].y - GKK[IG2].y)*layer.DL.y - 
				(GKK[IG1].z + GKK[IG2].z)*layer.DL.z));
		CQIV = exp(-CI*(
				 GKK[IG1].x*layer.DL.x + GKK[IG1].y*layer.DL.y - GKK[IG1].z*layer.DL.z +
				 GKK[IG2].x*layer.DR.x + GKK[IG2].y*layer.DR.y - GKK[IG2].z*layer.DR.z));

		for(int K2=0; K2<=1; K2++)
		{
			int IGK2 = IG2 * 2 + K2;
			for(int K1=0; K1<=1; K1++)
			{
				int IGK1 = IG1 * 2 + K1;
				slab.QI  [IGK1][IGK2] = CQI  * slab.QI  [IGK1][IGK2];
				slab.QII [IGK1][IGK2] = CQII * slab.QII [IGK1][IGK2];
				slab.QIII[IGK1][IGK2] = CQIII* slab.QIII[IGK1][IGK2];
				slab.QIV [IGK1][IGK2] = CQIV * slab.QIV [IGK1][IGK2];
			}
		}
	}
*/
	if (MULTIPLESCATTERING)
	{
		DeleteTable(XEVEN, LMEVEN, LMEVEN);
		DeleteTable(XODD, LMODD, LMODD);
		DeleteTable(XXMAT1, LMTD, LMTD);
		DeleteTable(XXMAT2, LMTD, LMTD);
	}
	
}

// -------------------------------------------------------------------------------------------
// This functions build a 2x2 scattering matrix (for both polarizations) and put it into a matrix

inline void BuildScatterMatrixEntry(int LMAX, int IGK1, int IGK2, const CExpansionCoefficients &B, const CExpansionCoefficients &D, c::complex<REAL> **Q, const c::complex<REAL> &phase)
{
	const c::complex<REAL> CZERO(0., 0.);
	c::complex<REAL> LAME[2][2];
	LAME[0][0] = CZERO;
	LAME[1][0] = CZERO;
	LAME[0][1] = CZERO;
	LAME[1][1] = CZERO;
	int II = 1;
	for(int L=1; L<=LMAX; L++)
	for(int M=-L; M<=L; M++)
	{
		// b = incoming pol, D = outgoing pol
		LAME[0][0] += D.Elm[II].pol[0] * B.Elm[II].pol[0];
		LAME[1][0] += D.Elm[II].pol[1] * B.Elm[II].pol[0];
		LAME[0][1] += D.Elm[II].pol[0] * B.Elm[II].pol[1];
		//printf("off %f %f\n", LAME[1][0].real(), LAME[1][0].imag());
		//printf("on  %f %f\n", LAME[0][0].real(), LAME[0][0].imag());
		LAME[1][1] += D.Elm[II].pol[1] * B.Elm[II].pol[1];
		II++;
	}
	Q[IGK1][IGK2]     += LAME[0][0]*phase;
	Q[IGK1+1][IGK2]   += LAME[1][0]*phase;
	Q[IGK1][IGK2+1]   += LAME[0][1]*phase;
	Q[IGK1+1][IGK2+1] += LAME[1][1]*phase;
}




void PCSLABEASY(
	int LMAX,
	const CLayer &layer,
	const REAL Efield,
	const c::complex<REAL> &KAPPA,
	const CCoor3D &AK,
	const CReciprocalLattice &rl,
	const REAL *ELM,
	CSlab &slab, bool volumecorrection)
{
	//printf("Calculate PCSLABEASY\n");

	//LMAX = 1;
//	const int LMAX = 1;
	const int LMAX1D = LMAX+1;
	const int LMODD = (LMAX*LMAX1D)/2;
	const int LM1SQD = LMAX1D*LMAX1D;
	const int LMEVEN = (LMAX1D*(LMAX1D+1))/2;
	const int LMTD = LM1SQD-1;
	const int IGMAX = rl.G.size();
	const int IGKMAX = 2 * IGMAX;

	c::complex<REAL> CQI, CQII, CQIII, CQIV;
	c::complex<REAL> LAME[2];
	
	CExpansionCoefficients A(LMAX);
	CExpansionCoefficients B(LMAX);
	CExpansionCoefficients D(LMAX);

	const c::complex<REAL> CZERO(0., 0.), CONE(1., 0.), CI(0., 1.);
	
	// incoming wave vectors
	CVector<CKCoord3D> GKK;
	GKK.assign(IGMAX, CKCoord3D());
	for(int IG1=0; IG1<IGMAX; IG1++)
		GKK[IG1] = ReciprocalWaveVector(AK, rl.G[IG1], KAPPA);

	//cout << "gz:" << GKK[0].z.real() << " " << AK.z << "\n";
	slab.Zero();
	//slab.Identity();

//C ------------------------------------------------------------------  

for(int atomidx=0; atomidx<layer.atoms.size(); atomidx++)
{
	CAtom *atom = dynamic_cast<CAtom*>(layer.atoms[atomidx]->Clone());
	atom->Set(LMAX, KAPPA, Efield);

	CKCoord3D GKIN, GKGOUT;
	
	for(int IG2=0; IG2<IGMAX; IG2++) // for all incoming wavectors
	{
		// incoming beam
		GKIN.x = GKK[IG2].x;
		GKIN.y = GKK[IG2].y;
		GKIN.z = GKK[IG2].z;
		// get plane wave in spherical harmonic expansion
		PLW(KAPPA, GKIN, LMAX, A);
		atom->Scatter(B, A);
//printf("pol0 %f %f\n", B.Elm[1].pol[0].real(), B.Elm[1].pol[0].real());
//printf("pol1 %f %f\n", B.Elm[1].pol[1].real(), B.Elm[1].pol[1].real());
		c::complex<REAL> phase;
		for(int IG1=0; IG1<IGMAX; IG1++) // for all outgoing rays
		{
			CKCoord3D GKOUT;
			// transmitted
			GKOUT.x =  GKK[IG1].x;
			GKOUT.y =  GKK[IG1].y;
			GKOUT.z =  GKK[IG1].z;

			phase = exp( CI*( (GKIN.x-GKOUT.x)*layer.position[atomidx].x + (GKIN.y-GKOUT.y)*layer.position[atomidx].y + (GKIN.z-GKOUT.z)*layer.position[atomidx].z));
			DLMKG(LMAX, rl.A0, GKOUT, REAL(-1.), KAPPA, D, volumecorrection);
			BuildScatterMatrixEntry(LMAX, IG1<<1, IG2<<1, B, D, slab.QI, phase);
		
			// reflected
			GKOUT.x =  GKK[IG1].x;
			GKOUT.y =  GKK[IG1].y;
			GKOUT.z = -GKK[IG1].z;
			phase = exp( CI*( (GKIN.x-GKOUT.x)*layer.position[atomidx].x + (GKIN.y-GKOUT.y)*layer.position[atomidx].y + (GKIN.z-GKOUT.z)*layer.position[atomidx].z));
			DLMKG(LMAX, rl.A0, GKOUT, REAL(1.), KAPPA, D, volumecorrection);
			BuildScatterMatrixEntry(LMAX, IG1<<1, IG2<<1, B, D, slab.QIII, phase);
		}

		GKIN.x =  GKK[IG2].x;
		GKIN.y =  GKK[IG2].y;
		GKIN.z = -GKK[IG2].z;
		PLW(KAPPA, GKIN, LMAX, A);
		atom->Scatter(B, A);
		for(int IG1=0; IG1<IGMAX; IG1++) // for all outgoing rays
		{
			CKCoord3D GKOUT;
			// transmitted
			GKOUT.x =  GKK[IG1].x;
			GKOUT.y =  GKK[IG1].y;
			GKOUT.z = -GKK[IG1].z;

			phase = exp( CI*( (GKIN.x-GKOUT.x)*layer.position[atomidx].x + (GKIN.y-GKOUT.y)*layer.position[atomidx].y + (GKIN.z-GKOUT.z)*layer.position[atomidx].z));
			DLMKG(LMAX, rl.A0, GKOUT, REAL(1.), KAPPA, D, volumecorrection);
			BuildScatterMatrixEntry(LMAX, IG1<<1, IG2<<1, B, D, slab.QIV, phase);
		
			// reflected
			GKOUT.x =  GKK[IG1].x;
			GKOUT.y =  GKK[IG1].y;
			GKOUT.z =  GKK[IG1].z;
			phase = exp( CI*( (GKIN.x-GKOUT.x)*layer.position[atomidx].x + (GKIN.y-GKOUT.y)*layer.position[atomidx].y + (GKIN.z-GKOUT.z)*layer.position[atomidx].z));
			DLMKG(LMAX, rl.A0, GKOUT, REAL(-1.), KAPPA, D, volumecorrection);
			BuildScatterMatrixEntry(LMAX, IG1<<1, IG2<<1, B, D, slab.QII, phase);
		}

	}
	delete atom;
} // atomidx

/*
	// use symmetry to determine QII and QIV
	int IGK2 = 0;
	for(int IG2=1; IG2<=IGMAX; IG2++)
	for(int K2=0; K2<2; K2++)
	{
		int IGK1 = 0;
		for(int IG1=1; IG1<=IGMAX; IG1++)
		for(int K1=0; K1<2; K1++)
		{
			REAL SIGNUS = 1.;
			if (K2 != K1) SIGNUS = -1.;
			slab.QII[IGK1][IGK2] = SIGNUS * slab.QIII[IGK1][IGK2];
			slab.QIV[IGK1][IGK2] = SIGNUS * slab.QI  [IGK1][IGK2];
			//cout << slab.QII[IGK1][IGK2].real() << "\n";
			IGK1++;
		}
		IGK2++;
	}
*/

	//slab.PAIR(slab, GKK);

	// already done in slab.idendity()
	// add 1 to transmitted matrices
	for(int IGK1=0; IGK1<IGKMAX; IGK1++)
	{
		slab.QI [IGK1][IGK1] = CONE + slab.QI [IGK1][IGK1];
		slab.QIV[IGK1][IGK1] = CONE + slab.QIV[IGK1][IGK1];
	}


/*
	for(int IG2=0; IG2<IGMAX; IG2++)
	for(int IG1=0; IG1<IGMAX; IG1++)
	{
		// this is what we need
		// CQI  = ++
		// CQII = -+
		// CQIII = +-
		// CQIV = --
		
		// this is what we do at the moment if dl=dr
		// CQI = ++
		// CQII = ++
		// CQIII = ++
		// CQIV = ++
		
		// if dl = -dr
		// CQI = 

		// short version
		
		CQI  = exp(- CI*( GKK[IG1].z*layer.DR.z + GKK[IG2].z*layer.DL.z));
		CQII = exp(- CI*( GKK[IG1].z*layer.DR.z + GKK[IG2].z*layer.DR.z));
		CQIII= exp(- CI*( GKK[IG1].z*layer.DL.z + GKK[IG2].z*layer.DL.z));
		CQIV = exp(- CI*( GKK[IG1].z*layer.DL.z + GKK[IG2].z*layer.DR.z));

		// maybe corrected version
		//CQI  = exp( CI*( GKK[IG1].z*DR.z + GKK[IG2].z * DL.z)); // incoming top and transmitted
		//CQII = exp( CI*(-GKK[IG1].z*DR.z + GKK[IG2].z * DR.z));
		//CQIII= exp( CI*( GKK[IG1].z*DL.z - GKK[IG2].z * DL.z)); // incoming top and reflected
		//CQIV = exp( CI*(-GKK[IG1].z*DL.z - GKK[IG2].z * DR.z));
*/
/*/*
		// transmitted top
		CQI  = exp(CI*(
				 GKK[IG1].x*layer.DR.x + GKK[IG1].y*layer.DR.y + GKK[IG1].z*layer.DR.z +
				 GKK[IG2].x*layer.DL.x + GKK[IG2].y*layer.DL.y + GKK[IG2].z*layer.DL.z));
		// reflected bottom
		CQII = exp(CI*(
				(GKK[IG1].x - GKK[IG2].x)*layer.DR.x + 
				(GKK[IG1].y - GKK[IG2].y)*layer.DR.y + 
				(GKK[IG1].z + GKK[IG2].z)*layer.DR.z));
		// reflected top
		CQIII= exp(-CI*(
				(GKK[IG1].x - GKK[IG2].x)*layer.DL.x + 
				(GKK[IG1].y - GKK[IG2].y)*layer.DL.y - 
				(GKK[IG1].z + GKK[IG2].z)*layer.DL.z));
		//transmitted bottom
		CQIV = exp(-CI*(
				 GKK[IG1].x*layer.DL.x + GKK[IG1].y*layer.DL.y - GKK[IG1].z*layer.DL.z +
				 GKK[IG2].x*layer.DR.x + GKK[IG2].y*layer.DR.y - GKK[IG2].z*layer.DR.z));
*/
/*
		int IGK1 = IG1<<1;
		int IGK2 = IG2<<1;
		
		slab.QI  [IGK1+0][IGK2+0] *= CQI;
		slab.QI  [IGK1+0][IGK2+1] *= CQI;
		slab.QI  [IGK1+1][IGK2+0] *= CQI;
		slab.QI  [IGK1+1][IGK2+1] *= CQI;

		slab.QII [IGK1+0][IGK2+0] *= CQII;
		slab.QII [IGK1+0][IGK2+1] *= CQII;
		slab.QII [IGK1+1][IGK2+0] *= CQII;
		slab.QII [IGK1+1][IGK2+1] *= CQII;
		
		slab.QIII[IGK1+0][IGK2+0] *= CQIII;
		slab.QIII[IGK1+0][IGK2+1] *= CQIII;
		slab.QIII[IGK1+1][IGK2+0] *= CQIII;
		slab.QIII[IGK1+1][IGK2+1] *= CQIII;

		slab.QIV [IGK1+0][IGK2+0] *= CQIV;
		slab.QIV [IGK1+0][IGK2+1] *= CQIV;
		slab.QIV [IGK1+1][IGK2+0] *= CQIV;
		slab.QIV [IGK1+1][IGK2+1] *= CQIV;
	}
*/
/*
// let's do a comparison

CSlab slab2(2*IGMAX);
PCSLAB(
	LMAX,
	*atom,
	KAPPA,
	AK,
	DL,
	DR,
	rl,
	ELM,
	slab2);

	double err = 0.;
	double maxerr = 0;
	for(int i=0; i<2*IGMAX; i++)
	for(int j=0; j<2*IGMAX; j++)
	{
		double er;
		double ea;
		ea = abs(slab.QI[i][j] - slab2.QI[i][j]);		
		er = ea / abs(slab2.QI[i][j]);
		if (ea > maxerr) maxerr = ea;
		err += er;
		
		ea = abs(slab.QII[i][j] - slab2.QII[i][j]);		
		er = ea / abs(slab2.QII[i][j]);
		if (ea > maxerr) maxerr = ea;
		err += er;

		ea = abs(slab.QIII[i][j] - slab2.QIII[i][j]);		
		er = ea / abs(slab2.QIII[i][j]);
		if (ea > maxerr) maxerr = ea;
		err += er;

		ea = abs(slab.QIV[i][j] - slab2.QIV[i][j]);
		er = ea / abs(slab2.QIV[i][j]);
		if (ea > maxerr) maxerr = ea;
		err += er;
	}
	 err /= 4.*4.*IGMAX*IGMAX;
	if (maxerr > 1e-20) fprintf(stderr, "avg rel. e.: %e   max abs. e.: %e\n", err, maxerr);
*/
	
	
	
}

