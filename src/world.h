#ifndef WORLD_H
#define WORLD_H

#include"global.h"
#include"CSlab.h"
#include"CLight.h"
#include"CLayer.h"
#include"CReciprocalLattice.h"
#include"CString.h"
#include "functions.h"

enum VARARGTYPE{CRYSTAL=0, HOM=1, INTERFACE=2};

class CVarargType
{
	public:
	VARARGTYPE type;
	CString crystal;
	CString crystal2;
	double d, sigma;
	int n;
};

class CWorld
{
	public:
		int LMAX;
		CVector<CLayer> layer;
		CReciprocalLattice rl;
		CLight light;
		CString multilayer;
		REAL *ELM;	// this is readonly and therefore can be copied without a problem
		MultipleScatteringAccuracy msaccuracy;
		NumericAccuracy numaccuracy;
		MatrixAccuracy matrixaccuracy;
		bool volumecorrection;
		CVector<REAL> r, t;
		
		CVector<CString> varnames;
		CVector<CVarargType> varargs;
		CVector<CSlab> varslabs;
		
		CVector<REAL> fields;
		int queueindex;
		int currentlayerindex;
		
		CWorld()
		{
			msaccuracy = LAYER;
			numaccuracy = NUMDOUBLE;
			matrixaccuracy = MFULL;
			ELM = NULL;
			queueindex = 0;
			currentlayerindex = 0;
			volumecorrection = true;
		}
		
		void SetLMAX(int _LMAX)
		{
			const int NELMD = 7680; //Number of tabulated CLEBSCH-GORDON TYPE COEFFICIENTS
			LMAX = _LMAX;
			
			if (LMAX > 7)
			{
				fprintf(stderr, "Error in SetLMAX: LMAX > 7. Table Denom in XMAT and table ELM does not support larger L");
				exit(1);
			}
			if (ELM != NULL) delete ELM;
			ELM = new REAL[NELMD];
			ELMGEN(ELM, NELMD, LMAX);
			PrepareChebychevTables(LMAX);		
		}

		void PrintStats()
		{
			cerr << "---------- STATS ----------\n";
			cerr << "LMAX: " << LMAX << "\n";
			cerr << "light theta: " << light.THETA << " deg  phi: " << light.FI << " deg  k0: " << light.KAPPA0 << " 1/A" <<"\n";

			cerr << "Number of reciprocal lattice vectors = " << rl.IGMAX << "\n";
			cerr << "Lattice vectors (H,K) : ";
			for(int i=0; i<rl.IGMAX; i++)
			{
				cerr << "(" << rl.NT1[i] << "," << rl.NT2[i] << ") ";
			}
			cerr << "\n";
			cerr << "Number of slabs = " << layer.size() << "\n";
			cerr << "Number of atoms per slab=";
			for(int i=0; i<layer.size(); i++) cerr << " " << layer[i].atoms.size();
			cerr << "\n";			
			cerr << "film structure = \"" << multilayer.c_str() << "\"\n";

			for(int i=0; i<varnames.size(); i++)
			{
				cerr << "Slab " << varnames[i].c_str() << " : ";
				switch(varargs[i].type)
				{
					case CRYSTAL:
						cerr << "CRYSTAL crystal_structure = \"" << varargs[i].crystal.c_str() << "\"";
						break;
					case HOM:
						cerr << "HOMOGENEOUS crystal_structure = \"" << varargs[i].crystal.c_str() << "\"";
						break;
					case INTERFACE:
						cerr 
							<< "INTERFACE cs_top = \"" << varargs[i].crystal.c_str() 
							<< "\" cs_bottom = \"" <<varargs[i].crystal2.c_str() << "\"" 
							<< " roughness = " << varargs[i].sigma <<"A"
							<< " thickness = " << varargs[i].d <<"A";
						break;
					default:
						cerr << "UNKNOWN ";
						break;
				}
				cerr << "\n";
			}
			
			cerr << "A1.x = " << rl.AR1.x << "\t";
			cerr << "A2.x = " << rl.AR2.x << "\t";
			cerr << "A3.x = " << 0. << "\n";
			cerr << "A1.y = " << rl.AR1.y << "\t";
			cerr << "A2.y = " << rl.AR2.y << "\t";
			cerr << "A3.y = " << 0. << "\n";
			cerr << "A1.z = " << 0. << "\t";
			cerr << "A2.z = " << 0. << "\t";
			cerr << "A3.z = " << rl.az << "\n";

			cerr << "B1.x = " << rl.B1.x << "\t";
			cerr << "B2.x = " << rl.B2.x << "\t";
			cerr << "B3.x = " << 0. << "\n";
			cerr << "B1.y = " << rl.B1.y << "\t";
			cerr << "B2.y = " << rl.B2.y << "\t";
			cerr << "B3.y = " << 0. << "\n";
			cerr << "B1.z = " << 0. << "\t";
			cerr << "B2.z = " << 0. << "\t";
			cerr << "B3.z = " << 2.*M_PI/rl.az << "\n";
			
			cerr << "---------------------------\n";
		}


void GetTransRefl(CSlab *slab)
{
	r.assign(rl.IGMAX, 0.);
	t.assign(rl.IGMAX, 0.);
	const c::complex<REAL> CZERO(0., 0.);
	CVector< c::complex<REAL> > EINCID;
	EINCID.assign(2 * rl.IGMAX, CZERO);
	GetIncomingLightasReciprocalLattice(rl.IGMAX, EINCID, rl, light);

	CVector<CKCoord3D> GKK;
	GKK.assign(rl.IGMAX, CKCoord3D());
	for(int IG1=0; IG1<rl.IGMAX; IG1++)
	{
		GKK[IG1] = ReciprocalWaveVector(light.AK, rl.G[IG1], light.KAPPA0);
		//double r = sqrt(GKK[IG1].x.real()*GKK[IG1].x.real() + GKK[IG1].y.real()*GKK[IG1].y.real() + GKK[IG1].z.real()*GKK[IG1].z.real());
		//printf("%f %f\n", r, light.KAPPA0);
	}
	
	double DOWN = 0.;
	if (volumecorrection)
	{
		for (int IGK1=0; IGK1 < 2*rl.IGMAX; IGK1++)
		{
			DOWN  += (EINCID[IGK1] * conj(EINCID[IGK1])).real() * fabs(GKK[IGK1/2].z.real());
		}
		for(int IG1=0; IG1<rl.IGMAX; IG1++)
		{
			c::complex<REAL> E_SIGMA = CZERO;
			c::complex<REAL> E_PI = CZERO;
			REAL intensity;
			int IGK1 = IG1*2;
			for (int IGK2=0; IGK2 < 2*rl.IGMAX; IGK2++)
			{
				E_PI += slab->QI[IGK1+0][IGK2] * EINCID[IGK2];
				E_SIGMA += slab->QI[IGK1+1][IGK2] * EINCID[IGK2];
			}
			E_PI *= light.filter.pol[0];
			E_SIGMA *= light.filter.pol[1];
			intensity = (E_SIGMA*conj(E_SIGMA) + E_PI*conj(E_PI)).real() * fabs(GKK[IG1].z.real());
			t[IG1] = intensity / DOWN;

			E_SIGMA = CZERO;
			E_PI = CZERO;
			for (int IGK2=0; IGK2 < 2*rl.IGMAX; IGK2++)
			{
				E_PI    += slab->QIII[IGK1+0][IGK2] * EINCID[IGK2];
				E_SIGMA += slab->QIII[IGK1+1][IGK2] * EINCID[IGK2];
			}
			E_PI *= light.filter.pol[0];
			E_SIGMA *= light.filter.pol[1];
			intensity = (E_SIGMA*conj(E_SIGMA) + E_PI*conj(E_PI)).real() * fabs(GKK[IG1].z.real());
			r[IG1] = intensity / DOWN;
		}
		
	} else
	{
		
		for (int IGK1=0; IGK1 < 2*rl.IGMAX; IGK1++)
		{
			DOWN  += (EINCID[IGK1] * conj(EINCID[IGK1])).real();
		}
		for(int IG1=0; IG1<rl.IGMAX; IG1++)
		{
			c::complex<REAL> E_SIGMA = CZERO;
			c::complex<REAL> E_PI = CZERO;
			REAL intensity;
			int IGK1 = IG1*2;
			for (int IGK2=0; IGK2 < 2*rl.IGMAX; IGK2++)
			{
				E_PI += slab->QI[IGK1+0][IGK2] * EINCID[IGK2];
				E_SIGMA += slab->QI[IGK1+1][IGK2] * EINCID[IGK2];
			}
			E_PI *= light.filter.pol[0];
			E_SIGMA *= light.filter.pol[1];
			intensity = (E_SIGMA*conj(E_SIGMA) + E_PI*conj(E_PI)).real();
			t[IG1] = intensity / DOWN;

			E_SIGMA = CZERO;
			E_PI = CZERO;
			for (int IGK2=0; IGK2 < 2*rl.IGMAX; IGK2++)
			{
				E_PI    += slab->QIII[IGK1+0][IGK2] * EINCID[IGK2];
				E_SIGMA += slab->QIII[IGK1+1][IGK2] * EINCID[IGK2];
			}
			E_PI *= light.filter.pol[0];
			E_SIGMA *= light.filter.pol[1];
			intensity = (E_SIGMA*conj(E_SIGMA) + E_PI*conj(E_PI)).real();
			r[IG1] = intensity / DOWN;
		}
		
	}

}

};

// ------------------------------------------------------


#endif
