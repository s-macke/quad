#ifndef SPHRM_H
#define SPHRM_H

#include"global.h"

void Xlm(const int LMAX, CKCoord3D *Y, CKCoord3D *P, CKCoord3D *X, const CCoor3D &p);
void TestPLW();
void TestSphericalHarmonics();
void IntegrateSpherical();

#endif