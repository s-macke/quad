#ifndef CSTRING_H
#define CSTRING_H

#include "CVector.h"
#include"string.h"

#include<iostream>

static char nullstr = 0;

class CString
{
public:

	CString()
	{
//		str.assign(1, 0);
	}

	// copy constructor
	CString(CString const &s)
	{
		if (s.empty()) return;
		str = s.str;
	}

	CString(int num)
	{
		char s[16];
		sprintf(s, "%d", num);

		int l = strlen(s);

		str.assign(l+1, 0);
		for(int i=0; i<l; i++) str[i] = s[i];
	}
/*
	CString& operator=(CString const &s)
	{
		str = s.str;
		return *this;
	}

	// move
	CString(CString&& s)
	{
#ifdef USECPP11
		str = std::move(s.str);
#else
		str = s.str;
#endif	
	}
	CString& operator=(CString&& s)
	{
#ifdef USECPP11
		str = std::move(s.str);
#else
		str = s.str();
#endif
		return *this;
	}
*/

	CString& operator=(const char x[])
	{
		int l = strlen(x);
		if (l == 0)
		{
			str.clear();
            return *this;
        }
		str.assign(l+1, 0);
//		str.assign(l, 0);
		for(int i=0; i<l; i++) str[i] = x[i];

		return *this;
	}

	const CString& operator+=( const CString& r)
	{
	if ((!str.empty()) && (str.last() == 0)) str.erase(str.size()-1);
	if ((!str.empty()) && (str.last() == 0)) str.erase(str.size()-1);
	if ((!str.empty()) && (str.last() == 0)) str.erase(str.size()-1);

	for(unsigned int i=0; i<r.str.size(); i++)
	{
		if (r.str[i] == 0) continue;
		str.push_back(r.str[i]);
	}
	str.push_back(0);

	return *this;
}

	CString(const char x)
	{
		str.assign(2, 0);
		str[0] = x;
	}

	CString(const wchar_t x[])
	{
		int l = wcslen(x);
		str.assign(l+1, 0);
		for(int i=0; i<l; i++) str[i] = x[i];
	}


	CString(const char x[])
	{
		int l = strlen(x);
		str.assign(l+1, 0);
		for(int i=0; i<l; i++) str[i] = x[i];
	}

	int size() const
	{
		if (str.empty()) return 0;
		return str.size()-1;
	}

	bool empty() const
	{
		return str.size() <= 1;
	}

	void clear()
	{
		str.clear();
	}


	const char* c_str() const
	{
		if (str.empty())
		{
			return &nullstr;
        }
		return &str[0];
	}

//private:
	CVector<char> str;
};

/*
CString& operator+(CString& str,  char const& c)
{

return str;
}
*/

inline bool operator==( CString& l,  const CString& r)
{
	if (l.size() != r.size()) return false;
	for(int i=0; i<l.size(); i++)
	{
		if (l.c_str()[i] != r.c_str()[i]) return false;
	}
	return true;
}

inline bool operator!=( CString& l,  const CString& r)
{
	if (l.size() != r.size()) return true;
	for(int i=0; i<l.size(); i++)
	{
		if (l.c_str()[i] != r.c_str()[i]) return true;
	}
	return false;
}


inline const CString operator+( const CString& l,  const CString& r)
{
	CString s(l);
	if ((!s.str.empty()) && (s.str.last() == 0)) s.str.erase(s.str.size()-1);
	if ((!s.str.empty()) && (s.str.last() == 0)) s.str.erase(s.str.size()-1);
	if ((!s.str.empty()) && (s.str.last() == 0)) s.str.erase(s.str.size()-1);

	for(unsigned int i=0; i<r.str.size(); i++)
	{
        if (r.str[i] == 0) continue;
		s.str.push_back(r.str[i]);
	}
	s.str.push_back(0);
	return s;
}

inline std::ostream& operator<<(std::ostream& stream,  CString& obj)
{
	stream << obj.c_str();
	return stream;
}

inline std::istream& operator>>(std::istream& stream,  CString& obj)
{
//	stream << obj.c_str();
	obj.str.clear();

	while (stream.good())
	{
		char c;
		stream.get(c);
		if (stream.good()) obj.str.push_back(c);
	}
		obj.str.push_back(0);

	return stream;
}



#endif
