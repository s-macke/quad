#include"global.h"
#include"CSlab.h"

/*
C-----------------------------------------------------------------------  
C     THIS SUBROUTINE CALCULATES THE  Q-MATRICES FOR A HOMOGENEOUS  
C     PLATE  '2' OF THICKNESS 'D', HAVING THE SEMI-INFINITE MEDIUM  
C     '1' ON ITS LEFT AND THE SEMI-INFINITE MEDIUM '3' ON ITS RIGHT  
C     ------------------------------------------------------------------  
*/
void HOSLAB(const int IGMAX, 
	const c::complex<REAL> KAPPA1,
	const c::complex<REAL> KAPPA2, 
	const c::complex<REAL> KAPPA3, 
	const CCoor3D &AK, 
	const CVector<CCoor2D> &G,
	const CCoor3D DL,
	const CCoor3D DR,
	const REAL D,
	const REAL sigmal, const REAL sigmar, 
	CSlab &slab)
{
	int I, J, IA, IB, JA, IG1;
    REAL GKKPAR;
    c::complex<REAL> GKKZ1, GKKZ2, GKKZ3, Z1, Z2, Z3, CQI, CQII;
    c::complex<REAL> CQIII, CQIV, DENOMA, DENOMB, GKKDUM;

	c::complex<REAL> T[4][2], R[4][2], X[4], P[4][2];

	const c::complex<REAL> CZERO(0., 0.), CONE(1., 0.), CI(0., 1.), CTWO(2., 0.);
    int IGKMAX = 2 * IGMAX;

	for(IA=0; IA<IGKMAX; IA++)
	for(IB=0; IB<IGKMAX; IB++)
	{
		slab.QI  [IA][IB] = CZERO;
		slab.QII [IA][IB] = CZERO;
		slab.QIII[IA][IB] = CZERO;
		slab.QIV [IA][IB] = CZERO;
	}

	X[0] = KAPPA1 / KAPPA2;
	X[1] = CONE / X[0];
	X[2] = KAPPA2 / KAPPA3;
	X[3] = CONE / X[2];

	for(IG1=0; IG1<IGMAX; IG1++)
	{
		GKKPAR = sqrt(
			(AK.x + G[IG1].x) * (AK.x + G[IG1].x) + 
			(AK.y + G[IG1].y) * (AK.y + G[IG1].y));  
		GKKZ1 = sqrt(KAPPA1*KAPPA1 - GKKPAR*GKKPAR);
		GKKZ2 = sqrt(KAPPA2*KAPPA2 - GKKPAR*GKKPAR);
		GKKZ3 = sqrt(KAPPA3*KAPPA3 - GKKPAR*GKKPAR);
		/*complex<REAL> roughnessl = exp(-CTWO*GKKZ1*GKKZ2*(sigmal*sigmal));*/
		/*complex<REAL> roughnessr = exp(-CTWO*GKKZ2*GKKZ3*(sigmar*sigmar));*/
		//printf("%e %e %e %e %e %e\n", abs(roughnessl), abs(roughnessr), GKKZ1.real(), GKKZ1.imag(), GKKZ2.real(), GKKZ2.imag());
	for(J=0; J<2; J++)
	{
		DENOMA = X[J] * X[J] * GKKZ2 + GKKZ1;
		DENOMB = GKKZ2 + GKKZ1;

		if((abs(DENOMA) < EMACH)  || (abs(DENOMB) < EMACH))
		{
			fprintf(stderr, "Stop, FATAL ERROR IN HOSLAB");
			exit(1);
		}
		
		R[J][0] = (GKKZ1 - X[J] * X[J] * GKKZ2) / DENOMA;// * roughnessl;
		R[J][1] = (GKKZ1 - GKKZ2) / DENOMB; //* roughnessl; // fresnel for sigma top
		T[J][0] = CTWO * X[J] * GKKZ1 / DENOMA;
		T[J][1] = CTWO * GKKZ1 / DENOMB;
		//T[J][0] = CONE + R[J][0];
		//T[J][1] = CONE + R[J][1];
		GKKDUM = GKKZ1;
		GKKZ1 = GKKZ2;
		GKKZ2 = GKKDUM;
	}

	for (J=2; J<4; J++)
	{
      DENOMA = X[J] * X[J] * GKKZ3 + GKKZ2; 
      DENOMB = GKKZ3 + GKKZ2;
      if ((abs(DENOMA)<EMACH) || (abs(DENOMB)<EMACH))
	  {
		fprintf(stderr, "Stop, FATAL ERROR IN HOSLAB");
		exit(1);
	  }
      R[J][0] = (GKKZ2 - X[J] * X[J] * GKKZ3) / DENOMA; // * roughnessr;
      R[J][1] = (GKKZ2 - GKKZ3) / DENOMB; // * roughnessr; // fresnel for sigma bottom
      T[J][0] = CTWO*X[J]*GKKZ2 / DENOMA;
      T[J][1] = CTWO * GKKZ2 / DENOMB; // = 1+R[j][1]
	  //T[J][0] = CONE + R[J][0];
	  //T[J][1] = CONE + R[J][1];
      GKKDUM = GKKZ2;
      GKKZ2 = GKKZ3;
      GKKZ3 = GKKDUM;
	}

	Z1 = exp(CI * GKKZ2 * D);
	Z2 = Z1 * Z1;
	for (I=0; I<2; I++)
	{
		Z3 = CONE / (CONE - Z2 * R[1][I] * R[2][I]);
		P[0][I] = T[2][I] * Z3 * Z1 * T[0][I]; // transmitted top->top
		P[1][I] = R[3][I] + T[3][I] * R[1][I] * T[2][I] * Z2 * Z3;   // reflected bottom
		P[2][I] = R[0][I] + T[1][I] * R[2][I] * T[0][I] * Z2 * Z3;   // reflected top->top
		P[3][I] = T[1][I] * Z3 * Z1 * T[3][I];
	}
/*
	CQI  = exp(CI * ((AK.x + G[IG1].x) * (DL.x + DR.x) + (AK.y + G[IG1].y) * (DL.y + DR.y) + GKKZ1 * DL.z + GKKZ3 * DR.z));
	CQII = exp(CTWO * CI * GKKZ3 * DR.z);
	CQIII= exp(CTWO * CI * GKKZ1 * DL.z);
	CQIV = exp(-CI * ((AK.x + G[IG1].x) * (DL.x + DR.x) + (AK.y + G[IG1].y) * (DL.y + DR.y) -  GKKZ1 * DL.z - GKKZ3 * DR.z));
*/
/*
	for(JA=0; JA<2; JA++)
	{
		IA = 2 * IG1 + JA;
		slab.QI  [IA][IA] = CQI   * P[0][JA];
		slab.QII [IA][IA] = CQII  * P[1][JA];
		slab.QIII[IA][IA] = CQIII * P[2][JA];
		slab.QIV [IA][IA] = CQIV  * P[3][JA];
    }
*/
	for(JA=0; JA<2; JA++)
	{
		IA = 2 * IG1 + JA;
		slab.QI  [IA][IA] = P[0][JA];
		slab.QII [IA][IA] = P[1][JA];
		slab.QIII[IA][IA] = P[2][JA];
		slab.QIV [IA][IA] = P[3][JA];
    }

	} // for IG1
	
}
