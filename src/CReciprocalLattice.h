#ifndef CRECIPROCALLATTICE_H
#define CRECIPROCALLATTICE_H

#include"CLight.h"

void LAT2D(CCoor2D A, CCoor2D B, const REAL RMAX, int &IMAX, CVector<int> &NTA, CVector<int> &NTB, CVector<REAL> &VECMOD);

class CReciprocalLattice
{
	public:
	// real primitive lattice vectors
	CCoor2D AR1, AR2;
	
	// reciprocal lattice vector
	CCoor2D B1, B2;
	
	// reciprocal lattice index
	CVector<int> NT1, NT2;
	
	// reciprocal lattice length
	CVector<REAL> VECMOD;
	
	// reciprocal lattice coordinates
	CVector<CCoor2D> G;
	
	// area of cell
	REAL A0;
	
	// unit cell size in z-direction
	double az;

	int IGMAX;

	REAL GetArea()
	{
		return A0;
	}
/*c++11 feature
	CReciprocalLattice():CReciprocalLattice(CCoor2D(1., 0.), CCoor2D(0., 1.), 1.)
	{		
		az = 1.;
		//CReciprocalLattice()
	}
*/

    void AddReciprocalLatticeVector(int h, int k)
    {
        NT1.push_back(h);
        NT2.push_back(k);
        IGMAX++;
        G.assign(IGMAX, CCoor2D());
   		for (int IG1=0;IG1 < IGMAX; IG1++)
        {
			G[IG1].x = NT1[IG1] * B1.x + NT2[IG1] * B2.x;  
			G[IG1].y = NT1[IG1] * B1.y + NT2[IG1] * B2.y;  
			//fprintf(stderr, "%i   %i %i  %f %f\n", IG1, NT1[IG1], NT2[IG1], G[IG1].x, G[IG1].y);
        }
    }

    void SetReciprocalLatticeVector(int h, int k)
    {
		NT1.clear();
		NT2.clear();
		IGMAX = 0;
		AddReciprocalLatticeVector(0, 0);
		if ((h == 0) && (k==0)) return;
		AddReciprocalLatticeVector(h, k);
	}
	
	
    void SetRadius(double r)
    {
		NT1.clear();
		NT2.clear();
		VECMOD.clear();
		IGMAX = 0;
		
		LAT2D(B1, B2, r, IGMAX, NT1, NT2, VECMOD);
	
		G.assign(IGMAX, CCoor2D());
		for (int IG1=0;IG1 < IGMAX; IG1++)
		{
			G[IG1].x = NT1[IG1] * B1.x + NT2[IG1] * B2.x;  
			G[IG1].y = NT1[IG1] * B1.y + NT2[IG1] * B2.y;  
			//fprintf(stderr, "%i   %i %i  %f %f\n", IG1, NT1[IG1], NT2[IG1], G[IG1].x, G[IG1].y);
		}
    }

	CReciprocalLattice(CCoor2D _AR1=CCoor2D(1., 0.), CCoor2D _AR2=CCoor2D(0., 1.))
	{
		az = 1.;
		AR1 = _AR1;
		AR2 = _AR2;
		// NT1 = new int[IGD];
		// NT2 = new int[IGD];
		// VECMOD = new REAL[IGD];
		//G = new CCoor2D[IGD];

		// IGKMAX = 2 * IGMAX;
/*
		printf("Primitive lattice vectors\n");
		printf("AR1 = %lf %lf\n", AR1.x, AR1.y);
		printf("AR2 = %lf %lf\n", AR1.x, AR1.y);
*/
		A0 = fabs(AR1.x*AR2.y - AR1.y*AR2.x);  // area?
		REAL RA0 = 2. * M_PI / A0;
		/*
		B1.x = -AR1.y * RA0;
		B1.y =  AR1.x * RA0;
		B2.x = -AR2.y * RA0;
		B2.y =  AR2.x * RA0;
		*/
		B1.x = AR2.y * RA0;
		B1.y = -AR2.x * RA0;
		B2.x = -AR1.y * RA0;
		B2.y = AR1.x * RA0;
		
		IGMAX = 0;

/*
		printf("UNIT VECTORS IN RECIPROCAL SPACE:\n");
		printf("B1 = %f %f\n", B1.x, B1.y);
		printf("B2 = %f %f\n", B2.x, B2.y);
*/
		AddReciprocalLatticeVector(0, 0);


//		printf("RECIPROCAL VECTORS LENGTH\n");
	/*
		G.assign(IGMAX, CCoor2D());
		for (int IG1=0;IG1 < IGMAX; IG1++)
		{
			G[IG1].x = NT1[IG1] * B1.x + NT2[IG1] * B2.x;  
			G[IG1].y = NT1[IG1] * B1.y + NT2[IG1] * B2.y;  
			//fprintf(stderr, "%i   %i %i  %f %f\n", IG1, NT1[IG1], NT2[IG1], G[IG1].x, G[IG1].y);
		}
	*/
	}
};


void GetIncomingLightasReciprocalLattice(int IGMAX, CVector<c::complex<REAL> > &EINCID, const CReciprocalLattice &rl, CLight &light);
void REDUCE(const CReciprocalLattice &rl, CCoor3D &AK, const int IGMAX, int &IG0);

#endif

