#include<time.h>
#include<stdlib.h>
#include<stdio.h>
//#include<fenv.h>
//include<float.h>
#include"global.h"
#include"world.h"

#include"CAtom.h"
#include"functions.h"

#include"OpticalConstants.h"
#include"multilayer.h"
#include"script.h"
//include"models.h"

#include"sphrm.h"

void TestINP5();

// definitions
// QI: incoming top and transmitted
// QIII: incoming top and reflected	  DL coordinate
// QII: incoming bottom and reflected DR coordinate
// QIV: incoming bottom and transmitted
// index: first=row, second=column
// EINCID: 2 neighbored indices belong to the same G-vector They are P-light and S-light

/*
     ------------------------------------------------------------------  
     THIS SUBROUTINE CALCULATES THE REFLECTIVITY, TRANSMITTANCE AND  
     ABSORBANCE OF A FINITE SLAB, CHARACTERIZED BY TRANSMISSION AND 
     REFLECTION MATRICES QI AND QIII, RESPECTIVELY.  
     ------------------------------------------------------------------  
*/

void ChangeQforScatteringplane(CLight &light, const CCoor3D &out, double q)
{
	CCoor3D n;
	//cerr << q << "\n";
	n.x = light.AK.x - out.x;
	n.y = light.AK.y - out.y;
	n.z = light.AK.z - out.z;
	double r = sqrt((n.x*n.x) + (n.y*n.y) + (n.z*n.z));
	n.x /= r;
	n.y /= r;
	n.z /= r;
	//cerr << n.x << " " << n.y << " " << n.z << "\n";
	
	CCoor3D kvpar;
	CCoor3D kvper;
	kvper.x = light.AK.x*n.x;
	kvper.y = light.AK.y*n.y;
	kvper.z = light.AK.z*n.z;
	kvpar.x = light.AK.x - kvper.x;
	kvpar.y = light.AK.y - kvper.y;
	kvpar.z = light.AK.z - kvper.z;
	
	r = sqrt((kvpar.x*kvpar.x) + (kvpar.y*kvpar.y) + (kvpar.z*kvpar.z));
	kvpar.x /= r;
	kvpar.y /= r;
	kvpar.z /= r;
	
	REAL kper = 0.5*q;
	REAL kpar = sqrt(light.KAPPA0*light.KAPPA0 - kper*kper);
	
	light.AK.x = kvpar.x * kpar + n.x * kper;
	light.AK.y = kvpar.y * kpar + n.y * kper;
	light.AK.z = kvpar.z * kpar + n.z * kper;
	light.FI = atan2(light.AK.y, light.AK.x)*180./M_PI;
	light.THETA = acos(light.AK.z/light.KAPPA0)*180./M_PI;
	light.CalculateWavevector();
}

int GetIncomingBeamFromHKL(int h, int k, double l, CLight &light, const CReciprocalLattice &rl)
{
//CCoor2D B1, B2;
//const CCoor3D &AK;
	CCoor3D B3;
	B3.x = 0.0;
	B3.y = 0.0;
	B3.z = REAL(2.*M_PI) / rl.az;

	int index = -1;
	for(int i=0; i<rl.IGMAX; i++)
	{
		if ((rl.NT1[i] != h) || (rl.NT2[i] != k)) continue;
		index = i;
		break;
	}

	if (index == -1)
	{
		fprintf(stderr, "Error in HKL: proper index not found\n");
		return index;
	}
	//printf("B1x = %f, B1y = %f\n", rl.B1.x, rl.B1.y);
	//printf("B2x = %f, B2y = %f\n", rl.B2.x, rl.B2.y);

	REAL gx, gy, gz;
	gx = rl.B1.x * h + rl.B2.x * k + B3.x * l;
	gy = rl.B1.y * h + rl.B2.y * k + B3.y * l;
	gz =      0. * h +      0. * k + B3.z * l;
	REAL K0 = light.KAPPA0;

/*
	printf("h k l = %i %i %i\n", h, k, l);
	printf("index = %i\n", index);
	printf("phi = %f\n", light.FI);
	printf("az = %f\n", rl.az);
	printf("d = %f\n", 2.*M_PI/sqrt(gx*gx+gy*gy+gz*gz));
*/
// gz = -gz;
// gx = -gx;
// gy = -gy;
// gz = -gz;

REAL theta1 = 0.;
{
double phi = light.FI/180.*M_PI;
double cg1 = gx/K0;
double cg3 = gy/K0;
double cg5 = gz/K0;
double t2 = sin(phi);
double t3 = t2 * cg3;
double t4 = cg5*cg5;
double t6 = cg1*cg1;
double t8 = cos(phi);
double t11 = cg3 * cg3;
double t20 = t4 * t4;
double t21 = t8 * t8;
double t22 = t11 * t21;
double t33 = cg1 * t8;
double t37 = t6 * t6;
double t42 = t11 * t11;
double t43 = -2. * t11 * t4 - t20 - 4. * t22 + 4. * t6 * t21 + 4. * t4 + 4. * t11 + 8. * t33 * t3 - t37 - 2. * t4 * t6 - 2. * t11 * t6 - t42;
double t44 = sqrt(t43);
double t45 = fabs(cg5);
double t53 = 1.;
double t59 = (cg5>0 ? 1 : (cg5<0 ? -1 : 0));
double t60 = t44 * (double)t59;
double t67 = atan2(	-t53 * (t4 * t3 + t6 * t3 + cg1 * t6 * t8 + t2 * cg3 * t11 + cg1 * t8 * t11 + cg1 * t8 * t4 + t45 * t44) / 2., 
					t53 * (-cg5 * t4 - t6 * cg5 - t11 * cg5 + t60 * t3 + t60 * t33) / 2.);
theta1 = t67;
}

REAL theta2 = 0.;
{
double phi = light.FI/180.*M_PI;
double cg1 = gx/K0;
double cg3 = gy/K0;
double cg5 = gz/K0;
double t2 = sin(phi);
double t3 = t2 * cg3;
double t4 = cg5*cg5;
double t6 = cg1*cg1;
double t8 = cos(phi);
double t11 = cg3 * cg3;
double t20 = t4 * t4;
double t21 = t8 * t8;
double t22 = t11 * t21;
double t33 = cg1 * t8;
double t37 = t6 * t6;
double t42 = t11 * t11;
double t43 = -2. * t11 * t4 - t20 - 4. * t22 + 4. * t6 * t21 + 4. * t4 + 4. * t11 + 8. * t33 * t3 - t37 - 2. * t4 * t6 - 2. * t11 * t6 - t42;
double t44 = sqrt(t43);
double t45 = fabs(cg5);
double t53 = 1.;
double t59 = (cg5>0. ? 1. : (cg5<0 ? -1. : 0.));
double t60 = t44 * t59;
double t67 = atan2(	-t53 * (t4 * t3 + t6 * t3 + cg1 * t6 * t8 + t2 * cg3 * t11 + cg1 * t8 * t11 + cg1 * t8 * t4 - t45 * t44) / 2., 
					-t53 * (cg5 * t4 + t6 * cg5 + t11 * cg5 + t60 * t3 + t60 * t33) / 2.);
theta2 = t67;
}


/*
double sqrt1 = gx*gx*(-gx*gx*gx*gx + 4.*K0*K0*gx*gx + 4.*K0*K0*gz*gz - 2.*gz*gz*gy*gy - 2.*gz*gz*gx*gx - gz*gz*gz*gz - 2.*gy*gy*gx*gx - gy*gy*gy*gy);
double denom1 = ((gz*gz + gx*gx)*gx*K0);
double denom2 = (K0*(gz*gz + gx*gx));

// transmission
theta1 = atan2(
-(1./2.)*(gz*gz*gx*gx + gx*gx*gx*gx + gy*gy*gx*gx + gz*sqrt(sqrt1)) / denom1
,
(1./2.)*(-gz*gx*gx - gz*gy*gy - gz*gz*gz + sqrt(sqrt1)) / denom2
);

// reflection
theta2 = atan2(
(1./2.)*(-gz*gz*gx*gx - gx*gx*gx*gx - gy*gy*gx*gx + gz*sqrt(sqrt1)) / denom1
,
-(1./2.)*(gz*gx*gx + gz*gy*gy + gz*gz*gz + sqrt(sqrt1)) / denom2
);
*/

/*
printf("theta1 = %f\n", theta1);
printf("theta1 = %f degrees\n", theta1*180./M_PI);
printf("theta2 = %f\n", theta2);
printf("theta2 = %f degrees\n", theta2*180./M_PI);
*/

//printf("%f %f\n", theta1*180./M_PI, theta2*180./M_PI);
//cout << theta1 << " " << theta2 << "\n";
bool valid1 = true;
if (!isnormal(theta1)) valid1 = false; 
else
{
	if (theta1 < M_PI/2.) theta1 += 2.*M_PI;
	if (theta1 > M_PI*3./2.) valid1 = false;
}

bool valid2 = true;
if (!isnormal(theta2)) valid2 = false; 
else
{
	if (theta2 < M_PI/2.) theta2 += 2.*M_PI;
	if (theta2 > M_PI*3./2.) valid2 = false;
}
//printf("%i %i %le %le %e\n", valid1, valid2, theta1, theta2, K0);


if ((!valid1) && (!valid2)) return -1;

if (valid1)
	light.THETA = theta1*180. / M_PI;

if (valid2)
	light.THETA = theta2*180. / M_PI;

light.CalculateWavevector();
//return index;
/*
double ONE =
(cos(theta1)         + gz/K0) * (cos(theta1)         + gz/K0) + 
(sin(theta1)*sin(0.) + gy/K0) * (sin(theta1)*sin(0.) + gy/K0) +
(sin(theta1)*cos(0.) + gx/K0) * (sin(theta1)*cos(0.) + gx/K0);
cout << "theta1 ONE: " << ONE << "\n";
ONE =
(cos(theta2)         + gz/K0) * (cos(theta2)         + gz/K0) + 
(sin(theta2)*sin(0.) + gy/K0) * (sin(theta2)*sin(0.) + gy/K0) +
(sin(theta2)*cos(0.) + gx/K0) * (sin(theta2)*cos(0.) + gx/K0);
cout << "theta2 ONE: " << ONE << "\n";
*/

	CVector<CKCoord3D> GKK;
	GKK.assign(rl.IGMAX, CKCoord3D());
	for(int IG1=0; IG1<rl.IGMAX; IG1++)
	{
		GKK[IG1] = ReciprocalWaveVector(light.AK, rl.G[IG1], light.KAPPA0);
		//GKK[IG1].z = -GKK[IG1].z; // reflection
	}
	
	REAL qx = -REAL(GKK[index].x.real() - light.AK.x);
	REAL qy = -REAL(GKK[index].y.real() - light.AK.y);
	REAL qz = -REAL(GKK[index].z.real() - light.AK.z);
	if (l > 0)
	{
	qz = -REAL(-GKK[index].z.real() - light.AK.z);
	}
	
	if (fabs(gz+qz) > 1e-5) index = -1; 
	
	/*
	cerr << qx << " " << gx << "\n";
	cerr << qy << " " << gy << "\n";
	cerr << qz << " " << gz << "\n";
	cerr << "koutx = " << GKK[index].x.real() << "\tkinx = " << light.AK.x << "\n";
	cerr << "kouty = " << GKK[index].y.real() << "\tkiny = " << light.AK.y << "\n";
	cerr << "koutz = " << GKK[index].z.real() << "\tkinz = " << light.AK.z << "\n";

	cerr << "THETA = " << light.THETA << "\n";
	cerr << "FI = " << light.FI << "\n";

	cerr << "KAPPA0 = " << light.KAPPA0 << "\n";
	cerr << "LAMBDA = " << 2.*M_PI/light.KAPPA0 << "\n";
*/
/*
	REAL r = sqrt(gx*gx + gy*gy + gz*gz);
	gx = gx / r;
	gy = gy / r;
	gz = gz / r;
	
	cerr << "projected q = " << qx*gx + qy*gy + qz*gz << "\n";
	cerr << "\n";
*/
//exit(1);

	return index;
}

void SCAT(
	const int IGMAX, 
	const CVector< c::complex<REAL> > &EINCID,
	c::complex<REAL> **QI,
	c::complex<REAL> **QIII,
	const CVector<CCoor2D> &G, const c::complex<REAL> KAPPA, const CCoor3D &AK)
{
	CVector<CKCoord3D> GKK;
	GKK.assign(IGMAX, CKCoord3D());
	for(int IG1=0; IG1<IGMAX; IG1++)
	{
		GKK[IG1] = ReciprocalWaveVector(AK, G[IG1], KAPPA);
	}

/*
	for (int i=0; i < 2*IGMAX; i++)
	{
		printf("%e %e\n", EINCID[i].real(), EINCID[i].imag());
	}
*/
/*
	for(int j=0; j<IGMAX; j++)
	for(int i=0; i<IGMAX; i++)
	{
		printf("%i %i %e %e\n", i, j, QIII[i][j].real(), QIII[i][j].imag());
	}
	printf("\n");
*/
	// exit(1);
	int IGK1, IG1, K1, IGK2, IGKMAX;
	REAL DOWN, REFLE, TRANS, ABSOR, GKZIN, GKZOUT, TES1;
	c::complex<REAL> CZERO(0., 0.);

	IGKMAX = 2 * IGMAX;

	//c::complex<REAL> ETRANS[IGKMAX], EREFLE[IGKMAX];
	c::complex<REAL> ETRANS, EREFLE;

	REFLE = 0.;
	TRANS = 0.;
	DOWN = 0.;
	IGK1 = 0;

//	cerr << "specularindex = " << specularindex << " DOWN=" << DOWN << "\n";
/*
		int specularindex = 0;
		for(IG1=0; IG1<IGKMAX; IG1++)
		{
			if ( abs(EINCID[IG1]) > 1e-2) 
			{
				specularindex = IG1;
				break;
			}
		}
*/
	for(IG1=0; IG1<IGMAX; IG1++)
	{
		for (K1=0; K1<2; K1++)
		{
			ETRANS = CZERO;
			EREFLE = CZERO;
			for (IGK2=0; IGK2 < IGKMAX; IGK2++)
			{
				ETRANS += QI  [IGK1][IGK2] * EINCID[IGK2];
				EREFLE += QIII[IGK1][IGK2] * EINCID[IGK2];
			}
			
			DOWN  += (EINCID[IGK1] * conj(EINCID[IGK1])).real()*GKK[IG1].z.real();
			TRANS += (ETRANS * conj(ETRANS)).real()*GKK[IG1].z.real();
			REFLE += (EREFLE * conj(EREFLE)).real()*GKK[IG1].z.real();
/*
		if (IGK1 == specularindex)
		{
			REAL specularrefl = 0.;
			specularrefl += (EREFLE[specularindex] * conj(EREFLE[specularindex])).real();
			std::cout << (specularrefl)	<< " " << 0. << " ";
		}
*/
			IGK1++;
		}
//		if (IG1 == 0) printf("refl=%e trans=%e ", REFLE/DOWN, TRANS/DOWN);
	}
      TRANS = TRANS / DOWN;
      REFLE = REFLE / DOWN;
      ABSOR = 1. - TRANS - REFLE;
//	  printf("trans=%e refl=%e absor=%e\n", TRANS, REFLE, ABSOR);
	std::cout << REFLE << " " << TRANS << " " << ABSOR<< "\n";
}

void HKLDiffraction(int h, int k, int l, REAL phi, CWorld &world)
{
	const c::complex<REAL> CZERO(0., 0.);
	
	//CLight light;
	world.light.THETA = 0.0; // perpendicular incidence
	world.light.FI = phi;
	world.light.POLAR = 'S';
	//light.POLAR = 'L';
	//light.SetEnergy(energy); // eV
	CSlab slab(world.rl.IGMAX * 2);
	CVector< c::complex<REAL> > EINCID;
	EINCID.assign(2 * world.rl.IGMAX, CZERO);
	CVector<CKCoord3D> GKK;
	GKK.assign(world.rl.IGMAX, CKCoord3D());

	for(int engidx=0; engidx<=200; engidx++)
	{
		REAL energy = 8333. - 50. + (REAL(engidx)/2.);
		world.light.SetEnergy(energy);
		int index = GetIncomingBeamFromHKL(h, k, l, world.light, world.rl);
		GetIncomingLightasReciprocalLattice(world.rl.IGMAX, EINCID, world.rl, world.light);
		//BuildScatterMatrix(LMAX, layer, rl, light, slab, ELM, 10);
		BuildScatterMatrix(world, slab);
		//cerr << "lambda: " << REAL(2.*M_PI)/light.KAPPA0 << "\n";
		for(int IG1=0; IG1<world.rl.IGMAX; IG1++) GKK[IG1] = ReciprocalWaveVector(world.light.AK, world.rl.G[IG1], world.light.KAPPA0);		
		
		c::complex<REAL> ETRANS_SIGMA, ETRANS_PI;
		c::complex<REAL> EREFLE_SIGMA, EREFLE_PI;
		c::complex<REAL> EIN_SIGMA, EIN_PI;
	
		ETRANS_SIGMA = CZERO;
		ETRANS_PI = CZERO;
		EREFLE_SIGMA = CZERO;
		EREFLE_PI = CZERO;
		//EIN_SIGMA = CZERO;
		//EIN_PI = CZERO;

		REAL intensityi = 0.;
		for(int IG1=0; IG1<world.rl.IGMAX*2; IG1++)
		{
			intensityi += (EINCID[IG1]*conj(EINCID[IG1])).real();
		}

		int IGK1 = index*2;
		for (int IGK2=0; IGK2 < 2*world.rl.IGMAX; IGK2++)
		{
			ETRANS_SIGMA += slab.QI  [IGK1+0][IGK2] * EINCID[IGK2];
			EREFLE_SIGMA += slab.QIII[IGK1+0][IGK2] * EINCID[IGK2];
			ETRANS_PI    += slab.QI  [IGK1+1][IGK2] * EINCID[IGK2];
			EREFLE_PI    += slab.QIII[IGK1+1][IGK2] * EINCID[IGK2];
		}
		REAL intensityt = (ETRANS_SIGMA*conj(ETRANS_SIGMA) + ETRANS_PI*conj(ETRANS_PI)).real();
		REAL intensityr = (EREFLE_SIGMA*conj(EREFLE_SIGMA) + EREFLE_PI*conj(EREFLE_PI)).real();				
		//printf("%f %e %e %e %e %e\n", energy, intensityr, intensityt, intensityi, world.light.THETA, world.light.FI);
		cout << " " << energy << " " << intensityr << " " << intensityt << " " << intensityi << " " << world.light.THETA << " " << world.light.FI << "\n";
		fflush(stdout);
	}
}

void Reflectivity(REAL energy, CWorld &world)
{
	const c::complex<REAL> CZERO(0., 0.);
	
	//CLight light;
	world.light.THETA = 89.9; // grazing incidence
	world.light.FI = 0.;
	//world.light.POLAR = 'S';
	world.light.POLAR = 'L';
	//light.SetEnergy(energy); // eV
	CSlab slab(world.rl.IGMAX * 2);
	CVector< c::complex<REAL> > EINCID;
	EINCID.assign(2 * world.rl.IGMAX, CZERO);
	CVector<CKCoord3D> GKK;
	GKK.assign(world.rl.IGMAX, CKCoord3D());

	world.light.SetEnergy(energy);

	//for(double theta=89.90; theta>=0.; theta-=0.5)
	for(REAL theta=180.25; theta>=-180.25; theta-=0.5)
	{
		world.light.THETA = theta;
		world.light.CalculateWavevector();
		GetIncomingLightasReciprocalLattice(world.rl.IGMAX, EINCID, world.rl, world.light);
		//BuildScatterMatrix(LMAX, layer, rl, light, slab, ELM, 10);
		BuildScatterMatrix(world, slab);
		//cerr << "lambda: " << REAL(2.*M_PI)/light.KAPPA0 << "\n";
		for(int IG1=0; IG1<world.rl.IGMAX; IG1++) GKK[IG1] = ReciprocalWaveVector(world.light.AK, world.rl.G[IG1], world.light.KAPPA0);		
		
		c::complex<REAL> ETRANS_SIGMA, ETRANS_PI;
		c::complex<REAL> EREFLE_SIGMA, EREFLE_PI;
		c::complex<REAL> EIN_SIGMA, EIN_PI;
	
		ETRANS_SIGMA = CZERO;
		ETRANS_PI = CZERO;
		EREFLE_SIGMA = CZERO;
		EREFLE_PI = CZERO;
		//EIN_SIGMA = CZERO;
		//EIN_PI = CZERO;

		REAL intensityi = 0.;
		for(int IG1=0; IG1<world.rl.IGMAX*2; IG1++)
		{
			intensityi += (EINCID[IG1]*conj(EINCID[IG1])).real();
		}
		
		int index = 0.;		
		int IGK1 = index*2;
		for (int IGK2=0; IGK2 < 2*world.rl.IGMAX; IGK2++)
		{
			ETRANS_SIGMA += slab.QI  [IGK1+0][IGK2] * EINCID[IGK2];
			EREFLE_SIGMA += slab.QIII[IGK1+0][IGK2] * EINCID[IGK2];
			ETRANS_PI    += slab.QI  [IGK1+1][IGK2] * EINCID[IGK2];
			EREFLE_PI    += slab.QIII[IGK1+1][IGK2] * EINCID[IGK2];
		}
		REAL intensityt = (ETRANS_SIGMA*conj(ETRANS_SIGMA) + ETRANS_PI*conj(ETRANS_PI)).real();
		REAL intensityr = (EREFLE_SIGMA*conj(EREFLE_SIGMA) + EREFLE_PI*conj(EREFLE_PI)).real();				
		printf("%f %e %e %e %e %e\n", energy, intensityr, intensityt, intensityi, world.light.THETA, world.light.FI);
	}
}

void IntegrateSpherical();
void IntegrateSpherical2();
void IntegrateSpherical3();

int main(int argc, char *argv[])
{
#ifdef EMSCRIPTEN
	return 0;
#endif


    const c::complex<REAL> CZERO(0., 0.);
    const c::complex<REAL> CONE(1., 0.); 	
	
	//IntegrateSpherical3();
	//return 0;
	
	//TestSphericalHarmonics();
	//IntegrateSpherical();

	CWorld world;
	world.SetLMAX(1);
	//fetestexcept(FE_DIVBYZERO | FE_INVALID | FE_OVERFLOW);
	//feenableexcept(FE_DIVBYZERO | FE_INVALID | FE_OVERFLOW);
	//_controlfp(0, _EM_OVERFLOW);

	if (argc != 2)
	{
		fprintf(stderr, "MultemX Version 1.6\nCopyright Max Planck Gesellschaft\nProgrammed by Sebastian Macke (sebastian@macke.de)\nUsage: multemX luafile\n");
		return 1;
	}
	/*
	c::complex<double> x(2.5, 0.);
	x = abs(x);
	cout << x.real() << " " << x.imag() << "\n";
	exit(1);
	*/
	int ret = ExecuteFile(argv[1], world);
	return ret;

// ---------------------
	world.light.THETA = 89.0; // grazing incidence
	world.light.FI = 0.;
	world.light.POLAR = 'S';
	world.light.SetEnergy(2000.); // eV
	cerr << "k0: " << world.light.KAPPA0 << "\n";
// ---------------------

	//c::complex<REAL> N1(1e-4, -1e-4);
	c::complex<REAL> N1(1e-4, 0.);

	// c::complex<REAL> N2(1e-4, -2e-4);
	REAL S = 0.1;
	REAL scale = 4.*4.*4. / (4./3. * M_PI * S*S*S);

	// TE = CI*REAL(2.)/REAL(9.) * R*R*R*KAPPA*KAPPA*KAPPA * (EPSSPH-REAL(1.));		
	// TE = CI*1/(3*Pi) * KAPPA*KAPPA*KAPPA * N; // with N = -delta+I*beta		
	// TE = CI*2/(3) * KAPPA * r0 * D * f; // with N = -delta+I*beta		

	//c::complex<REAL> f = c::complex<REAL>(0.14*64, 0.);
	//c::complex<REAL> f = c::complex<REAL>(0., 400.);

	c::complex<REAL> f = c::complex<REAL>(10., -1.); // negative imaginary value is damping
	//c::complex<REAL> f = c::complex<REAL>(10.*0.0015, -0.000000002); // negative imaginary value is damping
	N1 = f * c::complex<REAL>(2.*M_PI, 0.)/(world.light.KAPPA0*world.light.KAPPA0)*REAL(2.8179403267e-5) * REAL(1./(4.*4.*4.));
	cerr << "N1: " << N1.real() << " " << N1.imag() << "\n";

	 
/*
	{
	CAtom *atoma;
	CAtom *atomb;
	//atoma = new CSphereAtom((CONE - N1 * scale) * (CONE - N1 * scale), S);
	atoma = new CSphereAtom(0.999, S);
	atomb = new CScatteringFactorAtom(f);
	atoma->Set(1, light.KAPPA0);
	atomb->Set(1, light.KAPPA0);

	
	//c::complex<REAL> TE[2];
	//c::complex<REAL> TH[2];
	//atoma->GetIsotropicScatterMatrix(TE, TH);
	//cerr << TE[1].real() << " " << TE[1].imag() << "\n";
	//cerr << TH[1].real() << " " << TH[1].imag() << "\n";
	//atomb->GetIsotropicScatterMatrix(TE, TH);
	//cerr << TE[1].real() << " " << TE[1].imag() << "\n";
	//cerr << TH[1].real() << " " << TH[1].imag() << "\n";
	
	CKCoord3D K;
	K.x = 1.*light.KAPPA0;
	K.y = 0.*light.KAPPA0;
	K.z = 0.*light.KAPPA0;
	CExpansionCoefficients A(1);
	CExpansionCoefficients B(1);
	PLW(light.KAPPA0, K, 1, A);
	atomb->Scatter(B, A);
	//cerr << scientific << A.Elm[1].pol[1].real() << " " << A.Elm[1].pol[1].imag() << "\n";
	cerr << scientific << B.Elm[1].pol[1].real() << " " << B.Elm[1].pol[1].imag() << "\n";
	cerr << scientific << B.Elm[2].pol[1].real() << " " << B.Elm[2].pol[1].imag() << "\n";
	cerr << scientific << B.Elm[3].pol[1].real() << " " << B.Elm[3].pol[1].imag() << "\n";

	delete atoma;
	delete atomb;
	//return 0;	
	}
	*/
/*
	CLayer l;
	l.IT = 2;
	l.EPSMED = 1.;
	l.NLAYER = 16; // (1 << 9)
	l.S[0] = S;
	l.EPSSPH[0] = (CONE - N1 * scale) * (CONE - N1 * scale);
	cerr << "EPS: " << l.EPSSPH[0].real() << " " << l.EPSSPH[0].imag() << "\n";
	l.DT = CCoor3D(0., 0., 0.5);
	l.DB = CCoor3D(0., 0., 0.5);
	layer.push_back(l);
*/
	CLayer l;

	l.IT = 2; // Atoms
	l.EPSMED = 1.;
	//l.NLAYER = 7;
	l.NLAYER = 0;

	//l.AddAtom(new CSphereAtom((CONE - N1 * scale) * (CONE - N1 * scale), S), CCoor3D(0., 0., 0.));
	//l.AddAtom(new CSphereAtom(0.999, S), CCoor3D(0., 0., 0.));
	//l.AddAtom(new CSphereAtom(0.5, S), CCoor3D(0., 0., 0.));
	l.AddAtom(new CScatteringFactorAtom(f), CCoor3D(0.0, 0.0, 0.)); // O
	// cerr << "EPS: " << l.atom->e[0].real() << " " << l.atom->e[0].imag() << "\n";
	l.DT = CCoor3D(0., 0., 2.);
	l.DB = CCoor3D(0., 0., 2.);
	world.layer.push_back(l);

/*
	l.IT = 2;
	l.EPSMED = 1.;
	l.NLAYER = 20; // (1 << 20)
	l.S[0] = 0.1;
	l.EPSSPH[0] = (CONE - N2 * scale) * (CONE - N2 * scale);
	l.DT = CCoor3D(0.5, 0.5, 0.5);
	l.DB = CCoor3D(0.5, 0.5, 0.5);
	layer.push_back(l);
*/

/*
	layer.clear();
	l.IT    = 1;
	l.D     = 1<<(20-1);
	l.EPSL  = 1.;
	l.EPSMED  = (CONE - N1)*(CONE - N1);
	l.EPSR  = 1.;
	l.DT = CCoor3D(0., 0., 0.);
	l.DB = CCoor3D(0., 0., 0.);
	layer.push_back(l);
*/

/*
	l.IT    = 1;
	l.D     = 100000.;
	l.EPSL  = (CONE - N2)*(CONE - N2);
	l.EPSMED  = (CONE - N2)*(CONE - N2);
	l.EPSR  = 1.;
	l.DT = CCoor3D(0., 0., 0.);
	l.DB = CCoor3D(0., 0., 0.);
	layer.push_back(l);
*/

	//l.atom = NULL; // otherwise it is deleted twice. TODO: better solution, maybe shared_ptr

// ****** DEFINE THE 2D DIRECT AND RECIPROCAL-LATTICE VECTORS ******  
	world.rl = CReciprocalLattice(CCoor2D(4., 0.), CCoor2D(0., 4.));
// ----------------------------------
	//world.rl = CReciprocalLattice(CCoor2D(8., 0.), CCoor2D(0., 8.), 4.);
	//world.light.SetEnergy(8333.);
	//GetIncomingBeamFromHKL(1, 1, 1, world.light, world.rl);
// ----------------------------------

	LoadOpticalConstants("La", "Perovskite/La.ff");
	LoadOpticalConstants("O", "Perovskite/O.ff");
	LoadOpticalConstants("Al", "Perovskite/Al.ff");
	LoadOpticalConstants("Ni1", "Perovskite/unrotated/Ni1.F");
	LoadOpticalConstants("Ni2", "Perovskite/unrotated/Ni2.F");
	//LoadOpticalConstants("Ni1", "Perovskite/Ni.ff");
	//LoadOpticalConstants("Ni2", "Perovskite/Ni.ff");
	
	//BuildPerovskite(layer, rl);
	//BuildTiltedPerovskite(world);
	//BuildLargePerovskite(world);
	//BuildPrimitive(world);
	//BuildLargePrimitive(layer, rl); world.multilayer="10*(0)";
	//BuildLNOLAOBE125Perovskite(world);
	//BuildLNOLAOBE49Perovskite(world);
	//BuildThinFilm(world);

// ----------------------------------

clock_t starttime = clock();

/*
	light.KAPPA0 = 1.;
	light.THETA = 0.; // perpendicular
	light.CalculateWavevector();
	printf("AKX=%e AKY=%e AKZ=%e\n", light.AK[0], light.AK[1], light.AK[2]);
	GetIncomingLightasReciprocalLattice(rl.IGMAX, EINCID, rl, light, EMACH);

	N1 = c::complex<REAL>(1e-4, 0.);
	S = 0.1;
	scale = 1. / (4./3. * M_PI * S*S*S);
	layer[0].EPSSPH[0] = (CONE - N1 * scale) * (CONE - N1 * scale);
	cout << "n= " << layer[0].EPSSPH[0].real() << " " << layer[0].EPSSPH[0].imag() << "\n"; 
	layer[0].S[0] = S;

	layer[0].DT = CCoor3D(0., 0., 0.);
	layer[0].DB = CCoor3D(0., 0., 0.);
	
	PCSLAB(LMAX, rl.IGMAX, layer[0], layer[0].EPSSPH[0], 
				light.KAPPA0, light.AK,
				rl, ELM, EMACH, QIR, QIIR, QIIIR, QIVR);

// QI: incoming top and transmitted
// QII: incoming bottom and reflected   DB coordinate
// QIII: incoming top and reflected	    DT coordinate
// QIV: incoming bottom and transmitted
//for(int i=0; i<21; i++) printf("EINCID = %e\n", abs(EINCID[i]));
//	printf("IGMAX = %i\n", rl.IGMAX);
//	printf("QI  =%e %e\n", abs(QIR[0][0]), abs(QIR[1][1]));
//	printf("QII =%e %e\n", abs(QIIR[0][0]), abs(QIIR[1][1]));
//	printf("QIII=%e %e\n", abs(QIIIR[0][0]), abs(QIIIR[1][1]));
//	printf("QIV =%e %e\n", abs(QIVR[0][0]), abs(QIVR[1][1]));

//	printf("QI  =%e %e\n", QIR[0][0].real(), QIR[0][0].imag());
//	printf("QII =%e %e\n", QIIR[0][0].real(), QIIR[0][0].imag());
//	printf("QIII=%e %e\n", QIIIR[0][0].real(), QIIIR[0][0].imag());
//	printf("QIV =%e %e\n", QIVR[0][0].real(),  QIVR[0][0].imag());

//	SCAT(rl.IGMAX, EINCID, QIR, QIIIR);
// ----------------------------------

return 0;
*/
world.PrintStats();
//Laue(world);
//TestINP5();
HKLDiffraction(1, 0, 1, -53-45, world);
//HKLDiffraction(0, 0, 1, world);
//HKLDiffraction(1, 1, 1, -50, world);

//Reflectivity(800, world);

/*
	//	****** SCANNING OVER FREQUENCIES/WAVELENGTHS ******  
	CSlab slab(IGKMAX);
	CVector< c::complex<REAL> > EINCID;
	EINCID.assign(IGKMAX, CZERO);
	for(light.THETA=89.99; light.THETA >= 0.; light.THETA -= 0.1)
	{
		light.CalculateWavevector();
		BuildScatterMatrix(LMAX, layer, rl, light, slab, ELM);
		
		GetIncomingLightasReciprocalLattice(rl.IGMAX, EINCID, rl, light);
		std::cout << light.THETA << " ";
		SCAT(rl.IGMAX, EINCID, slab.QI, slab.QIII, rl.G, light.KAPPA0, light.AK);
		
		//return 1;
	}
*/
	clock_t deltatime = clock() - starttime;
	fprintf(stderr, "elapsed time: %f s\n",  double(deltatime)/double(CLOCKS_PER_SEC));

}


