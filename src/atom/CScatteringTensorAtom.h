
class CScatteringTensorFileAtom : public CAtom
{
	public:
		CScatteringTensorFileAtom(const CScatteringTensorFileAtom& r)
		{
			name = r.name;
			for(int i=0; i<3; i++)
			for(int j=0; j<3; j++)
			{
				F[i][j] = r.F[i][j];
				T[i][j] = r.T[i][j];
			}
		}

		CScatteringTensorFileAtom(const char* _name)
		{
			name = CString(_name);
			for(int i=0; i<3; i++)
			for(int j=0; j<3; j++)
			{
				F[i][j] = 0.;
				T[i][j] = 0.;
			}
		}
		
		void GetIsotropicScatterMatrix(c::complex<REAL> *TE, c::complex<REAL> *TH, int LMAX) const
		{
			TE[0] = 0.;
			TH[0] = 0.;
			TE[1] = (T[0][0] + T[1][1] + T[2][2]) * (1./ 3.);
			TH[1] = 0.;
		}
		
		void Set(const int LMAX, const c::complex<REAL> &KAPPA, const REAL Efield)
		{
			c::complex<REAL> CI(REAL(0.), REAL(1.));
			c::complex<REAL> CZERO(REAL(0.), REAL(0.));

			REAL lambda = 2.*M_PI / KAPPA.real();
			REAL energy = 12398.326 / lambda;
			CTableEntry te = GetOpticalConstants(name, energy);
			
			F[0][0] = c::complex<REAL>(te.y[0], -te.y[1]);
			F[0][1] = c::complex<REAL>(te.y[2], -te.y[3]);
			F[0][2] = c::complex<REAL>(te.y[4], -te.y[5]);
			F[1][0] = c::complex<REAL>(te.y[6], -te.y[7]);
			F[1][1] = c::complex<REAL>(te.y[8], -te.y[9]);
			F[1][2] = c::complex<REAL>(te.y[10], -te.y[11]);
			F[2][0] = c::complex<REAL>(te.y[12], -te.y[13]);
			F[2][1] = c::complex<REAL>(te.y[14], -te.y[15]);
			F[2][2] = c::complex<REAL>(te.y[16], -te.y[17]);
			//printf("%f %f\n", F[0][0].real(), F[0][0].imag());
			//printf("%f %f\n", F[1][1].real(), F[1][1].imag());
			
			/*
			F[0][0] = c::complex<REAL>(te.y[0], -te.y[1]);
			F[1][0] = c::complex<REAL>(te.y[2], -te.y[3]);
			F[2][0] = c::complex<REAL>(te.y[4], -te.y[5]);
			F[0][1] = c::complex<REAL>(te.y[6], -te.y[7]);
			F[1][1] = c::complex<REAL>(te.y[8], -te.y[9]);
			F[2][1] = c::complex<REAL>(te.y[10], -te.y[11]);
			F[0][2] = c::complex<REAL>(te.y[12], -te.y[13]);
			F[1][2] = c::complex<REAL>(te.y[14], -te.y[15]);
			F[2][2] = c::complex<REAL>(te.y[16], -te.y[17]);
			*/
			
			//c::complex<REAL> f = c::complex<REAL>(te.y[0], -te.y[1]);
			
			c::complex<REAL> Cr[3][3]; // transformation right
			c::complex<REAL> Cl[3][3]; // transformation left
			/*
			Cr[0][0] = CI*2.*sqrt(M_PI/3.);
			Cr[0][1] = 0.;
			Cr[0][2] = -CI*2.*sqrt(M_PI/3.);

			Cr[1][0] = -2.*sqrt(M_PI/3.);
			Cr[1][1] = 0.;
			Cr[1][2] = -2.*sqrt(M_PI/3.);

			Cr[2][0] = 0.;
			Cr[2][1] = CI*2.*sqrt(2.*M_PI/3.);
			Cr[2][2] = 0.;
			*/
			Cr[0][0] = -2.*sqrt(M_PI/3.);
			Cr[0][1] = 0.;
			Cr[0][2] =  2.*sqrt(M_PI/3.);

			Cr[1][0] = 2.*CI*sqrt(M_PI/3.);
			Cr[1][1] = 0.;
			Cr[1][2] = 2.*CI*sqrt(M_PI/3.);

			Cr[2][0] = 0.;
			Cr[2][1] = -2.*sqrt(2.*M_PI/3.);
			Cr[2][2] = 0.;

			double t = 8. / 3. * M_PI;
			Cl[0][0] = conj(Cr[0][0]) / t;
			Cl[0][1] = conj(Cr[1][0]) / t;
			Cl[0][2] = conj(Cr[2][0]) / t;
			Cl[1][0] = conj(Cr[0][1]) / t;
			Cl[1][1] = conj(Cr[1][1]) / t;
			Cl[1][2] = conj(Cr[2][1]) / t;
			Cl[2][0] = conj(Cr[0][2]) / t;
			Cl[2][1] = conj(Cr[1][2]) / t;
			Cl[2][2] = conj(Cr[2][2]) / t;

			MatrixMul(T, F, Cr);
			MatrixMul(Cr, Cl, T);

			const REAL r0 = 2.8179403267e-5; // in A
			c::complex<REAL> pre = -CI * KAPPA * ( REAL(2.)/REAL(3.) * r0 );

			for(int i=0; i<3; i++)
			for(int j=0; j<3; j++)
			{
				//printf("%f %f\n", T[i][j].real(), T[i][j].imag());
				T[i][j] = Cr[i][j] * pre;
			}
		}
		
		void Scatter(CExpansionCoefficients &B, const CExpansionCoefficients &A)
		{
			B.Hlm[0].pol[0] = 0.;
			B.Hlm[1].pol[0] = 0.;
			B.Hlm[2].pol[0] = 0.;
			B.Hlm[3].pol[0] = 0.;
			B.Hlm[0].pol[1] = 0.;
			B.Hlm[1].pol[1] = 0.;
			B.Hlm[2].pol[1] = 0.;
			B.Hlm[3].pol[1] = 0.;
			
			B.Elm[0].pol[0] = 0.;
			B.Elm[0].pol[1] = 0.;
			
			c::complex<REAL> x, y, z;
			x = A.Elm[1].pol[0];
			y = A.Elm[2].pol[0];
			z = A.Elm[3].pol[0];
			
			c::complex<REAL> Cr[3][3];
			c::complex<REAL> CI(REAL(0.), REAL(1.));
/*
			Cr[0][0] = -2.*sqrt(M_PI/3.);
			Cr[0][1] = 0.;
			Cr[0][2] =  2.*sqrt(M_PI/3.);

			Cr[1][0] = 2.*CI*sqrt(M_PI/3.);
			Cr[1][1] = 0.;
			Cr[1][2] = 2.*CI*sqrt(M_PI/3.);

			Cr[2][0] = 0.;
			Cr[2][1] = -2.*sqrt(2.*M_PI/3.);
			Cr[2][2] = 0.;
*/
/*
			printf("perp: %e\n", 
				abs(B.Elm[1].pol[0]*B.Elm[1].pol[1] + 
				B.Elm[2].pol[0]*B.Elm[2].pol[1] + 
				B.Elm[3].pol[0]*B.Elm[3].pol[1]));
*/
/*
			printf("pol pi %f %f %f %s\n",
			(Cr[0][0] * x + Cr[0][1] * y + Cr[0][2] * z).real()/(4.*M_PI), 
			(Cr[1][0] * x + Cr[1][1] * y + Cr[1][2] * z).real()/(4.*M_PI), 
			(Cr[2][0] * x + Cr[2][1] * y + Cr[2][2] * z).real()/(4.*M_PI), name.c_str());
*/
			B.Elm[1].pol[0] = T[0][0] * x + T[0][1] * y + T[0][2] * z;
			B.Elm[2].pol[0] = T[1][0] * x + T[1][1] * y + T[1][2] * z;
			B.Elm[3].pol[0] = T[2][0] * x + T[2][1] * y + T[2][2] * z;

			x = A.Elm[1].pol[1];
			y = A.Elm[2].pol[1];
			z = A.Elm[3].pol[1];
			B.Elm[1].pol[1] = T[0][0] * x + T[0][1] * y + T[0][2] * z;
			B.Elm[2].pol[1] = T[1][0] * x + T[1][1] * y + T[1][2] * z;
			B.Elm[3].pol[1] = T[2][0] * x + T[2][1] * y + T[2][2] * z;
			
			/*
			printf("pol sigma %f %f %f \n", 
			(Cr[0][0] * x + Cr[0][1] * y + Cr[0][2] * z).real()/(4.*M_PI), 
			(Cr[1][0] * x + Cr[1][1] * y + Cr[1][2] * z).real()/(4.*M_PI), 
			(Cr[2][0] * x + Cr[2][1] * y + Cr[2][2] * z).real()/(4.*M_PI));
			*/
			// TODO: set everything above L=1 to zero
		}
		
		CScatteringTensorFileAtom* Clone() const
		{
			return new CScatteringTensorFileAtom(*this);
		}

		CString name;
		c::complex<REAL> F[3][3];
		c::complex<REAL> T[3][3];
};
