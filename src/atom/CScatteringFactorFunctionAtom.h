
class CScatteringFactorFunctionAtom : public CIsotropicAtom
{
	public:
		CScatteringFactorFunctionAtom(const CScatteringFactorFunctionAtom &a)
		{
			ref = a.ref;
			TE = a.TE;
			TH = a.TH;
		}

		CScatteringFactorFunctionAtom(int _ref)
		{
			ref = _ref;
		}

		void Set(const int LMAX, const c::complex<REAL> &KAPPA, const REAL EField);

		CScatteringFactorFunctionAtom* Clone() const
		{
			return new CScatteringFactorFunctionAtom(*this);
		}

		int ref;
};
