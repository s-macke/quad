void Xlm(const int LMAX, CKCoord3D *Y, CKCoord3D *P, CKCoord3D *X, const CCoor3D &p);

class CFormFactorAtom : public CAtom
{
	public:
		CFormFactorAtom(const CFormFactorAtom &a)
		{
			f = a.f;
			TE = a.TE;
			TH = a.TH;
			name = a.name;
			LMAX = a.LMAX;
			k0 = a.k0;
			s = a.s;
		}
		
		CFormFactorAtom(c::complex<REAL> _f)
		{
			f = conj(_f);
			LMAX = 1;
			k0 = 1.;
		}
#ifndef EMSCRIPTEN
		void Integrate()
		{
			const int NL = (LMAX+1)*(LMAX+1);
			CKCoord3D X[NL];
			CKCoord3D Y[NL];
			CKCoord3D P[NL];

			CCoor3D p;
			CCoor3D Td;
			CCoor3D Pd;
			CKCoord3D E;

			CExpansionCoefficients A(LMAX);
			CKCoord3D GKIN;
			GKIN.x = k0;
			GKIN.y = 0.;
			GKIN.z = 0.;
			PLW(k0, GKIN, LMAX, A);
			
			c::complex<REAL> I[NL];
			for(int i=0; i<NL; i++) I[i] = 0.;
	
			c::complex<REAL> a;
			REAL dtheta =    M_PI / 2000.;
			REAL dphi   = 2.*M_PI / 2000.;
	
			for(REAL phi=0; phi<=2.*M_PI; phi+=dphi)
			for(REAL theta=0; theta<=M_PI; theta+=dtheta)
			{
				double dA = sin(theta) * REAL(dtheta) * REAL(dphi);
				p.x = sin(theta)*cos(phi);
				p.y = sin(theta)*sin(phi);
				p.z = cos(theta);

				CCoor3D Q;
				Q.x = p.x*k0 - (1.)*k0;
				Q.y = p.y*k0 - 0.*k0;
				Q.z = p.z*k0 - 0.*k0;
				double q = sqrt(Q.x*Q.x + Q.y*Q.y + Q.z*Q.z) / 4. / M_PI;
				double f = 20. * exp(-0.4*q*q);
				//printf("%f %f %f %f %f %f\n", q, f, k0, p.x, p.y, p.z);
				//double f = 20.;

				Xlm(LMAX, Y, P, X, p);
		
				Pd.x = -sin(phi);
				Pd.y = cos(phi);
				Pd.z = 0.;
				Td.x = cos(theta)*cos(phi);
				Td.y = cos(theta)*sin(phi);
				Td.z = -sin(theta);
				E.x = Pd.y * Pd.x + Td.y * Td.x;
				E.y = Pd.y * Pd.y + Td.y * Td.y;
				E.z = Pd.y * Pd.z + Td.y * Td.z;
				E.x *= f * 2.8179403267e-5;
				E.y *= f * 2.8179403267e-5;
				E.z *= f * 2.8179403267e-5;
				
				int i = 0;
				for(int L=0; L<=LMAX; L++)
				for(int M=-L; M<=L; M++)
				{
					a = conj(P[i].x) * E.x + conj(P[i].y) * E.y + conj(P[i].z) * E.z;
					//a = conj(P[i].x) * X[2].x + conj(P[i].y) * X[2].y + conj(P[i].z) * X[2].z;
					I[i] += a*dA;
					i++;
				}
			}
			int i = 0;
			for(int L=0; L<=LMAX; L++)
			for(int M=-L; M<=L; M++)
			{
				I[i] = I[i]*k0;
				s[i] = I[i];
				//I[i] = I[i] / A.Elm[i].pol[0] * k0; 
				cout << "#L=" << L <<  " M=" << M << " " << I[i].real() << " " << I[i].imag() << "\n";
				i++;
			}
			//cout << (-REAL(2.)/REAL(3.) * 20.) << "\n";
			//cout << -k0*2./3.*20. << "\n";
		}
#endif		
		void Set(const int _LMAX, const c::complex<REAL> &KAPPA, const REAL Efield)
		{
			//TE = CI*REAL(2.)/REAL(9.) * R*R*R*KAPPA*KAPPA*KAPPA * (EPSSPH-REAL(1.));		
			c::complex<REAL> CI(REAL(0.), REAL(1.));
			c::complex<REAL> CZERO(REAL(0.), REAL(0.));
			const REAL r0 = 2.8179403267e-5; // in A
			c::complex<REAL> pre = -CI * KAPPA * ( REAL(2.)/REAL(3.) * r0 );
			
			LMAX = _LMAX;
			k0 = KAPPA.real();
			TE.assign(LMAX+1, CZERO);
			TH.assign(LMAX+1, CZERO);
			//Integrate(LMAX, KAPPA.real());
		
			k0 = KAPPA.real();

			TE[0] = 0.;
			TH[0] = 0.;
			TE[1] = pre * f;
			TH[1] = 0.;
			for(int i=2; i<=LMAX; i++)
			{
				TE[i] = 0.;
				TH[i] = 0.;
			}
			//fprintf(stderr, "%e %e\n", TE[1].real(), TE[1].imag());		
		}
		
		void Scatter(CExpansionCoefficients &B, const CExpansionCoefficients &A)
		{
			
			if (s.size() == 0)
			{	
				s.assign((LMAX+1)*(LMAX+1), 0.);
				//Integrate();
			}
			
			int i = 0;
			for(int l = 0; l<LMAX; l++)
			for(int m = -l; m<=l; m++)
			{
				B.Elm[i].pol[0] = s[i];
				B.Hlm[i].pol[0] = 0.;
				B.Elm[i].pol[1] = s[i];
				B.Hlm[i].pol[1] = 0.;
				i++;
			}
			
			/*
			int i = 0;
			for(int l = 0; l<TE.size(); l++)
			for(int m = -l; m<=l; m++)
			{
				B.Elm[i].pol[0] = TE[l] * A.Elm[i].pol[0];
				B.Hlm[i].pol[0] = TH[l] * A.Hlm[i].pol[0];
				B.Elm[i].pol[1] = TE[l] * A.Elm[i].pol[1];
				B.Hlm[i].pol[1] = TH[l] * A.Hlm[i].pol[1];
				i++;
			}
			*/
			
		}
		
	void GetIsotropicScatterMatrix(c::complex<REAL> *_TE, c::complex<REAL> *_TH, int LMAX) const
	{
		for(int i=0; i<=LMAX; i++)
		{
			_TE[i] = 0.;
			_TH[i] = 0.;
		}
	}
		
		CFormFactorAtom* Clone() const
		{
			return new CFormFactorAtom(*this);
		}

		c::complex<REAL> f;
		CVector<c::complex<REAL> > s;
		REAL k0;
		int LMAX;
};
