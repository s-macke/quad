
class CScatteringFactorAtom : public CIsotropicAtom
{
	public:
		CScatteringFactorAtom(const CScatteringFactorAtom &a)
		{
			f = a.f;
			TE = a.TE;
			TH = a.TH;
		}

		CScatteringFactorAtom(c::complex<REAL> _f)
		{
			f = conj(_f);
		}

		void Set(const int LMAX, const c::complex<REAL> &KAPPA, const REAL Efield)
		{
			//TE = CI*REAL(2.)/REAL(9.) * R*R*R*KAPPA*KAPPA*KAPPA * (EPSSPH-REAL(1.));		
			c::complex<REAL> CI(REAL(0.), REAL(1.));
			c::complex<REAL> CZERO(REAL(0.), REAL(0.));
			const REAL r0 = 2.8179403267e-5; // in A
			c::complex<REAL> pre = -CI * KAPPA * ( REAL(2.)/REAL(3.) * r0 );
			
			TE.assign(LMAX+1, CZERO);
			TH.assign(LMAX+1, CZERO);
		
			TE[0] = 0.;
			TH[0] = 0.;
			TE[1] = pre * f;
			TH[1] = 0.;
			for(int i=2; i<=LMAX; i++)
			{
				TE[i] = 0.;
				TH[i] = 0.;
			}
			//fprintf(stderr, "%e %e\n", TE[1].real(), TE[1].imag());		
		}

		CScatteringFactorAtom* Clone() const
		{
			return new CScatteringFactorAtom(*this);
		}

		c::complex<REAL> f;
};

