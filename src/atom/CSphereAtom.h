class CSphereAtom : public CIsotropicAtom
{
	public:
		CSphereAtom(const CSphereAtom &a)
		{
			e = a.e;
			r = a.r;
			TE = a.TE;
			TH = a.TH;
		}
	
		CSphereAtom(c::complex<REAL> _e, REAL _r)
		{
			e = _e;
			r = _r;
		}
		void Set(const int LMAX, const c::complex<REAL> &KAPPA, const REAL Efield);
		
		CSphereAtom* Clone() const
		{
			return new CSphereAtom(*this);
		}
		c::complex<REAL> e;
		REAL r;
};
