
class CTetragonalFunctionAtom : public CAtom
{
	public:
		CTetragonalFunctionAtom(const CTetragonalFunctionAtom& r)
		{
			ref = r.ref;
			refm = r.refm;
			for(int i=0; i<3; i++)
			for(int j=0; j<3; j++)
			{
				F[i][j] = r.F[i][j];
				T[i][j] = r.T[i][j];
			}
		}

		CTetragonalFunctionAtom(int _ref, int _refm)
		{
			ref = _ref;
			refm = _refm;
			for(int i=0; i<3; i++)
			for(int j=0; j<3; j++)
			{
				F[i][j] = 0.;
				T[i][j] = 0.;
			}
		}
		
		void GetIsotropicScatterMatrix(c::complex<REAL> *TE, c::complex<REAL> *TH, int LMAX) const
		{
			TE[0] = 0.;
			TH[0] = 0.;
			TE[1] = T[0][0];
			TH[1] = 0.;
		}
		
		void Set(const int LMAX, const c::complex<REAL> &KAPPA, const REAL EField);

		void Scatter(CExpansionCoefficients &B, const CExpansionCoefficients &A)
		{
			B.Hlm[0].pol[0] = 0.;
			B.Hlm[1].pol[0] = 0.;
			B.Hlm[2].pol[0] = 0.;
			B.Hlm[3].pol[0] = 0.;
			B.Hlm[0].pol[1] = 0.;
			B.Hlm[1].pol[1] = 0.;
			B.Hlm[2].pol[1] = 0.;
			B.Hlm[3].pol[1] = 0.;
			
			B.Elm[0].pol[0] = 0.;
			B.Elm[0].pol[1] = 0.;
			
			c::complex<REAL> x,y,z;
			x = A.Elm[1].pol[0];
			y = A.Elm[2].pol[0];
			z = A.Elm[3].pol[0];
			
			B.Elm[1].pol[0] = T[0][0] * x + T[0][1] * y + T[0][2] * z;
			B.Elm[2].pol[0] = T[1][0] * x + T[1][1] * y + T[1][2] * z;
			B.Elm[3].pol[0] = T[2][0] * x + T[2][1] * y + T[2][2] * z;
				
			x = A.Elm[1].pol[1];
			y = A.Elm[2].pol[1];
			z = A.Elm[3].pol[1];
			B.Elm[1].pol[1] = T[0][0] * x + T[0][1] * y + T[0][2] * z;
			B.Elm[2].pol[1] = T[1][0] * x + T[1][1] * y + T[1][2] * z;
			B.Elm[3].pol[1] = T[2][0] * x + T[2][1] * y + T[2][2] * z;
			// TODO: set everything above L=1 to zero
		}

		CTetragonalFunctionAtom* Clone() const
		{
			return new CTetragonalFunctionAtom(*this);
		}

		CString name;
		c::complex<REAL> F[3][3];
		c::complex<REAL> T[3][3];
		int ref, refm;
};
