#include"OpticalConstants.h"
#include"periodic_table.h"
#include<math.h>

CVector<CAtomTable> tables;

void LoadOpticalConstants(const char* name, const char *filename)
{
	CAtomTable at;
	at.ftable.ncolumns = 2;
	at.name = CString(name);
	if (at.ftable.LoadAsciiTable(CString(filename)))
	{
		tables.push_back(at);
		return;
	}
	fprintf(stderr, "Error loading or parsing file %s.\n", filename);
	exit(1);
}

void LoadMagneticConstants(const char* name, const char *ffilename, const char *mfilename)
{
	CAtomTable at;
	at.ftable.ncolumns = 2;
	at.mtable.ncolumns = 2;
	at.name = CString(name);
	if (at.ftable.LoadAsciiTable(CString(ffilename)))
	if (at.mtable.LoadAsciiTable(CString(mfilename)))
	{
		tables.push_back(at);
		return;
	}
	fprintf(stderr, "Error loading or parsing file %s or %s.\n", ffilename, mfilename);
	exit(1);
}


void LoadOpticalConstantsFromDatabase(const char* name, const char *elementname, double qz)
{
	CAtomTable at;
	at.ftable.ncolumns = 2;
	at.name = CString(name);

	CElementInfo element;
	
	if (!FindElement(CString(elementname), element))
	{
		fprintf(stderr, "Element not found\n");
		exit(1);
	}
	at.Z = element.Z-1;
	for(int i=0; i<9; i++) at.cromer[i] = element.cromer[i];
		
	bool result = at.ftable.LoadAsciiTable(CString(elementname) + CString(".ff"));
	
	if (!result)
	if (!GetChantlerTable(element.Z-1, at.ftable))
	{
		fprintf(stderr, "Element not found\n");
		exit(1);
	}
	
	double deltaf1 = -double(at.Z);
    double k = qz / (4. * 3.141592);    
    double Z = at.cromer[4] +
           at.cromer[0] * exp(-at.cromer[5]*k*k) +
           at.cromer[1] * exp(-at.cromer[6]*k*k) +
           at.cromer[2] * exp(-at.cromer[7]*k*k) +
           at.cromer[3] * exp(-at.cromer[8]*k*k);
           deltaf1 += Z;
		   
	fprintf(stderr, "Using f(q,E) = %f + f1(E) + I*f2(E) for element %s\n", Z, elementname);
	for(int i=0; i<at.ftable.entry.size(); i++)
	{
		at.ftable.entry[i].y[0] += deltaf1;
	}
    //if (info.Z < 20) f1 = f1 + info.Z*7.970327701e35/(3.906250000e33*q*q*q*q+7.970327705e35+1.115958648e35*q*q);
    //else if (info.Z < 40) f1 = f1 + info.Z*7.970327704e35/(6.250000000e34*q*q*q*q+7.970327705e35+4.463834592e35*q*q);
    //else f1 = f1 + info.Z*2.519017151e36/(1.000000000e36*q*q*q*q+2.519017154e36+3.174282378e36*q*q);
    //f1 = f1 + double(info.Z)*7.970327704e35/(2.441406250e36*q*q*q*q+7.970327705e35+2.789896620e36*q*q);	
	
	tables.push_back(at);
}

void LoadScatteringTensor(const char* name, const char *filename)
{
	CAtomTable at;
	at.ftable.ncolumns = 18;
	at.name = CString(name);
	if (!at.ftable.LoadAsciiTable(CString(filename)))
	{
		fprintf(stderr, "Error loading file %s\n", filename);
		exit(1);
	}
	tables.push_back(at);	
}


CTableEntry GetOpticalConstants(CString name, double energy)
{
	CTableEntry te;
	CTableEntry m;
	te.y[0] = 0.;
	te.y[1] = 0.;
	te.y[2] = 0.;
	te.y[3] = 0.;

	for(int i=0; i<tables.size();i++)
	{
		if (tables[i].name != name) continue;
			te = tables[i].ftable.Get(energy);
		if (tables[i].mtable.ncolumns == 2) 
		{
			m = tables[i].mtable.Get(energy);
			te.y[2] = m.y[0];
			te.y[3] = m.y[1];
		}
		return te;
	}	
	fprintf(stderr, "Not found %s\n", name.c_str());
	exit(1);
	return te;
	
}