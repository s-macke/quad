#include"global.h"

#include"multiple_scattering.h"
#include"functions.h"

void Sort1(int LMAX, int LMXOD, int K2, c::complex<REAL> *BMEL1, c::complex<REAL> *BMEL2, CPolar *AE, CPolar *AH)
{
	int II = 0;
	int IEV = LMXOD;
	int IOD = 0;
	for(int L=1; L<=LMAX; L++)
	for(int M=-L; M<=L; M++)
	{
		II++;
		if (((L+M) % 2) == 0)
		{
			BMEL1[IEV] = AH[II].pol[K2-1];
			BMEL2[IEV] = AE[II].pol[K2-1];
			IEV++;
		}
		else
		{
			BMEL1[IOD] = AE[II].pol[K2-1];
			BMEL2[IOD] = AH[II].pol[K2-1];
			IOD++;
		}
	}
}


void Sort1(int LMAX, int LMXOD, int K2,
	c::complex<REAL> *BMEL1, c::complex<REAL> *BMEL2, 
	const CExpansionCoefficients &B)
{
//	c::complex<REAL> BMEL1[LMTD], BMEL2[LMTD];
//	CPolar AE[LM1SQD], AH[LM1SQD];	
//	c::complex<REAL> TE[LMAX1D], TH[LMAX1D];

	int II = 0;
	int IEV = LMXOD;
	int IOD = 0;
	for(int L=1; L<=LMAX; L++)
	for(int M=-L; M<=L; M++)
	{
		II++;
		if (((L+M) % 2) == 0)
		{
			BMEL1[IEV] = B.Hlm[II].pol[K2];
			BMEL2[IEV] = B.Elm[II].pol[K2];
			IEV++;
		}
		else
		{
			BMEL1[IOD] = B.Elm[II].pol[K2];
			BMEL2[IOD] = B.Hlm[II].pol[K2];
			IOD++;
		}
	}
}

void Sort2(int LMAX, int LMXOD, int K1, c::complex<REAL> *BMEL1, c::complex<REAL> *BMEL2, c::complex<REAL> *LAME, c::complex<REAL> *LAMH, CExpansionCoefficients &D)
{
//	c::complex<REAL> BMEL1[LMTD], BMEL2[LMTD];
//	c::complex<REAL> LAME[2], LAMH[2];
	const c::complex<REAL> CZERO(0., 0.);
	LAME[K1] = CZERO;
	LAMH[K1] = CZERO;
	int II = 0;
	int IEV = LMXOD;
	int IOD = 0;

	for(int L=1; L<=LMAX; L++)
	for(int M=-L; M<=L; M++)
	{
		II++;
		if (((L+M) % 2) == 0)
		{
			IEV++;
			LAME[K1] += D.Elm[II].pol[K1] * BMEL2[IEV-1];
			LAMH[K1] += D.Hlm[II].pol[K1] * BMEL1[IEV-1];
		}
		else
		{
			IOD++;
			LAME[K1] += D.Elm[II].pol[K1] * BMEL1[IOD-1];
			LAMH[K1] += D.Hlm[II].pol[K1] * BMEL2[IOD-1];
		}
	}
}

c::complex<REAL> CODD(const int L, const int M, const int L1, const int M1, c::complex<REAL> **XODD)
{
//      COMPLEX*16 XODD(LMODD,LMODD)  
	int I, J;
	c::complex<REAL> CZERO(0., 0.);

	if ((abs(M)<=L) && (abs(M1)<=L1))
	{
		I = (L * L + M + 1) / 2;
		J = (L1 * L1 + M1 + 1) / 2;
		return XODD[I-1][J-1];
	}
    return CZERO;
	  
}	  

//=======================================================================  
c::complex<REAL> CEVEN(const int L, const int M, const int L1, const int M1, c::complex<REAL> **XEVEN)
{
//      COMPLEX*16 XEVEN(LMEVEN,LMEVEN)  
	int I, J;
	c::complex<REAL> CZERO(0., 0.);

	if ((abs(M) <= L) && (abs(M1)<= L1))
	{
		I = (L*L + 2*L + M + 2) / 2;
		J = (L1*L1 + 2*L1 + M1 + 2) / 2;
		return XEVEN[I-1][J-1];
	}
	return CZERO;

 }

//C     ------------------------------------------------------------------  
//C     THIS SUBROUTINE CONSTRUCTS THE SECULAR MATRIX 
//C     ------------------------------------------------------------------  
//C=======================================================================  
void SETUP2(
	const int LMAX,
	c::complex<REAL> **XEVEN,
	c::complex<REAL> **XODD,
	const c::complex<REAL> *TE,
	const c::complex<REAL> *TH,
	c::complex<REAL> **XXMAT1,
	c::complex<REAL> **XXMAT2)
{

//C ..  ARRAY ARGUMENTS ..  

// COMPLEX*16 XEVEN(LMEVEN,LMEVEN), XODD(LMODD,LMODD)  
// COMPLEX*16 XXMAT2(LMTD,LMTD)  
// COMPLEX*16 TE(LMAX1D),TH(LMAX1D),XXMAT1(LMTD,LMTD)  

//C ..  LOCAL SCALARS ..  

	int IA, LA, MA, LTT, IB, LB, MB, I, IAOD, IAEV, IBOD;
	int IBEV;
	REAL C0, SIGNUS, UP, C, B1, B2, B3, U1, U2, A, DOWN;
	REAL ALPHA1, ALPHA2, BETA1, BETA2;
	c::complex<REAL> OMEGA1, OMEGA2, Z1, Z2, Z3;
	const c::complex<REAL> CZERO(0., 0.), CONE(1., 0.);

//     ------------------------------------------------------------------  
	const int LMAX1 = LMAX + 1;
	const int LMTOT = LMAX1*LMAX1-1;
	const int LMXOD = (LMAX*LMAX1)/2;
	
	C0 = sqrt(REAL(8.)*M_PI/3.);
	SIGNUS = 1.;
	IAOD = 0;
	IAEV = LMXOD;
	
	for(LA=1; LA<=LMAX; LA++)
	for(MA=-LA; MA<=LA; MA++)
	{
		if (((LA+MA) % 2) == 0)
		{
			IAEV++;
			IA = IAEV;
		} else
		{
			IAOD++;
			IA = IAOD;
		}  
		UP = REAL(2 * LA + 1);
		SIGNUS = -SIGNUS;
		C = SIGNUS*C0;
		B1 = 0.;
		if (abs(MA+1) <= (LA-1)) B1 = BLM(LA-1, MA+1, 1, -1, LA, -MA, LMAX);
		B2 = 0.;
		if (abs(MA-1) <= (LA-1)) B2 = BLM(LA-1, MA-1, 1,  1, LA, -MA, LMAX);
		U1 = REAL((LA+MA)*(LA-MA));
		U2 = REAL((2*LA-1)*(2*LA+1));
		B3 = sqrt(U1/U2);
		ALPHA1 = sqrt(REAL((LA-MA)*(LA+MA+1)))/2.;
		BETA1 = sqrt(REAL((LA+MA)*(LA-MA+1)))/2.;
		IBOD = 0;
		IBEV = LMXOD;
	
		for(LB=1; LB<=LMAX; LB++)
		for(MB=-LB; MB<=LB; MB++)
		{
			if (((LB + MB) % 2) == 0)
			{
				IBEV++;
				IB = IBEV;
			} else
			{
				IBOD++;
				IB = IBOD;
			}
			A = REAL(LB*(LB+1)*LA*(LA+1));
			DOWN = sqrt(A);
			ALPHA2 = sqrt(REAL((LB-MB)*(LB+MB+1))) / 2.;
			BETA2 = sqrt(REAL((LB+MB)*(LB-MB+1))) / 2.;
			LTT = LA + MA + LB + MB;
			
			if ((LTT % 2) != 0)
			{
				if (((LA+MA) % 2) == 0)
				{
					Z1 = CEVEN(LB, MB+1, LA-1, MA+1, XEVEN);
					Z2 = CEVEN(LB, MB-1, LA-1, MA-1, XEVEN);
					Z3 = CODD (LB, MB  , LA-1, MA  , XODD);
					Z1 =  C * ALPHA2 * B1 * Z1;
					Z2 = -C * BETA2  * B2 * Z2;
					Z3 = REAL(MB) * B3 * Z3;
					OMEGA2 = UP * (Z1 + Z2 + Z3) / DOWN;
					XXMAT1[IA-1][IB-1] = -TH[LA]*OMEGA2;
					XXMAT2[IA-1][IB-1] =  TE[LA]*OMEGA2;
				} else
				{
					Z1 = CODD (LB,MB+1, LA-1, MA+1, XODD );
					Z2 = CODD (LB,MB-1, LA-1, MA-1, XODD );
					Z3 = CEVEN(LB,MB  , LA-1, MA  , XEVEN);
					Z1 = C*ALPHA2*B1*Z1;
					Z2 = -C*BETA2* B2*Z2;
					Z3 = REAL(MB)*B3*Z3;
					OMEGA2 = UP * (Z1 + Z2 + Z3) / DOWN;
					XXMAT1[IA-1][IB-1] =  TE[LA] * OMEGA2;
					XXMAT2[IA-1][IB-1] = -TH[LA] * OMEGA2;
				}
			} else
			{
				if ( ((LA+MA)%2) == 0)
				{
					Z1 = CODD(LB, MB-1, LA, MA-1, XODD);
					Z2 = CODD(LB, MB+1, LA, MA+1, XODD);
					Z3 = CEVEN(LB, MB  , LA, MA  , XEVEN);
					Z1 = 2.*BETA1  * BETA2  * Z1;
					Z2 = 2.*ALPHA1 * ALPHA2 * Z2;
					Z3 = REAL(MA) * REAL(MB) * Z3;
					OMEGA1 = (Z1 + Z2 + Z3) / DOWN;
					XXMAT1[IA-1][IB-1] = -TH[LA] * OMEGA1;
					XXMAT2[IA-1][IB-1] = -TE[LA] * OMEGA1;
				} else
				{
					Z1 = CEVEN(LB,MB-1,LA,MA-1,XEVEN);
					Z2 = CEVEN(LB,MB+1,LA,MA+1,XEVEN);
					Z3 = CODD (LB,MB  ,LA,MA  ,XODD );
					Z1 = 2.*BETA1 *BETA2 *Z1;
					Z2 = 2.*ALPHA1*ALPHA2*Z2;
					Z3 = REAL(MA)*REAL(MB)*Z3;
					OMEGA1 = (Z1+Z2+Z3)/DOWN;
					XXMAT1[IA-1][IB-1] = -TE[LA]*OMEGA1;
					XXMAT2[IA-1][IB-1] = -TH[LA]*OMEGA1;
				}
			}
		}
	}

	for(I=0; I<LMTOT; I++)
	{
		XXMAT1[I][I] = CONE + XXMAT1[I][I];
		XXMAT2[I][I] = CONE + XXMAT2[I][I];
	}

}

//=======================================================================

void SETUP(
	const int LMAX,
	c::complex<REAL> **XEVEN,
	c::complex<REAL> **XODD,
	const c::complex<REAL> *TE,
	const c::complex<REAL> *TH,
	c::complex<REAL> **XXMAT1,
	c::complex<REAL> **XXMAT2)
{

//C ..  ARRAY ARGUMENTS ..  

// COMPLEX*16 XEVEN(LMEVEN,LMEVEN), XODD(LMODD,LMODD)  
// COMPLEX*16 XXMAT2(LMTD,LMTD)  
// COMPLEX*16 TE(LMAX1D),TH(LMAX1D),XXMAT1(LMTD,LMTD)  

//C ..  LOCAL SCALARS ..  

	int IA, LA, MA, LTT, IB, LB, MB, I, IAOD, IAEV, IBOD;
	int IBEV;
	REAL C0, SIGNUS, UP, C, B1, B2, B3, U1, U2, A, DOWN;
	REAL ALPHA1, ALPHA2, BETA1, BETA2;
	c::complex<REAL> OMEGA1, OMEGA2, Z1, Z2, Z3;
	const c::complex<REAL> CZERO(0., 0.), CONE(1., 0.);

//     ------------------------------------------------------------------  
	const int LMAX1 = LMAX + 1;
	const int LMTOT = LMAX1*LMAX1-1;
	const int LMXOD = (LMAX*LMAX1)/2;
	
	C0 = sqrt(REAL(8.)*M_PI/3.);
	SIGNUS = 1.;
	IAOD = 0;
	IAEV = LMXOD;
	
	for(LA=1; LA<=LMAX; LA++)
	for(MA=-LA; MA<=LA; MA++)
	{
		if (((LA+MA) % 2) == 0)
		{
			IAEV++;
			IA = IAEV-1;
		} else
		{
			IAOD++;
			IA = IAOD-1;
		}  
		UP = REAL(2 * LA + 1);
		SIGNUS = -SIGNUS;
		C = SIGNUS * C0;
		
		B1 = 0.;
		if (abs(MA+1) <= (LA-1)) B1 = BLM(LA-1, MA+1, 1, -1, LA, -MA, LMAX);
		B2 = 0.;
		if (abs(MA-1) <= (LA-1)) B2 = BLM(LA-1, MA-1, 1, 1, LA, -MA, LMAX);
		
		U1 = REAL((LA+MA) * (LA-MA));
		U2 = REAL((2*LA-1) * (2*LA+1));
		B3 = sqrt(U1 / U2);
		ALPHA1 = sqrt(REAL((LA-MA) * (LA+MA+1))) / 2.;
		BETA1  = sqrt(REAL((LA+MA) * (LA-MA+1))) / 2.;
		IBOD = 0;
		IBEV = LMXOD;
	
		for(LB=1; LB<=LMAX; LB++)
		for(MB=-LB; MB<=LB; MB++)
		{
			if (((LB + MB) % 2) == 0)
			{
				IBEV++;
				IB = IBEV-1;
			} else
			{
				IBOD++;
				IB = IBOD-1;
			}
			A = REAL(LB * (LB+1) * LA * (LA+1));
			DOWN = sqrt(A);
			ALPHA2 = sqrt(REAL((LB-MB)*(LB+MB+1))) / 2.;
			BETA2  = sqrt(REAL((LB+MB)*(LB-MB+1))) / 2.;
			LTT = LA + MA + LB + MB;
			
			if ((LTT % 2) != 0)
			{
				if (((LA+MA) % 2) == 0)
				{
					Z1 = CEVEN(LB, MB+1, LA-1, MA+1, XEVEN);
					Z2 = CEVEN(LB, MB-1, LA-1, MA-1, XEVEN);
					Z3 = CODD (LB, MB  , LA-1, MA  , XODD);
					Z1 =  C * ALPHA2 * B1 * Z1;
					Z2 = -C * BETA2  * B2 * Z2;
					Z3 =    REAL(MB) * B3 * Z3;
					OMEGA2 = UP * (Z1 + Z2 + Z3) / DOWN;
					XXMAT1[IA][IB] = -TH[LA] * OMEGA2;
					XXMAT2[IA][IB] =  TE[LA] * OMEGA2;
				} else
				{
					Z1 = CODD (LB,MB+1, LA-1, MA+1, XODD );
					Z2 = CODD (LB,MB-1, LA-1, MA-1, XODD );
					Z3 = CEVEN(LB,MB  , LA-1, MA  , XEVEN);
					Z1 = C*ALPHA2 * B1 * Z1;
					Z2 = -C*BETA2 * B2 * Z2;
					Z3 = REAL(MB) * B3 * Z3;
					OMEGA2 = UP * (Z1 + Z2 + Z3) / DOWN;
					XXMAT1[IA][IB] =  TE[LA] * OMEGA2;
					XXMAT2[IA][IB] = -TH[LA] * OMEGA2;
				}
			} else
			{
				if ( ((LA+MA)%2) == 0)
				{
					Z1 = CODD(LB, MB-1, LA, MA-1, XODD);
					Z2 = CODD(LB, MB+1, LA, MA+1, XODD);
					Z3 = CEVEN(LB, MB  , LA, MA  , XEVEN);
					Z1 = 2.*BETA1  * BETA2    * Z1;
					Z2 = 2.*ALPHA1 * ALPHA2   * Z2;
					Z3 = REAL(MA)  * REAL(MB) * Z3;
					OMEGA1 = (Z1 + Z2 + Z3) / DOWN;
					XXMAT1[IA][IB] = -TH[LA] * OMEGA1;
					XXMAT2[IA][IB] = -TE[LA] * OMEGA1;
				} else
				{
					Z1 = CEVEN(LB,MB-1,LA,MA-1,XEVEN);
					Z2 = CEVEN(LB,MB+1,LA,MA+1,XEVEN);
					Z3 = CODD (LB,MB  ,LA,MA  ,XODD );
					Z1 = 2.*BETA1  * BETA2   * Z1;
					Z2 = 2.*ALPHA1 * ALPHA2  * Z2;
					Z3 =  REAL(MA) * REAL(MB) * Z3;
					OMEGA1 = (Z1 + Z2 + Z3) / DOWN;
					XXMAT1[IA][IB] = -TE[LA] * OMEGA1;
					XXMAT2[IA][IB] = -TH[LA] * OMEGA1;
				}
			}
		}
	}

	for(I=0; I<LMTOT; I++)
	{
		XXMAT1[I][I] = CONE + XXMAT1[I][I];
		XXMAT2[I][I] = CONE + XXMAT2[I][I];
	}

}



  
//=======================================================================  
//     ------------------------------------------------------------------  
//     XMAT CALCULATES THE MATRIX DESCRIBING MULTIPLE SCATERING  WITHIN   
//     A  LAYER, RETURNING  IT AS :  XODD,  CORRESPONDING  TO  ODD  L+M,  
//    WITH LM=(10),(2-1),(21),... AND XEVEN, CORRESPONDING TO EVEN L+M, 
//     WITH LM=(00),(1-1),(11),(2-2),(20),(22),...  
//     THE  PROGRAM  ASSUMES  THAT  THE  LAYER IS A BRAVAIS LATTICE. THE  
//     SUMMATION OVER THE LATTICE FOLLOWS THE EWALD METHOD  SUGGESTED BY  
//     KAMBE. EMACH IS THE MACHINE ACCURACY.  
//     ------------------------------------------------------------------  

void XMAT(
	c::complex<REAL> **XODD,
	c::complex<REAL> **XEVEN,
	const int LMAX,
	const c::complex<REAL> &KAPPA,
	const CCoor3D &AK,
	const REAL* ELM, const CCoor2D &AR1, const CCoor2D &AR2)
{

//C ..  PARAMETER STATEMENTS  ..
	const int LMAX1D = LMAX+1;
	const int LM1SQD = LMAX1D*LMAX1D;
	const int LMDLMD = LMAX1D*(2*LMAX+1);

	const int LMAXARRAY = 7;
	const int LMAX1DARRAY = LMAXARRAY+1;
	const int LM1SQDARRAY = LMAX1DARRAY*LMAX1DARRAY;
	const int LMDLMDARRAY = LMAX1DARRAY*(2*LMAXARRAY+1);


// C ..  SCALAR ARGUMENTS  ..  
// COMPLEX*16 XODD(LMODD,LMODD),XEVEN(LMEVEN,LMEVEN)  

	int L2MAX, LL2, II, I, NNDLM, K, KK, L, MM, NN, M, J1, J2, I1, I2, I3, N1;
	int NA, LLL, N, IL, NM, IN, L2, IL2, M2, IL3, L3, M3, LA1, LB1, LA11, LB11;
	int LL, J, L1;
	REAL AB1, AB2, AC, ACSQ, AD, AL, AN, AN1, AN2, AP, AP1, AP2, AR, B;
	REAL DNORM, RTPI, RTV, TEST, TEST1, TEST2, TV;
	c::complex<REAL> ALPHA, RTA, RTAI, KAPSQ, KANT, KNSQ, XPK, XPA, CF, CP, CX, CZ;
	c::complex<REAL> Z, ZZ, W, WW, A, ACC, GPSQ, GP, BT, AA, AB, U, U1, U2, GAM;
	c::complex<REAL> GK, GKK, SD, ALM;

	REAL DENOM[204]; // variable NDEND
	CCoor2D R, B1, B2, AKPT; 
	REAL FAC[4*LMAXARRAY+1];
	c::complex<REAL> GKN[LMAX1DARRAY];
	c::complex<REAL> AGK[2*LMAXARRAY+1], XPM[2*LMAXARRAY+1];
	c::complex<REAL> PREF[LM1SQDARRAY];
	c::complex<REAL> DLM[LMDLMDARRAY];

	const c::complex<REAL> CZERO(0., 0.), CI(0., 1.);
// ----------------------------------------------------------------------  

//	if (LMAX > 7 || LMAX > LMAXD)
	//if (LMAX > LMAXD)
	if (LMAX > 7)
	{
		fprintf(stderr, "FROM XMAT: LMAX > 7");
		exit(1);
	}
	
//C     AK(1)  AND  AK(2)  ARE THE X  AND Y COMPONENTS OF THE  
//C     MOMENTUM PARALLEL TO THE SURFACE, MODULO A RECIPROCAL  
//C     LATTICE VECTOR  
 
	RTPI = sqrt(M_PI);
	KAPSQ = KAPPA*KAPPA;

//C     THE FACTORIAL  FUNCTION  IS TABULATED  IN FAC . THE ARRAY  
//C     DLM WILL CONTAIN NON-ZERO,I.E. L+M EVEN,VALUES AS DEFINED  
//C     BY KAMBE.DLM=DLM1+DLM2+DLM3.WITH LM=(00),(1-1),(11),(2-2)...  

	L2MAX = LMAX + LMAX;
	LL2 = L2MAX + 1;
	FAC[0] = 1.;
	II = L2MAX + L2MAX;
	for(I=0; I<II; I++) FAC[I+1] = REAL(I+1) * FAC[I];
	
	NNDLM = L2MAX*(L2MAX + 3)/2 + 1;
	for(I=0; I<NNDLM; I++)
	{
		DLM[I] = CZERO;
	}
	
//C     THE FORMULA OF KAMBE FOR THE SEPARATION CONSTANT,ALPHA,IS  
//C     USED,SUBJECT TO A RESTRICTION WHICH IS IMPOSED TO CONTROL  
//C     LATER ROUNDING ERRORS  

	TV = fabs(AR1.x*AR2.y - AR1.y*AR2.x); // Area
	
	ALPHA = TV / (REAL(4.) * M_PI) * KAPSQ;
	AL = abs(ALPHA);
	if ((exp(AL)*EMACH) > 5.e-5) AL = log(REAL(5e-5) / EMACH);
	ALPHA = c::complex<REAL>(AL, 0.);
	RTA = sqrt(ALPHA);
	
//C     DLM1 , THE  SUM  OVER  RECIPROCAL   LATTICE  VECTORS  , IS  
//C     CALCULATED FIRST. THE PREFACTOR P1 IS  TABULATED  FOR EVEN  
//C     VALUES OF L+|M|,THUS LM=(00),(11),(2 0),(22),(L2MAX,L2MAX)  
//C     THE  FACTORIAL  FACTOR  F1  IS SIMULTANEOUSLY TABULATED IN  
//C     DENOM,FOR ALL VALUES OF N=0,(L-|M|)/2  

	K = 0;
	KK = 0;
	AP1 = -2. / TV;
	AP2 = -1.;
	CF = CI / KAPPA;
	for(L=1; L<=LL2; L++)
	{
		AP1 = AP1 / 2.;  
		AP2 = AP2 + 2.;  
		//cout << AP1 << " " << AP2 << "\n";
		CP = CF;
		MM = 1;
		if ((L % 2) == 0)
		{
			MM = 2;
			CP = CI * CP;
		}
		NN = (L - MM) / 2 + 2;
		//cout << "NN " << NN << "\n";

		for(M=MM; M<=L; M+=2)
		{
			J1 = L + M - 1;
			J2 = L - M + 1;
			AP = AP1 * sqrt(AP2 * FAC[J1-1] * FAC[J2-1]);
			PREF[KK] = AP * CP;

			CP = -CP;
			KK++;
			NN = NN - 1;
			for(I=1; I<=NN; I++)
			{
				I1 = I;
				I2 = NN - I + 1;
				I3 = NN + M - I;
				DENOM[K] = 1. / (FAC[I1-1] * FAC[I2-1] * FAC[I3-1]);
//				cout << "DENOM " << DENOM[K-1] << "\n";
				K++;
			}
		}
	}
	
//C     THE  RECIPROCAL  LATTICE IS  DEFINED BY  B1,B2 . THE  SUMMATION  
//C     BEGINS WITH THE ORIGIN POINT OF THE LATTICE , AND  CONTINUES IN  
//C     STEPS OF 8*N1 POINTS , EACH  STEP INVOLVING THE  PERIMETER OF A  
//C     PARALLELOGRAM OF LATTICE POINTS ABOUT THE ORIGIN,OF SIDE 2*N1+1  
//C     EACH STEP BEGINS AT LABEL 9.  
//C     AKPT=THE CURRENT LATTICE VECTOR IN THE SUM  

		RTV = 2. * M_PI / TV;
		B1.x = AR2.y * RTV;
		B1.y = -AR2.x * RTV;
		B2.x = -AR1.y * RTV;
		B2.y = AR1.x * RTV;
		/*
		B1.x = -AR1.y * RTV;
		B1.y = AR1.x * RTV;
		B2.x = -AR2.y * RTV;
		B2.y = AR2.x * RTV;
		*/
		
		TEST1 = 1.e6; // at least one iteration for convergence criterion
		II = 1;
		N1 = -1; // number of iterations
pos9:
		N1++;
		//printf("%i\n", N1);
		NA = N1 + N1 + II;
		AN1 = REAL(N1);
		AN2 = -AN1 - 1.;
		for(I1=1; I1<=NA; I1++)
		{
			AN2 = AN2 + 1.;
			for(I2=1; I2<=4; I2++)
			{
//				fprintf(stderr, "I1=%i, I2=%i\n", I1, I2);
//				C     WRITE(16,307) I1,I2  
//				C 307 FORMAT(33X,'I1=',I2,' , I2=',I2/33X,12('='))  
				AN = AN1;
				AN1 = -AN2;
				AN2 = AN;
				AB1 = AN1 * B1.x + AN2 * B2.x;
				AB2 = AN1 * B1.y + AN2 * B2.y;
				AKPT.x = AK.x + AB1;
				AKPT.y = AK.y + AB2;

//C     FOR  EVERY LATTICE VECTOR OF THE SUM, THREE SHORT ARRAYS ARE  
//C     INITIALISED AS BELOW. AND USED AS TABLES:  
//C     XPM(M) CONTAINS VALUES OF XPK**|M|  
//C     AGK(I) CONTAINS VALUES OF (AC/KAPPA)**I  
//C     GKN(N) CONTAINS VALUES OF (GP/KAPPA)**(2*N-1)*GAM(N,Z)  
//C     WHERE L=0,L2MAX;M=-L,L;N=0,(L-|M|)/2;I=L-2*N  
//C     GAM IS THE INCOMPLETE GAMMA FUNCTION, WHICH IS CALCULATED BY  
//C     RECURRENCE  FROM  THE VALUE  FOR N=0, WHICH  IN TURN CAN  BE  
//C     EXPRESSED IN TERMS OF THE COMPLEX ERROR FUNCTION CERF  
//C     AC=MOD(AKPT). NOTE SPECIAL ACTION IF AC=0  

				ACSQ = AKPT.x*AKPT.x + AKPT.y*AKPT.y;
				GPSQ = KAPSQ - ACSQ;

				if (abs(GPSQ) < EMACH*EMACH)
				{
				fprintf(stderr, "FATAL ERROR\n");
				exit(1);
/*
      WRITE(7,100) 
  100 FORMAT(13X,'FATAL ERROR FROM XMAT:'/3X,'GPSQ IS TOO SMALL.' 
     & /3X,'GIVE A SMALL BUT NONZERO VALUE FOR "EPSILON"'/3X,  
     & 'IN THE DATA STATEMENT OF THE MAIN PROGRAM.' 
     & /3X,'THIS DEFINES A SMALL IMAGINARY PART' 
     & /3X,'IN THE FREQUENCY OR WAVELENGTH VALUE.') 
      STOP 
*/
				}
				AC = sqrt(ACSQ);
				GP = sqrt(GPSQ);
				XPK = CZERO;
				GK = CZERO;
				GKK = c::complex<REAL>(1., 0.);
				if ((AC-EMACH) > 0)
				{
					XPK = c::complex<REAL>(AKPT.x / AC, AKPT.y / AC);
					GK = AC / KAPPA;
					GKK = GPSQ / KAPSQ;
				}
				XPM[0] = c::complex<REAL>(1., 0.);
				AGK[0] = c::complex<REAL>(1., 0.);
				for(I=1; I<LL2; I++)
				{
					XPM[I] = XPM[I-1] * XPK;
					AGK[I] = AGK[I-1] * GK;
				}
		
				CF = KAPPA / GP;
				ZZ = -ALPHA * GKK;
				CZ = sqrt(-ZZ);
				Z = -CI * CZ;
				CX = exp(-ZZ);
				GAM = RTPI * CERF(CZ, EMACH);
				GKN[0] = CF*CX*GAM;
				BT = Z;
				B = 0.5;
				LLL = L2MAX / 2 + 1;
				for(I=1; I<LLL; I++)
				{
					BT = BT / ZZ;
					B = B - 1.;
					GAM = (GAM - BT) / B;
					CF = CF * GKK;
					GKN[I] = CF * CX * GAM;
//					cout << "GKN " << GKN[I] << "\n";
				}
//C     THE CONTRIBUTION TO THE SUM DLM1 FOR A PARTICULAR  
//C     RECIPROCAL LATTICE VECTOR IS NOW ACCUMULATED INTO  
//C     THE  ELEMENTS OF DLM,NOTE SPECIAL ACTION IF  AC=0  

				K = 0;
				KK = 0;
				for(L=1; L<=LL2; L++)
				{
					MM = 1;	
					if ((L % 2) == 0) MM = 2;
					N = (L * L + MM) / 2;
					NN = (L - MM) / 2 + 2;
					for(M=MM; M<=L; M+=2)
					{
						ACC = CZERO;
						NN--;
						IL = L;
						for(I=1; I<=NN; I++)
						{
							ACC += DENOM[K] * AGK[IL-1] * GKN[I-1];
							IL = IL - 2;
							K++;
						}
						ACC = PREF[KK] * ACC;
						if (AC <= 1.e-6) goto pos17;  
						DLM[N-1] = DLM[N-1] + ACC / XPM[M-1];
      
						if ((M-1) != 0)
						{
							pos17:
							NM = N - M + 1;
							DLM[NM-1] = DLM[NM-1] + ACC * XPM[M-1];
//							cout << DLM[NM-1] << "\n";
						}
						KK++;
						N = N + 1;
					}
				}
			// 21
				if (II > 0) break;
			}
		// 22	
		II=0;
		}
		
//C     AFTER EACH STEP OF THE SUMMATION A TEST ON THE  
//C     CONVERGENCE  OF THE  ELEMENTS OF  DLM IS  MADE  

	TEST2 = 0.;
	for(I=0; I<NNDLM; I++)
	{
		DNORM = abs(DLM[I]);
		TEST2 += DNORM * DNORM;
	}

	TEST = fabs((TEST2 - TEST1) / TEST1);
	TEST1 = TEST2;

		if (TEST > 0.0001)
		{
			if (N1 < 10)
			{
				goto pos9;
			}
			else
			{
				fprintf(stderr, "**DLM1 S NOT COONVERGED BY N1=%i\n", N1);
	//			WRITE(16,26) N1 
	//			FORMAT(29H**DLM1,S NOT CONVERGED BY N1=,I2)  
				goto pos285;
			}
		}

//		fprintf(stderr, "DLM1,S COONVERGED BY N1=%i\n", N1);
		//WRITE(16,28) N1  
		//FORMAT(25H DLM1,S CONVERGED BY N1 =,I2)  
//     WRITE(16,250) DLM  
//250  FORMAT(5H0DLM1,//,45(2E13.5,/))  

//C     DLM2, THE SUM OVER REAL SPACE LATTICE VECTORS, BEGINS WITH  
//C     THE ADJUSTMENT OF THE ARRAY PREF, TO CONTAIN VALUES OF THE  
//C     PREFACTOR  'P2' FOR LM=(00),(11),(20),(22),...  

pos285:
	KK = 0;
	AP1 = TV / (4. * M_PI);
	CF = KAPSQ / CI;
	for(L=1; L<=LL2; L++)
	{
		CP = CF;
		MM = 1;
		if ((L % 2) == 0)
		{
			MM = 2;
			CP = -CI * CP;
		}
//  30  
		J1 = (L - MM)/2 + 1;
		J2 = J1 + MM - 1;
		IN = J1 + L - 2;
		AP2 = pow(REAL(-1.), IN) * AP1;
		for(M = MM; M<=L; M+=2)
		{
			AP = AP2 / (FAC[J1-1] * FAC[J2-1]);
			PREF[KK] = AP * CP * PREF[KK];
			J1 = J1 - 1;
			J2 = J2 + 1;
			AP2 = -AP2;
			CP = -CP;
			KK++;
		}
	}

//C     THE SUMMATION PROCEEDS IN STEPS OF 8*N1 LATTICE POINTS  
//C     AS BEFORE, BUT THIS  TIME EXCLUDING  THE ORIGIN  POINT  
//C     R=THE CURRENT LATTICE VECTOR IN THE SUM  
//C     AR=MOD(R)  

	N1 = 0;
pos32:
	N1++;
	//printf("%i\n", N1);
	NA = N1 + N1;
	AN1 = REAL(N1);
	AN2 = -AN1 - 1.;
	for(I1=1; I1<=NA; I1++)
	{
		AN2++;
		for(I2=1; I2<=4; I2++)
		{	
			AN = AN1;
			AN1 = -AN2;
			AN2 = AN;
			R.x = AN1*AR1.x + AN2*AR2.x;
			R.y = AN1*AR1.y + AN2*AR2.y;
			AR = sqrt(R.x*R.x + R.y*R.y);
			XPK = c::complex<REAL>(R.x/AR, R.y/AR);
			XPM[0] = 1;
			for(I=1; I<LL2; I++)
			{
				XPM[I] = XPM[I-1] * XPK;
			}
			AD = AK.x * R.x + AK.y * R.y;
			SD = exp(-AD * CI);

//C     FOR EACH LATTICE VECTOR THE INTEGRAL 'U' IS OBTAINED  
//C     FROM THE RECURRENCE RELATION IN L SUGGESTED BY KAMBE  
//C     U1 AND U2 ARE THE  INITIAL TERMS OF THIS RECURRENCE,   
//C     FOR L#-1 AND L=0, AND THEY ARE EVALUATED IN TERMS OF   
//C     THE COMPLEX ERROR FUNCTION CERF  

			KANT = REAL(0.5) * AR * KAPPA;
			KNSQ = KANT * KANT;
			Z = CI * KANT / RTA;
			ZZ = RTA - Z;
			Z = RTA + Z;
			WW = CERF(-ZZ, EMACH);
			W = CERF(Z, EMACH);
			AA = REAL(0.5) * RTPI * (W - WW) / CI;
			AB = REAL(0.5) * RTPI * (W + WW);
			A = ALPHA - KNSQ / ALPHA;
			XPA = exp(A);
			U1 = AA * XPA;
			U2 = AB * XPA / KANT;

//C     THE CONTRIBUTION TO DLM2 FROM A PARTICULAR LATTICE  
//C     VECTOR  IS  ACCUMULATED INTO  THE ELEMENTS OF  DLM  
//C     THIS PROCEDURE INCLUDES THE TERM (KANT**L) AND THE  
//C     RECURRENCE FOR THE INTEGRAL 'U' 

			KK = 0;
			AL = -0.5;
			CP = RTA;
			CF = c::complex<REAL>(1., 0.);
			for(L=1; L<=LL2; L++)
			{
				MM = 1;
				if ((L % 2) == 0)  MM = 2;
				N = (L*L + MM) / 2;
  
				for(M=MM; M<=L; M+=2)
				{
					ACC = PREF[KK] * U2 * CF * SD;
					DLM[N-1] = DLM[N-1] + ACC / XPM[M-1];
		
					if ((M-1) != 0)
					{
						NM = N - M + 1;
						DLM[NM-1] = DLM[NM-1] + ACC*XPM[M-1];
//						cout << "DLM " << DLM[NM-1] << "\n";
					}
					KK++;
					N = N + 1;
				}
				AL = AL + 1.;
				CP = CP / ALPHA;
				U = (AL*U2 - U1 + CP*XPA) / KNSQ;
				U1 = U2;
				U2 = U;
				CF = KANT * CF;
			}
		}
	}

//C     AFTER EACH STEP OF THE SUMMATION A TEST ON THE  
//C     CONVERGENCE OF THE ELEMENTS OF DLM IS MADE  

	TEST2 = 0.;
	for(I=0; I<NNDLM; I++)
	{
		DNORM = abs(DLM[I]);
		TEST2 = TEST2 + DNORM*DNORM;
	}
	TEST = fabs((TEST2 - TEST1) / TEST1);
	TEST1 = TEST2;
	if (TEST > 0.001)
	{	  
		if (N1 < 10) goto pos32;
		
		fprintf(stderr, "S not converged by N1 %i\n", N1);
		//WRITE(16,44)N1  
		//FORMAT(31H0**DLM2,S NOT CONVERGED BY N1 =,I2)  
		goto pos465;
	}
	
	/*
	WRITE(16,46)N1
	FORMAT(24H DLM2,S CONVERGED BY N1=,I2)  
*/
//fprintf(stderr, "DLM2,S converged by N1=%i\n", N1);
//C     THE TERM DLM3 HAS A NON-ZERO CONTRIBUTION  ONLY  
//C     WHEN L=M=0.IT IS EVALUATED HERE IN TERMS OF THE  
//C     COMPLEX ERROR FUNCTION CERF  

pos465:
		XPA = exp(-ALPHA);
		RTAI = REAL(1.) / (RTPI*RTA);
		ACC = KAPPA*(CI*(XPA - CERF(RTA, EMACH)) - RTAI)/XPA;
		AP = -0.5 / RTPI;
		DLM[0] = DLM[0] + AP*ACC;

//C     FINALLY THE ELEMENTS OF DLM ARE MULTIPLIED BY THE  
//C     FACTOR (-1.0D0)**((M + |M|)/2)  

		for(L=2;L<=LL2;L+=2)
		{
			N = L*L/2 + 1;
			for(M=2; M<=L; M+=2)
			{
				DLM[N-1] = -DLM[N-1];
				N = N + 1;
			}
		}

//C     WRITE(16,251) DLM  
//C 251 FORMAT(15H0DLM1+DLM2+DLM3,//45(2E13.5,/))  

//C     SUMMATION OVER THE CLEBSCH-GORDON TYPE COEFFICIENTS  
//C     ELM PROCEEDS, FIRST FOR  XODD, AND THEN  FOR XEVEN.  
//C     THIS GIVES THE KAMBE ELEMENTS  A(L2,M2;L3,M3) WHICH  
//C     GIVE THE ELEMENTS  X(L3,M3;L2,M2) OF XODD AND XEVEN  

	K = 1;
	II = 0;
pos48:  
	LL = LMAX + II;
	I = 1;
	
	for(IL2=1;IL2<=LL; IL2++)
	{
		L2 = IL2 - II;
		M2 = -L2 + 1 - II;
	
		for(I2=1; I2<=IL2; I2++)
		{
		J = 1;
		//55
		for(IL3=1; IL3<=LL; IL3++)
		{
			L3 = IL3 - II;
			M3 = -L3 + 1 - II;
			for(I3=1;I3<=IL3;I3++)
			{
				ALM = CZERO;
				LA1 = MAX(abs(L2-L3), abs(M2-M3));
				LB1 = L2 + L3;
				N = (LA1*(LA1+2) + M2 - M3 + 2) / 2;
				NN = 2*LA1 + 4;
				LB11 = LB1 + 1;
				LA11 = LA1 + 1;
	
				// 49
				for(L1=LA11; L1<=LB11; L1+=2)
				{
					ALM = ALM + ELM[K-1] * DLM[N-1];
					N = N + NN;
					NN = NN + 4;
					K = K + 1;
				}
				ALM = ALM / KAPPA;
				if ((I-J) == 0) ALM = ALM + CI;
				if (II <= 0) XODD[J-1][I-1] = CI*ALM;
				else XEVEN[J-1][I-1] = CI*ALM;
				M3 = M3 + 2;
				J = J + 1;
			}
		}
		M2 = M2 + 2;
	
		I = I + 1;
		}
	}
	
	if (II <= 0)
	{
		II = 1;
		goto pos48;
	}
}
//=========================================================
