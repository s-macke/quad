#ifndef CATOM_H
#define CATOM_H

#include"global.h"
//include"CVector.h"
#include"CTable.h"
#include"stdlib.h"
#include"OpticalConstants.h"

static void inline MatrixMul(c::complex<REAL> r[3][3], const c::complex<REAL> a[3][3], const c::complex<REAL> b[3][3])
{
	// [row][column] for all matrices
	
	for(int i=0; i<3; i++)
	for(int j=0; j<3; j++)
	{
		r[i][j] = a[i][0]*b[0][j] + a[i][1]*b[1][j] + a[i][2]*b[2][j];
	}
}

// This is the interface for the atom class
// Contains functions for scattering
class CAtom
{
	public:
		CAtom(){};
		~CAtom(){};
		//virtual ~CAtom() = 0;
		// Used to precalculate some coefficients. You can assume that LMAX >= 1
		virtual void Set(const int LMAX, const c::complex<REAL> &KAPPA, const REAL Efield)=0;
		
		//The multiple scattering implemented in multem is using only the isotropic part 
		virtual void GetIsotropicScatterMatrix(c::complex<REAL> *TE, c::complex<REAL> *TH, int LMAX) const = 0;
		
		// Given the Electric and Magnetic field in spherical harmonics (bElm, bHlm), 
		// this function will give back the coefficients aElm and aHlm. 
		virtual void Scatter(CExpansionCoefficients &A, const CExpansionCoefficients &B) = 0;
		virtual CAtom* Clone() const = 0;
		CVector<c::complex<REAL> > TE;
		CVector<c::complex<REAL> > TH;
		CString name;
};

// For the special case of an isotropic scatterer this class contains already some helper functions. 
// Only the function Set must be implemented 
class CIsotropicAtom : public CAtom
{
	public:

	~CIsotropicAtom()
	{
	}

	void GetIsotropicScatterMatrix(c::complex<REAL> *_TE, c::complex<REAL> *_TH, int LMAX) const
	{
		for(int i=0; i<=LMAX; i++)
		{
			_TE[i] = TE[i];
			_TH[i] = TH[i];
		}
	}

	void Scatter(CExpansionCoefficients &B, const CExpansionCoefficients &A)
	{
		int i = 0;
		for(int l = 0; l<TE.size(); l++)
		for(int m = -l; m<=l; m++)
		{
			B.Elm[i].pol[0] = TE[l] * A.Elm[i].pol[0];
			B.Hlm[i].pol[0] = TH[l] * A.Hlm[i].pol[0];
			B.Elm[i].pol[1] = TE[l] * A.Elm[i].pol[1];
			B.Hlm[i].pol[1] = TH[l] * A.Hlm[i].pol[1];
			i++;
		}
	}

//	CVector<c::complex<REAL> > TE;
//	CVector<c::complex<REAL> > TH;
};

#include"atom/CSphereAtom.h"
#include"atom/CScatteringFactorAtom.h"
#include"atom/CFormFactorAtom.h"
#include"atom/CScatteringTensorAtom.h"
#include"atom/CScatteringFactorFunctionAtom.h"
#include"atom/CTetragonalFunctionAtom.h"

// ------------------------------------------------------------------------------

class CScatteringFactorFileAtom : public CIsotropicAtom
{
	public:
		CScatteringFactorFileAtom(const CScatteringFactorFileAtom &a)
		{
			TE = a.TE;
			TH = a.TH;
			name = a.name;
		}

		CScatteringFactorFileAtom(const char* _name)
		{
			name = CString(_name);
		}
		
		void Set(const int LMAX, const c::complex<REAL> &KAPPA, REAL Efield)
		{
			// TE = CI*REAL(2.)/REAL(9.) * R*R*R*KAPPA*KAPPA*KAPPA * (EPSSPH-REAL(1.));		
			c::complex<REAL> CI(REAL(0.), REAL(1.));
			c::complex<REAL> CZERO(REAL(0.), REAL(0.));

			REAL lambda = 2.*M_PI / KAPPA.real();
			REAL energy = 12398.326 / lambda;
//printf("%e\n", energy);
			CTableEntry te = GetOpticalConstants(name, energy);
			c::complex<REAL> f = c::complex<REAL>(te.y[0], -te.y[1]);

			const REAL r0 = 2.8179403267e-5; // in A
			c::complex<REAL> pre = -CI * KAPPA * ( REAL(2.)/REAL(3.) * r0 );
			
			TE.assign(LMAX+1, CZERO);
			TH.assign(LMAX+1, CZERO);
		
			TE[0] = 0.;
			TH[0] = 0.;
			TE[1] = pre * f;
			TH[1] = 0.;
			for(int i=2; i<=LMAX; i++)
			{
				TE[i] = 0.;
				TH[i] = 0.;
			}
			
			//fprintf(stderr, "%e %e\n", TE[1].real(), TE[1].imag());		
		}

		CScatteringFactorFileAtom* Clone() const
		{
			return new CScatteringFactorFileAtom(*this);
		}
//		CString name;
};


// ------------------------------------------------------------------------------

class CMagneticFileAtom : public  CAtom
{
	public:
		CMagneticFileAtom(const CMagneticFileAtom &a)
		{
			name = a.name;
			theta = a.theta;
			phi = a.phi;
			scale = a.scale;
			for(int i=0; i<3; i++)
			for(int j=0; j<3; j++)
			{
				F[i][j] = a.F[i][j];
				T[i][j] = a.T[i][j];
			}
		}

		CMagneticFileAtom(const char* _name, double _theta, double _phi, double _scale)
		{
			name = CString(_name);
			theta = _theta / 180. * M_PI;
			phi = _phi / 180. * M_PI;
			scale = _scale;
			for(int i=0; i<3; i++)
			for(int j=0; j<3; j++)
			{
				F[i][j] = 0.;
				T[i][j] = 0.;
			}
		}
		
		void GetIsotropicScatterMatrix(c::complex<REAL> *TE, c::complex<REAL> *TH, int LMAX) const
		{/*
			for(int i=0; i<9; i++) TH[i] = 0.;
			
			TE[0] = T[0][0];
			TE[1] = T[1][0];
			TE[2] = T[2][0];
			TE[3] = T[0][1];
			TE[4] = T[1][1];
			TE[5] = T[2][1];
			TE[6] = T[0][2];
			TE[7] = T[1][2];
			TE[8] = T[2][2];
			*/
			
			TE[0] = 0.;
			TH[0] = 0.;
			TE[1] = (T[0][0] + T[1][1] + T[2][2]) * (1./ 3.);
			TH[1] = 0.;
			//printf("%e %e\n", TE[1].real(), TE[1].imag());
		}

		void Set(const int LMAX, const c::complex<REAL> &KAPPA, REAL Efield);

		void Scatter(CExpansionCoefficients &B, const CExpansionCoefficients &A)
		{
			B.Hlm[0].pol[0] = 0.;
			B.Hlm[1].pol[0] = 0.;
			B.Hlm[2].pol[0] = 0.;
			B.Hlm[3].pol[0] = 0.;
			B.Hlm[0].pol[1] = 0.;
			B.Hlm[1].pol[1] = 0.;
			B.Hlm[2].pol[1] = 0.;
			B.Hlm[3].pol[1] = 0.;
			
			B.Elm[0].pol[0] = 0.;
			B.Elm[0].pol[1] = 0.;
			
			c::complex<REAL> x, y, z;
			x = A.Elm[1].pol[0];
			y = A.Elm[2].pol[0];
			z = A.Elm[3].pol[0];
			
			B.Elm[1].pol[0] = T[0][0] * x + T[1][0] * y + T[2][0] * z;
			B.Elm[2].pol[0] = T[0][1] * x + T[1][1] * y + T[2][1] * z;
			B.Elm[3].pol[0] = T[0][2] * x + T[1][2] * y + T[2][2] * z;

			x = A.Elm[1].pol[1];
			y = A.Elm[2].pol[1];
			z = A.Elm[3].pol[1];
			B.Elm[1].pol[1] = T[0][0] * x + T[1][0] * y + T[2][0] * z;
			B.Elm[2].pol[1] = T[0][1] * x + T[1][1] * y + T[2][1] * z;
			B.Elm[3].pol[1] = T[0][2] * x + T[1][2] * y + T[2][2] * z;
			// TODO: set everything above L=1 to zero
		}

		CMagneticFileAtom* Clone() const
		{
			return new CMagneticFileAtom(*this);
		}
		CString name;
		c::complex<REAL> F[3][3];
		c::complex<REAL> T[3][3];

		REAL theta, phi, scale;

};

// ------------------------------------------------------------------------------

class CMagneticAtom : public CAtom
{
	public:
		CMagneticAtom(const CMagneticAtom& r)
		{
			f = r.f;
			fm = r.fm;
			theta = r.theta;
			phi = r.phi;
			for(int i=0; i<3; i++)
			for(int j=0; j<3; j++)
			{
				F[i][j] = r.F[i][j];
				T[i][j] = r.T[i][j];
			}
		}

		CMagneticAtom(c::complex<REAL> _f, c::complex<REAL> _fm, REAL _theta, REAL _phi)
		{
			f = _f;
			fm = _fm;
			theta = _theta / 180. * M_PI;
			phi   = _phi / 180. * M_PI;
			for(int i=0; i<3; i++)
			for(int j=0; j<3; j++)
			{
				F[i][j] = 0.;
				T[i][j] = 0.;
			}
		}
		
		void SetAngle(REAL _theta, REAL _phi) {
                theta = _theta / 180. * M_PI;
                phi   = _phi / 180. * M_PI;
        }

        void Setfm(REAL _f1m, REAL _f2m) {
                fm = c::complex<REAL>(_f1m, _f2m);
        }

		void GetIsotropicScatterMatrix(c::complex<REAL> *TE, c::complex<REAL> *TH, int LMAX) const
		{
			TE[0] = 0.;
			TH[0] = 0.;
			TE[1] = (T[0][0] + T[1][1] + T[2][2]) * (1./ 3.);;
			TH[1] = 0.;
		}

		void Set(const int LMAX, const c::complex<REAL> &KAPPA, const REAL EField)
		{
			c::complex<REAL> CI(REAL(0.), REAL(1.));
			c::complex<REAL> CZERO(REAL(0.), REAL(0.));

			REAL lambda = 2.*M_PI / KAPPA.real();
			REAL energy = 12398.326 / lambda;
			//CTableEntry te = GetOpticalConstants(name, energy);

			F[0][0] = c::complex<REAL>(f.real(), -f.imag());
			F[0][1] = c::complex<REAL>(fm.real(), -fm.imag()) * cos(theta);
			F[0][2] = c::complex<REAL>(fm.real(), -fm.imag()) * (-sin(theta)*sin(phi));

			F[1][0] = c::complex<REAL>(fm.real(), -fm.imag()) *(-cos(theta));
			F[1][1] = c::complex<REAL>(f.real(), -f.imag());
			F[1][2] = c::complex<REAL>(fm.real(), -fm.imag()) * (sin(theta)*cos(phi));
			
			F[2][0] = c::complex<REAL>(fm.real(), -fm.imag()) * (sin(theta)*sin(phi));
			F[2][1] = c::complex<REAL>(fm.real(), -fm.imag()) * (-sin(theta)*cos(phi));
			F[2][2] = c::complex<REAL>(f.real(), -f.imag()) ;
			//c::complex<REAL> f = c::complex<REAL>(te.y[0], -te.y[1]);

			c::complex<REAL> Cr[3][3]; // transformation right
			c::complex<REAL> Cl[3][3]; // transformation left
			
			Cr[0][0] = -2.*sqrt(M_PI/3.);
			Cr[0][1] = 0.;
			Cr[0][2] =  2.*sqrt(M_PI/3.);

			Cr[1][0] = 2.*CI*sqrt(M_PI/3.);
			Cr[1][1] = 0.;
			Cr[1][2] = 2.*CI*sqrt(M_PI/3.);

			Cr[2][0] = 0.;
			Cr[2][1] = -2.*sqrt(2.*M_PI/3.);
			Cr[2][2] = 0.;
			
			double t = 8./3.*M_PI;
			Cl[0][0] = conj(Cr[0][0])/t;
			Cl[0][1] = conj(Cr[1][0])/t;
			Cl[0][2] = conj(Cr[2][0])/t;
			Cl[1][0] = conj(Cr[0][1])/t;
			Cl[1][1] = conj(Cr[1][1])/t;
			Cl[1][2] = conj(Cr[2][1])/t;
			Cl[2][0] = conj(Cr[0][2])/t;
			Cl[2][1] = conj(Cr[1][2])/t;
			Cl[2][2] = conj(Cr[2][2])/t;			
						
			MatrixMul(T, F, Cr);
			MatrixMul(Cr, Cl, T);

			const REAL r0 = 2.8179403267e-5; // in A
			c::complex<REAL> pre = -CI * KAPPA * ( REAL(2.)/REAL(3.) * r0 );
			
			for(int i=0; i<3; i++)
			for(int j=0; j<3; j++)
			{
				//printf("%f %f\n", Cr[i][j].real(), F[i][j].real());
				T[i][j] = Cr[i][j] * pre;
			}
		}
		
		void Scatter(CExpansionCoefficients &B, const CExpansionCoefficients &A)
		{
			B.Hlm[0].pol[0] = 0.;
			B.Hlm[1].pol[0] = 0.;
			B.Hlm[2].pol[0] = 0.;
			B.Hlm[3].pol[0] = 0.;
			B.Hlm[0].pol[1] = 0.;
			B.Hlm[1].pol[1] = 0.;
			B.Hlm[2].pol[1] = 0.;
			B.Hlm[3].pol[1] = 0.;
			
			B.Elm[0].pol[0] = 0.;
			B.Elm[0].pol[1] = 0.;
			
			c::complex<REAL> x,y,z;
			x = A.Elm[1].pol[0];
			y = A.Elm[2].pol[0];
			z = A.Elm[3].pol[0];
			
			B.Elm[1].pol[0] = T[0][0] * x + T[0][1] * y + T[0][2] * z;
			B.Elm[2].pol[0] = T[1][0] * x + T[1][1] * y + T[1][2] * z;
			B.Elm[3].pol[0] = T[2][0] * x + T[2][1] * y + T[2][2] * z;
				
			x = A.Elm[1].pol[1];
			y = A.Elm[2].pol[1];
			z = A.Elm[3].pol[1];
			B.Elm[1].pol[1] = T[0][0] * x + T[0][1] * y + T[0][2] * z;
			B.Elm[2].pol[1] = T[1][0] * x + T[1][1] * y + T[1][2] * z;
			B.Elm[3].pol[1] = T[2][0] * x + T[2][1] * y + T[2][2] * z;
			// TODO: set everything above L=1 to zero
		}

		CMagneticAtom* Clone() const
		{
			return new CMagneticAtom(*this);
		}

		CString name;
		c::complex<REAL> F[3][3];
		c::complex<REAL> T[3][3];
		c::complex<REAL> f, fm;
		REAL theta, phi;
};


class CMagneticFunctionAtom : public CAtom
{
	public:
		CMagneticFunctionAtom(const CMagneticFunctionAtom& r)
		{
			ref = r.ref;
			refm = r.refm;
			theta = r.theta;
			phi = r.phi;
			for(int i=0; i<3; i++)
			for(int j=0; j<3; j++)
			{
				F[i][j] = r.F[i][j];
				T[i][j] = r.T[i][j];
			}
		}

		CMagneticFunctionAtom(int _ref, int _refm, REAL _theta, REAL _phi)
		{
			ref = _ref;
			refm = _refm;
			theta = _theta / 180. * M_PI;
			phi = _phi / 180. * M_PI;
			for(int i=0; i<3; i++)
			for(int j=0; j<3; j++)
			{
				F[i][j] = 0.;
				T[i][j] = 0.;
			}
		}
		
		void GetIsotropicScatterMatrix(c::complex<REAL> *TE, c::complex<REAL> *TH, int LMAX) const
		{
			TE[0] = 0.;
			TH[0] = 0.;
			TE[1] = T[0][0];
			TH[1] = 0.;
		}
		
		void Set(const int LMAX, const c::complex<REAL> &KAPPA, const REAL EField);

		void Scatter(CExpansionCoefficients &B, const CExpansionCoefficients &A)
		{
			B.Hlm[0].pol[0] = 0.;
			B.Hlm[1].pol[0] = 0.;
			B.Hlm[2].pol[0] = 0.;
			B.Hlm[3].pol[0] = 0.;
			B.Hlm[0].pol[1] = 0.;
			B.Hlm[1].pol[1] = 0.;
			B.Hlm[2].pol[1] = 0.;
			B.Hlm[3].pol[1] = 0.;
			
			B.Elm[0].pol[0] = 0.;
			B.Elm[0].pol[1] = 0.;
			
			c::complex<REAL> x,y,z;
			x = A.Elm[1].pol[0];
			y = A.Elm[2].pol[0];
			z = A.Elm[3].pol[0];
			
			B.Elm[1].pol[0] = T[0][0] * x + T[0][1] * y + T[0][2] * z;
			B.Elm[2].pol[0] = T[1][0] * x + T[1][1] * y + T[1][2] * z;
			B.Elm[3].pol[0] = T[2][0] * x + T[2][1] * y + T[2][2] * z;
				
			x = A.Elm[1].pol[1];
			y = A.Elm[2].pol[1];
			z = A.Elm[3].pol[1];
			B.Elm[1].pol[1] = T[0][0] * x + T[0][1] * y + T[0][2] * z;
			B.Elm[2].pol[1] = T[1][0] * x + T[1][1] * y + T[1][2] * z;
			B.Elm[3].pol[1] = T[2][0] * x + T[2][1] * y + T[2][2] * z;
			// TODO: set everything above L=1 to zero
		}

		CMagneticFunctionAtom* Clone() const
		{
			return new CMagneticFunctionAtom(*this);
		}

		CString name;
		c::complex<REAL> F[3][3];
		c::complex<REAL> T[3][3];
		int ref, refm;
		REAL theta, phi;
};

// ------------------------------------------------------------------------------


void TMTRX(int LMAX,
	const c::complex<REAL> &R,
	const c::complex<REAL> &KAPPA,
	const c::complex<REAL> &EPSSPH,
	c::complex<REAL> *TE,
	c::complex<REAL> *TH);

#endif
