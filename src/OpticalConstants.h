#ifndef OPTICALCONSTANTS_H
#define OPTICALCONSTANTS_H

#include"CTable.h"

class CAtomTable
{
	public:
	CTable ftable;
	CTable mtable;
	CString name;
	
	int Z;
	double cromer[9];
	
	CAtomTable()
	{
		Z = -1;
	}
};

void LoadOpticalConstants(const char* name, const char *filename);
void LoadScatteringTensor(const char* name, const char *filename);
void LoadMagneticConstants(const char* name, const char *ffilename, const char *mfilename);

void LoadOpticalConstantsFromDatabase(const char* name, const char *elementname, double qz);
CTableEntry GetOpticalConstants(CString name, double energy);


#endif