extern "C"
{
#include"lua/src/lua.h"
#include "lua/src/lauxlib.h"
#include "lua/src/lualib.h"
}

#include"scriptvec.h"
// -------------------------------------------------

int vec2new(lua_State *L)
{
	vec2 *res;
	
	if (lua_isnumber(L, 1) && lua_isnumber(L, 2))
	{
		double x = lua_tonumber(L, 1);
  		double y = lua_tonumber(L, 2);		
		res = (vec2*)lua_newuserdata(L, sizeof(vec2));
		res->x = x;
		res->y = y;
		luaL_setmetatable(L, "Lua.vec2");
		return 1; 
	}
	else
	{
		return luaL_error(L, "Error vec2 can only contain numbers");
	}
}

static int vec2index(lua_State *L)
{
	vec2 *a = (vec2*)luaL_checkudata(L, 1, "Lua.vec2");
	const char *str = lua_tostring(L, 2);

	switch(str[0])
	{
		case 'x':
			lua_pushnumber(L, a->x);
			return 1;
		case 'y':
			lua_pushnumber(L, a->y);	
			return 1;
	}	
	return luaL_error(L, "Error vec2 variable not found");;
}

static int vec2newindex(lua_State *L)
{
	vec2 *a = (vec2*)luaL_checkudata(L, 1, "Lua.vec2");
	const char *str = lua_tostring(L, 2);
	double v = lua_tonumber(L, 3);

	switch(str[0])
	{
		case 'x':
			a->x = v;
			return 0;
		case 'y':
			a->y = v;	
			return 0;
	}
	
	
	//fprintf(stderr, "Index %s\n", str);
	return luaL_error(L, "Error vec3 variable not found");;
}


/* Stringrepresentation des Vektors erzeugen */
static int vec2tostring(lua_State *L){
	/* Pruefen, ob richtige Userdata auf dem Stack liegen und lesen */
	/* Fehlerbehandluung fehlt !!! */
	vec2 *v = (vec2*)luaL_checkudata(L, 1, "Lua.vec2");
	
	/* Das Ergebnis auf dem Stack ablegen */
	lua_pushfstring(L, "(%f, %f)", v->x, v->y);
	return 1; /* Wie viele Rueckgabewerte */
}

static const struct luaL_Reg vec2_meta [] = {
	{"__index", vec2index},
	{"__newindex", vec2newindex},
	{"__tostring", vec2tostring},
	{NULL, NULL}
};

// -------------------------------------------------

/* Einen neuen Vektor erzeugen */
int vec3new(lua_State *L)
{
	vec3 *res;
  	
	/* Pruefen, ob Argumente gueltig sind */
	if (lua_isnumber(L, 1) && lua_isnumber(L, 2) && lua_isnumber(L, 3))
	{
		/* Argumente vom Stack lesen */
		double x = lua_tonumber(L, 1);
		double y = lua_tonumber(L, 2);
		double z = lua_tonumber(L, 3);
	
		/* Ein neues Full-Userdata erzeugen */
		res = (vec3 *)lua_newuserdata(L, sizeof(vec3));
		res->x = x;
		res->y = y;
		res->z = z;

		/* Dem neuen Userdata die selbe Metatabelle, die alle Vektoren haben, zuordnen */
		//luaL_getmetatable(L, "Lua.vec3");
		//lua_setmetatable(L, -2);
		luaL_setmetatable(L, "Lua.vec3");

		return 1; /* Wie viele R�ckgabewerte */
	}
	else
	{
		return luaL_error(L, "Error vec3 can only contain numbers");
	}
}


static int vec3index(lua_State *L)
{
	vec3 *a = (vec3*)luaL_checkudata(L, 1, "Lua.vec3");
	const char *str = lua_tostring(L, 2);

	switch(str[0])
	{
		case 'x':
			lua_pushnumber(L, a->x);
			return 1;
		case 'y':
			lua_pushnumber(L, a->y);	
			return 1;
		case 'z':
			lua_pushnumber(L, a->z);	
			return 1;
	}
	
	
	//fprintf(stderr, "Index %s\n", str);
	return luaL_error(L, "Error vec3 variable not found");;
}

static int vec3newindex(lua_State *L)
{
	vec3 *a = (vec3*)luaL_checkudata(L, 1, "Lua.vec3");
	const char *str = lua_tostring(L, 2);
	double v = lua_tonumber(L, 3);

	switch(str[0])
	{
		case 'x':
			a->x = v;
			return 0;
		case 'y':
			a->y = v;	
			return 0;
		case 'z':
			a->z = v;
			return 0;
	}
	
	
	//fprintf(stderr, "Index %s\n", str);
	return luaL_error(L, "Error vec3 variable not found");;
}


/* Zwei Vektoren addieren */
static int vec3add(lua_State *L)
{
	vec3 *res;

	/* Pr�fen, ob richtige Userdata auf dem Stack liegen und lesen */
	/* Fehlerbehandluung fehlt !!! */
	vec3 *a = (vec3*)luaL_checkudata(L, 1, "Lua.vec3");
	vec3 *b = (vec3*)luaL_checkudata(L, 2, "Lua.vec3");
  
	/* Das Ergebnis erzeugen */
	res = (vec3 *)lua_newuserdata(L, sizeof(vec3));
	res->x = a->x + b->x;
	res->y = a->y + b->y;
	res->z = a->z + b->z;	
	
	/* Wieder die Metatabelle zuordnen */
	luaL_setmetatable(L, "Lua.vec3");

	return 1; /* Wie viele R�ckgabewerte */
}

static int vec3sub(lua_State *L)
{
	vec3 *res;

	/* Pr�fen, ob richtige Userdata auf dem Stack liegen und lesen */
	/* Fehlerbehandluung fehlt !!! */
	vec3 *a = (vec3*)luaL_checkudata(L, 1, "Lua.vec3");
	vec3 *b = (vec3*)luaL_checkudata(L, 2, "Lua.vec3");
  
	/* Das Ergebnis erzeugen */
	res = (vec3 *)lua_newuserdata(L, sizeof(vec3));
	res->x = a->x - b->x;
	res->y = a->y - b->y;
	res->z = a->z - b->z;	
	
	/* Wieder die Metatabelle zuordnen */
	luaL_setmetatable(L, "Lua.vec3");

	return 1; /* Wie viele R�ckgabewerte */
}



/* Stringrepr�sentation des Vektors erzeugen */
static int vec3tostring(lua_State *L){
	/* Pr�fen, ob richtige Userdata auf dem Stack liegen und lesen */
	/* Fehlerbehandluung fehlt !!! */
	vec3 *v = (vec3*)luaL_checkudata(L, 1, "Lua.vec3");
	
	/* Das Ergebnis auf dem Stack ablegen */
	lua_pushfstring(L, "(%f, %f, %f)", v->x, v->y, v->z);
	return 1; /* Wie viele Rueckgabewerte */
}

/* Eintr�ge f�r die Tabelle, die in Lua zugreifbar sein wird */
/* Array von Paaren aus Namen und Funktionszeiger */
static const struct luaL_Reg vec3_func [] = {
	{"new", vec3new}, /* Neuen Vektor erzeugen */
	{NULL, NULL}  /* Markierung f�r das Ende */
};

/* Eintr�ge f�r die Metatabelle */
/* Array von Paaren aus Namen und Funktionszeiger */
static const struct luaL_Reg vec3_meta [] = {
	{"__add", vec3add},           /* Metamethode zum addieren */
	{"__sub", vec3sub},           /* Metamethode zum addieren */
	{"__index", vec3index},           /* Metamethode zum addieren */
	{"__newindex", vec3newindex},           /* Metamethode zum addieren */
	{"__tostring", vec3tostring}, /* Metamethode um Stringrepr�sentation zu erzeugen */
	{NULL, NULL}              /* Markierung f�r das Ende */
};

/* Wird aufgerufen, wenn Bibliothek ge�ffnet wird */
int luaopen_vec3 (lua_State *L) 
{
	luaL_newmetatable(L, "Lua.vec3");
	luaL_setfuncs(L, vec3_meta, 0);

	//luaL_newlib (L, vec3_func);
	lua_pushcfunction(L, vec3new);
	lua_setglobal(L, "Vec3");

	return 1;
}


int luaopen_vec2 (lua_State *L) 
{
	luaL_newmetatable(L, "Lua.vec2");
	luaL_setfuncs(L, vec2_meta, 0);
	lua_pushcfunction(L, vec2new);
	lua_setglobal(L, "Vec2");
	return 1;
}


