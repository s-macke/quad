#include<stdlib.h>
#include<stdio.h>
#include "global.h"

/*
C     ------------------------------------------------------------------  
C     ZGE IS A STANDARD SUBROUTINE TO PERFORM GAUSSIAN ELIMINATION ON  
C     A N*N MATRIX 'A' PRIOR  TO INVERSION, DETAILS STORED IN 'INT'  
C     ------------------------------------------------------------------  
*/

void ZGE(c::complex<REAL> **A, int *INT, const int N)
{
	int I, II, IN, J, K;
	c::complex<REAL> YR, DUM;

	for(II=2; II<=N; II++)
	{
		I = II - 1;  
		YR = A[I-1][I-1];  
		IN = I;  
	
		for(J=II; J<=N; J++)
		{
			if (abs(YR) - abs(A[J-1][I-1]) < 0)
			{
				YR = A[J-1][I-1];
				IN = J;
			}
		}

		INT[I-1] = IN;  
		if ((IN - I) != 0)
		for(J=I; J<=N; J++)
		{
			DUM = A[I-1][J-1];
			A[I-1][J-1] = A[IN-1][J-1];
			A[IN-1][J-1] = DUM;
		}
  
	if ((abs(YR) - EMACH) <= 0) continue;
   
	for (J=II; J<=N; J++)
	{
		if ((abs(A[J-1][I-1]) - EMACH) <= 0) continue;  
		A[J-1][I-1] = A[J-1][I-1] / YR;  

		for(K=II; K<=N; K++)
			A[J-1][K-1] = A[J-1][K-1] - A[I-1][K-1] * A[J-1][I-1];
   }

}

//printf("invert1\n");

}

/*
C     ------------------------------------------------------------------  
C     ZSU  IS  A STANDARD BACK-SUBSTITUTION  SUBROUTINE  USING THE   
C     OUTPUT OF ZGE TO CALCULATE  A-INVERSE TIMES X, RETURNED IN X  
C     ------------------------------------------------------------------  
*/
void ZSU(c::complex<REAL> **A, int *INT, c::complex<REAL> *X, const int N)  
{	
//      COMPLEX*16 A(N,N),X(N)  
	int I, II, IN, J, IJ;  
	c::complex<REAL> DUM;  

	for(II=2; II<=N; II++)
	{
		I = II - 1;
		if ((INT[I-1] - I) != 0)
		{
			IN = INT[I-1];
			DUM = X[IN-1];  
			X[IN-1] = X[I-1];
			X[I-1] = DUM;
		}
		
		for( J=II; J<=N; J++)
			if ((abs(A[J-1][I-1]) - EMACH) > 0) X[J-1] = X[J-1] - A[J-1][I-1] * X[I-1];
	}
	
	for(II=1;II<=N;II++)
	{
		I = N - II + 1;  
		IJ = I + 1;  
	
		if((I-N) != 0)
			for(J=IJ; J<=N; J++)
				X[I-1] = X[I-1] - A[I-1][J-1] * X[J-1];
	
//		if (abs(A[I-1][I-1]) < (EMACH * 1e-8)) A[I-1][I-1] = EMACH * 1e-8 * c::complex<REAL>(1., 1.);  
		X[I-1] = X[I-1] / A[I-1][I-1];
	}
  
}

