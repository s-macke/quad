#ifndef CLAYER_H
#define CLAYER_H

#include "global.h"
#include "CAtom.h"
#include "CLight.h"
#include <memory>

class CLayer
{
	// at most 2 layers allowed for spheres at this time
	public:
	int IT; // type 1=homogeneous, 2=spheres
	int NLAYER;
	
	// HOMOGENEOUS
	REAL D; // thickness
	c::complex<REAL> EPSMED, EPSL, EPSR;
//	c::complex<REAL> EPS1, EPS2, EPS3;  // left, middle, right
	
	// ATOMS
	CVector<CAtom*> atoms;
	CVector<CCoor3D> position;
	
	// light propagation coordinates
	CCoor3D DT;
	CCoor3D DB;
	
	CLayer()
	{
		EPSMED = 1.;		
		NLAYER = 0;
		IT = 1;
	}
	
	~CLayer()
	{
		//if (atom != NULL) delete atom;
	}
	
	void AddAtom(CAtom *_atom, const CCoor3D &p)
	{
		atoms.push_back(_atom);
		position.push_back(p);
		//if (atom != NULL) delete atom;
		//atom = std::shared_ptr<CAtom>(_atom);
	}
};

//void BuildScatterMatrix(int LMAX, const CVector<CLayer> &layer, const CReciprocalLattice &rl, const CLight &light, CSlab &slabL, const REAL *ELM, const int NUNIT=0);


#endif