#ifndef MATRIX_H
#define MATRIX_H

void ZGE(c::complex<REAL> **A, int *INT, const int N);
void ZSU(c::complex<REAL> **A, int *INT, c::complex<REAL> *X, const int N);

#endif