#takeown /F lkkr /R
#icacls lkkr /grant everyone:F /t


CC = gcc
CXX = g++ 
DEPENDFILE = .depend 

#debug
#CFLAGS = -g -O0 -std=c++11 -DUSECPP11 -fopenmp -Isrc
#LFLAGS = -g -lpthread -fopenmp -lm src/lua/src/liblua.a -L.

#gcc static
#CFLAGS = -std=c++11 -fopenmp -DUSECPP11  -Ofast -ffast-math -Isrc
#LFLAGS = -static -static-libstdc++ -static-libgcc -lpthread -fopenmp -lm src/lua/src/liblua.a  -L.

#gcc
CXXFLAGS = -std=c++11 -fopenmp -DUSECPP11 -Ofast -ffast-math -Isrc
LFLAGS = -lpthread -fopenmp -lm src/lua/src/liblua.a -L.

#icc
#CC = icpc
#CFLAGS = -std=c++11 -fopenmp -O3 -Isrc
#LFLAGS = -L/opt/intel/lib/intel64/ -lmkl_intel_ilp64 -lmkl_sequential -lmkl_core -lpthread -fopenmp -lm lua-5.2.1/src/liblua.a -L.

CFILES = $(wildcard src/*.cpp) 
COBJS = $(CFILES:.cpp=.o) src/math/Faddeeva.o src/CFit.o

quad: $(COBJS) src/lm/liblevmar.a
	cd src/lua; make generic
	$(CXX) -o quad $(COBJS) $(LFLAGS) src/lm/liblevmar.a -lblas -llapack -include $(DEPENDFILE)

%.o: src/%.cpp
	$(CXX) $(CXXFLAGS) -c $< dep: $(CFILES)

%.o: src/math/%.cpp
	$(CXX) $(CXXFLAGS) -c $<

src/CFit.o: src/fit/CFit.cpp
	$(CXX) $(CXXFLAGS) -o src/CFit.o -c src/fit/CFit.cpp
#	$(CXX) $(CXXFLAGS) -MM $+ > $(DEPENDFILE)

src/lm/liblevmar.a:
	cd src/lm; make
	
convertcrystal: src/convertcrystal/convertcrystal.cpp
	$(CXX) -o convertcrystal $(CFLAGS) src/convertcrystal/convertcrystal.cpp

emscripten:
	rm -f $(COBJS)
	cd lua; make clean
	cd lua; make generic CC=emcc AR="emar rcu" RANLIB=emranlib
	$(MAKE) $(COBJS) CC=em++ CXX=em++ CFLAGS="-O2 -std=c++11 -DUSECPP11 -Isrc -Ilua/src"

	em++                               \
	-g1					\
	-o QUAD.js                         \
	-O2                                \
	$(COBJS)			\
	lua/src/liblua.a                   \
	-Ilua/src                          \
	-Isrc				   \
	-std=c++11 -DUSECPP11		   \
	-lm                                \
	-s EXPORTED_FUNCTIONS="['_ExecLUA']"

.PHONY: clean	
clean:
	cd src/lua; make clean
	cd src/lm; make clean
	rm -f src/lm/liblevmar.a
	rm -f $(COBJS)
	rm -f quad

test:
	echo -e "\n" > space
	mkdir -p testsuite
	cd testsuite; ../quad ../Tutorial/tutorial12.lua
	cd testsuite; ../quad ../Tutorial/tutorial1.lua < ../space
	cd testsuite; ../quad ../Tutorial/tutorial2.lua < ../space
	cd testsuite; ../quad ../Tutorial/tutorial3.lua < ../space
	cd testsuite; ../quad ../Tutorial/tutorial4.lua < ../space
	cd testsuite; ../quad ../Tutorial/tutorial5.lua < ../space
	cd testsuite; ../quad ../Tutorial/tutorial6.lua < ../space
	cd testsuite; ../quad ../Tutorial/tutorial7.lua < ../space
	cd testsuite; ../quad ../Tutorial/tutorial11.lua < ../space
	cd testsuite; ../quad ../Tutorial/tutorial10.lua < ../space
