-------- TUTORIAL 2 ----------
-- Reflectivity and Transmission of thin film

-- file in which the data is saved
filename = "refl.txt";

quad.light.SetThetaPhi(0., 0.);
quad.light.SetPolarization("S");
quad.light.SetEnergy(1000);

-- generate simple atomtype with scattering factor f=50+10*i.
atom = quad.atom.NewScatteringFactorAtom(50., 10.);

-- latticeconstant
lc = 4.;

-- set 2D reciprocal lattice defined by the first two vectors
-- third parameter gives the lattice size in z-direction. This parameter is normally not necessary, but for diffraction a useful parameter
-- the Miller indices taken into account are H,K=0,0 (specular )
quad.rl.Set(Vec2(lc, 0.), Vec2(0., lc), lc);

-- we define a cubic primitive lattice
-- define one slab with thickness of lc
quad.slab.AddSlabs(1, lc);

-- this slab has one atom of type "La" and the position (0,0,0) in real space and Angstrom. The z-coordinate is defined as the center of the slab. 
quad.slab.AddAtom(0, atom, Vec3(0, 0, 0));

-- this defines the film of 10 layers of slab number 0
-- the thickness (from top atom layer to bottom atom layer) of the film of 9*4 = 36A
quad.slab.SetStructure("10*(0)");

-- Print some information before continue
quad.PrintStatus();

-- wait
print("Press any key to continue");
io.read();

------ Reflectivity calculation ------

-- open file
local file = io.open(filename, "w");
file:write("#theta Reflection Transmission\n");

-- loop theta from 0 to 89 degrees in 1 degree steps
for theta = 90.01, 180, 1 do 
	print("Calculate Theta = ", theta);
	io.flush(); -- sometimes we have to flush the output buffer to see the printed text	
	quad.light.SetThetaPhi(theta, 0);
	quad.Scatter();
	-- output specular reflection and transmission
	file:write(theta, " ", quad.GetReflectedIntensity(0), " ", quad.GetTransmittedIntensity(0), "\n");
end

file:close();
print("Angle sweep saved in file: ", filename);

-- change phi and observe that the reflectivity is not influenced by it




