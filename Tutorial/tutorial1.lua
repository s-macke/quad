-------- TUTORIAL 1 ----------
-- Vacuum scattering

-- set light direction (0 0 -1) in spherical coordinates
quad.light.SetThetaPhi(180., 0.);

-- set energy in eV of incoming ray
quad.light.SetEnergy = 1000;
-- set polarization. Valid values are 
-- "S" = sigma
-- "P" = pi
-- "L" = left circular 
-- "R" = right circular
quad.light.SetPolarization("S");

print("Incoming Wave vector: ", quad.light.GetKVector());

quad.Scatter();
-- Get the scattered intensity in transmitted and reflected geometry to the incoming beam
print("Reflected intensity: ", quad.GetReflectedIntensity(0), " in direction ", quad.GetReflectedKVector(0));
print("Transmitted intensity: ", quad.GetTransmittedIntensity(0), " in direction ", quad.GetTransmittedKVector(0));

-- try to change theta and phi and observe how the wave vectors directions are changing, espcially the reflected one 

