-------- Energy Conservation ----------


-- file in which the data is saved
filename = "refl.txt";

quad.light.SetPolarization("S");
quad.light.SetEnergy(1000);
quad.light.SetThetaPhi(180., 0);

-- latticeconstant
lc = 4.;

-- set 2D reciprocal lattice defined by the first two vectors
-- third parameter gives the lattice size in z-direction. This parameter is normally not necessary, but for diffraction a useful parameter
-- the Miller indices taken into account are H,K=0,0 (specular )
quad.rl.Set(Vec2(lc, 0.), Vec2(0., lc), lc);
quad.rl.AddReciprocalLatticeVector(1, 0);
quad.rl.AddReciprocalLatticeVector(-1, 0);
quad.rl.AddReciprocalLatticeVector(0, 1);
quad.rl.AddReciprocalLatticeVector(0, -1);

-- generate simple atomtype with scattering factor f=50+0i. No absorption
atom = quad.atom.NewScatteringFactorAtom(50., 0.);

-- we define a cubic primitive lattice
-- define one slab with thickness of lc
quad.slab.AddSlabs(1, lc);

-- this slab has one atom of type "La" and the position (0,0,0) in real space and Angstrom. The z-coordinate is defined as the center of the slab. 
quad.slab.AddAtom(0, atom, Vec3(0, 0, 0));
--quad.SetMultipleScattering(MS.INTERATOMIC)
-- this defines the film of 10 layers of slab number 0
-- the thickness (from top atom layer to bottom atom layer) of the film of 9*4 = 36A
quad.slab.SetStructure("1000*(0)");

-- Print some information before continue
quad.PrintStatus();

-- wait
print("Press any key to continue");
io.read();

-- open file
local file = io.open(filename, "w");
--file:write("#theta Reflection Transmission\n");

-- loop theta from 0 to 89 degrees in 1 degree steps

for energy = 1500., 1600., 0.2 do
	quad.light.SetEnergy(energy);
	quad.Scatter();
	print("energy = ", energy, " transmitted = ", quad.GetTransmittedIntensity(0), " absorbance = ", quad.GetAbsorbance());
	io.flush(); -- sometimes we have to flush the output buffer to see the printed text	
	--file:write(theta, " ", quad.GetReflectedIntensity(0), " ", quad.GetTransmittedIntensity(0), "\n");
end

file:close();
print("Energy scan saved in file: ", filename);

-- change phi and observe that the reflectivity is not influenced by it




