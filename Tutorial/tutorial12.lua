-------- TUTORIAL 12 ----------
-- Fitting routine. Find minimum of f(x,y)=x^2 + y^2

function Error(params)
	local x = params[0];
	local y = params[1];
	local f = x*x + y*y;
	
	local s = string.format("x: %f, y: %f, f: %e", x, y, f);
	print(s);
	return f;
end

-- initial value, stepsize, lower boundary, upper boundary
quad.fit.AddVariable(-0.4, 0.1, -10, 10);
quad.fit.AddVariable(1.2, 0.1, -10, 10);

quad.fit.SetErrorFunction(Error);

quad.fit.Iterate(30);




