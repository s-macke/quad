-------- TUTORIAL 11 ----------
-- Tutorial 3 but with unordered roughness at interfaces

ThetaSweep = function(filename)

	local file = io.open(filename, "w")
	file:write("#qz Reflection Transmission\n");
	for theta = 90.01, 130., 0.1 do 
		io.flush();
		light.theta = theta;
		BuildScatterMatrix();
		qz = light.k.z - GetReflK(0).z;
		file:write(-qz, " ", GetRefl(0), " ", GetTrans(0), "\n");
	end
	file:close()
	print("Angle sweep saved in file: ", filename);
end

light.phi = 0.;
light.polarization = "S";
light.energy = 3000;

LaAtom = atom.NewScatteringFactorAtom(57., 10.);
AlAtom = atom.NewScatteringFactorAtom(13., 5.);

-- define cubic primitive lattice for film and substrate
-- lattice constants 
lc1 = 4.;
lc2 = 3.7; -- this one is for the subrate, but only in z-direction
-- reciprocal lattice
rl.Set(Vec2(lc1, 0.), Vec2(0., lc1), lc1);

slabs.AddSlabs(1, lc1);
slabs.AddSlabs(1, lc2);
slabs.AddAtom(0, LaAtom, Vec3(0, 0, 0));
slabs.AddAtom(1, AlAtom, Vec3(0, 0, 0));


-- First calculate the crystal without roughness
print("Calculating no roughness (same as Tutorial 3):");
print("==============================================");
slabs.SetStructure("10*(0), 100000*(1)");
PrintStats();
ThetaSweep("roughness1.txt");

-- Add surface roughness with following data:
-- The name of this interface is "A"
-- The crystal one is the vaccum defined as ""
-- The crystal two is the top film defined as "0" (You can put all complex crystal configurations you want)
-- roughness: sigma = 2A
-- total thickness of unordered media: 4A (should always be larger then the roughness 1.5-3*sigma)
-- slice the interface into 6 elements (around 3*thickness/sigma)
-- Note that the total thickness of the film is increased by half of the total thickness of the interface (by 2A)
print("\nCalculating 2A roughness at surface:");
print("======================================");
sigmasurface = 2;
slabs.CreateInterface("A", "", "0", sigmasurface*2., 6, sigmasurface);
slabs.SetStructure("A, 10*(0), 100000*(1)");
PrintStats();
ThetaSweep("roughness2.txt");

-- Add surface interface with following data:
--   The name of this interface is "B"
--   The crystal one is the vaccum defined as "0"
--   The crystal two is the top film defined as "1"
--   Roughness: sigma = 2A
--   Total thickness of unordered media: 4A
--   Slice the interface into 6 elements
-- Note that the total thickness of the film is increased now by 4 A. Therefore remove one atomic layer
print("\nCalculating 2A roughness at interface:");
print("========================================");
sigmainterface = 2;
slabs.CreateInterface("B", "0", "1", sigmainterface*2., 6, sigmainterface);
slabs.SetStructure("A, 9*(0), B, 100000*(1)");
PrintStats();
ThetaSweep("roughness3.txt");

-- change roughness and redefine the interfaces
-- remove two atomic layers
print("\nCalculating 4A roughness at interface and surface:");
print("========================================");
sigmasurface = 4;
slabs.CreateInterface("A", "", "0", sigmasurface*2., 6, sigmasurface);
sigmainterface = 4;
slabs.CreateInterface("B", "0", "1", sigmainterface*2., 6, sigmainterface);
slabs.SetStructure("A, 8*(0), B, 100000*(1)");
PrintStats();
ThetaSweep("roughness4.txt");


print("\nPlot commands for gnuplot:");
print("set logscale y");
print("plot \"roughness1.txt\" w l, \"roughness2.txt\" w l, \"roughness3.txt\" w l, \"roughness4.txt\" w l");


