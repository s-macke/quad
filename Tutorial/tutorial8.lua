-------- TUTORIAL 8 ----------
-- Powder diffraction

-- file in which the data is saved
filename = "powder.txt";

light.theta = 180.;
light.phi = 0.;
light.polarization = "S";
light.energy = 8042; -- Cu K-alpha

-- Set accuracy to the lowest possible to speed up simulation. This is (very close to ?) kinematical scattering
SetMatrixAccuracy(M.DIAGONAL); -- ignore off-diagonal terms reducing the computational cost of one polynomial order
SetMultipleScattering(MS.NONE); -- no multiple scattering taken into account

FeAtom = atom.NewScatteringFactorAtom(26., 10.);

-- define body-centred cubic lattice
lc = 2.87;
rl.Set(Vec2(lc, 0.), Vec2(0., lc), lc, 7.);
slabs.AddSlabs(2, lc/2.);
--slabs.AddSlabs(1, lc);
slabs.AddAtom(0, FeAtom, Vec3(0, 0, 0));
slabs.AddAtom(1, FeAtom, Vec3(2, 2, 0));
slabs.SetStructure("200000*(0,1)");
--slabs.SetStructure("200000*(0)");
PrintStats();

print("Press any key to continue");
io.read();

------ Powder diffraction calculation ------

-- in order to calculate the spectrum we have to use random wave vectors distributed over a sphere. 
-- look at http://mathworld.wolfram.com/SpherePointPicking.html

math.randomseed(os.time());
math.random(); -- the first numbers are not really random
math.random();
math.random();

for i = 0, 50000 do
	u = math.random();
	v = math.random();
	light.phi = 360.*u;
	light.theta = math.acos(2.*v - 1)*180./math.pi;
	if (light.k.z < 0) then
		queue.Add();
	end
end

print("Number of jobs in queue:", queue.GetN());
print("Execute queue");
queue.Run();

-- define array where we add the intensities for different scattering angles theta
-- we cannot use q-channels here because then we have to correct the intensities for experimental constrains
thetachannel = {};
-- let us define it from theta=0 till theta=180 in 0.2 steps
for i=0, 180*50+1 do
      thetachannel[i] = 0;
end

N = rl.GetN();
for task = 0, queue.GetN()-1 do 
	queue.Activate(task);
	kin = light.k;
	for j = 0, N-1 do
-- from here on the result depends on the exact geometry you are using
		kout = GetTransK(j);
		intensity = GetTrans(j);
		-- check if it is really a propagating wave and not an evanescent one
		if (math.abs(kout.z) >= 1e-3) then 			
			-- scalar product to calculate scattering angle
			theta = (kin.x*kout.x+kin.y*kout.y+kin.z*kout.z)/(light.k0*light.k0);
			theta = theta * 0.9999999; -- tiny numerical correction to catch values which are above the acos limit
			theta = math.acos(theta) * 180./math.pi;
			thetachannel[math.floor(theta*10)] = thetachannel[math.floor(theta*10)] + intensity;
		end
		kout = GetReflK(j);
		intensity = GetRefl(j);
		if (math.abs(kout.z) >= 1e-3) then 
			theta = (kin.x*kout.x+kin.y*kout.y+kin.z*kout.z)/(light.k0*light.k0);
			theta = theta * 0.9999999; 
			theta = math.acos(theta) * 180./math.pi;
			thetachannel[math.floor(theta*5)] = thetachannel[math.floor(theta*5)] + intensity;
		end
	end
end

-- save the results
local file = io.open(filename, "w");
file:write("#2theta Intensity\n");
for i=0, 180*5 do
	file:write(i/5, " ", thetachannel[i], "\n");
end
file:close();
print("Powder diffraction saved in file: ", filename);





