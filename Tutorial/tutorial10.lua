-------- TUTORIAL 10 ----------
-- Simulation of a Laue picture

-- resolution of the photographic plate
width = 1024;
height = 1024;

-- Create a 1024 x 1024 grid
gridt = {}
for i = 1, width do
    gridt[i] = {}
    for j = 1, height do
        gridt[i][j] = 0. -- Fill the values here
    end
end

-- the same for reflection
gridr = {}
for i = 1, width do
    gridr[i] = {}
    for j = 1, height do
        gridr[i][j] = 0. -- Fill the values here
    end
end

-- Add one gaussian beam profile on the photoplate
function AddGaussian(x, y, intensityt, intensityr)
	imin = math.floor(x - 15);
	jmin = math.floor(y - 15);
	imax = math.floor(x + 15);
	jmax = math.floor(y + 15);
	
	for j = jmin, jmax do
	for i = imin, imax do
		if i>=1 and i <= width and j >= 1 and j <= height then
			r = math.sqrt((i-x)*(i-x) + (j-y)*(j-y));
			gridt[i][j] = gridt[i][j] + math.exp(-r*r)*intensityt;
			gridr[i][j] = gridr[i][j] + math.exp(-r*r)*intensityr;
		end
	end
	end	
end

-- file in which the data is saved
filename = "Laue.txt";
light.theta = 180.; -- perpendicular incoming light
light.phi = 0.;
light.polarization = "L"; -- unpolarized light
light.energy = 1000;

-- Set accuracy to the lowest possible to speed up simulation. This is (very close to ?) kinematical scattering
SetMatrixAccuracy(M.DIAGONAL); -- ignore off-diagonal terms reducing the computational cost of one polynomial order
SetMultipleScattering(MS.NONE); -- no multiple scattering taken into account

-- define lattice
LaAtom = atom.NewScatteringFactorAtom(57., 0.);

lc = 4.;
--rl.Set(Vec2(lc, 0.), Vec2(0., lc), lc, 20.);
rl.Set(Vec2(lc, 0.), Vec2(0., lc), lc);
rl.SetRadius(5.);

slabs.AddSlabs(1, lc);
slabs.AddAtom(0, LaAtom, Vec3(0, 0, 0));

-- this defines the film of 10 layers of slab number 0
-- the thickness (from top atom layer to bottom atom layer) of the film of 9*4 = 36A
slabs.SetStructure("500*(0)");

-- Print some information before continue
PrintStats();

-- wait
print("Press any key to continue");
io.read();

N = rl.GetN();

-- loop energy from 2000 eV to 18000 eV (white light)
--for energy = 2000, 18000, 1 do
for energy = 2000, 10000, 1 do
	print("Add energy = ", energy, "eV to queue");
	io.flush(); -- sometimes we have to flush the output buffer to see the printed text	
	light.energy = energy;
	queue.Add();
end

print("Run queue");
-- Note that due to overhead this example is maybe slower than tutorial 2
queue.Run();

print("Build Image");
	-- Add Intensity to map
ntasks = queue.GetN();
for task = 0, ntasks-1 do
	queue.Activate(task);
	for index = 0, N-1 do
		-- correct perspective
		k = GetTransK(index);
		if math.abs(k.z) >= 1e-10 then
		
		qx = light.k.x - k.x;
		qy = light.k.y - k.y;
		qz = light.k.z - k.z;
		qx = qx * lc/(2.*3.141592);
		qy = qy * lc/(2.*3.141592);
		qz = qz * lc/(2.*3.141592);
		qxi = math.floor(qx + 0.5);
		qyi = math.floor(qy + 0.5);
		qzi = math.floor(qz + 0.5);
		r = (qx-qxi)*(qx-qxi) + (qy-qyi)*(qy-qyi) + (qz-qzi)*(qz-qzi);
		r = math.sqrt(r);
		intens = 0.;
		if (r < 0.02) then 
			intens = 1e-5; 
		end
		
		coorx = k.x / k.z*100. + width/2;
		coory = k.y / k.z*100. + height/2;
		
		AddGaussian(coorx, coory, GetTrans(index), GetRefl(index));
		--AddGaussian(coorx, coory, GetTrans(index), intens);
		end
	end
end

--save map in file
print("Saving image data to file:", filename)
local file = io.open(filename, "w");
file:write("#x y ForwardScattering BackwardScattering\n");
	for j = 1, height do
	for i = 1, width do
		s = string.format('%i %i %e %e', i-width/2, j-height/2, gridt[i][j], gridr[i][j]);
		file:write(s,"\n");
	end
		file:write("\n");
	end
file:close();
-------------------------
print("Laue Map saved in file: ", filename);
print("\nPlot in gnuplot by using the commands");
print("  set pm3d map");
print("  splot [:] [:] [:1e-5] \"Laue.txt\" u 1:2:4");





