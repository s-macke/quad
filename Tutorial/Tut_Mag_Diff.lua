-------- Perovskite superlattice  ----------

function BuildLNOLAOBE125()

	quad.rl.Set(Vec2(10.94, 0.), Vec2(0., 10.94), 1.89*2*4);

	quad.slab.SetStructure("46*(7,6,5,4,3,2,1,0)");

	LaAtom = quad.atom.NewScatteringFactorAtom(145, 62);
	OAtom = quad.atom.NewScatteringFactorAtom(8, 2);
	AlAtom = quad.atom.NewScatteringFactorAtom(12, 1);

	Ni1Atom = quad.atom.NewMagneticAtom(0, 50.,    -0.,   -10.,     28.,  -0.);
	Ni2Atom = quad.atom.NewMagneticAtom(0, 50.,    -0.,   -10.,    -28.,  0.);
	Ni3Atom = quad.atom.NewMagneticAtom(0, 50.,     0.,    10.,     28.,  -0.);
	Ni4Atom = quad.atom.NewMagneticAtom(0, 50.,     0.,    10.,    -28.,  0.);

	quad.slab.AddSlabs(4, 1.890000);
	quad.slab.AddSlabs(4, 1.890000);

	-- z = 0.000000
	quad.slab.AddAtom(0, Ni1Atom, Vec3(0.000000, 0.000000, 0.000000));
	quad.slab.AddAtom(0, Ni1Atom, Vec3(0.000000, 5.470000, 0.000000));

	quad.slab.AddAtom(0, Ni2Atom, Vec3(2.735000, 2.735000, 0.000000));
	quad.slab.AddAtom(0, Ni2Atom, Vec3(2.735000, 8.205000, 0.000000));

	quad.slab.AddAtom(0, Ni3Atom, Vec3(5.470000, 0.000000, 0.000000));
	quad.slab.AddAtom(0, Ni3Atom, Vec3(5.470000, 5.470000, 0.000000));

	quad.slab.AddAtom(0, Ni4Atom, Vec3(8.205000, 2.735000, 0.000000));
	quad.slab.AddAtom(0, Ni4Atom, Vec3(8.205000, 8.205000, 0.000000));

	quad.slab.AddAtom(0, OAtom, Vec3(1.388833, 4.170328, 0.165564));
	quad.slab.AddAtom(0, OAtom, Vec3(1.346167, 1.435328, 0.165564));
	quad.slab.AddAtom(0, OAtom, Vec3(1.346167, 6.905328, 0.165564));
	quad.slab.AddAtom(0, OAtom, Vec3(6.816167, 1.435328, 0.165564));
	quad.slab.AddAtom(0, OAtom, Vec3(6.816167, 6.905328, 0.165564));
	quad.slab.AddAtom(0, OAtom, Vec3(1.388833, 9.640328, 0.165564));
	quad.slab.AddAtom(0, OAtom, Vec3(6.858833, 4.170328, 0.165564));
	quad.slab.AddAtom(0, OAtom, Vec3(6.858833, 9.640328, 0.165564));
	quad.slab.AddAtom(0, OAtom, Vec3(4.123833, 4.034672, -0.165565));
	quad.slab.AddAtom(0, OAtom, Vec3(4.081167, 1.299672, -0.165565));
	quad.slab.AddAtom(0, OAtom, Vec3(4.081167, 6.769672, -0.165565));
	quad.slab.AddAtom(0, OAtom, Vec3(9.551167, 1.299672, -0.165565));
	quad.slab.AddAtom(0, OAtom, Vec3(9.551167, 6.769672, -0.165565));
	quad.slab.AddAtom(0, OAtom, Vec3(4.123833, 9.504672, -0.165565));
	quad.slab.AddAtom(0, OAtom, Vec3(9.593833, 4.034672, -0.165565));
	quad.slab.AddAtom(0, OAtom, Vec3(9.593833, 9.504672, -0.165565));

	-- z = 1.890000
	quad.slab.AddAtom(1, LaAtom, Vec3(-0.000219, 8.205000, 0.000000));
	quad.slab.AddAtom(1, LaAtom, Vec3(5.469781, 8.205000, 0.000000));
	quad.slab.AddAtom(1, LaAtom, Vec3(2.735219, 0.000000, 0.000000));
	quad.slab.AddAtom(1, OAtom, Vec3(0.000547, 0.000000, 0.000000));
	quad.slab.AddAtom(1, OAtom, Vec3(0.000547, 5.470000, 0.000000));
	quad.slab.AddAtom(1, OAtom, Vec3(5.470547, 0.000000, 0.000000));
	quad.slab.AddAtom(1, OAtom, Vec3(5.470547, 5.470000, 0.000000));
	quad.slab.AddAtom(1, OAtom, Vec3(2.734453, 8.205000, 0.000000));
	quad.slab.AddAtom(1, OAtom, Vec3(8.204453, 2.735000, 0.000000));
	quad.slab.AddAtom(1, OAtom, Vec3(8.204453, 8.205000, 0.000000));
	quad.slab.AddAtom(1, OAtom, Vec3(2.734453, 2.735000, 0.000000));
	quad.slab.AddAtom(1, LaAtom, Vec3(5.469781, 2.735000, 0.000000));
	quad.slab.AddAtom(1, LaAtom, Vec3(2.735219, 5.470000, 0.000000));
	quad.slab.AddAtom(1, LaAtom, Vec3(8.205219, 0.000000, 0.000000));
	quad.slab.AddAtom(1, LaAtom, Vec3(8.205219, 5.470000, 0.000000));
	quad.slab.AddAtom(1, LaAtom, Vec3(-0.000219, 2.735000, 0.000000));

	-- z = 3.780000
	quad.slab.AddAtom(2, OAtom, Vec3(6.816167, 4.034672, -0.165564));
	quad.slab.AddAtom(2, OAtom, Vec3(6.816167, 9.504672, -0.165564));
	quad.slab.AddAtom(2, OAtom, Vec3(1.388833, 6.769672, -0.165564));
	quad.slab.AddAtom(2, OAtom, Vec3(6.858833, 1.299672, -0.165564));
	quad.slab.AddAtom(2, OAtom, Vec3(6.858833, 6.769672, -0.165564));
	quad.slab.AddAtom(2, OAtom, Vec3(1.388833, 1.299672, -0.165564));
	quad.slab.AddAtom(2, OAtom, Vec3(1.346167, 4.034672, -0.165564));
	quad.slab.AddAtom(2, OAtom, Vec3(1.346167, 9.504672, -0.165564));
	
	quad.slab.AddAtom(2, Ni4Atom, Vec3(0.000000, 0.000000, 0.000000));
	quad.slab.AddAtom(2, Ni4Atom, Vec3(0.000000, 5.470000, 0.000000));

	quad.slab.AddAtom(2, Ni1Atom, Vec3(2.735000, 2.735000, 0.000000));
	quad.slab.AddAtom(2, Ni1Atom, Vec3(2.735000, 8.205000, 0.000000));

	quad.slab.AddAtom(2, Ni2Atom, Vec3(5.470000, 0.000000, 0.000000));
	quad.slab.AddAtom(2, Ni2Atom, Vec3(5.470000, 5.470000, 0.000000));

	quad.slab.AddAtom(2, Ni3Atom, Vec3(8.205000, 2.735000, 0.000000));
	quad.slab.AddAtom(2, Ni3Atom, Vec3(8.205000, 8.205000, 0.000000));

	quad.slab.AddAtom(2, OAtom, Vec3(9.551167, 4.170328, 0.165564));
	quad.slab.AddAtom(2, OAtom, Vec3(9.551167, 9.640328, 0.165564));
	quad.slab.AddAtom(2, OAtom, Vec3(4.123833, 1.435328, 0.165564));
	quad.slab.AddAtom(2, OAtom, Vec3(4.081167, 4.170328, 0.165564));
	quad.slab.AddAtom(2, OAtom, Vec3(4.123833, 6.905328, 0.165564));
	quad.slab.AddAtom(2, OAtom, Vec3(9.593833, 1.435328, 0.165564));
	quad.slab.AddAtom(2, OAtom, Vec3(9.593833, 6.905328, 0.165564));
	quad.slab.AddAtom(2, OAtom, Vec3(4.081167, 9.640328, 0.165564));

	-- z = 5.670000
	quad.slab.AddAtom(3, LaAtom, Vec3(2.734781, 0.000000, 0.000000));
	quad.slab.AddAtom(3, LaAtom, Vec3(8.204781, 0.000000, 0.000000));
	quad.slab.AddAtom(3, LaAtom, Vec3(8.204781, 5.470000, 0.000000));
	quad.slab.AddAtom(3, LaAtom, Vec3(0.000219, 8.205000, 0.000000));
	quad.slab.AddAtom(3, LaAtom, Vec3(5.470219, 2.735000, 0.000000));
	quad.slab.AddAtom(3, LaAtom, Vec3(5.470219, 8.205000, 0.000000));
	quad.slab.AddAtom(3, OAtom, Vec3(-0.000547, 0.000000, 0.000000));
	quad.slab.AddAtom(3, OAtom, Vec3(-0.000547, 5.470000, 0.000000));
	quad.slab.AddAtom(3, OAtom, Vec3(2.735547, 8.205000, 0.000000));
	quad.slab.AddAtom(3, OAtom, Vec3(8.205547, 2.735000, 0.000000));
	quad.slab.AddAtom(3, OAtom, Vec3(8.205547, 8.205000, 0.000000));
	quad.slab.AddAtom(3, OAtom, Vec3(5.469453, 0.000000, 0.000000));
	quad.slab.AddAtom(3, OAtom, Vec3(5.469453, 5.470000, 0.000000));
	quad.slab.AddAtom(3, OAtom, Vec3(2.735547, 2.735000, 0.000000));
	quad.slab.AddAtom(3, LaAtom, Vec3(2.734781, 5.470000, 0.000000));
	quad.slab.AddAtom(3, LaAtom, Vec3(0.000219, 2.735000, 0.000000));

	-- z = 7.560000
	quad.slab.AddAtom(4, OAtom, Vec3(4.123833, 4.034672, -0.165564));
	quad.slab.AddAtom(4, OAtom, Vec3(4.081167, 1.299672, -0.165564));
	quad.slab.AddAtom(4, OAtom, Vec3(4.123833, 9.504672, -0.165564));
	quad.slab.AddAtom(4, OAtom, Vec3(9.593833, 4.034672, -0.165564));
	quad.slab.AddAtom(4, OAtom, Vec3(9.593833, 9.504672, -0.165564));
	quad.slab.AddAtom(4, OAtom, Vec3(4.081167, 6.769672, -0.165564));
	quad.slab.AddAtom(4, OAtom, Vec3(9.551167, 1.299672, -0.165564));
	quad.slab.AddAtom(4, OAtom, Vec3(9.551167, 6.769672, -0.165564));
	quad.slab.AddAtom(4, AlAtom, Vec3(5.470000, 0.000000, 0.000000));
	quad.slab.AddAtom(4, AlAtom, Vec3(5.470000, 5.470000, 0.000000));
	quad.slab.AddAtom(4, AlAtom, Vec3(2.735000, 2.735000, 0.000000));
	quad.slab.AddAtom(4, AlAtom, Vec3(2.735000, 8.205000, 0.000000));
	quad.slab.AddAtom(4, AlAtom, Vec3(8.205000, 2.735000, 0.000000));
	quad.slab.AddAtom(4, AlAtom, Vec3(8.205000, 8.205000, 0.000000));
	quad.slab.AddAtom(4, AlAtom, Vec3(0.000000, 0.000000, 0.000000));
	quad.slab.AddAtom(4, AlAtom, Vec3(0.000000, 5.470000, 0.000000));
	quad.slab.AddAtom(4, OAtom, Vec3(1.346167, 1.435328, 0.165564));
	quad.slab.AddAtom(4, OAtom, Vec3(1.346167, 6.905328, 0.165564));
	quad.slab.AddAtom(4, OAtom, Vec3(6.816167, 1.435328, 0.165564));
	quad.slab.AddAtom(4, OAtom, Vec3(6.816167, 6.905328, 0.165564));
	quad.slab.AddAtom(4, OAtom, Vec3(1.388833, 4.170328, 0.165564));
	quad.slab.AddAtom(4, OAtom, Vec3(1.388833, 9.640328, 0.165564));
	quad.slab.AddAtom(4, OAtom, Vec3(6.858833, 4.170328, 0.165564));
	quad.slab.AddAtom(4, OAtom, Vec3(6.858833, 9.640328, 0.165564));

	-- z = 9.450000
	quad.slab.AddAtom(5, OAtom, Vec3(8.204453, 2.735000, 0.000000));
	quad.slab.AddAtom(5, OAtom, Vec3(8.204453, 8.205000, 0.000000));
	quad.slab.AddAtom(5, LaAtom, Vec3(-0.000219, 2.735000, 0.000000));
	quad.slab.AddAtom(5, LaAtom, Vec3(-0.000219, 8.205000, 0.000000));
	quad.slab.AddAtom(5, LaAtom, Vec3(5.469781, 8.205000, 0.000000));
	quad.slab.AddAtom(5, LaAtom, Vec3(5.469781, 2.735000, 0.000000));
	quad.slab.AddAtom(5, OAtom, Vec3(0.000547, 0.000000, 0.000000));
	quad.slab.AddAtom(5, OAtom, Vec3(0.000547, 5.470000, 0.000000));
	quad.slab.AddAtom(5, OAtom, Vec3(2.734453, 2.735000, 0.000000));
	quad.slab.AddAtom(5, LaAtom, Vec3(2.735219, 0.000000, 0.000000));
	quad.slab.AddAtom(5, LaAtom, Vec3(2.735219, 5.470000, 0.000000));
	quad.slab.AddAtom(5, LaAtom, Vec3(8.205219, 0.000000, 0.000000));
	quad.slab.AddAtom(5, LaAtom, Vec3(8.205219, 5.470000, 0.000000));
	quad.slab.AddAtom(5, OAtom, Vec3(5.470547, 0.000000, 0.000000));
	quad.slab.AddAtom(5, OAtom, Vec3(5.470547, 5.470000, 0.000000));
	quad.slab.AddAtom(5, OAtom, Vec3(2.734453, 8.205000, 0.000000));

	-- z = 11.340000
	quad.slab.AddAtom(6, OAtom, Vec3(6.858833, 1.299672, -0.165564));
	quad.slab.AddAtom(6, OAtom, Vec3(6.858833, 6.769672, -0.165564));
	quad.slab.AddAtom(6, OAtom, Vec3(1.388833, 1.299672, -0.165564));
	quad.slab.AddAtom(6, OAtom, Vec3(1.346167, 4.034672, -0.165564));
	quad.slab.AddAtom(6, OAtom, Vec3(1.346167, 9.504672, -0.165564));
	quad.slab.AddAtom(6, OAtom, Vec3(6.816167, 4.034672, -0.165564));
	quad.slab.AddAtom(6, OAtom, Vec3(6.816167, 9.504672, -0.165564));
	quad.slab.AddAtom(6, OAtom, Vec3(1.388833, 6.769672, -0.165564));
	quad.slab.AddAtom(6, AlAtom, Vec3(8.205000, 2.735000, 0.000000));
	quad.slab.AddAtom(6, AlAtom, Vec3(8.205000, 8.205000, 0.000000));
	quad.slab.AddAtom(6, AlAtom, Vec3(5.470000, 5.470000, 0.000000));
	quad.slab.AddAtom(6, AlAtom, Vec3(2.735000, 2.735000, 0.000000));
	quad.slab.AddAtom(6, AlAtom, Vec3(0.000000, 0.000000, 0.000000));
	quad.slab.AddAtom(6, AlAtom, Vec3(0.000000, 5.470000, 0.000000));
	quad.slab.AddAtom(6, AlAtom, Vec3(5.470000, 0.000000, 0.000000));
	quad.slab.AddAtom(6, AlAtom, Vec3(2.735000, 8.205000, 0.000000));
	quad.slab.AddAtom(6, OAtom, Vec3(4.081167, 4.170328, 0.165565));
	quad.slab.AddAtom(6, OAtom, Vec3(4.123833, 6.905328, 0.165565));
	quad.slab.AddAtom(6, OAtom, Vec3(9.593833, 1.435328, 0.165565));
	quad.slab.AddAtom(6, OAtom, Vec3(9.593833, 6.905328, 0.165565));
	quad.slab.AddAtom(6, OAtom, Vec3(4.081167, 9.640328, 0.165565));
	quad.slab.AddAtom(6, OAtom, Vec3(9.551167, 4.170328, 0.165565));
	quad.slab.AddAtom(6, OAtom, Vec3(9.551167, 9.640328, 0.165565));
	quad.slab.AddAtom(6, OAtom, Vec3(4.123833, 1.435328, 0.165565));

	-- z = 13.230000
	quad.slab.AddAtom(7, OAtom, Vec3(8.205547, 8.205000, 0.000000));
	quad.slab.AddAtom(7, LaAtom, Vec3(2.734781, 5.470000, 0.000000));
	quad.slab.AddAtom(7, LaAtom, Vec3(8.204781, 0.000000, 0.000000));
	quad.slab.AddAtom(7, LaAtom, Vec3(8.204781, 5.470000, 0.000000));
	quad.slab.AddAtom(7, OAtom, Vec3(-0.000547, 0.000000, 0.000000));
	quad.slab.AddAtom(7, OAtom, Vec3(-0.000547, 5.470000, 0.000000));
	quad.slab.AddAtom(7, LaAtom, Vec3(0.000219, 8.205000, 0.000000));
	quad.slab.AddAtom(7, LaAtom, Vec3(5.470219, 2.735000, 0.000000));
	quad.slab.AddAtom(7, LaAtom, Vec3(5.470219, 8.205000, 0.000000));
	quad.slab.AddAtom(7, OAtom, Vec3(5.469453, 0.000000, 0.000000));
	quad.slab.AddAtom(7, OAtom, Vec3(5.469453, 5.470000, 0.000000));
	quad.slab.AddAtom(7, LaAtom, Vec3(2.734781, 0.000000, 0.000000));
	quad.slab.AddAtom(7, OAtom, Vec3(2.735547, 2.735000, 0.000000));
	quad.slab.AddAtom(7, LaAtom, Vec3(0.000219, 2.735000, 0.000000));
	quad.slab.AddAtom(7, OAtom, Vec3(2.735547, 8.205000, 0.000000));
	quad.slab.AddAtom(7, OAtom, Vec3(8.205547, 2.735000, 0.000000));
end

--------------------------------------------------------------

function CalcHKL(h, k, l, psi)
	index = quad.light.SetThetaPhiHKL(h, k, l, psi);

--	print("Calculate l =  ", l, " at index ", index, " psi: ", psi,"\nphi ", light.phi, " and theta ", light.theta);
	--print("Calculate phi=", psi ," and theta ", light.theta-90);
	if (index == -1) then
		--print("invalid");
		return -1, -1, -1, -1, -1, -1;
	else
		quad.light.SetPolarization("S", index);
		quad.Scatter();
		S1 = quad.GetReflectedIntensity(index);
		S2 = quad.GetReflectedIntensity(0);
		S3 = quad.GetTransmittedIntensity(0);
		
		quad.light.SetPolarization("P", index);
		quad.Scatter();
		P1 = quad.GetReflectedIntensity(index);
		P2 = quad.GetReflectedIntensity(0);
		P3 = quad.GetTransmittedIntensity(0);

		Qrefl = quad.light.GetKVector() - quad.GetReflectedKVector(index);
		return S1, P1, S2, P2, S3, P3
	end
end

--------------------------------------------------------------

-- perpendicular incidence
quad.light.SetThetaPhi(0., 0.);
quad.light.SetPolarization("S");
quad.light.SetEnergy(853);

BuildLNOLAOBE125();
quad.PrintStatus();

h = -1;
k = 0;
l = 1;
quad.rl.SetReciprocalLatticeVector(h, k);

-----------------------------------------------

local file = io.open("azimuthalscan.dat", "w")

for psi = -50, 310, 1 do

	quad.SetVolumeCorrection(1);
	quad.SetMultipleScattering(MS.LAYER);
	quad.SetMatrixAccuracy(M.FULL);
	--SetMultipleScattering(MS.INTERATOMIC);
	DS1, DP1, DS2, DP2, DS3, DP3 = CalcHKL(h, k, l, psi+180);

	quad.SetVolumeCorrection(0);
    quad.SetMultipleScattering(MS.KINEMATIC);
    quad.SetMatrixAccuracy(M.DIAGONAL);
	KS1, KP1, KS2, KP2, KS3, KP3 = CalcHKL(h, k, l, psi+180);

		file:write(psi,
			" ", l, " ",
			DS1, " ", DP1, " ", -- sigma and pi dynamic 1/4, 1/4, 1/4
			KS1, " ", KP1, " ", -- sigma and pi kinematic 1/4, 1/4, 1/4
			DS2, " ", DP2, " ", -- sigma and pi dynamic refl
			KS2, " ", KP2, " ", -- sigma and pi kinematic refl
			DS3, " ", DP3, " ",
			KS3, " ", KP3, " ",
			"\n");
end
file:close();




