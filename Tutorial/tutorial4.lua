-------- TUTORIAL 4 ----------
-- Bragg Peak of face-centred substrate

-- file in which the data is saved
filename = "bragg2.txt";

-- define light
light.theta = 180.;
light.phi = 0.;
light.polarization = "S";
light.energy = 12000;

LaAtom = atom.NewScatteringFactorAtom(57., 10.);

-- define face-centred cubic lattice
-- lattice constants
lc = 4.;

-- reciprocal lattice
rl.Set(Vec2(lc, 0.), Vec2(0., lc), lc);
slabs.AddSlabs(2, lc/2.);
slabs.AddAtom(0, LaAtom, Vec3(0,    0,    0));
slabs.AddAtom(0, LaAtom, Vec3(lc/2, lc/2, 0));
slabs.AddAtom(1, LaAtom, Vec3(lc/2, 0.  , 0));
slabs.AddAtom(1, LaAtom, Vec3(0.  , lc/2, 0));

-- infinite substrate
slabs.SetStructure("100000*(0,1)");

PrintStats();

-- wait
print("Press any key to continue");
io.read();

-- calculate bragg peak

local file = io.open(filename, "w")
file:write("#qz Reflection Ttransmission\n");
for theta = 90.1, 180, 0.1 do 
	print("Calculate Theta = ", theta);
	io.flush();
	light.theta = theta;
	BuildScatterMatrix();
	-- instead of theta we output the table with the momentum transfer in units of l
	l = -(light.k.z - GetReflK(0).z)/rl.b3().z;
	file:write(l, " ", GetRefl(0), " ", GetTrans(0), "\n");
end

print("Press any key to continue");
file:close()
print("Angle sweep saved in file: ", filename);






