-------- TUTORIAL 6 ----------
-- Truncation rod

function BuildCubicLattice(lc)
	rl.Set(Vec2(lc, 0.), Vec2(0., lc), lc);
	LaAtom = atom.NewScatteringFactorAtom(57., 10.);
	AlAtom = atom.NewScatteringFactorAtom(13., 10.);
	lc2 = lc * 1.05;
	slabs.AddSlabs(1, lc);
	slabs.AddSlabs(1, lc2);
	slabs.AddAtom(0, LaAtom, Vec3(0, 0, 0));
	slabs.AddAtom(1, AlAtom, Vec3(0, 0, 0));
	slabs.SetStructure("20*(0), 100000*(1)");
end

-- perpendicular incidence
light.theta = 180.;
light.phi = 0.;
light.polarization = "S";
light.energy = 8300;

-- build primitive substrate with lattice constant of 4
BuildCubicLattice(4.);
PrintStats();

-- wait
print("Press any key to continue");
io.read();

-- calculate symmetry for hkl = (1,1,1)
h = 1;
k = 1;
l = 1;

rl.AddReciprocalLatticeVector(h, k);


-- Truncation rod. h and k are always fixed
--psi = 150.;
psi = 90.;

filename = "truncation_rod111.txt"
local file = io.open(filename, "w");
file:write("#l Reflection\n");

--for theta = mintheta, maxtheta, 0.05 do
for l = 0.8, 1.2, 0.001 do
	-- l is the only variable that can be a decimal number. h and k are fixed
	index = SetThetaPhiHKL(h, k, l, psi);
	-- check
	l = -(light.k.z - GetReflK(index).z)/rl.b3().z;
	print("Calculating l: ", l);
	io.flush();
	BuildScatterMatrix();
	-- output specular reflection and transmission
	file:write(l, " ", GetRefl(index), "\n");	
end

file:close();
print("Truncation rod saved in file: ", filename);




