-------- TUTORIAL 3 ----------
-- Bragg Peak of thin film + substrate

-- file in which the data is saved
filename = "bragg1.txt";

quad.light.SetPolarization("S");
quad.light.SetEnergy(3000);

-- add some absorption to the scattering factors
atoma = quad.atom.NewScatteringFactorAtom(57., 10.);
atomb = quad.atom.NewScatteringFactorAtom(13., 5.);

-- define cubic primitive lattice for film and substrate
-- lattice constants 
lc1 = 4.;
lc2 = 3.7; -- this one is for the subrate, but only in z-direction

-- reciprocal lattice
quad.rl.Set(Vec2(lc1, 0.), Vec2(0., lc1), lc1);

quad.slab.AddSlabs(1, lc1);
quad.slab.AddSlabs(1, lc2);
quad.slab.AddAtom(0, atoma, Vec3(0, 0, 0));
quad.slab.AddAtom(1, atomb, Vec3(0, 0, 0));

-- thin film on top of infinite substrate
quad.slab.SetStructure("10*(0), 100000*(1)");

quad.PrintStatus();

-- wait
print("Press any key to continue");
io.read();

-- reflectivity

local file = io.open(filename, "w")
file:write("#qz Reflection Transmission\n");
for theta = 110, 130., 0.1 do 
	print("Calculate Theta = ", theta);
	io.flush();
	quad.light.SetThetaPhi(theta, 0.);
	quad.Scatter();
	-- instead of theta we output the table with the momentum transfer qz
	local qz = quad.light.GetKVector().z - quad.GetReflectedKVector(0).z;
	file:write(-qz, " ", quad.GetReflectedIntensity(0), " ", quad.GetTransmittedIntensity(0), "\n");
end

print("Press any key to continue");
file:close()
print("Angle sweep saved in file: ", filename);



