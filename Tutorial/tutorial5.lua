-------- TUTORIAL 5 ----------
-- Diffraction Geometry
-- This tutorial shows how to determine the correct angles and correct reciprocal lattice vector for a certain h k l plane

function BuildCubicLattice(lc)
	LaAtom = atom.NewScatteringFactorAtom(57., 10.);
	slabs.AddSlabs(1, lc);
	slabs.AddAtom(0, LaAtom, Vec3(0, 0, 0));
	slabs.SetStructure("100000*(0)");
end

-- perpendicular incidence
light.theta = 180.;
light.phi = 0.;
light.polarization = "S";
light.energy = 8300;

lc = 4;
rl.Set(Vec2(lc, 0.), Vec2(0., lc), lc);

-- build primitive substrate with lattice constant of 4
BuildCubicLattice(lc);

PrintStats();

-- wait
print("Press any key to continue");
io.read();

-- calculate symmetry for hkl = (1,1,1)
h = 1;
k = 1;
l = 1;  -- in reflection, negative values are for transmission
rl.AddReciprocalLatticeVector(h, k);

-- calculate G
-- get reciprocal lattice vectors
b1 = rl.b1();
b2 = rl.b2();
b3 = rl.b3();
G = Vec3(0., 0., 0.);
G.x = b1.x*h + b2.x * k + b3.x * l;
G.y = b1.y*h + b2.y * k + b3.y * l;
G.z = b1.z*h + b2.z * k + b3.z * l;
print("Reciprocal Lattice Vector: ", G);

-- The Bragg condition is G = -Q !!!

-- this loop checks for which values of light.phi we get a reflected beam direction with the correct momentum transfer Q, Qx and Qy are always correct
print("\nphi\t\tTheta\t\tQz in reflection");
print("-------------------------------------------------------------------------------");
for psi = 0, 360, 5. do	 
	io.flush();
	
	-- the theta and phi of the incoming light depending on psi . The return value is the index of the reciprocal lattice vector or -1
	index = SetThetaPhiHKL(h, k, l, psi);
	if (index == -1) then -- is this a valid angle?
		print(psi, "invalid");
	else
	-- calculate Q for reflection and transmission
		Qrefl = light.k - GetReflK(index);
		Qtrans = light.k - GetTransK(index);
		print(psi, "q=", -Qrefl.z);
	end
end

-- only a few angles are possible to get the scattered light in reflection

index = SetThetaPhiHKL(h, k, l, 150);
BuildScatterMatrix();
print("\nreflected intensity in 111 direction: ", GetRefl(index));



