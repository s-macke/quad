-------- TUTORIAL 7 ----------
-- Queue Parallelization 

-- file in which the data is saved
filename = "refl.txt";

light.theta = 180.; 
light.phi = 0.;
light.polarization = "S";
light.energy = 1000;

LaAtom = atom.NewScatteringFactorAtom(57., 10.);

lc = 4.;
rl.Set(Vec2(lc, 0.), Vec2(0., lc), lc);
slabs.AddSlabs(1, lc);
slabs.AddAtom(0, LaAtom, Vec3(0, 0, 0));
slabs.SetStructure("10*(0)");


PrintStats();

print("Press any key to continue");
io.read();

------ Reflectivity calculation ------

-- loop theta from 90.1 to 180 degrees in 1 degree steps
for theta = 90.01, 180, 1 do 
	print("Add Theta = ", theta, "to queue");
	io.flush(); -- sometimes we have to flush the output buffer to see the printed text	
	light.theta = theta;
	queue.Add();
end

print("Execute queue");
-- unfortunately this can take longer than tutorial 2 because of overhead
queue.Run();

-- Access and save the results in the queue
local file = io.open(filename, "w");
file:write("#theta Reflection Transmission\n");

for task = 0, queue.GetN()-1 do 
	queue.Activate(task);
	file:write(light.theta, " ", GetRefl(0), " ", GetTrans(0), "\n");
end

file:close();
print("Angle sweep saved in file: ", filename);





