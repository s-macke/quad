-------- TUTORIAL 13 ----------
-- Levenberg-Marquardt fitting routine. Find best parabola based on four points

-- Parabola
function P(x, a, b, c)
    return a*x*x + b*x + c;
end


function Error(params)
	local i = 0;
	local a = params[0];
	local b = params[1];
	local c = params[2];

	local square = 0;

	-- assume almost ideal parabola y = x*x
	-- result must be near a=1, b=0 and c=0
	local x = {0, 1, -1, 2};
	local y = {0, 1,  1, 4.1};
	
	local rettable = {};
    for i=1, #x, 1 do
		rettable[i] = P(x[i], a, b, c) - y[i];
		square = square + rettable[i]*rettable[i];
	end
	
	local s = string.format("a: %f, b: %f, c: %f,  ||e||_2: %e", a, b, c, square);
	print(s);
	return rettable;
end

-- initial value, stepsize, lower boundary, upper boundary
-- for Levenberg-Marquardt only the first parameter is used
quad.fit.AddVariable(-0.4, 0.1, -10, 10);
quad.fit.AddVariable(1.2, 0.1, -10, 10);
quad.fit.AddVariable(-1.0, 0.1, -10, 10);

quad.fit.SetErrorFunction(Error);

niter = 100;  -- number of iterations
nmeas = 4;   -- number of measurement points
quad.fit.LM(niter, nmeas);




