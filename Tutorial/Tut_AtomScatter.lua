-------- TUTORIAL 2 ----------
-- Scattered Intensity of an atom


quad.light.SetThetaPhi(90., 0.);
quad.light.SetEnergy(1000);

print("Incoming wave vector: ", quad.light.GetKVector());

print("-------------------------------------");
quad.light.SetPolarization("S");
print("Electric field vector: ", Vec3(0, 1, 0));

-- generate simple atomtype with scattering factor f = 50 + 0*i.
atom = quad.atom.NewScatteringFactorAtom(50., 0.);

print(
	"Scattered intensity in direction  ", 
	Vec3(1, 0, 0), 
	" : ", 
	quad.ScatterAtom(atom, Vec3(1, 0, 0))
);

print(
	"Scattered intensity in direction  ", 
	Vec3(0, 1, 0), 
	" : ", 
	quad.ScatterAtom(atom, Vec3(0, 1, 0))
);

print(
	"Scattered intensity in direction  ", 
	Vec3(0, 0, 1), 
	" : ", 
	quad.ScatterAtom(atom, Vec3(0, 0, 1))
);

print("-------------------------------------");

quad.light.SetPolarization("P");
print("Electric field vector: ", Vec3(0, 0, 1));

print(
	"Scattered intensity in direction  ", 
	Vec3(1, 0, 0), 
	" : ", 
	quad.ScatterAtom(atom, Vec3(1, 0, 0))
);

print(
	"Scattered intensity in direction  ", 
	Vec3(0, 1, 0), 
	" : ", 
	quad.ScatterAtom(atom, Vec3(0, 1, 0))
);

print(
	"Scattered intensity in direction  ", 
	Vec3(0, 0, 1), 
	" : ", 
	quad.ScatterAtom(atom, Vec3(0, 0, 1))
);

print("-------------------------------------");

-- generate magnetic atom with f=50+0*i fm=10+0*i and magnetization in z-diretion
atom = quad.atom.NewMagneticAtom(50., 0.,   5, 5,   90, 0);
quad.light.SetPolarization("L");
print("Electric field vector: (0, 1, i)");

print(
	"Scattered intensity in direction  ", 
	Vec3(1, 0, 0), 
	" : ", 
	quad.ScatterAtom(atom, Vec3(1, 0, 0))
);

quad.light.SetPolarization("R");
print("Electric field vector: (0, 1, -i)");
print(
	"Scattered intensity in direction  ", 
	Vec3(1, 0, 0), 
	" : ",
	quad.ScatterAtom(atom, Vec3(1, 0, 0))
);

print("-------------------------------------");

