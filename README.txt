=======================================
================ QUAD =================
=======================================

QUAD (Quantitative Diffraction) is a scientific tool to calculate X-ray diffraction on arbitrary crystals and heterostructures.
It solves the Maxwell equations exactly!

Start the tutorials by executing

quad.exe Tutorial/tutorial1.lua
The script language of this program is LUA: www.lua.org

Tutorial1: First basic commands and the simulation of vacuum scattering :)
Tutorial2: Reflectivity on a thin film
Tutorial3: Bragg Peak for a thin film + substrate
Tutorial4: Bragg Peaks for a face-centred substrate
Tutorial5: First steps in Diffraction
Tutorial6: Truncation rod
Tutorial7: Queue Parallelization of Tutorial 2
Tutorial8: Powder diffraction (statistical approach)
Tutorial10: Simulation of a Laue picture
Tutorial11: Unordered slabs (roughness)
Tutorial12: Fitting routine

In order to understand the underlying geometry used by the program take a look in the doc folder. 


Not done yet:

Tutorial9: Energy conservation and accuracy
Tutorial12: Scattering Tensors (Magnetic scattering)
Tutorial13: Fit Data with Simplex algorithm


Units
=====

The Maxwell equations are in principle scale-invariant. However the program is intended to run in the atomic range.
To be consistent use for:
	coordinates, lengths: Angstrom (1e-10 m)
	energy: eV (Electronvolt)

Variables
=========

 light.theta  
    type: decimal number
	Desc: Theta angle of the incoming light in degrees

 light.phi 
   type: decimal number
   Desc: Phi angle of the incoming light in degrees
   
 light.k
   type: Vec3 (only readable)
   Desc: The current wavevector of the incoming light
   
 light.energy 
	type: decimal number
	Desc: Energy of incoming light in eV
 
 light.polarization (string)
	type: string
	valid values: 
		"S" : Sigma polarization (perpendicular to the scattering plane)
		"P" : Pi polarization (parallel to the scattering plane)
		"L" : Left circular polarization
		"R" : Right circular polarization
	Desc: Defines the polarization of the incoming light dependent in relation to the HK=0,0 scattering plane. 
	
 light.filter (string)
	type: string
	valid values: 
		"N" : none polarization filter (default)
		"S" : Sigma polarization filter (perpendicular to the scattering plane)
		"P" : Pi polarization filter (parallel to the scattering plane)
		"L" : Left circular polarization filter
		"R" : Right circular polarization filter
	Desc: Defines the polarization filter of the outgoing light.
	
  light.out 
	TODO
   
Main Functions
==============

BuildScatterMatrix()
   return value: nothing
   Desc: Calculates the scattering matrices for the whole layer structure. It depends on almost all parameters given earlier and executes the main routine.

Atom creation
=============

atom.NewScatteringFactorAtom(number, number)
	1. argument: real part of scattering factor (f1)
	2. argument: imaginary part of scattering factor (f2)
	return value: atom type
	Desc: Generates a energy-independent atom type

atom.NewAtom(string)
	1. argument: short element name like "Ti" for Titanium
	return value: atom type
	Desc: tries to find name.ff in order to load the scattering factor files or uses the internal database.
	
atom.NewAtom(string, q)
	1. argument: short element name like "Ti" for Titanium
	2. argument: defines the scattering for a certain momentum transfer q [Unit: A^-1]
	return value: atom type
	Desc: tries to find name.ff in order to load the scattering factor files or uses the internal database.

atom.NewMagneticAtom(number, number, number, number, number, number)
	1. argument: real part of scattering factor (f1)
	2. argument: imagninary part of scattering factor (f2)
	3. argument: real part of magnetic scattering factor (f1m)
	4. argument: imagninary part of magnetic scattering factor (f2m)
	5. argument: direction of magnetiztion (theta angle in degrees)
	6. argument: direction of magnetization (phi angle in degrees)
	return value: atom type
	Desc: Generates a energy-independent atom type with magnetic contributions

atom.NewScatteringFactorFileAtom(string)
	1. argument: filename of tabulated scattering factors	
	return value: atom type
	Desc: Generates a energy-dependent atom type

atom.NewScatteringTensorFileAtom(string)
	1. argument: filename of tabulated scattering tensors
	return value: atom type
	Desc: Generates a energy-dependent atom type

Reciprocal lattice
==================

rl.Set(vec2, vec2, Decimal number, Decimal number)
   1. Argument: First unit cell vector in-plane
   2. Argument: Second unit cell vector in-plane
   3. Argument: unit cell length in z-direction. This is needed to define the l-value in the function SetThetaHKL
   Desc: Sets the lattice parameters in 2D. Adds the mandatory reciprocal lattice vector (H,K)=(0,0)
   return value: nothing

rl.b1()
rl.b2()
rl.b3()
   return value: Vec3
   Desc: Returns the three reciprocal lattice vectors.

rl.GetN()
   return value: integer
   Desc: return the number of reciprocal lattice vectors calculated.

rl.AddReciprocalLatticeVector(number, number)
   1. Argument: Miller indice H of the reciprocal lattice vector
   2. Argument: Miller indice K of the reciprocal lattice vector
   return value: nothing
   Desc: Adds a reciprocal lattice vector HK to the list of vector, which have to be calculated

rl.SetRadius(number)
   1. Argument: Radius in the reciprocal lattice space
   return value: nothing
   Desc: Renews the list of reciprocal lattice vectors with a list of all reciprocal lattice vector withing the radius
   
Slab Settings
=============

slabs.AddSlabs(number, number)
   1. Argument: number of slabs to create
   2. Argument: thickness of each slab
   return value: nothing

slabs.AddAtom(number, atom type, Vec3)
   1. Argument: add to the slab with index i
   2. Argument: the variable with the atom type
   3. Argument: Position of the atom inside the slab
   return value: nothing

slabs.SetStructure(string)
   Desc: Describes the final film structure. These are basically comma seperated slab indices or variables defined in slabs.DefineCrystal
         Additionally it allows to place brackets and repetitions
   Examples:  "0,1"
              "100*(0,1)"
              "100*(1,2), 0"
              "100*(0,1), 100000*(2)"
              "100*( 2*(0,1), 2*(3,4) )"
              "Crys1, Crys2" (if "Crys1" and "Crys2" were defined with slabs.CreateCrystal or the other functions)
   return value: nothing
   
slabs.CreateCrystal(string, string)
   1. argument: Variable name of this structure. Allowed signs: 'a'-'z', 'A'-'Z', '_', and '0'-'9' if it is not the first sign
   2. argument: Crystal definition like in the function slabs.SetStructure
   return value: nothing
   Desc: Defines a seperate crystal which can be used in the function slabs.SetStructure

slabs.CreateHom(string, string)
   1. argument: Variable name of this structure. Allowed signs: 'a'-'z', 'A'-'Z', '_', and '0'-'9' if it is not the first sign
   2. argument: Crystal definition like in the function slabs.SetStructure
   return value: nothing
   Desc: Defines a slab of an unordered crystal. The crystal is reduced to a continuum with the correct optical constants

slabs.CreateInterface(string, string, string, number, number)
   1. argument: Variable name of this structure. Allowed signs: 'a'-'z', 'A'-'Z', '_', and '0'-'9' if it is not the first sign
   2. argument: Crystal definition of the top crystal like in the function slabs.SetStructure
   3. argument: Crystal definition of the bottom crystal like in the function slabs.SetStructure
   4. argument: Total thickness of this interface with unordered crystal. The interface will be in the center of this slab.
   5. argument: Discretization of this interface. (around 3*thickness/sigma)
   6. argument: Roughness sigma in Angstrom
   return value: nothing
   Desc: Defines a slab with two unordered crystal and a rough interface. 

   
General Functions
=================

PrintStats()
   return value: nothing

GetRefl(number)
   return value: Decimal Number

GetTrans(number)
   return value: Decimal Number
 
GetReflK(number)
    return value: Vec3
 
GetTransK(number)
    return value: Vec3

SetThetaHKL(integer h, integer k, Decimal number l)
  return value: integer (-1 if error)
  Desc: Calculates the angle theta for a fixed phi angle and given hkl triple. Variable l can be non-integer. Returns the index of the reciprocal lattice vector.
		a negative number of l means transmission whereas a positive number means reflection. The calculated angle theta is automatically set in the variable light.theta

SetMultipleScattering(type)
  return value: nothing
  valid values: 
     MS.KINEMATIC:   Kinematical scattering (independent scatterers and independent light)
     MS.NONE:   No multiple scattering is calculated (enable light absorbtion and refraction)
     MS.LAYER:  Interlayer multiple scattering is enabled
     MS.INTERATOMIC: Interlayer as well as interatomic multiple scattering is enabled (not supported yet)

SetNumericAccuracy(type)  (not supported yet)
  valid values: 
     NUM.FLOAT: The decimals numbers are defined as 4 byte floating point numbers
     NUM.DOUBLE: The decimals numbers are defined as 8 byte floating point numbers
     NUM.LONGDOUBLE: The decimals numbers are defined as 10 byte floating point numbers. (Architecture dependent)
     NUM.QUAD: The decimals numbers are defined as 16 byte floating point numbers

SetMatrixAccuracy(type)
  valid values:
     M.DIAGONAL: Neglect off-diagonal terms of the matrices. Very fast but leads to separate scattering channels.
     M.FULL: Use Full Matrix-Matrix-Multiplications
     M.BLAS: Like M.FULL but use of BLAS-libraries to increase performance of matrix-matrix-multiplication (not working yet)

	 
Queue functions for Parallelization
===================================
	 
queue.Add()
    Desc: Add current light configuration to queue

queue.Run()
    Desc: Execute the current queue parallelized

queue.Activate(integer)
    Variable1: Index in the queue beginning from zero
    Desc: Activates a result of the queue.

queue.GetN()
    return value: Number of entries in the queue

queue.Clear()
   Desc: Clears the queue (Bug: Memory is not freed)

Fitting functions
=================

fit.AddVariable(number, number, number, number)
	1. argument: initial value for number
	2. argument: stepsize of 1.arguement in first iteration . If unsure choose 0.1*initial_value. The value must not be zero!
	3. argument: lower boundary for variable
	4. argument: upper boundary for variable

fit.SetErrorFunction(function)
	1. argument: Lua function which should return an error
	Desc: The function receives a list of parameters (array of numbers), and should return an error (number)

fit.Iterate(number) 
	1. argument: Number of simplex iterations to perform

Functions for convertcrystal
============================

The tool convertcrystal converts crystal files in the format .xyz to an output that is usable for multemX.
The standard .cif can be usually exported to a .xyz file by using software tools like "Diamond" or "Vesta".
convertcrystal executes a script file which can contain following commands line by line.
A example should be provided in the Tutorials folder

openxyz filename
    opens a .xyz file

savexyz filename
    saves a .xyz file

unitcell a b c anglea anbleb anglec
    defines the unit cell structure of the loaded file. The definition can be found here: http://en.wikipedia.org/wiki/Fractional_coordinates

findneighbors 
    just prints the atom and tells how many other atoms at the same crystal position could be found

atomdistance d
    Sets the distance limit in which the atoms are treated identical. 
    (default 0.1A)

reduce
    removes atoms that are multiple times in the crystal lattice  (e. g. atoms at the corners)
    the function "removeoutside" should be executed first.

expand min max
    expands the unit cell around the borders

sort
    sort the atoms for increasing z-coordinate

rotatex angle
    rotates crystal around x-axis

rotatey angle
    rotates crystal around y-axis

rotatez angle
    rotates crystal around z-axis

moveatom atomnr x y z
    moves an atom around
    if atomnr is -1 then the whole crystal is moved

findcubiccell
    trys to find the smallest cubic unitcell of the system. and sets the unit cell accordingly. If this function does not work you have to do it manually by the "unitcell" command.

buildlayers nlayers
    prints the atom layers which you can use directly use in the lua script.
    Note that the layer will be build up along z-axis and not around c-axis.

pause
    waits for a keypress (enter)

check
    checks if there is something outside of the primitive outside of the unit cell and warns
    In future there will be more checks included

removeoutside
    remove all atoms outside of the unit cell 

printunitcellvectors
    prints the current unit cell vectors a,b and c


Changes
=======

V 1.7:
	

V 1.6:
	Bug fixes for anisotropic atoms
	Fitting routines
	More option to choose the reciprocal lattice coordinates


V 1.5:
	Support for unordered slabs:
		New function slabs.CreateHom
		New function slabs.CreateInterface
	New function slabs.CreateCrystal
	convertcrystal stabilized and more functions added.
	New stabilized interpretation of layer structure (Token based and more error messages)

V 1.4:
	Added kinematical scattering
	Support for magnetic atoms

V 1.3:
	Added Powder diffraction tutorial
	Reduced memory usage for parallelization (save intensities instead of matrices)
	Matrix accuracy: Diagonal approximation is enabled.


V 1.2:
	Added Powder diffraction tutorial
	Reduced memory usage for parallelization (save intensities instead of matrices)
	Matrix accuracy: Diagonal approximation is enabled.

V 1.1:
	Add atoms with anisotropic scattering tensors
	Changed the way atoms are handled handling in the script (new atom type)
	Changed the geometry, especially the z-direction. The incoming wave vector in z-direction must be negative at all time. 
	SetThetaHKL returns an negative index if there is no plane found
	Improved Documentation of the functions in Readme
	SetThetaHKL supports non-integer l-values
	Tutorial6: Use SetThetaHKL for truncation rod
	Laue-Picture Tutorial added
	Parallelization Tutorial added
	Support of different accuracies of multiple scattering (new function SetMultipleScattering)
	
V 1.0:
   Initial release


Contact
=======

Sebastian Macke (s.macke@fkf.mpg.de or sebastian@macke.de)



